package com.gibs.peixunapi.controller;

import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.VO.post.NoticeVO;
import com.gibs.peixunapi.VO.show.ShowNoticeVO;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.NoticeService;
import com.gibs.peixunapi.utils.ResultUtil;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 功能：【消息通知】的后台控制器
 * 项目：装配式培训平台
 * 日期：2020-08-20
 * 作者：梁嘉伟
 */

@Api(tags = "消息通知")
@Slf4j
@RestController
@RequestMapping("/notice")
public class NoticeController {

    @Autowired
    private NoticeService noticeService;

    @ApiOperation(value = "发送消息")

    @PostMapping("/sendNotice")
    public Result sendNotice(@RequestBody NoticeVO notice) {
        noticeService.sendNotice(notice);
        return null;
    }

    @ApiOperation(value = "获取消息")
    @ApiImplicitParam(name = "userId", value = "接收人Id", dataType = "int", required = true, paramType = "query")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowNoticeVO.class)})
    @GetMapping("/getNotice")
    public Result getNotice(Integer userId) {
        List<ShowNoticeVO> data = noticeService.getNotice(userId);
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, data);
    }

    @ApiOperation(value = "读取消息")
    @ApiImplicitParam(name = "noticeId", value = "消息id", dataType = "int", required = true, paramType = "query")
    @GetMapping("/readNotice")
    public Result readNotice(Integer noticeId) {
        ShowNoticeVO data = noticeService.readNotice(noticeId);
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, data);
    }

    @ApiOperation(value = "删除已读消息")
    @GetMapping("/deleteReadNotice")
    public Result deleteReadNotice() {
        noticeService.deleteReadNotice();
        return ResultUtil.success(ResultEnum.DELETE_SUCCESS);
    }
}

