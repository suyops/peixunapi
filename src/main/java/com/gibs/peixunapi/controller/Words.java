package com.gibs.peixunapi.controller;

import com.gibs.peixunapi.mapper.ScheduledMapper;
import com.gibs.peixunapi.model.Word;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.JobService;
import com.gibs.peixunapi.service.ScheduledService;
import com.gibs.peixunapi.service.WordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import javax.validation.Valid;

//import com.gibs.peixunapi.config.ConfigUtils;
//import com.gibs.peixunapi.controller.Application;

/**
 * 功能：【数据字典】的后台控制器
 * 项目：装配式培训平台
 * 日期：2020-08-20
 * 作者：梁嘉伟
 */

@Slf4j
@RestController
@RequestMapping("/Words")
public class Words {

    @Autowired
    private WordService WordService;
    @Autowired
    private ScheduledService scheduledService;
    @Autowired
    private JobService jobService;
    @Autowired
    private ScheduledMapper scheduledMapper;

    @Transactional
    @PostMapping("/create")
    public Result create(@Valid Word object, BindingResult bindingResult) {

        return null;
    }

    @PostMapping("/save")
    public Result save(@Valid Word object, BindingResult bindingResult) {
        return null;
    }

    @PostMapping("/delete")
    public Result delete(Integer id) {
        return WordService.delete(id);
    }

    @GetMapping("/get")
    public Result get(Integer id) {
        return WordService.get(id);
    }

    @GetMapping("/getList")
    public Result getList(String key) {
        return WordService.getList(key);
    }

    @GetMapping("/getListPage")
    public Result getListPage(String key, Integer current, Integer limit) {

        return WordService.getListPage(key, current, limit);

    }


//
//	@GetMapping("/getOptions")
//	public Result getOptions() throws Exception {
//		Scheduled scheduled = scheduledMapper.getCron(2);
//		scheduledService.addOrUpdateScheduleJob(scheduled);
//		List<Scheduled> list = jobService.getAllJob();
//		list.forEach(i->log.info(i.toString()));
//		return ResultUtil.success(ResultEnum.SUCCESS);
//	}


}

