package com.gibs.peixunapi.controller;

import com.gibs.peixunapi.VO.post.DailyScoreVO;
import com.gibs.peixunapi.VO.post.ExamClassVO;
import com.gibs.peixunapi.VO.show.*;
import com.gibs.peixunapi.aop.ActionLogAop;
import com.gibs.peixunapi.model.ExamInfo;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ResultUtil;
import com.github.pagehelper.util.StringUtil;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

import static com.gibs.peixunapi.enums.ResultEnum.*;


/**
 * 功能：【考试】的后台控制器
 * 项目：装配式培训平台
 * 日期：2020-08-20
 * 作者：梁嘉伟
 * getExamClassStudent
 *
 * @author liangjiawei
 */

@Api(tags = "考务管理API")
@Slf4j
@RestController
@RequestMapping("/exam")
public class ExamController {

    @Autowired
    private ExamInfoService examInfoService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private ExamClassInfoService examClassInfoService;
    @Autowired
    private ExamStudentService examStudentService;
    @Autowired
    private StudentCourseService studentCourseService;
    @Autowired
    private AnswerSheetService answerSheetService;
    @Autowired
    private ExamTeacherService examTeacherService;
    @Autowired
    private CertificateService certificateService;
    @Autowired
    private UserService userService;

    @ApiOperation(value = "新增考试")
    @ActionLogAop(module = "考试中心-考试管理", type = "新增考试")
    @PostMapping("/createExamClass")
    public Result createExamClass(@RequestBody @ApiParam("考试班级VO类") ExamClassVO examClassVO) {

        User user = userService.getAttributeUser();
        return examClassInfoService.create(examClassVO, user);
    }


//
//    @ApiOperation(value = "学生自主报名考试", consumes = "application/x-www-form-urlencoded")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "classId", value = "考试班级Id", paramType = "form", required = true, dataType = "int"),
//            @ApiImplicitParam(name = "studentId", value = "学生id", paramType = "form", required = true, dataType = "int"),
//    })
//    @ActionLogAop(module = "考试中心-考试管理", type = "新增考试", desc = "添加考生去考试班级")
//    @PostMapping("/signUpExamClass")
//    public Result signUpExamClass(Integer classId, Integer studentId) {
//
//        if (examStudentService.signUpClass(classId, studentId)) {
//            return examClassInfoService.changeNumber(1, classId, "add");
//        } else {
//            return ResultUtil.fail(OPERATING_FAIL);
//        }
//    }


    @ApiOperation(value = "考试-用户名单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "department", value = "部门id", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "year", value = "入职年份", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "key", value = "搜索关键字", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "examId", value = "考试id", paramType = "query", dataType = "int"),
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = AllowExamVO.class)})
    @GetMapping("/getAllowExamList")
    public Result<List<AllowExamVO>> getAllowExamList(Integer department, Integer year, String key, Integer examId) {
        List<AllowExamVO> data = studentCourseService.getAllowExamList(department, year, key, examId);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "考试班级信息")
    @ApiImplicitParam(name = "examInfoId", value = "考试基本信息id", paramType = "query", required = true, dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowExamClassVO.class)})
    @GetMapping("/getExamClass")
    public Result<ShowExamClassVO> getExamClass(Integer classId) {

        ShowExamClassVO data = examClassInfoService.getExamClassVO(classId);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "考试班级查看考生列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "搜索关键字", paramType = "query"),
            @ApiImplicitParam(name = "classId", value = "考试班级Id", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "page", value = "页数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条数", paramType = "query", required = true, dataType = "int"),
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StudentInExamClassVO.class)})
    @GetMapping("/getExamClassStudent")
    public Result<PageData<StudentInExamClassVO>> getExamClassStudent(String key, Integer classId, Integer page, Integer limit) {
        PageData<StudentInExamClassVO> data = examStudentService.getStudentListInExamClass(key, classId, page, limit);
        return ResultUtil.successPage(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "删除考生")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentIds", value = "学生ids,使用\",\"隔开", required = true, paramType = "form"),
            @ApiImplicitParam(name = "examClassId", value = "考试班级id", required = true, paramType = "form", dataType = "int"),
    })
    @PostMapping("/deleteStudentToExam")
    public Result deleteStudentToExam(String studentIds, Integer examClassId) {
        if (examStudentService.deleteStudentToExam(studentIds, examClassId)) {
            return examClassInfoService.changeNumber(studentIds.split(",").length,
                    examClassId, "delete");
        } else {
            return ResultUtil.fail(OPERATING_FAIL);
        }
    }

    @ApiOperation(value = "添加学生去考试班级 ")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentIds", value = "学生ids 学生ids,使用\",\"隔开 ", paramType = "form", required = true),
            @ApiImplicitParam(name = "examClassId", value = "考试班级id", paramType = "form", required = true, dataType = "int"),
    })
    @ActionLogAop(module = "考试中心-考试管理", type = "新增考试", desc = "添加考生去考试班级")
    @PostMapping("/addStudentToExam")
    public Result addStudentToExam(String studentIds, Integer examClassId) {
        if (!StringUtil.isEmpty(studentIds)) {
            if (examStudentService.addStudentToExam(studentIds, examClassId)) {
                return examClassInfoService.changeNumber(studentIds.split(",").length, examClassId, "add");
            } else {
                return ResultUtil.fail(OPERATING_FAIL);
            }
        } else {
            return ResultUtil.success(PARAM_ERROR);
        }
    }


    @ApiOperation(value = "校验考场号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "校验码", paramType = "form", required = true),
            @ApiImplicitParam(name = "studentId", value = "学生id", paramType = "form", required = true, dataType = "int"),
    })
    @PostMapping("/checkExamCode")
    public Result checkExamCode(String code, Integer studentId) {
        examStudentService.checkExamCode(code, studentId);
        return ResultUtil.success(CHECK_SUCCESS);
    }

    @ApiOperation(value = "获取考生信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "classId", value = "考试班级id", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "studentId", value = "学生id", paramType = "query", required = true, dataType = "int"),
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StudentInExamClassVO.class)})
    @GetMapping("/getExamStudent")
    public Result getExamStudent(Integer classId, Integer studentId) {

        StudentInExamClassVO data = examStudentService.getExamStudent(classId, studentId);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "填写答卷", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uniqueCode", value = "准考证", required = true, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "testPaperSubjectId", value = "试卷题目id", required = true, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "studentAnswer", value = "答卷回答", required = true, paramType = "form")
    })
    @PostMapping("/fillTheAnswerPaper")
    public Result fillTheAnswerPaper(String uniqueCode, Integer testPaperSubjectId, String studentAnswer) {

        answerSheetService.fillTheAnswerPaper(uniqueCode, testPaperSubjectId, studentAnswer);
        return ResultUtil.success(OPERATING_SUCCESS);
    }

    @ApiOperation(value = "考试交卷")
    @ApiImplicitParam(name = "uniqueCode", value = "准考证", required = true, dataType = "int")
    @PostMapping("/submitPaper")
    public Result submitPaper(String uniqueCode) {
        examStudentService.submitPaper(uniqueCode);
        return ResultUtil.success(OPERATING_SUCCESS);
    }


    @ApiOperation(value = "编辑考试班级信息")
    @ActionLogAop(module = "考试中心-组织考试", type = "编辑考试班级信息")
    @PostMapping("/editExamClass")
    public Result editExamClass(@RequestBody @ApiParam("考试班级信息") ExamClassVO examClass) {
        if (examClassInfoService.isStartExam(examClass.getId())) {
            return ResultUtil.fail(EXAM_STARTED);
        } else {
            ExamInfo examInfo = examInfoService.edit(examClass);

            examClassInfoService.edit(examClass, examInfo);
            return ResultUtil.success(OPERATING_SUCCESS);
        }
    }

    @ApiOperation(value = "编辑考试监考/阅卷老师")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "老师id列表", paramType = "form", required = true),
            @ApiImplicitParam(name = "type", value = "类型 监考:manage;阅卷:correct", paramType = "form", required = true),
            @ApiImplicitParam(name = "classId", value = "考试班级id", paramType = "form", required = true, dataType = "int"),
    })
    @ActionLogAop(module = "考试中心-组织考试", type = "编辑考试班级信息", desc = "编辑考试监考/阅卷老师")
    @PostMapping("/editExamTeacher")
    public Result editExamTeacher(String ids, String type, Integer classId) {
        if (examClassInfoService.isStartExam(classId)) {
            return ResultUtil.fail(EXAM_STARTED);
        } else {
            examTeacherService.editExamTeacher(ids, classId, type);
            return ResultUtil.success(OPERATING_SUCCESS);
        }
    }

    @ApiOperation(value = "管理员-批量缓考申请")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentId", value = "学生id", paramType = "form", required = true),
            @ApiImplicitParam(name = "examClassId", value = "考试班级id", paramType = "form", required = true, dataType = "int"),
    })
    @PostMapping("/applyForDelayedBatch")
    public Result applyForDelayedBatch(String studentIds, Integer examClassId) {
        examStudentService.applyForDelayedBatch(studentIds, examClassId);
        return ResultUtil.success(OPERATING_SUCCESS);
    }

    @ApiOperation(value = "缓考申请")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentId", value = "学生id", paramType = "form", required = true, dataType = "int"),
            @ApiImplicitParam(name = "examClassId", value = "考试班级id", paramType = "form", required = true, dataType = "int"),
    })
    @PostMapping("/applyForDelayed")
    public Result applyForDelayed(Integer studentId, Integer examClassId) {
        examStudentService.applyForDelayed(studentId, examClassId);
        return ResultUtil.success(OPERATING_SUCCESS);
    }

    @ApiOperation(value = "缓考审核")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentId", value = "学生id", paramType = "query", required = true),
            @ApiImplicitParam(name = "examClassId", value = "考试班级id", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "delayed", value = "审批结果", paramType = "query", required = true, dataType = "int"),

    })
    @ActionLogAop(module = "考试中心-编辑考生", type = "缓考审核")
    @PostMapping("/checkDelayed")
    public Result checkDelayed(String ids, Integer examClassId, Integer delayed) {

        examStudentService.checkDelayed(ids, examClassId, delayed);
        return ResultUtil.success(OPERATING_SUCCESS);
    }

    @ApiOperation(value = "阅卷-判断是否哟下一张答卷")
    @ApiImplicitParam(name = "classId", value = "考试班级id", paramType = "query", required = true, dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = AnswerPaperVO.class)})
    @GetMapping("/haveNextPaper")
    public Result haveNextPaper(Integer classId) {
        return answerSheetService.haveNextPaper(classId);
    }

    @ApiOperation(value = "阅卷-获取答卷")
    @ApiImplicitParam(name = "classId", value = "考试班级id", paramType = "query", required = true, dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = AnswerPaperVO.class)})
    @GetMapping("/getAnswerPaper")
    public Result getAnswerPaper(Integer classId) {
        return answerSheetService.getAnswerPaper(classId);
    }

    @ApiOperation(value = "批改答卷")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "answerSheetId", value = "答卷题目id", paramType = "form", required = true, dataType = "int"),
            @ApiImplicitParam(name = "score", value = "得分", paramType = "form", required = true, dataType = "double"),
            @ApiImplicitParam(name = "markManId", value = "批改人id", paramType = "form", required = true, dataType = "int")
    })
    @PostMapping("/correct")
    public Result correct(Integer answerSheetId, BigDecimal score, Integer markManId) {
        if (answerSheetId != null) {
            answerSheetService.correct(answerSheetId, score, markManId);
        }
        return ResultUtil.success(OPERATING_SUCCESS);
    }

    @ApiOperation(value = "完成此答卷批改")
    @ApiImplicitParam(name = "uniqueCode", value = "准考证号", paramType = "query", required = true)
    @GetMapping("/finishCorrect")
    public Result finishCorrect(String uniqueCode) {
        if (examStudentService.finishCorrect(uniqueCode)) {
            return ResultUtil.success(REMAIN_ANSWER_PAPER);
        }
        return ResultUtil.success(NO_ANSWER_PAPER);
    }

    @ApiOperation(value = "考试管理列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "token", value = "token", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "搜索课程名", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "最大条数", required = true, dataType = "int", paramType = "query"),
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ExamCourseVO.class)})
    @GetMapping("/getExamInfoInCourseList")
    public Result getExamInfoInCourse(String token, String key, Integer page, Integer limit) {

        return courseService.getExamCourseVOInUndone(key, page, limit);
    }

    @ApiOperation(value = "考试管理班级列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "搜索关键字", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "page", value = "页数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条数", paramType = "query", required = true, dataType = "int"),
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ExamClassListVO.class)})
    @GetMapping("/getExamClassList")
    public Result getExamClassList(String key, Integer page, Integer limit) {
        PageData data = examClassInfoService.getExamClassList(key, page, limit);

        return ResultUtil.successPage(FIND_SUCCESS, data);

    }

    @ApiOperation(value = "阅卷管理列表")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = CorrectManageVO.class)})
    @GetMapping("/getCorrectManageList")
    public Result getCorrectManageList(String key) {

        List<CorrectManageVO> data = examClassInfoService.getCorrectManageList(key == null ? "" : key);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "阅卷管理-成绩管理")
    @ApiImplicitParam(name = "classId", value = "班级id", paramType = "query", required = true, dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ScoreManageVO.class)})
    @GetMapping("/getScoreManageList")
    public Result getScoreManageList(String key, Integer classId) {
        List<ScoreManageVO> data = examStudentService.getScoreManageList(key == null ? "" : key, classId);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "编辑平时分")
    @PostMapping("/editDailyScore")
    public Result editDailyScore(@RequestBody DailyScoreVO vo) {
        Integer classId = examStudentService.editDailyScore(vo);
        if (!certificateService.CertificateClassExist(classId)) {
            certificateService.createCertificateClass(classId);
        }

        return ResultUtil.success(UPDATE_SUCCESS);
    }

    @ApiOperation(value = "考务管理列表")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ExamManageVO.class)})
    @GetMapping("/getExamManageList")
    public Result getExamManageList(String key) {
        List<ExamManageVO> data = examClassInfoService.getExamManageList(key == null ? "" : key);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "考务管理-班级详情列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "classId", value = "班级id", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "page", value = "页数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条数", paramType = "query", required = true, dataType = "int")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ExamManageStudentVO.class)})
    @GetMapping("/getExamManageStudentList")
    public Result getExamManageStudentList(String key, Integer classId, Integer page, Integer limit) {
        PageData data = examStudentService.getExamManageStudentList(key == null ? "" : key, classId, page, limit);
        return ResultUtil.successPage(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "学生考试中心")
    @ApiImplicitParam(name = "studentId", value = "用户id", paramType = "query", required = true, dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StudentExamCenterVO.class)})
    @GetMapping("/getStudentExamInfoList")
    public Result< List<StudentExamCenterVO>> getStudentExamInfoList(String key, Integer studentId) {
        List<StudentExamCenterVO> data = examClassInfoService.getStudentExamClassList(key, studentId);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "学生成绩管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "page", value = "页数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条数", paramType = "query", required = true, dataType = "int")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StudentExamManageVO.class)})
    @GetMapping("/getStudentScoreManageList")
    public Result getStudentScoreManageList(Integer userId, Integer page, Integer limit) {
        PageData data = examStudentService.getStudentScoreManage(userId, page, limit);
        return ResultUtil.successPage(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "查看准考证")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "classId", value = "考试班级Id", paramType = "query", required = true, dataType = "int")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = AdmissionTicketVO.class)})
    @GetMapping("/getAdmissionTicket")
    public Result getAdmissionTicket(Integer userId, Integer classId) {
        AdmissionTicketVO data = examStudentService.getAdmissionTicket(userId, classId);
        return ResultUtil.success(FIND_SUCCESS, data);
    }


    @ApiOperation(value = "监考查看考卷")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uniqueCode", value = "用户id", paramType = "query", required = true),
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowTestPaperVO.class)})
    @GetMapping("/watchAnswerPaper")
    public Result watchAnswerPaper(String uniqueCode) {
        ShowTestPaperVO data = answerSheetService.watchAnswerPaper(uniqueCode);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "绑定试卷到考试班级")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "classId", value = "班级Id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "testPaper", value = "试卷Id", dataType = "int", paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = OptionVO.class)})
    @PostMapping("/bindTestPaperToClass")
    public Result bindTestPaperToClass(Integer classId, Integer testPaper) {
        examClassInfoService.bindTestPaperToClass(classId, testPaper);
        return ResultUtil.success(BIND_SUCCESS);
    }
}

