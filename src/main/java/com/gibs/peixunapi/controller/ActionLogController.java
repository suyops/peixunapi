package com.gibs.peixunapi.controller;

import com.gibs.peixunapi.dao.ScheduledDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.model.ActionLog;
import com.gibs.peixunapi.model.Scheduled;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.ActionLogService;
import com.gibs.peixunapi.service.JobService;
import com.gibs.peixunapi.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;

/**
 * 功能：【操作日志】的后台控制器
 * 项目：装配式培训平台
 * 日期：2020-08-20
 * 作者：梁嘉伟
 */

@Slf4j
@Api(tags = "操作日志")
@RestController
@RequestMapping("/actionLog")
public class ActionLogController {

    @Autowired
    private ActionLogService actionLogService;
    @Autowired
    private JobService jobService;
    @Autowired
    private ScheduledDao scheduledDao;


    @ApiOperation(value = "获取操作日志列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "content", value = "搜索关键字", paramType = "query"),
            @ApiImplicitParam(name = "dateFrom", value = "开始日期", paramType = "query"),
            @ApiImplicitParam(name = "dateTo", value = "结束日期", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "最大条数", required = true, dataType = "int", paramType = "query")
    })
    @GetMapping("/getListPage")
    public Result<PageData<ActionLog>> getListPage(String content, Date dateFrom, Date dateTo, Integer page, Integer limit) {
        PageData<ActionLog> list = actionLogService.getListPage(content,dateFrom,dateTo, page, limit);
        return ResultUtil.successPage(ResultEnum.FIND_SUCCESS, list);
    }

    @ApiOperation(value = "增加任务到定时列表")
    @GetMapping("/addOrUpdateJob")
    public Result addOrUpdateJob(Integer id) throws Exception {
        Scheduled scheduled = scheduledDao.findById(id).orElseThrow(EntityNotFoundException::new);
        jobService.addOrUpdateJob(scheduled);
        return ResultUtil.success(ResultEnum.CREATE_SUCCESS);
    }

    @ApiOperation(value = "立即运行定时任务")
    @GetMapping("/runScheduleJob")
    public Result runScheduleJob(Integer id) throws Exception {
        jobService.runScheduleJob(id);
        return ResultUtil.success(ResultEnum.CREATE_SUCCESS);
    }
    @ApiOperation(value = "列表中的待运行任务")
    @GetMapping("/allDoingSchedule")
    public Result allDoingSchedule() throws Exception {
        List<Scheduled> list = jobService.allDoingSchedule();
        return ResultUtil.success(ResultEnum.CREATE_SUCCESS, list);
    }

    @ApiOperation(value = "数据库中所有任务")
    @GetMapping("/allSchedule")
    public Result allSchedule() throws Exception {
        List<Scheduled> list = jobService.allSchedule();
        return ResultUtil.success(ResultEnum.CREATE_SUCCESS, list);
    }
    @ApiOperation(value = "添加今天的任务")
    @GetMapping("/addTodayJob")
    public Result addTodayJob()  {
        jobService.addTodayJobs();
        return ResultUtil.success(ResultEnum.OPERATING_SUCCESS);
    }
}