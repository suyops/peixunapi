package com.gibs.peixunapi.controller;

import com.gibs.peixunapi.VO.SubOptionListVO;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.model.SubjectHub;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.SubOptionService;
import com.gibs.peixunapi.service.SubjectHubService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 功能：【题库选项】的后台控制器
 * 项目：装配式培训平台
 * 日期：2020-08-24
 * 作者：梁嘉伟
 *
 * @author liangjiawei
 */

@Slf4j
@RestController
@RequestMapping("/subOption")
@Api(tags = "选项api")
public class SubOptionController {

    @Autowired
    private SubOptionService subOptionService;
    @Autowired
    private SubjectHubService subjectHubService;

    @PostMapping("/create")
    @ApiOperation(value = "新增选项")
    public Result create(@RequestBody SubOptionListVO subOptionListVO) {
        if (subOptionListVO.getSubOptionVOList().isEmpty() || subOptionListVO.getSubjectHubId() == null) {
            throw new BaseException(ResultEnum.PARAM_ERROR);
        }
        SubjectHub subjectHub = subjectHubService.get(subOptionListVO.getSubjectHubId());
        return subOptionService.create(subOptionListVO.getSubOptionVOList(), subjectHub);
    }

    @PostMapping("/save")
    public Result save(@RequestBody SubOptionListVO subOptionListVO) {
        if (subOptionListVO.getSubOptionVOList().isEmpty() || subOptionListVO.getSubjectHubId() == null) {
            throw new BaseException(ResultEnum.PARAM_ERROR);
        }
        return subOptionService.edit(subOptionListVO.getSubOptionVOList(), subOptionListVO.getSubjectHubId());

    }

    @PostMapping("/delete")
    public Result delete(Integer id) {
        if (id == null) {
            throw new BaseException(ResultEnum.PARAM_ERROR);
        }
        return subOptionService.delete(id);

    }

    @GetMapping("/get")
    public Result get(Integer id) {
        return subOptionService.get(id);
    }

    @GetMapping("/getList")
    public Result getList(String key) {
        return subOptionService.getList(Integer.parseInt(key));
    }


}

