package com.gibs.peixunapi.controller;


import com.gibs.peixunapi.VO.post.UserVO;
import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.VO.show.ShowUserVo;
import com.gibs.peixunapi.VO.show.UserListVO;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.UserService;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.gibs.peixunapi.enums.ResultEnum.CAN_NOT_DELETE_SELF;
import static com.gibs.peixunapi.enums.ResultEnum.DELETE_SUCCESS;


/**
 * 功能：【用户】的后台控制器
 * 项目：装配式培训平台
 * 日期：2020-08-20
 * 作者：梁嘉伟
 *
 * @author liangjiawei
 */

@Slf4j
@RestController
@Api(tags = "用户信息模块")
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "根据User对象新建用户")
    @PostMapping("/create")
    public Result create(@RequestBody @ApiParam("用户信息") UserVO object) {
        return userService.create(object);
    }


    @ApiOperation(value = "根据User对象编辑用户")
    @PostMapping("/save")
    public Result save(@RequestBody @ApiParam("用户信息") UserVO user) {

        return userService.save(user);
    }

    @ApiOperation(value = "根据id删除用户")
    @ApiImplicitParam(name = "id", value = "用户id", required = true, paramType = "query", dataType = "int")
    @GetMapping("/delete")
    public Result delete(Integer id) {
        User user = userService.getAttributeUser();
        if (user.getId().equals(id)) {
            return ResultUtil.fail(CAN_NOT_DELETE_SELF);
        } else {
            userService.delete(id);
            return ResultUtil.success(DELETE_SUCCESS);
        }
    }

    @ApiOperation(value = "根据id字符串批量删除用户")
    @ApiImplicitParam(name = "ids", value = "ids字符串", required = true, paramType = "query")
    @GetMapping("/deleteBatch")
    public Result deleteBatch(String ids) {
        return userService.deleteBatch((ConverterUtil.stringConverterToIntegerList(ids)));
    }

    @ApiOperation(value = "根据用户id获取用户")
    @ApiImplicitParam(name = "id", value = "用户id", required = true, paramType = "query", dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowUserVo.class)})
    @GetMapping("/get")
    public Result get(Integer id) {

        return userService.getVO(id);

    }

    @ApiOperation(value = "企业管理员账户管理列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "查询关键字", paramType = "query"),
            @ApiImplicitParam(name = "businessId", value = "企业id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "audit", value = "用户审核-已审核:1 未审核:0", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "current", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条目数", required = true, paramType = "query", dataType = "int")}
    )
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = UserListVO.class)})
    @GetMapping("/getPageByKeyAndType")
    public Result getPageByKeyAndType(String name, Integer businessId, Integer audit,
                                      Integer current, Integer limit) {

        return userService.getUserList(name, businessId, audit, current, limit);

    }

    @ApiOperation(value = "管理员账户管理列表(分页)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "查询关键字", paramType = "query"),
            @ApiImplicitParam(name = "businessId", value = "公司Id", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "type", value = "用户角色类型:teacher/student/businessAdmin", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "用户启用状态-已激活:1 已停用:0", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "audit", value = "用户审核-已审核:1 未审核:0", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条目数", required = true, paramType = "query", dataType = "int")}
    )
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowUserVo.class)})
    @GetMapping("/getListPage")
    public Result getListPage(String name, Integer businessId, String type, Integer status,
                              Integer audit, Integer page, Integer limit) {

        return userService.getUserList(name, businessId, type, status, audit, page, limit);

    }

    @ApiOperation(value = "关键字查询用户列表")
    @ApiImplicitParam(name = "name", value = "查询关键字", paramType = "query")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowUserVo.class)})

    @GetMapping("/getList")
    public Result getList(String name) {

        return userService.getList(name);

    }

    @ApiOperation(value = "根据关键字获取所有用户选项列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "查询关键字", paramType = "query"),
            @ApiImplicitParam(name = "current", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条目数", required = true, paramType = "query", dataType = "int")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = OptionVO.class)})
    @GetMapping("/getOptions")
    public Result getOptions(String name, Integer current, Integer limit) {

        return userService.getOptions(name, current, limit);

    }

    @ApiOperation(value = "批量审查用户", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParam(name = "ids", value = "用户ids", required = true, paramType = "form")
    @PostMapping("/audit")
    public Result audit(String ids) {
        return userService.audit(ConverterUtil.stringConverterToIntegerList(ids));
    }

    @ApiOperation(value = "用户修改密码", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户id", paramType = "form", required = true, dataType = "int"),
            @ApiImplicitParam(name = "password", value = "密码", paramType = "form", required = true)}
    )
    @PostMapping("/changePassword")
    public Result changePassword(Integer id, String password) {
        userService.changePassword(id, password);
        return ResultUtil.success(ResultEnum.OPERATING_SUCCESS);
    }

    @ApiOperation(value = "获取教师选项列表")
    @ApiImplicitParam(name = "name", value = "查询关键字", paramType = "query")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = OptionVO.class)})

    @GetMapping("/getTeacherOption")
    public Result getTeacherOption(String name) {

        return userService.getTeacherOption(name);
    }

    @ApiOperation(value = "获取助教选项列表")
    @ApiImplicitParam(name = "name", value = "查询关键字", paramType = "query")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = OptionVO.class)})

    @GetMapping("/getAssistTeacherOption")
    public Result getAssistTeacherOption(String name) {

        return userService.getAssistTeacherOption(name);
    }

    @ApiOperation(value = "获取教师选项列表(分页)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "查询关键字", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条目数", required = true, paramType = "query", dataType = "int")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = OptionVO.class)})
    @GetMapping("/getTeacherOptionPage")
    public Result getTeacherOptionPage(String name, Integer page, Integer limit) {

        return userService.getTeacherOptionPage(name, page, limit);
    }

    @ApiOperation(value = "初始化密码")
    @ApiImplicitParam(name = "id", value = "用户id", required = true, dataType = "int")
    @GetMapping("/resetPassword")
    public Result resetPassword(Integer id) {
        userService.resetPassword(id);
        return ResultUtil.success(ResultEnum.OPERATING_SUCCESS);
    }

    @ApiOperation(value = "改变用户启用状态", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户id", required = true, paramType = "form", dataType = "int"),
            @ApiImplicitParam(name = "status", value = "状态 禁用:0 启用:1", required = true, paramType = "form", dataType = "int"),
    })
    @PostMapping("/changeStatus")
    public Result changeStatus(Integer id, Integer status) {
        userService.changeStatus(id, status);
        return ResultUtil.success(ResultEnum.OPERATING_SUCCESS);
    }

    @ApiOperation(value = "获取企业员工选项列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "查询关键字", paramType = "query"),
            @ApiImplicitParam(name = "businessId", value = "企业id", paramType = "query", dataType = "int", required = true)
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = OptionVO.class)})

    @GetMapping("/getStaffOption")
    public Result getStaffOption(String name, Integer businessId) {

        return userService.getStaffOption(name, businessId);
    }


}

