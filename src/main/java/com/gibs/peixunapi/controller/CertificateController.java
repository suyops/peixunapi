package com.gibs.peixunapi.controller;


import com.gibs.peixunapi.VO.post.CertificateVO;
import com.gibs.peixunapi.VO.post.IssueCertificateVO;
import com.gibs.peixunapi.VO.show.*;
import com.gibs.peixunapi.aop.ActionLogAop;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.CertificateService;
import com.gibs.peixunapi.service.ExamClassInfoService;
import com.gibs.peixunapi.service.ExamStudentService;
import com.gibs.peixunapi.utils.ResultUtil;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.gibs.peixunapi.enums.ResultEnum.*;


/**
 * 功能：【证书模板】的后台控制器
 * 项目：装配式培训平台
 * 日期：2020-08-20
 * 作者：梁嘉伟
 */

@Slf4j
@RestController
@RequestMapping("/certificate")
@Api(tags = "证书模板API")
public class CertificateController {
    @Autowired
    private CertificateService certificateService;
    @Autowired
    private ExamClassInfoService examClassInfoService;
    @Autowired
    private ExamStudentService examStudentService;

    @ApiOperation(value = "证书模板列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "搜索关键字", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条数", paramType = "query", required = true, dataType = "int")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = CertificateListVO.class)})
    @GetMapping("/getCertificateList")
    public Result getCertificateList(String name, Integer page, Integer limit) {
        PageData Pagedata = certificateService.getCertificateList(name, page, limit);
        return ResultUtil.successPage(FIND_SUCCESS, Pagedata);
    }


    @ApiOperation(value = "证书模板详细信息")
    @ApiImplicitParam(name = "id", value = "证书模板id", paramType = "query", required = true, dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowCertificateVO.class)})
    @GetMapping("/getCertificate")
    public Result getCertificate(Integer id) {
        ShowCertificateVO data = certificateService.getCertificate(id);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "新增证书模板")
    @ActionLogAop(module = "证书中心-证书模板",type = "新增模板")
    @PostMapping("/createCertificate")
    public Result createCertificate(@RequestBody CertificateVO vo) {

        certificateService.create(vo);
        return ResultUtil.success(FIND_SUCCESS);
    }

    @ApiOperation(value = "编辑证书模板")
    @ApiImplicitParam(name = "certificate", value = "证书模板id", paramType = "query", required = true, dataType = "int")
    @ActionLogAop(module = "证书中心-证书模板",type = "编辑证书模板")
    @PostMapping("/edit")
    public Result edit(@RequestBody CertificateVO certificate) {
        certificateService.save(certificate);
        return ResultUtil.success(FIND_SUCCESS);
    }


    @ApiOperation(value = "证书发放列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "搜索关键字", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条数", paramType = "query", required = true, dataType = "int")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = IssueCertificateListVO.class)})
    @GetMapping("/getIssueCertificateList")
    public Result getIssueCertificateList(String name, Integer page, Integer limit) {
        PageData data = examClassInfoService.getIssueCertificateList(name, page, limit);
        return ResultUtil.successPage(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "获取通过考试学生列表")
    @ApiImplicitParam(name = "classId", value = "班级id", paramType = "query", required = true, dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = PassExamStudentVO.class)})
    @GetMapping("/getPassExamStudent")
    public Result getPassExamStudent(Integer classId) {
        List<PassExamStudentVO> data = examStudentService.getPassExamStudent(classId);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "发放证书")
    @ActionLogAop(module = "证书中心-证书发放",type = "发放证书")
    @PostMapping("/issueCertificate")
    public Result issueCertificate(@RequestBody IssueCertificateVO vo) {
        Integer number = certificateService.issueCertificate(vo);
        certificateService.addIssueCount(vo.getExamClassId(), number);
        return ResultUtil.success(OPERATING_SUCCESS);
    }

    @ApiOperation(value = "管理员、企业管理员-证书管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "expiry", value = "已过期", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "status", value = "已吊销", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pause", value = "已暂停", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "comingExpiry", value = "即将到期", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "businessId", value = "单位Id", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "key", value = "搜索关键字", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", paramType = "query", dataType = "int", required = true),
            @ApiImplicitParam(name = "limit", value = "最大条数", paramType = "query", dataType = "int", required = true)
    })

    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = CertificateManageVO.class)})
    @GetMapping("/getCertificateManageList")
    public Result getCertificateManageList(Integer expiry, Integer status, Integer pause,
                                           Integer comingExpiry, Integer businessId, String key,
                                           Integer page, Integer limit) {
        PageData data = certificateService.getCertificateManageList(expiry, status, pause, comingExpiry, businessId, key, page, limit);
        return ResultUtil.successPage(FIND_SUCCESS, data);
    }


    @ApiOperation(value = "证书查询")
    @ApiImplicitParam(name = "uniqueCode", value = "证书编码", paramType = "query", required = true)
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = PassExamStudentVO.class)})
    @GetMapping("/checkCertificate")
    public Result checkCertificate(String uniqueCode) {
        certificateService.checkCertificate(uniqueCode);
        return ResultUtil.success(CERTIFICATE_VALID);
    }


    @ApiOperation(value = "获取个人证书")
    @ApiImplicitParam(name = "id", value = "证书编码", paramType = "query", required = true)
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = PersonCertificateVO.class)})
    @GetMapping("/getCertificateStudent")
    public Result getCertificateStudent(Integer id) {
        PersonCertificateVO data = certificateService.getCertificateStudent(id);
        return ResultUtil.success(FIND_SUCCESS, data);
    }


    @ApiOperation(value = "证书批量续期")
    @ActionLogAop(module = "证书中心-证书管理",type = "证书批量续期")
    @PostMapping("/renewalBatch")
    public Result renewalBatch(@RequestBody List<String> uniqueCode) {
        certificateService.renewalBatch(uniqueCode);
        return ResultUtil.success(OPERATING_SUCCESS);
    }


    @ApiOperation(value = "证书暂停")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uniqueCode", value = "证书编码", paramType = "query", required = true),
            @ApiImplicitParam(name = "status", value = "状态：启用1 禁用0", paramType = "query", required = true, dataType = "int")})
    @ActionLogAop(module = "证书中心-证书管理",type = "证书暂停")
    @PostMapping("/pause")
    public Result pause(String uniqueCode, Integer status) {
        certificateService.pause(uniqueCode, status);
        return ResultUtil.success(OPERATING_SUCCESS);
    }


    @ApiOperation(value = "证书吊销")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uniqueCode", value = "证书编码", paramType = "query", required = true),
            @ApiImplicitParam(name = "status", value = "状态：启用1 禁用0", paramType = "query", required = true, dataType = "int")})
    @ActionLogAop(module = "证书中心-证书管理",type = "证书吊销")
    @PostMapping("/revoke")
    public Result revoke(String uniqueCode, Integer status) {
        certificateService.revoke(uniqueCode, status);
        return ResultUtil.success(OPERATING_SUCCESS);
    }


    @ApiOperation(value = "证书模板选项列表")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = OptionVO.class)})
    @GetMapping("/getOptionList")
    public Result getOptionList() {
        List<OptionVO> data = certificateService.getOptionList();
        return ResultUtil.success(OPERATING_SUCCESS, data);
    }

    @ApiOperation(value = "学生-证书中心")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StudentCertificateVO.class)})
    @GetMapping("/getStudentCertificate")
    public Result getStudentCertificate() {
        List<StudentCertificateVO> data = certificateService.getStudentCertificate();
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "学生-申请续期")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StudentCertificateVO.class)})
    @PostMapping("/applyRenewal")
    public Result applyRenewal(String uniqueCode) {
        certificateService.applyRenewal(uniqueCode);
        return ResultUtil.success(OPERATING_SUCCESS);
    }


}

