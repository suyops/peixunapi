package com.gibs.peixunapi.controller;

import com.gibs.peixunapi.VO.show.CalendarVO;
import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.VO.show.ShowExerciseRecordVO;
import com.gibs.peixunapi.VO.show.StudyRecordListVO;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.model.Menu;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ResultUtil;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;

import static com.gibs.peixunapi.enums.BaseEnum.*;
import static com.gibs.peixunapi.enums.ResultEnum.*;


/**
 * 功能：【登录】的后台控制器
 * 项目：装配式培训平台
 * 日期：2020-08-20
 * 作者：梁嘉伟
 *
 * @author liangjiawei
 */

@Slf4j
@RestController
@Api(tags = "登录API")
//@RequestMapping("/LoginController")
@Transactional(rollbackOn = Exception.class)
public class LoginController {

    @Autowired
    private LoginInfoService loginInfoService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private ExerciseRecordService exerciseRecordService;
    @Autowired
    private StudyProgressService studyProgressService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private UserService userService;


    @ApiOperation(value = "用户密码登录", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户登录名", required = true, paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, paramType = "query"),
    })
    @PostMapping(path = "/login")
    public Result login(String username, String password) {
        HashMap<String, Object> result = loginInfoService.login(request, username, password);
        return ResultUtil.success(LOGIN_SUCCESS, result);
    }

    @ApiOperation(value = "用户token登录", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户登录名", required = true, paramType = "query"),
    })
    @GetMapping(path = "/loginInToken")
    public Result loginInToken(String token) {

        HashMap<String, Object> result = loginInfoService.loginInToken(request, token);
        return ResultUtil.success(LOGIN_SUCCESS, result);
    }

    @ApiOperation(value = "忘记密码-修改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "passWord", value = "密码", required = true, paramType = "query"),
    })
    @PostMapping(path = "/forgetPassword")
    public Result forgetPassword(String passWord) {

        userService.forgetPassword(passWord);
        return ResultUtil.success(OPERATING_SUCCESS);
    }

    @ApiOperation(value = "获取角色菜单权限")
    @ApiImplicitParam(name = "role", value = "角色 超级管理员:superadmin 管理员:admin 企业管理员:busadmin 老师:teacher 学生:student", paramType = "query", required = true)
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = Menu.class)})
    @GetMapping("/getMenu")
    public Result getMenu(String role) {

        List<Menu> list = roleService.getMenu(role);
        return ResultUtil.success(FIND_SUCCESS, list);
    }

    @ApiOperation(value = "登出")
    @ApiImplicitParam(name = "userId", value = "用户Id", paramType = "query", required = true)
    @GetMapping("/logout")
    public Result logout(Integer userId) {
        return loginInfoService.logout(userId);
    }

    @ApiOperation(value = "获取首页EChars数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "图表名称 用户统计:userBar 培训统计:courseLine 用户比例:userPie" +
                    "培训比例:coursePie 全部单位统计:allBusinessLine 单个单位统计:oneBusinessLine", paramType = "query", required = true),
            @ApiImplicitParam(name = "role", value = "角色类型:superAdmin/admin/teacher/student 当type=courseLine时必填", paramType = "query"),
            @ApiImplicitParam(name = "businessId", value = "企业Id,当type=oneBusinessLine时必填", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "studentId", value = "学员Id,当role=student时必填", paramType = "query", dataType = "int")
    })
    @GetMapping("/getECharsData")
    public Result getECharsData(String name, String role, Integer businessId, Integer id) {
        if (USER_BAR.getMsg().equals(name)) {
            return ResultUtil.success(FIND_SUCCESS, loginInfoService.getUserBarData(businessId));
        } else if (COURSE_LINE.getMsg().equals(name)) {
            return ResultUtil.success(FIND_SUCCESS, loginInfoService.getCourseLineData(businessId, id));
        } else if (ALL_BUSINESS_LINE.getMsg().equals(name)) {
            return ResultUtil.success(FIND_SUCCESS, loginInfoService.getAllBusinessLineData());
        } else if (ONE_BUSINESS_LINE.getMsg().equals(name) && businessId != null) {
            return ResultUtil.success(FIND_SUCCESS, loginInfoService.getOneBusinessLineData(businessId));
        } else if (USER_PIE.getMsg().equals(name)) {
            return ResultUtil.success(FIND_SUCCESS, loginInfoService.getUserPieData());
        } else if (COURSE_PIE.getMsg().equals(name)) {
            return ResultUtil.success(FIND_SUCCESS, loginInfoService.getCoursePieData());
        } else {
            throw new BaseException(PARAM_ERROR);
        }
    }

    @ApiOperation(value = "学员首页-最新练习题记录 ")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int", required = true, paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", dataType = "int", required = true, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "最大条数", dataType = "int", required = true, paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowExerciseRecordVO.class)})
    @GetMapping("/getLatestExerciseRecord")
    public Result<PageData<OptionVO>> getLatestExerciseRecord(Integer userId, Integer page, Integer limit) {
        PageData<OptionVO> data = exerciseRecordService.getLatestListPage(userId, page, limit);
        return ResultUtil.successPage(ResultEnum.SUCCESS, data);

    }

    @ApiOperation(value = "学员首页-最新学习记录 ")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int", required = true, paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", dataType = "int", required = true, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "最大条数", dataType = "int", required = true, paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StudyRecordListVO.class)})
    @GetMapping("/getLatestStudyRecord")
    public Result<PageData<StudyRecordListVO>> getLatestStudyRecord(Integer userId, Integer page, Integer limit) {
        PageData<StudyRecordListVO> data = studyProgressService.getLatestStudyRecord(userId, page, limit);
        return ResultUtil.successPage(FIND_SUCCESS, data);
    }


    @ApiOperation(value = "学员首页-日历安排 ")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = CalendarVO.class)})
    @GetMapping("/getCalendarStroke")
    public Result<CalendarVO> getCalendarStroke() {
        CalendarVO data = loginInfoService.getCalendarStroke();
        return ResultUtil.success(FIND_SUCCESS, data);
    }
}

