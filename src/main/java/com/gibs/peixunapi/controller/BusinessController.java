package com.gibs.peixunapi.controller;

import com.gibs.peixunapi.VO.post.BusinessVO;
import com.gibs.peixunapi.VO.show.BusinessListVO;
import com.gibs.peixunapi.VO.show.BusinessSubscribeVO;
import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.aop.ActionLogAop;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.BusinessService;
import com.gibs.peixunapi.service.CourseService;
import com.gibs.peixunapi.utils.ResultUtil;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.gibs.peixunapi.enums.ResultEnum.*;


/**
 * 功能：【企业】的后台控制器
 * 项目：装配式培训平台
 * 日期：2020-08-20
 * 作者：梁嘉伟
 *
 * @author liangjiawei
 */

@Slf4j
@Api(tags = "企业API")
@RestController
@RequestMapping("/business")
public class BusinessController {

    @Autowired
    private BusinessService businessService;
    @Autowired
    private CourseService courseService;

    @ApiOperation(value = "新增企业")
    @ActionLogAop(module = "管理中心-部门管理", type = "新增企业")
    @PostMapping("/create")
    public Result create(@RequestBody @ApiParam("企业信息") BusinessVO business) {
        return businessService.create(business);
    }

    @ApiOperation(value = "修改企业信息")
    @ActionLogAop(module = "管理中心-企业管理", type = "修改企业信息")
    @PostMapping("/save")
    public Result save(@RequestBody @ApiParam("企业信息") BusinessVO business) {
        return businessService.save(business);
    }

    @ApiOperation(value = "删除企业")
    @ApiImplicitParam(name = "id", value = "企业id", required = true, dataType = "int", paramType = "query")
    @ActionLogAop(module = "管理中心-企业管理", type = "删除企业")
    @GetMapping("/delete")
    public Result delete(@RequestParam Integer id) {
        try {
            return businessService.delete(id) ? ResultUtil.success(DELETE_SUCCESS) : ResultUtil.success(DELETE_FAIL);
        } catch (Exception e) {
            throw new BaseException(USER_NOT_CLEAR);
        }

    }

    @ApiOperation(value = "查询企业信息")
    @ApiImplicitParam(name = "id", value = "企业id", required = true, dataType = "int", paramType = "query")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = BusinessVO.class)})
    @GetMapping("/get")
    public Result<BusinessVO> get(Integer id) {
        return ResultUtil.success(FIND_SUCCESS, businessService.getVO(id));
    }

    @ApiOperation(value = "查询企业列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "企业名称", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "最大条数", required = true, dataType = "int", paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = BusinessListVO.class)})
    @GetMapping("/getListPage")
    public Result<PageData<BusinessListVO>> getListPage(String name, Integer status, @RequestParam Integer page, @RequestParam Integer limit) {
        PageData<BusinessListVO> data = businessService.getListPage(name, status, page, limit);
        return ResultUtil.successPage(ResultEnum.FIND_SUCCESS, data);
    }

    @ApiOperation(value = "选项列表")
    @ApiImplicitParam(name = "key", value = "查询关键字", paramType = "query")
    @GetMapping("/getOptions")
    public Result<List<OptionVO>> getOptions(String key) {
        List<OptionVO> list = businessService.getOptions(key);
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, list);
    }

    @ApiOperation(value = "选项列表")
    @GetMapping("/department/getOptions")
    public Result<List<OptionVO>> getOptions() {
        List<OptionVO> list = businessService.getDepartmentOptions();
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, list);
    }

    @ApiOperation(value = "更改企业状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "businessId", value = "企业id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = true, dataType = "int", paramType = "query")
    })
    @GetMapping("/changeStatus")
    public Result changeStatus(Integer businessId, Integer status) {
        businessService.changeStatus(businessId, status);
        return ResultUtil.success(OPERATING_SUCCESS);
    }

    @ApiOperation(value = "企业管理-培训信息-订阅课程")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "businessId", value = "企业id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", required = true, dataType = "int", paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = BusinessSubscribeVO.class)})
    @GetMapping("/businessSubscribeList")
    public Result<List<BusinessSubscribeVO>> businessSubscribeList(Integer businessId) {
        List<BusinessSubscribeVO> data = courseService.businessSubscribeList(businessId);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

}

