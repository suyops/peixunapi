package com.gibs.peixunapi.controller;

import com.gibs.peixunapi.VO.post.ChapterVO;
import com.gibs.peixunapi.VO.post.CourseInfoVO;
import com.gibs.peixunapi.VO.show.*;
import com.gibs.peixunapi.aop.ActionLogAop;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ResultUtil;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.CourseEnum.DOCUMENT;
import static com.gibs.peixunapi.enums.CourseEnum.VIDEO;
import static com.gibs.peixunapi.enums.ResultEnum.*;

/**
 * 功能：课程模块
 * 项目：装配式培训平台
 * 日期：2020-08-20
 *
 * @author liangjiawei
 */

@Slf4j
@Api(tags = "课程模块API")
@RestController
@RequestMapping("/course")
public class CourseController {

    @Autowired
    private CourseService courseService;
    @Autowired
    private DocumentationService documentationService;
    @Autowired
    private VideoClassService videoClassService;
    @Autowired
    private StudyProgressService studyProgressService;
    @Autowired
    private StudentCourseService studentCourseService;
    @Autowired
    private UserService userService;
    @Autowired
    private TestPaperService testPaperService;

    @ApiOperation(value = "新增课程基本信息")
    @ActionLogAop(module = "管理中心-培训管理", type = "新增课程", desc = "填写课程基本信息")
    @PostMapping("/createBaseInfo")
    public Result<?> createBaseInfo(@RequestBody @ApiParam(value = "课程信息VO") CourseInfoVO courseInfoVO) {
//        if (courseInfoVO.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().isAfter(courseInfoVO.getEndTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())) {
//            return ResultUtil.fail(DATE_ERROR);
//        }
        return courseService.create(courseInfoVO);
    }

    @ApiOperation(value = "新增文档章节")
    @ActionLogAop(module = "管理中心-培训管理", type = "新增课程", desc = "新增文档章节")
    @PostMapping("/createDocumentation")
    public Result<?> createDocumentation(@RequestBody @ApiParam(value = "章节信息VO") ChapterVO documentList) {
        return documentationService.create(documentList);
    }

    @ApiOperation(value = "新增视频章节")
    @ActionLogAop(module = "管理中心-培训管理", type = "新增课程", desc = "新增视频章节")
    @PostMapping("/createVideoClass")
    public Result<?> createVideoClass(@RequestBody @ApiParam(value = "章节信息VO") ChapterVO videoClassList) {
        return videoClassService.create(videoClassList);
    }


//    @ApiOperation(value = "培训审核", consumes = "application/x-www-form-urlencoded")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "audit", value = "审核状态", required = true, paramType = "form", dataType = "int"),
//            @ApiImplicitParam(name = "courseId", value = "课程id", required = true, paramType = "form", dataType = "int")
//    })
//    @ActionLogAop(module = "管理中心-培训管理",type = "培训审核")
//    @PostMapping("/auditCourse")
//    public Result auditCourse(Integer audit, Integer courseId) {
//
//        return courseService.auditCourse(audit, courseId);
//    }

    @ApiOperation(value = "培训删除", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "delete", value = "删除状态", required = true, paramType = "form", dataType = "int"),
            @ApiImplicitParam(name = "courseId", value = "课程id", required = true, paramType = "form", dataType = "int")
    })
    @ActionLogAop(module = "管理中心-培训管理", type = "培训删除")
    @PostMapping("/deleteCourse")
    public Result<?> deleteCourse(Integer delete, Integer courseId) {
        return courseService.deleteCourse(delete, courseId);
    }

    @ApiOperation(value = "获取视频/文档的课程列表(审核修改用)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "type", value = "课程类型", paramType = "query", required = true),
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowChapterVO.class)})
    @GetMapping("/getChapterList")
    public Result<List<ShowChapterVO>> getChapterList(Integer courseId, String type) {
        List<ShowChapterVO> chapterList;
        if (VIDEO.getMessage().equals(type)) {
            chapterList = courseService.getList(courseId, VIDEO);
        } else if (CourseEnum.DOCUMENT.getMessage().equals(type)) {
            chapterList = courseService.getList(courseId, CourseEnum.DOCUMENT);
        } else {
            throw new BaseException(PARAM_ERROR);
        }
        return ResultUtil.success(FIND_SUCCESS, chapterList);
    }

    @ApiOperation(value = "最新课程列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条目数", required = true, paramType = "query", dataType = "int")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = CourseListVO.class)})
    @GetMapping("/getLatestCourseList")
    public Result<PageData<CourseListVO>> getLatestCourseList(Integer page, Integer limit) {
        PageData<CourseListVO> data = courseService.getLatestCourseList(page, limit);
        return ResultUtil.successPage(FIND_SUCCESS, data);

    }

    @ApiOperation(value = "学员自主报名课程", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentId", value = "学生id", required = true, dataType = "int", paramType = "form"),
            @ApiImplicitParam(name = "courseId", value = "课程id", required = true, dataType = "int", paramType = "form")
    })
    @ActionLogAop(module = "首页-培训发布", type = "学员自主报名课程")
    @PostMapping("/signUpCourse")
    public Result signUpCourse(Integer studentId, Integer courseId) {
        studentCourseService.subscribeCourse(studentId, courseId);
        return studentCourseService.signUpCourse(studentId, courseId);
    }

    @ApiOperation(value = "批量选择报名课程", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userIds", value = "用户ids,使用','隔开", required = true, paramType = "form"),
            @ApiImplicitParam(name = "courseId", value = "课程id", required = true, dataType = "int", paramType = "form")
    })
    @ActionLogAop(module = "管理中心-订阅管理", type = "批量报名课程", desc = "企业账户批量选择报名课程")
    @PostMapping("/signUpCourseBatch")
    public Result signUpCourseBatch(String userIds, Integer courseId) {
        List<Integer> idList = new ArrayList<>();
        if (userIds.contains(",")) {

            idList = Arrays.stream(userIds.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        } else {
            idList.add(Integer.parseInt(userIds));
        }
        studentCourseService.signUpCourse(idList, courseId);
        return ResultUtil.success(OPERATING_SUCCESS);
    }


//    @ApiOperation(value = "关联课程", consumes = "application/x-www-form-urlencoded")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "courseId", value = "当前课程id", required = true, paramType = "form", dataType = "int"),
//            @ApiImplicitParam(name = "linkCourseId", value = "被关联课程id", required = true, paramType = "form", dataType = "int")
//    })
//    @PostMapping("/linkCourse")
//    public Result linkCourse(Integer courseId, Integer linkCourseId) {
//
//        return courseService.linkCourse(courseId, linkCourseId);
//    }

    @ApiOperation(value = "查询单个课程详细信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "type", value = "用户当前身份,student/admin/teacher/businessAdmin", paramType = "query")
    })
    @GetMapping("/getCourseInfo")
    public Result<ShowCourseInfoVO> getCourseInfo(Integer courseId, String type) {
        ShowCourseInfoVO data = courseService.getCourseInfo(courseId, type);

        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "视频章节详细信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "videoClassId", value = "课程id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "timeRecord", value = "视频已看时长", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "用户类型: student/teacher/groupAdmin/businessAdmin/", required = true, paramType = "query")
    })
    @GetMapping("/getVideoClass")
    public Result<ShowChapterVO> getVideoClass(Integer videoClassId, Integer userId, Double timeRecord, String type) {
        if (CourseEnum.STUDENT.getMessage().equals(type)) {
            studyProgressService.updatePersonalProgress(videoClassId, userId, timeRecord, VIDEO.getMessage());
        }
        ShowChapterVO data = videoClassService.getVO(videoClassId, userId, type);

        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "保存视频时长")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "videoClassId", value = "视频课程Id", paramType = "form", dataType = "int"),
            @ApiImplicitParam(name = "time", value = "时间", paramType = "form"),
            @ApiImplicitParam(name = "isVideoPass", value = "是否答题", paramType = "form", dataType = "int"),
            @ApiImplicitParam(name = "isClose", value = "是否关闭", paramType = "form", dataType = "int"),
    })
    @PostMapping("/saveVideoTime")
    public Result saveVideoTime(Integer videoClassId, String time, Integer isVideoPass, Integer isClose) {
        User user = userService.getAttributeUser();
        studyProgressService.saveVideoTime(user.getId(), videoClassId, time, isVideoPass);
        if (isClose != null && isClose == 1) {
            studentCourseService.updateCourseProgress(videoClassId, user.getId(), VIDEO);
            studentCourseService.isFinishCourse(user.getId(), videoClassId, VIDEO.getMessage());
        }
        return ResultUtil.success(OPERATING_SUCCESS);
    }

    @ApiOperation(value = "文档章节详细信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "documentationId", value = "课程id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "userId", value = "用户id(当以学生身份进入系统时必填)", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "timeRecord", value = "视频已看时长", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "用户类型(当以学生身份进入系统时必填):\n student/admin/teacher/superAdmin", paramType = "query")
    })
    @GetMapping("/getDocumentation")
    public Result<ShowChapterVO> getDocumentation(Integer documentationId, Integer userId, Double timeRecord, String type) {
        if (StringUtils.isNotBlank(type) && CourseEnum.STUDENT.getMessage().equals(type)) {
            studyProgressService.updatePersonalProgress(documentationId, userId, timeRecord, DOCUMENT.getMessage());
            studentCourseService.updateCourseProgress(documentationId, userId, DOCUMENT);
            //判断是否满足考试资格
            studentCourseService.isFinishCourse(userId, documentationId, DOCUMENT.getMessage());
        }
        ShowChapterVO data = documentationService.getVO(documentationId, userId);

        return ResultUtil.success(FIND_SUCCESS, data);
    }


    @ApiOperation(value = "试卷管理-课程列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "搜索关键字", paramType = "query"),

            @ApiImplicitParam(name = "page", value = "页数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "最大条数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "teacherId", value = "老师id", required = true, dataType = "int", paramType = "query")
    })
    @GetMapping("/getTestPaperCourseList")
    public Result<PageData<CourseListVO>> getCourseListOfExam(Integer page, Integer limit, Integer teacherId, String key) {
        PageData<CourseListVO> data = courseService.getTestPaperCourseList(page, limit, teacherId, key);
        return ResultUtil.successPage(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "试卷管理-试卷列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "搜索关键字", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "最大条数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "courseId", value = " 课程Id", required = true, dataType = "int", paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = CourseListVO.class)})
    @GetMapping("/getTestPaperList")
    public Result<PageData<ShowTestPaperListVO>> getTestPaperList(Integer courseId, String key, Integer page, Integer limit) {
        PageData<ShowTestPaperListVO> data = testPaperService.getTestPaperList(courseId, key, page, limit);
        return ResultUtil.successPage(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "传入培训VO类,编辑培训基本信息")
    @ActionLogAop(module = "管理中心-培训管理", type = "编辑培训基本信息")
    @PostMapping("/editCourse")
    public Result editCourse(@RequestBody @ApiParam("培训VO类") CourseInfoVO courseInfoVO) {
        return courseService.editCourse(courseInfoVO);
    }

    @ApiOperation(value = "编辑视频课程")
    @ActionLogAop(module = "管理中心-培训管理", type = "编辑视频课程")
    @PostMapping("/editVideoClass")
    public Result editVideoClass(@RequestBody @ApiParam("课程VO类") ChapterVO videoClass) {
        return videoClassService.editVideoClass(videoClass);
    }

    @ApiOperation(value = "编辑文档课程")
    @ActionLogAop(module = "管理中心-培训管理", type = "编辑文档课程")
    @PostMapping("/editDocumentation")
    public Result editDocumentation(@RequestBody @ApiParam("课程VO类") ChapterVO documentation) {
        return documentationService.editDocument(documentation);
    }

    @ApiOperation(value = "获取课程选项列表")
    @ApiImplicitParam(name = "key", value = "查询关键字", required = true, paramType = "query")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = OptionVO.class)})
    @GetMapping("/getOptionList")
    public Result getOptionList(String key) {
        return courseService.getOptionList(key);
    }


    @ApiOperation(value = "管理员-学习中心文档/视频管理列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "类型:video/document", required = true, paramType = "query"),
            @ApiImplicitParam(name = "teacherId", value = "老师Id", paramType = "query", dataType = "int")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ChapterOfAdminTeacherVO.class)})
    @GetMapping("/getPassChapterList")
    public Result<List<ChapterOfAdminTeacherVO>> getPassChapterList(String type, Integer teacherId) {
        List<ChapterOfAdminTeacherVO> data = courseService.getPassChapterList(type, teacherId);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "学员-学习中心文档/视频管理列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentId", value = "学生id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "课程类型:video/document", paramType = "query", required = true)
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StudentCourseVO.class)})
    @GetMapping("/getStudentCourseList")
    public Result getStudentCourseList(Integer studentId, String type) {
        List<StudentCourseVO> data;
        if (VIDEO.getMessage().equals(type)) {
            data = courseService.getStudentCourseList(studentId, VIDEO);
            return ResultUtil.success(FIND_SUCCESS, data);
        } else if (CourseEnum.DOCUMENT.getMessage().equals(type)) {
            data = courseService.getStudentCourseList(studentId, DOCUMENT);
            return ResultUtil.success(FIND_SUCCESS, data);
        } else {
            throw new BaseException(PARAM_ERROR);
        }

    }


    @ApiOperation(value = "管理中心-课程管理列表")
    @ApiImplicitParams({
//            @ApiImplicitParam(name = "isAudit", value = "是否通过审批", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "teacherId", value = "老师id", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "name", value = "课程名", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条数", required = true, paramType = "query", dataType = "int"),
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = CourseManageVO.class)})
    @GetMapping("/getCourseManageList")
    public Result getCourseManageList(Integer isAudit, Integer teacherId, String name, Integer page, Integer limit) {
        //TODO 修改审批部分
        PageData<CourseManageVO> data = courseService.getCourseManageList(null, teacherId, name, page, limit);
        return ResultUtil.successPage(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "老师-当前课程")
    @ApiImplicitParam(name = "id", value = "用户id", required = true, paramType = "query", dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StartingCourseVO.class)})
    @GetMapping("/getStartingList")
    public Result<List<StartingCourseVO>> getStartingList(Integer id) {
        List<StartingCourseVO> data = courseService.getStartingCourseList(id);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "更改章节状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "章节id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "type", value = "章节类型:video/document", required = true, paramType = "query"),
            @ApiImplicitParam(name = "status", value = "章节id", required = true, paramType = "query", dataType = "int")
    })
    @GetMapping("/changeChapterStatus")
    public Result changeChapterStatus(Integer id, Integer status, String type) {
        if (VIDEO.getMessage().equals(type)) {
            videoClassService.changeChapterStatus(id, status);
        }
        if (DOCUMENT.getMessage().equals(type)) {
            documentationService.changeChapterStatus(id, status);
        }
        return ResultUtil.success(OPERATING_SUCCESS);
    }

    @ApiOperation(value = "上线,下线章节")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "章节id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "type", value = "章节类型:video/document", required = true, paramType = "query"),
            @ApiImplicitParam(name = "isPublish", value = "上线,下线 ;上线:1 下线:0", required = true, paramType = "query", dataType = "int")
    })
    @GetMapping("/auditChapter")
    public Result auditChapter(Integer id, Integer isPublish, String type) {
        //TODO isPublish需要
        if (VIDEO.getMessage().equals(type)) {
            videoClassService.auditChapter(id, isPublish);
        }
        if (DOCUMENT.getMessage().equals(type)) {
            documentationService.auditChapter(id, isPublish);
        }
        return ResultUtil.success(OPERATING_SUCCESS);
    }



    @ApiOperation(value = "课程详细页面-获取其他课程列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程Id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "type", value = "类型:video/document", required = true, paramType = "query")
    })

    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = CourseOptionVO.class)})
    @GetMapping("/getOptionListByCourse")
    public Result<List<CourseOptionVO>> getOptionListByCourse(Integer courseId, String type) {
        List<CourseOptionVO> data = courseService.getOptionListByCourse(courseId, type);
        return ResultUtil.success(FIND_SUCCESS, data);
    }


    @ApiOperation(value = "管理员-报名课程-学生选项列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程Id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "signUp", value = "是否报名", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "businessId", value = "公司id", paramType = "query", dataType = "int")
    })

    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StudentSignUpTypeVO.class)})
    @GetMapping("/getStudentListByCourseOrBusiness")
    public Result getStudentListByCourseOrBusiness(Integer courseId, Integer signUp, Integer businessId) {
        List<StudentSignUpTypeVO> data = studentCourseService.getStudentListByCourseOrBusiness(courseId, signUp, businessId);
        return ResultUtil.success(FIND_SUCCESS, data);
    }


    @ApiOperation(value = "视频替换")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "videoClassId", value = "是否报名", paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "videoId", value = "公司id", paramType = "query", dataType = "int")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StudentSignUpTypeVO.class)})
    @ActionLogAop(module = "管理中心-培训管理", type = "编辑视频课程", desc = "更换视频")
    @PostMapping("/changeVideo")
    public Result changeVideo(Integer videoClassId, Integer videoId) {
        videoClassService.changeVideo(videoClassId, videoId);
        return ResultUtil.success(OPERATING_SUCCESS);
    }


    @ApiOperation(value = "添加助教")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程Id", paramType = "form", dataType = "int"),
            @ApiImplicitParam(name = "ids", value = "助教ids", paramType = "form")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = StudentSignUpTypeVO.class)})
    @PostMapping("/addAssistTeacher")
    public Result addAssistTeacher(Integer courseId, String ids) {
        courseService.addAssistTeacher(courseId, ids);
        return ResultUtil.success(OPERATING_SUCCESS);
    }


    @ApiOperation(value = "老师-学员学习情况")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程Id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "limit", value = "最大条数", required = true, paramType = "query", dataType = "int"),
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = CourseStudyVO.class)})
    @GetMapping("/getCourseStudyList")
    public Result<PageData<CourseStudyVO>> getCourseStudyList(Integer courseId, Integer page, Integer limit) {
        PageData<CourseStudyVO> data = studentCourseService.getCourseStudyList(courseId, page, limit);
        return ResultUtil.successPage(FIND_SUCCESS, data);
    }
//
//    @ApiOperation(value = "申请审核")
//    @ApiImplicitParam(name = "courseId", value = "课程Id", required = true, paramType = "query", dataType = "int")
//    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = CourseStudyVO.class)})
//    @PostMapping("/applyAudit")
//    public Result applyAudit(Integer courseId) {
//        courseService.applyAudit(courseId);
//        return ResultUtil.success(POST_SUCCESS);
//    }

    @ApiOperation(value = "编辑报名列表-获取学生列表")
    @ApiImplicitParam(name = "courseId", value = "课程Id", required = true, paramType = "query", dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = SignUpStudentListVO.class)})
    @GetMapping("/getEditSignUpList")
    public Result getEditSignUpList(Integer courseId) {
        List<SignUpStudentListVO> data = studentCourseService.getSignUpList(courseId);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "编辑章节列表-删除章节")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chapterId", value = "章节Id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "type", value = "类型:video/document", required = true, paramType = "query"),
    })
    @GetMapping("/deleteChapter")
    public Result deleteChapter(Integer chapterId, String type) {
        if (DOCUMENT.getMessage().equals(type)) {
            documentationService.deleteChapter(chapterId);
        }
        if (VIDEO.getMessage().equals(type)) {
            videoClassService.deleteChapter(chapterId);
        }
        return ResultUtil.success(DELETE_SUCCESS);
    }

}

