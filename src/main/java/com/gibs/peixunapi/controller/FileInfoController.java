package com.gibs.peixunapi.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.gibs.peixunapi.aop.ActionLogAop;
import com.gibs.peixunapi.config.Myconfig;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.listener.ExcelModelListener;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;

import static com.gibs.peixunapi.enums.BaseEnum.STATIC_FILE_PATH;
import static com.gibs.peixunapi.enums.ResultEnum.*;

/**
 * 功能：【文件】的后台控制器
 * 项目：装配式培训平台
 * 日期：2020-08-20
 * 作者：梁嘉伟
 *
 * @author liangjiawei
 */

@Slf4j
@Api(tags = "文件模块API")
@RestController
@RequestMapping("/fileInfo")
public class FileInfoController {

    @Autowired
    private FileInfoService fileInfoService;
    @Autowired
    private SubjectHubService subjectHubService;
    @Autowired
    private CourseSubjectHubService courseSubjectHubService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private TestPaperSubjectService testPaperSubjectService;
    @Autowired
    private SubOptionService subOptionService;


    @ApiOperation(value = "上传文件")
    @PostMapping("/uploadFile")
    public Result uploadFile(MultipartFile files) throws Exception {
        return fileInfoService.uploadFile(files);
    }

    @ApiOperation(value = "批量上传文件")
    @PostMapping("/uploadFileMulti")
    public Result uploadFileMulti(MultipartFile[] files) throws Exception {
        return fileInfoService.uploadFileMulti(files);
    }

    @ApiOperation(value = "分片上传step1-检查文件是否已经存在", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "md5", value = "文件md5", paramType = "form", required = true),
            @ApiImplicitParam(name = "fileName", value = "文件名", paramType = "form", required = true),
            @ApiImplicitParam(name = "ext", value = "文件后缀", paramType = "form", required = true)}
    )
    @PostMapping("/ifExist")
    public Result ifExist(String fileMd5,
                          String fileName,
                          String fileExt) {
        System.out.println(fileMd5 + "==" + fileName + "==" + fileExt);
        return fileInfoService.checkFile(fileMd5, fileName, fileExt);
    }

    @ApiOperation(value = "分片上传step2-校验文件块是否已上传", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "md5", value = "文件md5", paramType = "form", required = true),
            @ApiImplicitParam(name = "chunk", value = "目前分片序号", paramType = "form", required = true, dataType = "int"),
            @ApiImplicitParam(name = "chunkSize", value = "总分片数量", paramType = "form", required = true, dataType = "int")}
    )
    @PostMapping("/checkChunk")
    public Result checkChunk(String fileMd5,
                             Integer chunk,
                             Integer chunkSize) {
        if (fileInfoService.checkChunk(fileMd5, chunk, chunkSize)) {
            return ResultUtil.success(ResultEnum.CHUNK_FILE_UPLOAD_EXIST);
        } else {
            return ResultUtil.fail(ResultEnum.CHUNK_NOT_EXIST);
        }
    }

    @ApiOperation(value = "分片上传step3-上传文件块", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "分块文件", paramType = "form", required = true),
            @ApiImplicitParam(name = "fileMd5", value = "md5", paramType = "form", required = true),
            @ApiImplicitParam(name = "chunk", value = "分块序号", paramType = "form", required = true, dataType = "int")}
    )
    @PostMapping("/uploadChunk")
    public Result uploadChunk(MultipartFile file,
                              String fileMd5,
                              Integer chunk) {
        String path = fileInfoService.uploadChunk(file, fileMd5, chunk);
        return ResultUtil.success(UPLOAD_SUCCESS, path);

    }


    @ApiOperation(value = "分片上传step4-合并文件块", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileMd5", value = "md5", paramType = "form", required = true),
            @ApiImplicitParam(name = "fileName", value = "文件名", paramType = "form", required = true),
            @ApiImplicitParam(name = "fileSize", value = "文件大小", paramType = "form", required = true),
            @ApiImplicitParam(name = "fileExt", value = "文件后缀", paramType = "form", required = true)}
    )
    @ActionLogAop(module = "文件管理", type = "视频上传")
    @PostMapping("/mergeFile")
    public Result mergeFile(String fileMd5, String fileName, Long fileSize, String fileExt) {
        HashMap<String, String> data = fileInfoService.mergeChunks(fileMd5, fileName, fileSize, fileExt);
        return ResultUtil.success(MERGE_SUCCESS, data);
    }


    @ApiOperation(value = "上传解析excel")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "course时传courseId testPaper时传testPapId", paramType = "query", required = true),
            @ApiImplicitParam(name = "type", value = "course/testPaper", paramType = "query", required = true)}
    )
//    @ActionLogAop(module = "文件模块", type = "题库、试卷导入")
    @PostMapping("/sheetImport")
    public Result sheetImport(MultipartFile file,
                              @RequestParam Integer id, @RequestParam String type) {
        ExcelReader excelReader = null;
        try {
            // 读取部分sheet
            excelReader = EasyExcel.read(file.getInputStream()).build();

            ReadSheet readSheet1 = EasyExcelFactory.readSheet(0).registerReadListener(
                    new ExcelModelListener(subjectHubService, courseSubjectHubService, courseService, testPaperSubjectService, subOptionService, id, type,"radio")).build();
            ReadSheet readSheet2 = EasyExcelFactory.readSheet(1).registerReadListener(
                    new ExcelModelListener(subjectHubService, courseSubjectHubService, courseService, testPaperSubjectService, subOptionService, id, type,"check")).build();
            ReadSheet readSheet3 = EasyExcelFactory.readSheet(2).registerReadListener(
                    new ExcelModelListener(subjectHubService, courseSubjectHubService, courseService, testPaperSubjectService, subOptionService, id, type,"judge")).build();
            ReadSheet readSheet4 = EasyExcelFactory.readSheet(3).registerReadListener(
                    new ExcelModelListener(subjectHubService, courseSubjectHubService, courseService, testPaperSubjectService, subOptionService, id, type,"fill")).build();
            ReadSheet readSheet5 = EasyExcelFactory.readSheet(4).registerReadListener(
                    new ExcelModelListener(subjectHubService, courseSubjectHubService, courseService, testPaperSubjectService, subOptionService, id, type,"question")).build();
            excelReader.read(readSheet1, readSheet2, readSheet3, readSheet4,readSheet5);
//
        } catch (IOException e) {
            throw new BaseException(INPUT_STREAM_ERROR, e);
        } finally {
            if (excelReader != null) {
                // 这里千万别忘记关闭，读的时候会创建临时文件，到时磁盘会崩的
                excelReader.finish();
                return ResultUtil.success(UPLOAD_SUCCESS);
            }
        }
        return ResultUtil.success(UPLOAD_SUCCESS);
    }

    @ApiOperation(value = "获取静态资源")
    @ApiImplicitParam(name = "fileName", value = "文件名 :培训平台题目模板.xls", paramType = "query", required = true)
    @GetMapping("/getStaticFile")
    public Result getStaticFile(String fileName) {
        String path = Myconfig.HTTP_PATH + STATIC_FILE_PATH.getMsg() + fileName;
        return ResultUtil.success(FIND_SUCCESS, path);
    }
}

