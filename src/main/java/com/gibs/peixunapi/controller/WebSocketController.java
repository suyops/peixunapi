package com.gibs.peixunapi.controller;

import com.gibs.peixunapi.VO.post.CommentVO;
import com.gibs.peixunapi.VO.show.ShowCommentVO;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.CommentService;
import com.gibs.peixunapi.service.UserService;
import com.gibs.peixunapi.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * https://blog.csdn.net/yingxiake/article/details/51224569
 *
 * @author liangjiawei
 */
@RestController
@Slf4j
@Api(tags = "评论提问模块API")
public class WebSocketController {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    private CommentService commentService;
    @Autowired
    private UserService userService;

    /**
     * 广播订阅
     * 接收客户端发送的消息，当客户端发送消息的目的地为/app/sendTest时，交给该注解所在的方法处理消息，其中/app是配置的客户端请求前缀
     * 若没有添加@SendTo注解且该方法有返回值，则返回的目的地地址为/topic/sendTest，经过消息代理，客户端需要订阅了这个主题才能收到返回消息
     * <p>
     * 课程章节讨论部分
     *
     * @param comment          讨论实体类
     * @param subscribeChannel subscribeChannel跟随URL一起返回
     * @return
     */
    @ApiOperation(value = "推送课程章节讨论")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowCommentVO.class)})
    @MessageMapping("/pushComment/{subscribeChannel}")
    @SendTo("/topic/pushComment/{subscribeChannel}")
    @PostMapping("/pushComment/{subscribeChannel}")
    public Result pushComment(@RequestBody CommentVO comment, @PathVariable String subscribeChannel) {
        //保存最新评论
        Integer id = commentService.pushComment(comment, subscribeChannel);
        //返回最新的评论
        ShowCommentVO data = commentService.getOneComment(id);
        return ResultUtil.success(ResultEnum.CREATE_SUCCESS, data);
    }

    /**
     * 获取课程章节全部评论
     *
     * @param subscribeChannel subscribeChannel跟随URL一起返回  video+{id} /getAllComment/video1
     * @return
     */
    @ApiOperation(value = "获取课程章节全部评论")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowCommentVO.class)})
    @SubscribeMapping("/getAllComment/{subscribeChannel}")
    @GetMapping("/getAllComment/{subscribeChannel}")
    public Result<List<ShowCommentVO>> getAllComment(@PathVariable String subscribeChannel) {
        //获取全部评论
        List<ShowCommentVO> data = commentService.getAllComment(subscribeChannel, 1);

        return ResultUtil.success(ResultEnum.FIND_SUCCESS, data);
    }

    /**
     * 广播订阅
     * 接收客户端发送的消息，当客户端发送消息的目的地为/app/sendTest时，交给该注解所在的方法处理消息，其中/app是配置的客户端请求前缀
     * 若没有添加@SendTo注解且该方法有返回值，则返回的目的地地址为/topic/sendTest，经过消息代理，客户端需要订阅了这个主题才能收到返回消息
     * <p>
     * 课程章节提问部分
     *
     * @param comment          讨论实体类
     * @param subscribeChannel subscribeChannel跟随URL一起返回
     * @return
     */
    @ApiOperation(value = "推送课程章节提问")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowCommentVO.class)})
    @MessageMapping("/pushQuestion/{subscribeChannel}")
    @SendTo("/topic/pushQuestion/{subscribeChannel}")
    @PostMapping("/pushQuestion/{subscribeChannel}")
    public Result<ShowCommentVO> pushQuestion(@RequestBody CommentVO comment, @PathVariable String subscribeChannel) {
        //保存最新提问
        if (comment.getAccepter() == null) {
            throw new BaseException(ResultEnum.PARAM_ERROR);
        }
        Integer id = commentService.pushQuestion(comment, subscribeChannel);
        //返回最新的提问
        ShowCommentVO data = commentService.getOneComment(id);
        return ResultUtil.success(ResultEnum.CREATE_SUCCESS, data);
    }

    /**
     * 获取课程章节全部提问
     *
     * @param subscribeChannel subscribeChannel跟随URL一起返回
     * @return
     */
    @ApiOperation(value = "获取课程章节全部提问")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowCommentVO.class)})
    @SubscribeMapping("/getAllQuestion/{subscribeChannel}")
    public Result<List<ShowCommentVO>> getAllQuestion(@DestinationVariable String subscribeChannel) {
        //获取全部提问
        List<ShowCommentVO> data = commentService.getAllComment(subscribeChannel, 2);
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, data);
    }

    /**
     * 获取课程章节全部评论
     *
     * @return
     */
    @ApiOperation(value = "老师获取提问")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowCommentVO.class)})
    @SubscribeMapping("/getPersonQuestion")
    @GetMapping("/getPersonQuestion")
    public Result<Object> getPersonQuestion() {
        //获取全部评论
        User user = userService.getAttributeUser();
        List<ShowCommentVO> data = commentService.getPersonQuestion(user.getId(), 2);
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, data);
    }

    /**
     * 广播订阅
     *
     * @param name
     * @return
     * @throws InterruptedException
     */
    @MessageMapping("/topic2")
    public void sub2(String name) throws InterruptedException {
        log.info("广播模板方式订阅执行了");
        Thread.sleep(1000);
        simpMessagingTemplate.convertAndSend("/topic/getResponse", "广播模板方式订阅成功了");
    }


    /**
     * 精准推送
     * SendToUser 把消息唯一的推送到请求者的订阅路径中去
     *
     * @return
     * @throws Exception
     */
    @MessageMapping("/welcome1")
    @SendToUser//默认发送到/user/queue/welcome1,broadcast = false把避免推送到所有的session中
    public String welcome(String name) throws Exception {

        log.info(name + "单点订阅注解方式执行了");
        Thread.sleep(1000);

        return "单点订阅注解方式成功了";
    }


    /**
     * 单点发送
     *
     * @return
     * @throws Exception
     */
    @MessageMapping("/welcome2/{userId}/{stationId}")
    public void say2(@DestinationVariable("userId") String userId, @DestinationVariable("stationId") String stationId) throws Exception {

        log.info(userId + "单点订阅模板方式执行了" + stationId);
        Thread.sleep(1000);
        simpMessagingTemplate.convertAndSendToUser("123", "/queue/getResponse", "单点订阅模板方式成功了");
    }

    /**
     * 单点发送
     *
     * @return
     * @throws Exception
     */
    @MessageMapping("/welcome2")
    public void say2(String name) throws Exception {

        log.info(name + "单点订阅模板方式执行了");
        Thread.sleep(1000);
        simpMessagingTemplate.convertAndSendToUser("123", "/queue/getResponse", "单点订阅模板方式成功了");
    }

    /**
     * 定时广播
     */
//    @Scheduled(fixedRate = 10000)
//    public void sendTopicMessage() {
//        System.out.println("后台广播推送！");
//        simpMessagingTemplate.convertAndSend("/topic/getResponse","感谢您订阅了我");
//    }


}
