package com.gibs.peixunapi.controller;

import com.gibs.peixunapi.DTO.ShowExerciseRecordDTO;
import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.VO.post.*;
import com.gibs.peixunapi.VO.show.*;
import com.gibs.peixunapi.aop.ActionLogAop;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.model.TestPaper;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ResultUtil;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.gibs.peixunapi.enums.ResultEnum.*;

/**
 * 功能： 题库中心
 * 项目：装配式培训平台
 * 日期：2020-08-20
 * 作者：梁嘉伟
 */

@Slf4j
@Api(tags = "题目中心API")
@RestController
@RequestMapping("/subjectHub")
public class SubjectHubController {

    @Autowired
    private SubjectHubService subjectHubService;
    @Autowired
    private CourseSubjectHubService courseSubjectHubService;
    @Autowired
    private ExamInfoService examInfoService;
    @Autowired
    private ExamClassInfoService examClassInfoService;
    @Autowired
    private TestPaperService testPaperService;
    @Autowired
    private TestPaperSubjectService testPaperSubjectService;
    @Autowired
    private ExercisesService exercisesService;
    @Autowired
    private ExerciseRecordService exerciseRecordService;
    @Autowired
    private StudyProgressService studyProgressService;
    @Autowired
    private CourseService courseService;

    @ApiOperation(value = "在题库中直接创建单条题目")
    @ActionLogAop(module = "管理中心-题库管理", desc = "在题库中直接创建单条题目")
    @PostMapping("/createSingleSubject")
    public Result createSingleSubject(@RequestBody SubjectHubVO object) {
        subjectHubService.create(object);
        return ResultUtil.success(ResultEnum.CREATE_SUCCESS);
    }

    @ApiOperation(value = "在文档/视频中直接创建单条题目")
    @ActionLogAop(module = "管理中心-题库管理", desc = "在文档/视频中直接创建单条题目")
    @PostMapping("/createSubjectInChapter")
    public Result createSubjectInChapter(@RequestBody PostSubjectHubVO subject) {
        exercisesService.createInChapter(subject);
        return ResultUtil.success(ResultEnum.CREATE_SUCCESS);
    }

    @ApiOperation(value = "编辑单条题目")
    @ActionLogAop(module = "管理中心-题库管理", desc = "编辑单条题目")
    @PostMapping("/editSubject")
    public Result editSubject(@RequestBody PostSubjectHubVO object) {
        subjectHubService.edit(object);
        return ResultUtil.success(ResultEnum.UPDATE_SUCCESS);
    }


    @ApiOperation(value = "获取单条题目")
    @ApiImplicitParam(name = "id", value = "题目id", dataType = "int", paramType = "query")
    @GetMapping("/getSubject")
    public Result getSubject(Integer id) {
        SubjectHubVO data = subjectHubService.getVO(id);
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, data);
    }

    @ApiOperation(value = "删除单条题目")
    @ApiImplicitParam(name = "id", value = "题目id", dataType = "int")
    @ActionLogAop(module = "管理中心-题库管理", desc = "删除单条题目")
    @PostMapping("/delete")
    public Result delete(Integer id) {
        subjectHubService.delete(id);
        return ResultUtil.success(ResultEnum.DELETE_SUCCESS);
    }

    @ApiOperation(value = "关联课程习题库")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "link", value = "关联课程id", dataType = "int"),
            @ApiImplicitParam(name = "beLink", value = "被关联课程id", dataType = "int")
    })
    @GetMapping("/linkCourseSubjectHub")
    public Result linkCourseSubjectHub(Integer link, Integer beLink) {
        courseSubjectHubService.linkSubjectHubByCourseId(link, beLink);
        return ResultUtil.success(OPERATING_SUCCESS);
    }


    @ApiOperation(value = "根据班级获取试卷")
    @ApiImplicitParam(name = "classId", value = "考试班级Id", paramType = "query", dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowTestPaperVO.class)})
    @GetMapping("/getTestPaper")
    public Result getTestPaper(Integer classId) {
        TestPaper testPaper = examInfoService.getTestPaperByExamClass(classId);
        Map<String, List<SubjectHubVO>> subjectHubVOMap = testPaperSubjectService.getMapByTestPaper(testPaper.getId());
        ShowTestPaperVO data = testPaperService.getVOList(testPaper, subjectHubVOMap);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "获取试卷选项列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程id", paramType = "query"),
            @ApiImplicitParam(name = "key", value = "搜索关键字", paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = OptionVO.class)})
    @GetMapping("/getTestPaperOption")
    public Result getTestPaperOption(Integer courseId, String key) {
        return testPaperService.getOptions(courseId, key);
    }


    @ApiOperation(value = "编辑试卷 前段传递需要更改的题目列表")
    @ActionLogAop(module = "管理中心-试卷管理", desc = "编辑试卷")
    @PostMapping("/editTestPaperAndSubject")
    public Result editTestPaperAndSubject(@RequestBody TestPaperVO editTestPaperVO) {
        if (editTestPaperVO.getSubjectHubVOList().isEmpty()) {
            throw new BaseException(ResultEnum.PARAM_ERROR);
        }
        //获取原来卷子的题目list
        Map<String, List<SubjectHubVO>> subjectHubVOMap = testPaperSubjectService.getMapByTestPaper(editTestPaperVO.getTestPaperId());
        List<SubjectHubVO> subjectHubVOList = new ArrayList<>();
        subjectHubVOMap.forEach((key, value) -> subjectHubVOList.addAll(value));

        if (examClassInfoService.isTestPaperUsed(editTestPaperVO.getExamInfoId())) {
            // 如果此试卷已经使用,根据传来的id进行替换并新增试卷,新增试卷题目中间表
            TestPaper testPaper = testPaperSubjectService.switchTestPaperSubject(subjectHubVOList, editTestPaperVO);
            //更新examInfo
            return examInfoService.updateTestPaper(testPaper, editTestPaperVO.getExamInfoId());
        } else {//如果此试卷并未使用那么就直接更改题目
            return testPaperSubjectService.updateTestPaperSubject(subjectHubVOList, editTestPaperVO);
        }
    }


    @ApiOperation(value = "根据课程和目标类型Type获取题目")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程Id", paramType = "query", dataType = "int", required = true),
            @ApiImplicitParam(name = "type", value = "类型:radio/check/judge/fill/question", paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = SubjectHubVO.class)})
    @GetMapping("/getSubjectInCourse")
    public Result getSubjectInCourse(Integer courseId, String type) {

        return courseSubjectHubService.getSubjectList(courseId, type);
    }


    @ApiOperation(value = "获取章节的练习题")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chapterId", value = "章节Id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "number", value = "题目数量", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "type", value = "类型 video/document", required = true, paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = SubjectHubVO.class)})
    @GetMapping("/getSomeChapterExercise")
    public Result getSomeChapterExercise(Integer chapterId, Integer number, String type) {
        List<SubjectHubVO> data = exercisesService.getSomeChapterExercise(chapterId, number, type);

        return ResultUtil.success(ResultEnum.SUCCESS, data);
    }


    @ApiOperation(value = "学员获得练习题记录 ")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int", required = true, paramType = "query"),
            @ApiImplicitParam(name = "courseId", value = "课程id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "chapterId", value = "章节id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "类型", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", dataType = "int", required = true, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "最大条数", dataType = "int", required = true, paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowExerciseRecordVO.class)})
    @GetMapping("/getExerciseRecord")
    public Result<PageData<ShowExerciseRecordDTO>> getExerciseRecord(Integer userId, Integer courseId,
                                    Integer chapterId, String type,
                                    Integer page, Integer limit) {
        PageData<ShowExerciseRecordDTO> data = exerciseRecordService.getListPage(userId, courseId, chapterId, type, page, limit);
        return ResultUtil.successPage(ResultEnum.SUCCESS, data);

    }


    @ApiOperation(value = "新增学员的习题回答记录")

    @PostMapping("/createExerciseRecord")
    public Result createExerciseRecord(@RequestBody RecordVO recordVO) {
        for (StudyRecordVO e : recordVO.getRecordVOList()) {
            if (StringUtils.isBlank(e.getMyAnswer())) {
                throw new BaseException(ResultEnum.PARAM_ERROR);
            }
        }
        //新增学员的习题回答记录
        exerciseRecordService.create(recordVO.getStudentId(), recordVO.getRecordVOList());
        //更新学生学习进度
//        studyProgressService.saveProgress(recordVO.getStudentId(), recordVO.getRecordVOList());
        return ResultUtil.success(ResultEnum.SUCCESS);
    }


    @ApiOperation(value = "获取该学员在此视频的观看时长")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentId", value = "学员id", paramType = "form", required = true, dataType = "int"),
            @ApiImplicitParam(name = "videoClassId", value = "视频课程id", paramType = "form", required = true, dataType = "int"),
    })
    @PostMapping("/getVideoClassProgress")
    public Result getVideoClassProgress(Integer studentId, Integer videoClassId) {

        return studyProgressService.getVideoClassProgress(studentId, videoClassId);
    }

    @ApiOperation(value = "管理员-题库管理")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "搜索关键字", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "最大条数", required = true, dataType = "int", paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = SubjectHubManageVO.class)})
    @GetMapping("/getSubjectHubManageList")
    public Result getSubjectHubManageList(String name, Integer page, Integer limit) {
        PageData data = courseService.getSubjectHubManageList(name, page, limit);
        return ResultUtil.successPage(ResultEnum.FIND_SUCCESS, data);
    }

    @ApiOperation(value = "老师-题库管理入口课程列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", value = "搜索关键字", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "最大条数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "teacherId", value = "老师id", dataType = "int", paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = CourseListVO.class)})
    @GetMapping("/getSubjectHubCourseList")
    public Result getCourseListOfSubjectHub(String key, Integer page, Integer limit, Integer teacherId) {
        return courseService.getSubjectHubCourseList(page, limit, teacherId, key);
    }

    @ApiOperation(value = "随机生成试卷")
    @ActionLogAop(module = "考试中心-新增考试", type = "随机生成试卷", desc = "编辑试卷")
    @PostMapping("/randomTestPaper")
    public Result randomTestPaper(@RequestBody RandomTestPaperVO vo) {
        Integer id = testPaperService.randomTestPaper(vo);
        return ResultUtil.success(ResultEnum.OPERATING_SUCCESS, id);
    }

    @ApiOperation(value = "设置习题进度限制", consumes = "application/x-www-form-urlencoded")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "当前课程id", required = true, paramType = "form", dataType = "int"),
            @ApiImplicitParam(name = "learnChapterMaxNum", value = "习题进度限制", required = true, paramType = "form", dataType = "int"),
            @ApiImplicitParam(name = "learnRestrict", value = "习题进度限制状态:开启:1 关闭:0", required = true, paramType = "form", dataType = "int")
    })
    @PostMapping("/setLearnRestrict")
    public Result setLearnRestrict(Integer courseId, Integer learnChapterMaxNum, Integer learnRestrict) {
        courseService.setLearnRestrict(courseId, learnChapterMaxNum, learnRestrict);
        return ResultUtil.success(ResultEnum.OPERATING_SUCCESS);
    }

    @ApiOperation(value = "错题集")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentId", value = "学生id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "courseId", value = "课程id", required = true, dataType = "int", paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = SubjectHubVO.class)})
    @GetMapping("/getWrongExercises")
    public Result getWrongExercises(Integer studentId, Integer courseId) {
        List<SubjectHubVO> list = exercisesService.getWrongExercises(studentId, courseId);
        Map<String, List<SubjectHubVO>> data = exercisesService.classify(list);
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, data);
    }


    @ApiOperation(value = "章节-随机获取练习题")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "limit", value = "数量", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "chapterId", value = "课程id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "章节类型", required = true, paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = SubjectHubVO.class)})
    @GetMapping("/getRandomExercises")
    public Result getRandomExercises(Integer limit, Integer chapterId, String type) {
        List<SubjectHubVO> data = exercisesService.getRandomExercises(limit, chapterId, type);
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, data);
    }

    @ApiOperation(value = "视频选择防挂机题目列表")
    @ApiImplicitParam(name = "courseId", value = "课程id", required = true, dataType = "int", paramType = "query")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = SubjectHubVO.class)})
    @GetMapping("/getHangUpSubjectList")
    public Result getHangUpSubjectList(Integer courseId) {
        List<SubjectHubVO> data = exercisesService.getHangUpSubjectChooseList(courseId);
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, data);
    }

    @ApiOperation(value = "题库管理-获取章节题目")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "chapterId", value = "课程id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "章节类型", required = true, paramType = "query")
    })
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = SubjectHubVO.class)})
    @GetMapping("/getExercises")
    public Result getChapterExercises(Integer chapterId, String type) {
        List<Integer> list = exercisesService.getSubjectHubIdList(chapterId, type);
        if (CollectionUtils.isEmpty(list)) {
            throw new NoDataException();
        }
        List<SubjectHubVO> data = subjectHubService.getVOListByIdList(list);
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, data);
    }

    @ApiOperation(value = "根据试卷Id获取试卷")
    @ApiImplicitParam(name = "id", value = "试卷Id", paramType = "query", dataType = "int")
    @ApiResponses({@ApiResponse(code = 200, message = "查找成功", response = ShowTestPaperVO.class)})
    @GetMapping("/getTestPaperById")
    public Result getTestPaperById(@RequestParam Integer id) {
        TestPaper testPaper = testPaperService.get(id);
        Map<String, List<SubjectHubVO>> subjectHubVOMap = testPaperSubjectService.getMapByTestPaper(testPaper.getId());
        ShowTestPaperVO data = testPaperService.getVOList(testPaper, subjectHubVOMap);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "新增空白试卷")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程id", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "试卷名称", paramType = "query"),
            @ApiImplicitParam(name = "radioScore", value = "单选分数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "checkScore", value = "多选分数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "judgeScore", value = "判断分数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "fillScore", value = "填空分数", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "questionScore", value = "问答分数", required = true, dataType = "int", paramType = "query")
    })
    @ActionLogAop(module = "考试模块", type = "试卷导入", desc = "新增空白试卷")
    @PostMapping("/createTestPaper")
    public Result createTestPaper(Integer courseId, String name, BigDecimal radioScore, BigDecimal checkScore,
                                  BigDecimal judgeScore, BigDecimal fillScore, BigDecimal questionScore) {
        radioScore = radioScore == null ? BigDecimal.ZERO : radioScore;
        checkScore = checkScore == null ? BigDecimal.ZERO : checkScore;
        judgeScore = judgeScore == null ? BigDecimal.ZERO : judgeScore;
        fillScore = fillScore == null ? BigDecimal.ZERO : fillScore;
        questionScore = questionScore == null ? BigDecimal.ZERO : questionScore;
        Integer id = testPaperService.create(courseId, name, radioScore, checkScore, judgeScore, fillScore, questionScore);
        return ResultUtil.success(CREATE_SUCCESS, id);
    }


    @ApiOperation(value = "题库题目选项列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "key", value = "查询关键字", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "题目类型:radio/check/judge/fill/question", paramType = "query")
    })
    @ApiResponse(code = 200, message = "查找成功", response = SubjectHubVO.class)
    @GetMapping("/getSubjectHubOption")
    public Result getSubjectHubOption(Integer courseId, String key, String type) {
        List<SubjectHubVO> data = subjectHubService.getSubjectHubOption(courseId, key, type);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @ApiOperation(value = "从题库绑定练习题")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "题目id", required = true, paramType = "query"),
            @ApiImplicitParam(name = "chapterId", value = "章节id", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "type", value = "章节类型:document/radio", required = true, paramType = "query")
    })
    @PostMapping("/bindExerciseFromSubjectHub")
    public Result bindExerciseFromSubjectHub(String ids, Integer chapterId, String type) {
        exercisesService.BindExerciseFromSubjectHub(ids, chapterId, type);
        return ResultUtil.success(BIND_SUCCESS);
    }

    @ApiOperation(value = "查看课程题库的题目")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "courseId", value = "课程id", required = true, paramType = "query", dataType = "int")
    })
    @ApiResponse(code = 200, message = "查找成功", response = SubjectHubVO.class)
    @GetMapping("/getCourseSubjectHub")
    public Result getCourseSubjectHub(Integer courseId) {
        Map<String, List<SubjectHubVO>> subjectHubVOMap = courseSubjectHubService.getCourseSubjectHub(courseId);
        ShowTestPaperVO data = subjectHubService.sortSubject(subjectHubVOMap);
        return ResultUtil.success(FIND_SUCCESS, data);
    }


}

