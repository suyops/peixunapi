package com.gibs.peixunapi.config;

import com.gibs.peixunapi.service.JobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author liangjiawei
 * @date 2020/12/02/15:23
 * @Version 1.0
 * @Description:
 */
@Component
@Slf4j
public class MyApplicationRunner implements ApplicationRunner {

    private final JobService jobService;

    public MyApplicationRunner(JobService jobService) {
        this.jobService = jobService;
    }

    @Override
    public void run(ApplicationArguments args) {
        try {
            jobService.addTodayJobs();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
