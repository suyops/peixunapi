package com.gibs.peixunapi.config;

import com.google.gson.Gson;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author liangjiawei
 * @date 2021/03/22/18:14
 * @Version 1.0
 * @Description:
 */

public class MyRequestWrapper extends HttpServletRequestWrapper {


    private final byte[] body;
    private String bodyStr;

    public MyRequestWrapper(HttpServletRequest request) throws IOException {
        super(request);
        String bodyString = "";
        String header = request.getHeader("content-type");
        /**
         * 根据Servlet规范，如果同时满足下列条件，则请求体(Entity)中的表单数据，将被填充到request的parameter集合中（request.getParameter系列方法可以读取相关数据）：
         * 1 这是一个HTTP/HTTPS请求
         * 2 请求方法是POST（querystring无论是否POST都将被设置到parameter中）
         * 3 请求的类型（Content-Type头）是application/x-www-form-urlencoded
         * 4 Servlet调用了getParameter系列方法
         *
         * 调用了getInputStream导致getParameter数据流空了，这两个操作是互斥的
         */
        if (header != null && header.contains("x-www-form-urlencoded")) {
            request.getParameter("");
            Map<String, String[]> map = request.getParameterMap();

            bodyString = new Gson().toJson(map);
        } else if (header != null && header.contains("json") ) {
            bodyString = getBodyString(request);
        }else if (header != null && header.contains("multipart/form-data")){
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            Map<String, String[]> map = multipartRequest.getParameterMap();
            map.put("file", multipartRequest.getFileMap().keySet().toArray(new String[0]));
            bodyString = new Gson().toJson(map);
        }
        body = bodyString.getBytes(StandardCharsets.UTF_8);

        bodyStr = bodyString;
    }

    public String getBodyStr() {
        return bodyStr;
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(body);
        return new ServletInputStream() {
            @Override
            public int read() throws IOException {
                return byteArrayInputStream.read();
            }

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {
            }
        };
    }

    public String getBodyString(HttpServletRequest request) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStream inputStream = null;
        BufferedReader reader = null;
        try {
            inputStream = request.getInputStream();
            reader = new BufferedReader(
                    new InputStreamReader(inputStream, StandardCharsets.UTF_8));

            char[] bodyCharBuffer = new char[1024];
            int len = 0;
            while ((len = reader.read(bodyCharBuffer)) != -1) {
                sb.append(new String(bodyCharBuffer, 0, len));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
