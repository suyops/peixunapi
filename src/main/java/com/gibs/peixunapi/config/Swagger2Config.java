package com.gibs.peixunapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/08/12/11:02
 * @Version 1.0
 * @Description:
 */
@Configuration
@EnableSwagger2WebMvc
public class Swagger2Config {

    /**
     * 配置swagger2
     * 注册一个bean属性
     * swagger2其实就是重新写一个构造器，因为他没有get set方法\
     * enable() 是否启用swagger false swagger不能再浏览器中访问
     * groupName()配置api文档的分组 那就注册多个Docket实例 相当于多个分组
     * @return
     */
    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //组名称
//                .groupName("")
                .select()
                /*
                 * RequestHandlerSelectors配置扫描接口的方式
                 *   basePackage 配置要扫描的包
                 *   any 扫描全部
                 *   none 不扫描
                 *   withClassAnnotation 扫描类上的注解
                 *   withMethodAnnotation 扫描方法上的注解
                 */
                .apis(RequestHandlerSelectors.basePackage("com.gibs.peixunapi.controller"))
                /*
                 * paths() 扫描过滤方式
                 *   any过滤全部
                 *   none不过滤
                 *   regex正则过滤
                 *   ant过滤指定路径
                 */
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(security());         //主要关注点--统一填写一次token;
    }
    private List<ApiKey> security() {
        List<ApiKey> list = new ArrayList<>();
        list.add( new ApiKey("token", "authorization", "header"));
        return list;
    }
    private ApiInfo apiInfo() {
        Contact contact = new Contact("梁嘉伟", "", "xxx@qq.com");
        return new ApiInfoBuilder()
                .title("培训项目")
                .description("培训项目接口Api")
                .termsOfServiceUrl("")
                .contact(contact)
                .version("1.0")
                .build();
    }

}
