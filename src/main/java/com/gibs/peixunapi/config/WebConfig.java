package com.gibs.peixunapi.config;

import com.gibs.peixunapi.Interceptor.LoginInterceptor;
import com.gibs.peixunapi.Interceptor.ParamsInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.config.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/10/10/12:46
 * @Version 1.0
 * @Description:
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor;
    @Autowired
    private ParamsInterceptor paramsInterceptor;

    /**
     * 静态资源位置
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/public/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/upload/**").addResourceLocations("file:D:/peixunFile/");
        registry.addResourceHandler("/download/**").addResourceLocations("file:D:/peixunFile/");

        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")//设置允许跨域的路径
                .allowedOrigins("*")//设置允许跨域请求的域名
                .allowedHeaders("*")
                .allowCredentials(true)//是否允许证书 不再默认开启
                .allowedMethods("GET", "POST", "PUT", "DELETE")//设置允许的方法
                .maxAge(3600);//跨域允许时间
    }

    /**
     * url忽略大小写
     */
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        AntPathMatcher matcher = new AntPathMatcher();
        matcher.setCaseSensitive(false);
        configurer.setPathMatcher(matcher);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        List<String> excludePathLists = new ArrayList<>();
        excludePathLists.add("/error");
        excludePathLists.add("/getECharsData");
        excludePathLists.add("/getMenu");
        excludePathLists.add("/public/*");
        excludePathLists.add("/static/**");
        excludePathLists.add("/upload/**");
        excludePathLists.add("/login");
        excludePathLists.add("/loginInToken");
        excludePathLists.add("/roles/**");
        excludePathLists.add("/words/**");
        excludePathLists.add("/user/create");
        excludePathLists.add("/swagger-resources/**");
        List<String> includePathLists = new ArrayList<>();

        includePathLists.add("/user/createExamClass");
        includePathLists.add("/business/create");
        includePathLists.add("/business/save");
        includePathLists.add("/business/delete");
        includePathLists.add("/certificate/createCertificate");
        includePathLists.add("/certificate/edit");
        includePathLists.add("/certificate/issueCertificate");
        includePathLists.add("/certificate/renewalBatch");
        includePathLists.add("/certificate/pause");
        includePathLists.add("/certificate/revoke");
        includePathLists.add("/certificate/getStudentCertificate");
        includePathLists.add("/certificate/renewalBatch");
        includePathLists.add("/course/saveVideoTime");
        includePathLists.add("/course/businessSubCourse");
        includePathLists.add("/course/createBaseInfo");
        includePathLists.add("/course/createDocumentation");
        includePathLists.add("/course/createVideoClass");
        includePathLists.add("/course/auditCourse");
        includePathLists.add("/course/businessSubCourse");
        includePathLists.add("/course/signUpCourse");
        includePathLists.add("/course/signUpCourseBatch");
        includePathLists.add("/course/editVideoClass");
        includePathLists.add("/course/editCourse");
        includePathLists.add("/course/editDocumentation");
        includePathLists.add("/course/changeVideo");
        includePathLists.add("/course/getCourseInfo");
        includePathLists.add("/exam/createExamClass");
        includePathLists.add("/exam/addStudentToExam");
        includePathLists.add("/exam/signUpExamClass");
        includePathLists.add("/exam/editExamClass");
        includePathLists.add("/exam/editExamTeacher");
        includePathLists.add("/exam/checkDelayed");
        includePathLists.add("/exam/getExamInfoInCourseList");
        includePathLists.add("/exam/getExamClassList");
        includePathLists.add("/exam/getExamManageList");
        includePathLists.add("/exam/getCorrectManageList");
        includePathLists.add("/subjectHub/createSingleSubject");
        includePathLists.add("/subjectHub/createSubjectInChapter");
        includePathLists.add("/subjectHub/editSubject");
        includePathLists.add("/subjectHub/delete");
        includePathLists.add("/subjectHub/getSomeChapterExercise");
        includePathLists.add("/subjectHub/editTestPaperAndSubject");
        includePathLists.add("/subjectHub/randomTestPaper");
        includePathLists.add("/subjectHub/createTestPaper");
        includePathLists.add("/notice/deleteReadNotice");
        includePathLists.add("/fileInfo/sheetImport");
        includePathLists.add("/pushComment/*");
        includePathLists.add("/pushQuestion/*");
        includePathLists.add("/getCalendarStroke");
        includePathLists.add("/getPersonQuestion");
        includePathLists.add("/user/**");

        registry.addInterceptor(paramsInterceptor).addPathPatterns("/**");
        registry.addInterceptor(loginInterceptor)
                .excludePathPatterns(excludePathLists)
                .addPathPatterns("/**");


    }
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        ArrayList<String> commonPathPatterns = getExcludeCommonPathPatterns();
//        registry.addInterceptor(authInterceptor()).addPathPatterns("/**").excludePathPatterns(commonPathPatterns.toArray(new String[] {}));
//        super.addInterceptors(registry);
//    }
}
