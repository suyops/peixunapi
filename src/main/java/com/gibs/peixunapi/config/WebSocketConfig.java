package com.gibs.peixunapi.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import javax.websocket.EndpointConfig;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;

@Configuration
@EnableWebSocketMessageBroker
@Slf4j
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        //添加这个Endpoint，这样在网页中就可以通过websocket连接上服务了
        //设置端点，客户端通过http://localhost:8080/webSocketServer来和服务器进行websocket连接
        registry.addEndpoint("/webSocket").setAllowedOrigins("*").withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        log.info("服务器启动成功");
//        long[] hert = {1000L,1000L};
        //这句表示在topic和user这两个域上可以向客户端发消息；
        registry.enableSimpleBroker("/topic", "/user") ;
        //.setHeartbeatValue(hert)//心跳设置;
        // 这句表示客户端向服务端发送时的主题上面需要加"/app"作为前缀；
        registry.setApplicationDestinationPrefixes("/app");
        // 这句表示给指定用户发送（一对一）的主题前缀是“/user/”;
        registry.setUserDestinationPrefix("/user/");
    }


}
