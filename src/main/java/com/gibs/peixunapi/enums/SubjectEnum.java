package com.gibs.peixunapi.enums;

import java.nio.channels.FileLock;

/**
 * @author liangjiawei
 * @date 2020/09/23/17:37
 * @Version 1.0
 * @Description:
 */
public enum SubjectEnum {
    /** 题目类型 */
    RADIO("radio", "单选题"),
    CHECK("check", "多选题"),
    JUDGE("judge", "判断题"),
    FILL("fill", "填空题"),
    QUESTION("question", "问答题"),
    ;
    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    SubjectEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
