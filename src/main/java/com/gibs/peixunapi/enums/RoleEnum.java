package com.gibs.peixunapi.enums;

/**
 * @author liangjiawei
 * @date 2020/09/14/18:47
 * @Version 1.0
 * @Description:
 */
public enum RoleEnum {
    /** 学生 */
    STUDENT("student"),
    /** 授课老师 */
    TEACHER("teacher"),
    /** 集团账户 */
    GROUP_ADMIN("groupAdmin"),
    /** 企业账户 */
    BUSINESS_ADMIN("businessAdmin"),
    /** 系统管理员 */
    ADMIN("admin"),
    ;
//    private Integer sort;
    private String code;

    RoleEnum() {
    }

    RoleEnum( String code) {
//        this.sort = sort;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

//    public Integer getSort() {
//        return sort;
//    }
//
//    public void setSort(Integer sort) {
//        this.sort = sort;
//    }

    public static String getCode(String name) {
        for (RoleEnum role : RoleEnum.values()) {
            if(role.getCode().equals(name)){
                return role.getCode();
            }
        }
        return null;
    }
}
