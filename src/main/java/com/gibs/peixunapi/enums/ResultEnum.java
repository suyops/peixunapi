package com.gibs.peixunapi.enums;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/08/20/11:16
 * @Version 1.0
 * @Description:
 */
public enum ResultEnum {

    SUCCESS(200, "成功"),
    CREATE_SUCCESS(200,"新增成功"),
    DELETE_SUCCESS(200,"删除成功"),
    UPDATE_SUCCESS(200, "更新数据成功"),
    FIND_SUCCESS(200, "查找成功"),
    OPERATING_SUCCESS(200, "操作成功"),
    LOGIN_SUCCESS(200, "登录成功"),
    LOGOUT_SUCCESS(200, "登出成功"),
    CHECK_SUCCESS(200, "验证成功"),
    EMPTY_DATA(200, "无数据"),
    CERTIFICATE_VALID(200, "证书有效!"),
    UPLOAD_SUCCESS(200,"上传成功"),
    MERGE_SUCCESS(200, "合并成功"),
    SUBSCRIBE_EXIST(200, "已订阅"),
    SIGN_UP_EXIST(200, "已报名"),
    SUBSCRIBE_SUCCESS(200, "订阅成功"),
    POST_SUCCESS(200, "提交审核成功"),
    REMAIN_ANSWER_PAPER(200, "还有答卷未批改"),
    LINK_SUCCESS(200, "关联成功"),
    IMPORT_SUCCESS(200, "导入成功"),
    BIND_SUCCESS(200, "绑定成功"),
    SEND_SUCCESS(200, "发送成功"),
    CHUNK_FILE_UPLOAD_EXIST(200,"文件块已存在"),

    OPERATING_FAIL(1000,"操作失败"),
    NETWORK_ERROR(1003,"网络错误，待会重试"),
    UNKNOWN_ERROR(1002, "未知错误"),
    SERVER_ERROR(500,"服务器错误"),

    CREATE_FAIL(411,"新增失败"),
    SAVE_FAIL(412, "保存失败"),
    DELETE_FAIL(420,"删除失败"),
    UPDATE_FAIL(431, "更新数据失败"),
    GET_SUCCESS(200,"查找成功"),
    FIND_FAIL(441, "查找失败"),
    DATA_NOT_FIND(442, "找不到数据"),
    PARAM_ERROR(450, "参数错误"),

    COURSE_NOT_AUDIT(2020,"课程尚未审核"),
    VIDEO_NOT_AUDIT(2021,"视频尚未审核"),
    DOCUMENT_NOT_AUDIT(2022,"文档尚未审核"),
    LINK_FAIL(2023, "关联失败"),

    NO_USER(2000, "找不到用户"),
    USER_DISABLE(2001, "用户未激活"),
    USER_NOT_AUDIT(2001, "用户未过审"),
    PASSWORD_WRONG(2002, "密码错误"),
    USER_ALREADY_EXISTS(2003,"用户已存在"),
    TELEPHONE_ALREADY_EXISTS(2003,"该手机已注册"),

    CODE_NOT_EXIST(2004, "校验码不存在"),
    NOT_ALLOW_TO_EXAM(2005,"没有该场考试资格"),
    EXAM_STARTED(2006, "考试一开始,禁止修改信息"),
    BUSINESS_DISABLE(2007, "企业已禁用"),
    PASS_SIGN_UP_DEADLINE(2008, "超过截止日期"),

    FILE_TYPE_ERROR(2009, "文件格式出错!\"文件后缀应为jpg/png/jpeg/pdf/docx/doc/xlsx/xls/mp4\""),
    CERTIFICATE_INVALID(2010, "证书无效!"),
    CERTIFICATE_NOT_EXIST(2010, "证书不存在!"),
    REQUEST_ERROR(2011, " 请求失败，没携带加密信息"),
    MERGE_FILE_FAIL(2012,"合并失败"),
    MERGE_FILE_CREATE_FAIL(2013,"创建合并文件失败"),
    MERGE_FILE_CHECK_FAIL(2014,"合并文件校验失败"),
    CHUNK_FILE_UPLOAD_FAIL(2015,"上传文件块失败"),
    UPLOAD_FILE_ISNULL(2016,"上传文件为空！"),
    UPLOAD_FILE_EXIST(2017,"上传文件已存在"),
    UPLOAD_PATH_CREATE_FAIL(2018,"创建目录失败"),
    CHUNK_NOT_EXIST(2019,"文件块未创建"),
    IO_ERROR(2020,"IO操作异常"),
    NO_ANSWER_PAPER(2021, "所有答卷批改完毕"),
    BUSINESS_MUST_POST(2022, "必须绑定公司"),
    NO_SUBJECT_TO_RANDOM(2023, "缺少题库无法组卷!"),
    RANDOM_ERROR(2024, "随机选取题目失败"),
    SUBSCRIBE_NOT_EXIST(2025, "未订阅"),
    JOB_UPDATE_ERROR(2026, "job更新失败"),
    START_COURSE_ERROR(2027, "课程开始出错"),
    EXPIRY_ERROR(2028, "过期操作出错"),
    NO_TEACHER(2029, "找不到相关老师"),
    INPUT_STREAM_ERROR(2030, "文件流错误"),
    ADD_JOB_ERROR(2031, "定时任务添加异常"),
    PIN_ERROR(2032, "验证码错误"),
    ALL_TEST_PAPER_CORRECT(2033, "全部试卷批改完毕"),
    USER_NOT_CLEAR(2034,"删除失败,原因:该公司还有员工未删除"),
    CAN_NOT_DELETE_SELF(2035, "禁止删除本机登录账户"),
    DATE_ERROR(2036, "开始日期不能在结束日期之后"),
    NO_MANAGER(2037,"无法发送通知到企业管理!请通知系统管理员设置企业管理"),
    USER_NOT_EXIST(2088,"用户不存在")
;

    private Integer code;
    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
