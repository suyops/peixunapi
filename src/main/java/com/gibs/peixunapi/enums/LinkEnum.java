package com.gibs.peixunapi.enums;

public enum LinkEnum {
    SYSTEM_CALL(0, "系统通知"),
    CHECK_MY_CERTIFICATE(1,""),
    MANAGE_CERTIFICATE(2,""),
    COURSE_MANAGE(3, ""),
    VIDEO_MANAGE(4, ""),
    EXAM_MANAGE(5, ""),
    QUESTION_LINK(6, ""),
    SCORE_MANAGE(5, ""),
    STUDENT_COURSE_MANAGE(3, ""),

    ;
    private Integer num;
    private String link;

    LinkEnum(Integer num, String link) {
        this.num = num;
        this.link = link;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
