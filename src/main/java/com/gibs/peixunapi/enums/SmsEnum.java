package com.gibs.peixunapi.enums;

/**
 * @author liangjiawei
 * @date 2020/11/19/15:21
 * @Version 1.0
 * @Description:
 */
public enum  SmsEnum {
    LOGIN("", "广建装配式培训平台】您正在登录广建装配式培训平台,本次操作验证码为${code},10分钟内有效，如非本人操作，请注意账号安全。"),
    FORGET_PASSWORD("","【广建装配式培训平台】您正在找回密码,本次操作验证码为${code},10分钟内有效，如非本人操作，请注意账号安全。"),
    BINDING_TELEPHONE("","广建装配式培训平台】您正在登录广建装配式培训平台,本次操作验证码为${code},10分钟内有效，如非本人操作，请注意账号安全。"),
    ;
    private String code;
    private String message;

    SmsEnum() {
    }

    SmsEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
