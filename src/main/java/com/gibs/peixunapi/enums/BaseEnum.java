package com.gibs.peixunapi.enums;

/**
 * @author liangjiawei
 * @date 2020/08/21/11:48
 * @Version 1.0
 * @Description:
 */
public enum BaseEnum {
    /**
     * 图表类型
     */
    USER_BAR("userBar"),
    COURSE_LINE("courseLine"),
    ALL_BUSINESS_LINE("allBusinessLine"),
    ONE_BUSINESS_LINE("oneBusinessLine"),
    USER_PIE("userPie"),
    COURSE_PIE("coursePie"),
    /**
     * 项目地址
     */
    PATH("http://localhost"),
    //    PATH("http://m12.gibsc.com"),
    HTTP_PATH("http://localhost:9902"),
    //    HTTP_PATH("http://m12.gibsc.com:9902"),
    UPLOAD_FILE_PATH("http://localhost:9902/upload"),
//    UPLOAD_FILE_PATH("http://m12.gibsc.com:9902/upload"),

    STATIC_FILE_PATH("/public/"),
    UPLOAD_PATH("D:/peixunFile/"),
    ALL_FILE_PATH("D:/peixunFile/file"),
    TEMP_FILE_PATH("D:/peixunFile/tempFile/"),
    CERTIFICATE_FILE_PATH("D:/peixunFile/certificate"),


    ;
    private String msg;

    BaseEnum(String msg) {
        this.msg = msg;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
