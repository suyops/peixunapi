package com.gibs.peixunapi.enums;

/**
 * @author liangjiawei
 * @date 2020/09/04/15:05
 * @Version 1.0
 * @Description:
 */
public enum CourseEnum {
    ON_STUDY(1,"学习中"),
    NOT_START(-1,"未开始"),
    COMPLETE(2, "已完成"),
    NOT_COMPLETE(-2, "未完成"),
    STUDENT(0, "student"),
    TEACHER(1, "teacher"),
    ASSIST_TEACHER(0,"assistTeacher"),
    DOCUMENT(0, "document"),
    VIDEO(0, "video"),
    MANAGE(0, "manage"),
    CORRECT(0, "correct"),
    READ(0, "read"),
    EXERCISE(0, "exercise"),
    ;
    private Integer code;
    private String message;

    CourseEnum() {
    }

    CourseEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String getMessage(Integer code) {
        for (CourseEnum ele : values()) {
            if(ele.getCode().equals(code.toString())){
                return ele.getMessage();
            }
        }
        return null;
    }
}
