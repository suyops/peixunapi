package com.gibs.peixunapi.enums;

/**
 * @author liangjiawei
 * @date 2020/11/03/16:09
 * @Version 1.0
 * @Description:
 */
public enum ScheduledEnum {
    /**
     * 类名+方法名
     */
    START_COURSE("scheduledServiceImpl", "startCourse"),
    FINISH_COURSE("scheduledServiceImpl", "finishCourse"),
    START_EXAM("scheduledServiceImpl", "startExam"),
    FINISH_EXAM("scheduledServiceImpl", "completeExam"),
    START_CORRECT("scheduledServiceImpl", "startCorrect"),
    FINISH_CORRECT("scheduledServiceImpl", "finishCorrect"),
    ANNOUNCEMENT_SCORE("scheduledServiceImpl", "announcementScore"),
    CERTIFICATE_EXPIRY("scheduledServiceImpl", "certificateExpiry"),
    CERTIFICATE_UPDATE_PAUSE("scheduledServiceImpl", "certificateUpdatePause"),
    ;
    private String className;
    private String methodName;

    ScheduledEnum() {
    }

    ScheduledEnum(String className, String methodName) {
        this.className = className;
        this.methodName = methodName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
