package com.gibs.peixunapi.enums;

/**
 * @author liangjiawei
 * @date 2020/10/04/15:05
 * @Version 1.0
 * @Description:
 */
public enum ExamEnum {
    NOT_TAKE_EXAM("未参加考试"),
    NOT_FINISH_EXAM("为完成考试"),
    FINISH_EXAM("已完成考试"),
    CORRECTING("阅卷中"),
    DELAYED_EXAM("缓考"),
    ;
    private String message;

    ExamEnum() {
    }

    ExamEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
