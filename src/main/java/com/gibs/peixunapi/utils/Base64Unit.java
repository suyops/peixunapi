package com.gibs.peixunapi.utils;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;

public class Base64Unit {
	// 加密
	public static String encode(String str) {
		// 编码
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(str.getBytes()).replaceAll("\n", "").replaceAll("\r", "");// 转换成Base64形式
	}

	// 解密
	public static String decode(String str) {
		// 解码
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] array=null;
		try {
			array = decoder.decodeBuffer(str);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new String(array);
	}

}
