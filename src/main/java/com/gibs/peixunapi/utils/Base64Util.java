package com.gibs.peixunapi.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class Base64Util {
	// 加密
	public static String encode(String str) throws UnsupportedEncodingException {
		// 编码
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(str.getBytes("UTF-8")).replaceAll("\n", "").replaceAll("\r", "");
	}

	// 解密
	public static String decode(String str) throws UnsupportedEncodingException {
		// 解码
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] array=null;
		try {
			array = decoder.decodeBuffer(str);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new String(array,"UTF-8");
	}

	public static void main(String[] args) throws IOException {
		String a = "1234";
		String base64 = encode(a);
		System.out.println(base64);
		String fromBase64 = decode(base64);
		System.out.println(fromBase64);
	}
}
