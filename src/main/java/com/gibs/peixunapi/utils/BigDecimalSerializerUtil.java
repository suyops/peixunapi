package com.gibs.peixunapi.utils;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * @author liangjiawei
 * @date 2020/11/05/17:21
 * @Version 1.0
 * @Description:
 */
public class BigDecimalSerializerUtil extends JsonSerializer<BigDecimal> {
    private DecimalFormat df = new DecimalFormat("0.00");
    @Override
    public void serialize(BigDecimal aBigDecimal, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if(aBigDecimal != null) {
            jsonGenerator.writeString(df.format(aBigDecimal));
        }
    }
}
