package com.gibs.peixunapi.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import static com.gibs.peixunapi.enums.BaseEnum.UPLOAD_PATH;

@Slf4j
public class UploadUtils {
	//普通上传
	public static String uploadCustom(MultipartFile file) throws Exception {
		String filename = file.getOriginalFilename();
		String ext = Objects.requireNonNull(filename).substring(filename.lastIndexOf(".")+1);
		String saveFileName = UUIDUtil.get32UUID().toUpperCase() + "." + ext;
		String folderPath = DateUtils.format(new Date(), "yyyyMM") + "/" + DateUtils.format(new Date(), "dd");
		File folder = new File(UPLOAD_PATH.getMsg(),folderPath);
		if(!folder.exists()) folder.mkdirs();
		File to = new File(folder,saveFileName);
		try {
			file.transferTo(to);
			log.info(saveFileName);
			if ("docx,doc".contains(ext)) {
				wordToPdf( folderPath + "/" + saveFileName );
				saveFileName = saveFileName.substring(0, saveFileName.lastIndexOf(".")) + ".pdf";
			}
		} catch (IllegalStateException | IOException e) {

			log.error(e.getMessage());
			throw new Exception(e);
		}
		return "/"+folderPath + "/"  + saveFileName ;
	}
	//word转pdf
	public static void wordToPdf(String opath) {
		String absolutePath = UPLOAD_PATH.getMsg();
		String path = (absolutePath + opath).replace("/", "\\");
		String pdfPath = path.substring(0, path.lastIndexOf('.')) + ".pdf";
		File file = new File(path);
		if(!file.exists()) {
			System.out.println("本地文件不存在！");
		}
		if(!new File(pdfPath).exists()) {
			Word2Pdf.wToPdfChange(path, pdfPath);
		}
		pdfPath = pdfPath.replace("\\", "/");
		pdfPath = pdfPath.replace(absolutePath, "");
	}
	
	//上传至指定文件夹下
	public static String uploadToFolder(MultipartFile file, String folderPath){
		String filename = file.getOriginalFilename();
		File folder = new File(folderPath);
		if(!folder.exists()) folder.mkdirs();
		File to = new File(folder,filename);
		try {
			file.transferTo(to);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filename;
	}



}
