package com.gibs.peixunapi.utils;

import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liangjiawei
 * @date 2021/03/22/15:17
 * @Version 1.0
 * @Description:
 */
public class ApiHttpUtil {
    public static String sendPostAsBody(String url, String data) {
        /*数据处理*/
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> requestMap = new LinkedMultiValueMap<>();
        requestMap.add("dataStr", data);
        HttpEntity requestEntity = new HttpEntity<>(requestMap, null);
        return restTemplate.postForObject(url, requestEntity, String.class);
    }

    public static String sendPostAsFrom(String url, Map<String, String> map) {
        /*数据处理*/
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        //请勿轻易改变此提交方式，大部分的情况下，提交方式都是表单提交
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> requestMap = new LinkedMultiValueMap<>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            requestMap.add(entry.getKey(), entry.getValue());
        }

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(requestMap, headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
        return response.getBody();
    }

    public static HashMap sendGet(String url, String token, String data, String key) {
        /*数据处理*/
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("authorization", token);
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity(null, headers);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam(key, data);
        ResponseEntity<HashMap> response = restTemplate.exchange(builder.build().toString(), HttpMethod.GET, request, HashMap.class);
        return response.getBody();
    }
}
