package com.gibs.peixunapi.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtils {

	public static final String PATTERN_TIME = "yyyy-MM-dd HH:mm:ss";
	public static final String PATTERN_DATE = "yyyy-MM-dd";
	public static final String PATTERN_DATE_ZN = "yyyy年MM月dd日";
	
	public static Date format(String date) {
		return format(date, PATTERN_TIME);
	}
	
	public static Date formatDate(String date) {
		return format(date, PATTERN_DATE);
	}

	public static String toString(Date date) {
		return format(date, PATTERN_TIME);
	}
	
	public static String toDateString(Date date) {
		return format(date, PATTERN_DATE);
	}
	public static String toDateZNString(Date date) {
		return format(date, PATTERN_DATE_ZN);
	}

	public static String format(Date date, String pattern) {
		if (date == null)
			return null;
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		return df.format(date);
	}

	public static Date format(String date, String pattern) {
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		try {
			return df.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new Date();
	}

	public static Date now(String pattern) {
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat(pattern);
		try {
			return df.parse(format(date, pattern));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new Date();
	}

	// 获取相差天数
	public static int getDays(Date start, Date end) {
		int data = 0;
		try {
			long l = end.getTime() - start.getTime();
			long day = l / (24 * 60 * 60 * 1000);
			if (day != 0)
				data = (int) day;
		} catch (Exception e) {
		}
		return data;
	}

	public static String getDifferdata(Date date) {
		String data = "";
		try {
			Date now = new Date();
			long l = now.getTime() - date.getTime();
			long day = l / (24 * 60 * 60 * 1000);
			long hour = (l / (60 * 60 * 1000) - day * 24);
			long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
			long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
			if (day != 0)
				data = day + "天前";
			if (day == 0 && hour != 0)
				data = hour + "小时前";
			if (day == 0 && hour == 0 && min != 0)
				data = min + "分钟前";
			if (day == 0 && hour == 0 && min == 0)
				data = "当前";
		} catch (Exception e) {
		}
		return data;
	}

	public static String getDifferdata(Date date, Date date2) {
		String data = "";
		try {
			long l = date2.getTime() - date.getTime();
			long day = l / (24 * 60 * 60 * 1000);
			long hour = (l / (60 * 60 * 1000) - day * 24);
			long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
			long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
			if (day != 0)
				data = day + "天";
			if (hour != 0)
				data += hour + "小时";
			if (min != 0)
				data += min + "分钟";
			if (day == 0 && hour == 0 && min == 0)
				data += "不到1分钟";
		} catch (Exception e) {
		}
		return data;
	}

	public static String getDifferdata_EN(Date date, Date date2) {
		String data = "";
		try {
			long l = date2.getTime() - date.getTime();
			long day = l / (24 * 60 * 60 * 1000);
			long hour = (l / (60 * 60 * 1000) - day * 24);
			long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
			long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
			if (day != 0)
				data = day + "D ";
			if (hour != 0)
				data += hour + "H ";
			if (min != 0)
				data += min + "M";
			if (day == 0 && hour == 0 && min == 0)
				data += "<1M";
		} catch (Exception e) {
		}
		return data;
	}

	public static String getMinute(long time) {
		try {
			return time / (60 * 1000) + "分钟";
		} catch (Exception e) {
		}
		return null;
	}

	// 获取时间的前一天
	public static Date getLastDay(Date date) {
		Calendar ca = Calendar.getInstance();// 得到一个Calendar的实例
		ca.setTime(date); // 设置时间为当前时间
		ca.add(Calendar.DATE, -1); // 日期减1
		Date lastDay = ca.getTime(); // 结果
		return lastDay;
	}

	// 获取时间的下一个月
	public static Date getNextMonth(Date date) {
		Calendar ca = Calendar.getInstance();// 得到一个Calendar的实例
		ca.setTime(date); // 设置时间为当前时间
		ca.add(Calendar.MONTH, 1); // 月加1
		Date lastDay = ca.getTime(); // 结果
		return lastDay;
	}

	// 获取本月第一天
	public static String getFirstDayString(Date date) {
		String ss = format(date, "yyyy-MM") + "-01";
		return ss;
	}

	// 获取本月最后一天
	public static String getLastDayString(Date date) {
		Date date2 = format(getFirstDayString(date), "yyyy-MM-dd");
		Date resDate = getLastDay(getNextMonth(date2));
		return format(resDate, "yyyy-MM-dd");
	}

	// 获取下月第一天
	public static String getFirstDayStringNextMonth(Date date) {
		String ss = getFirstDayString(getNextMonth(date));
		return ss;
	}

	// 获取下月最后一天
	public static String getLastDayStringNextMonth(Date date) {
		Date resDate = getLastDay(getNextMonth(getNextMonth(date)));
		return format(resDate, "yyyy-MM-dd");
	}

	// 获取年月日字符串
	public static String getDayString(Date date) {
		return format(date, "yyyy-MM-dd");
	}

	// 获取日期后几天
	public static Date getAfterDate(Date date, Integer days) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+08:00"));
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, days);
		return calendar.getTime();
	}

	// 获取日期所在周的周几
	public static Date getWeekByDate(Date time, Integer i) {
		Date resDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // 设置时间格式
		Calendar cal = Calendar.getInstance();
		cal.setTime(time);
		// 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
		if (1 == dayWeek) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
		}
		cal.setFirstDayOfWeek(Calendar.MONDAY);// 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
		int day = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
		cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);// 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
		if (i != null) {
			cal.add(Calendar.DATE, i - 1);
			resDate = cal.getTime();
		}
		return resDate;

	}

	// 获取日期为周几
	public static final String getWeekZhCN(Date d1) {
		String weekDay = "";
		String[] weekDays = { "周日", "周一", "周二", "周三", "周四", "周五", "周六" };
		Calendar cal = Calendar.getInstance();
		cal.setTime(d1);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0)
			w = 0;
		weekDay = weekDays[w];
		return weekDay;
	}

	// 获取日期为所在月第几天
	public static Integer getDayOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	// isZhouRiFirst为true 获取本周第一天，按周一开始算 ;为false按周日开始 ;week 0代表本周，1代表下周，依次类推
	public static Date getFirstDayWeek(Boolean isZhouRiFirst, Date justifyDate, Integer week) {
		// 使用Calendar类进行时间的计算
		Calendar cal = Calendar.getInstance();
		cal.setTime(justifyDate);
		// 获得当前日期是一个星期的第几天
		// 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会计算到下一周去。
		// dayWeek值（1、2、3...）对应周日，周一，周二...
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
		dayWeek = isZhouRiFirst ? --dayWeek : dayWeek;
		// System.out.println("dayWeek:"+dayWeek);
		if (dayWeek == 0) {
			dayWeek = 6;
		} else {
			dayWeek -= 1;
		}
		// 计算本周开始的时间
		int startWeek;
		if (isZhouRiFirst)
			startWeek = 1;
		else
			startWeek = 0;

		cal.add(Calendar.DAY_OF_MONTH, startWeek - dayWeek + (week * 7));
		return cal.getTime();
	}

	// 获取本周开始时间
	public static List<Date> startWeek(int week, Date date) {
		List<Date> dateWeek = new ArrayList<Date>();
		// 使用Calendar类进行时间的计算
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// 获得当前日期是一个星期的第几天
		// 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会计算到下一周去。
		// dayWeek值（1、2、3...）对应周日，周一，周二...
		int dayWeek = cal.get(Calendar.DAY_OF_WEEK);
		if (dayWeek == 1) {
			dayWeek = 7;
		} else {
			dayWeek -= 1;
		}
		// 计算本周开始的时间
		cal.add(Calendar.DAY_OF_MONTH, -dayWeek + week);
		for (int i = 0; i < 7; i++) {
			dateWeek.add(cal.getTime());
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		return dateWeek;
	}

	// 获取传入时间的时分
	public static Date getSFOfDate(Date date) {
		return DateUtils.format(DateUtils.format(date, "HH:mm"), "HH:mm");
	}

	// 判断传入是否处于上午
	public static Boolean isAM(Date date) {
		Date targetDate = DateUtils.format(DateUtils.format(date, "HH:mm"), "HH:mm");
		Date baseDate = DateUtils.format("12:00", "HH:mm");
		return (targetDate.before(baseDate));
	}
	
	//获取当前日期年份
	public static Integer getCurrentYear(){
		Calendar calendar  = Calendar.getInstance();
		return Integer.valueOf(calendar.get(Calendar.YEAR));
	}
	//获取当前日期月份
	public static Integer getCurrentMonth(){
		Calendar calendar  = Calendar.getInstance();
		return Integer.valueOf(calendar.get(Calendar.MONTH)+1);
	}
	//获取传入日期年份
	public static Integer getCurrentYear(Date date){
		Calendar calendar  = Calendar.getInstance();
		calendar.setTime(date);
		return Integer.valueOf(calendar.get(Calendar.YEAR));
	}
	//获取传入日期月份
	public static Integer getCurrentMonth(Date date){
		Calendar calendar  = Calendar.getInstance();
		calendar.setTime(date);
		return Integer.valueOf(calendar.get(Calendar.MONTH)+1);
	}
	//获取传入日期所在月第一天
	public static Date getFirstDayOfMonth(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Integer year = calendar.get(Calendar.YEAR);
		Integer month = calendar.get(Calendar.MONTH) + 1;
		calendar.set(year, month - 1, 1);
		return calendar.getTime();
	}
	//获取传入年月的第一天
	public static Date getFirstDayOfMonth(Integer year, Integer month){
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month-1, 1,23,59,59);
		calendar.add(Calendar.DATE, -1);
		return calendar.getTime();
	}
	//获取传入日期所在月最后一天
	public static Date getLastDayOfMonth(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Integer year = calendar.get(Calendar.YEAR);
		Integer month = calendar.get(Calendar.MONTH) + 1;
		calendar.set(year, month,1,23,59,59);
		calendar.add(Calendar.DATE, -1);
		return calendar.getTime();
	}
	//获取传入年月的最后一天
	public static Date getLastDayOfMonth(Integer year, Integer month){
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month-1, 1);
		return getLastDayOfMonth(calendar.getTime());
	}
}
