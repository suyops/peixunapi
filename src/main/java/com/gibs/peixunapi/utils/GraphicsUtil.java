package com.gibs.peixunapi.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author liangjiawei
 * @date 2020/11/16/10:03
 * @Version 1.0
 * @Description:
 */
public class GraphicsUtil {

    public static BufferedImage MarkImageByText(File inputFile, Integer degree, String username, String content, String foot, String time, String code, String name) throws IOException {

        //获取源图像的宽度、高度
        Image image = ImageIO.read(inputFile);
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        //绘图工具对象
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.drawImage(image, 0, 0, width, height, null);

        //设置水印文字样式
        graphics2D.setFont(new Font("宋体", Font.PLAIN, 20));

        //设置水印颜色(默认黑色)
        graphics2D.setColor(new Color(1, 0, 0));

        //设置水印透明度
        graphics2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, 1.0f));

        //设置旋转
        if (null != degree) {
            /*
            graphics2D.rotate(Math.toRadians(degree),(double) bufferedImage.getWidth() / 2, (double) bufferedImage.getHeight() / 2);
            */
        }
        /*设置水印位置*/
        //设置姓名水印坐标
        int usernamex = 100;
        int usernamey = 100;
        graphics2D.drawString(username, usernamex, usernamey);
        //设证书内容水印坐标
        int contentx = 200;
        int contenty = 100;
        graphics2D.drawString(content, contentx, contenty);

        //设置落脚的坐标
        int footx = 300;
        int footy = 1000;
        graphics2D.drawString(foot, footx, footy);

        //设置时间水印的坐标
        int timex = 4000;
        int timey = 100;
        graphics2D.drawString(time, timex, timey);

        //设置证书编号水印坐标
        int codex = 0;
        int codey = 0;
        graphics2D.drawString(code, codex, codey);

        //设置证书名称水印坐标
        int namex = 0;
        int namey = 0;
        graphics2D.drawString(name, namex, namey);

        //图片输出
        graphics2D.dispose();
        return bufferedImage;
    }

    public BufferedImage markImageByMosaic(File inputFile, String imageName, String imageType, int size) throws IOException {

        BufferedImage bufferedImage = ImageIO.read(inputFile);
        int width = bufferedImage.getWidth(null);
        int height = bufferedImage.getHeight(null);
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        //马赛克格尺寸太大或太小
        if (width < size || height < size) {
            return null;
        }
        if (size <= 0) {
            return null;
        }
        //x方向绘制个数
        int xcount = 0;
        //y方向绘制个数
        int ycount = 0;

        if (width % size == 0) {
            xcount = width / size;
        } else {
            xcount = width / size + 1;
        }

        if (height % size == 0) {
            ycount = height / size;
        } else {
            ycount = height / size + 1;
        }

        //x坐标
        int x = 0;
        //y坐标
        int y = 0;

        //绘制马赛克(绘制矩形并填充颜色)
        Graphics2D g = bi.createGraphics();
        for (int i = 0; i < xcount; i++) {
            for (int j = 0; j < ycount; j++) {
                //马赛克矩形格大小
                int mwidth = size;
                int mheight = size;
                //横向最后一个不够一个size
                if (i == xcount - 1) {
                    mwidth = width - x;
                }
                //纵向最后一个不够一个size
                if (j == ycount - 1) {
                    mheight = height - y;
                }

                //矩形颜色取中心像素点RGB值
                int centerX = x;
                int centerY = y;

                if (mwidth % 2 == 0) {
                    centerX += mwidth / 2;
                } else {
                    centerX += (mwidth - 1) / 2;
                }

                if (mheight % 2 == 0) {
                    centerY += mheight / 2;
                } else {
                    centerY += (mheight - 1) / 2;
                }

                Color color = new Color(bufferedImage.getRGB(centerX, centerY));
                g.setColor(color);
                g.fillRect(x, y, mwidth, mheight);
                // 计算下一个矩形的y坐标
                y = y + size;
            }

            // 还原y坐标
            y = 0;
            // 计算x坐标
            x = x + size;
        }
        g.dispose();
        return bufferedImage;
    }

}
