package com.gibs.peixunapi.utils;

import com.gibs.peixunapi.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.gibs.peixunapi.enums.BaseEnum.ALL_FILE_PATH;
import static com.gibs.peixunapi.enums.BaseEnum.TEMP_FILE_PATH;
import static com.gibs.peixunapi.enums.ResultEnum.*;

@Slf4j
public class ChunkUploadUtil {
    /**
     * 分块文件所在全路径
     * D:/peixunUpload/tempFile/{md5}/
     */
    public static String getFilePath(String fileMd5, String fileExt) {
        return ALL_FILE_PATH.getMsg() + "/" + fileMd5 + "." + fileExt;
    }

    public static String getFileRelativePath(String fileMd5, String fileExt) {
        return "/" + "file" + "/" + fileMd5 + "." + fileExt;
    }

    /**
     * 临时文件夹中Md5文件夹相对路径
     * D:/peixunUpload/tempFile/{md5}/
     *
     * @param fileMd5
     * @return
     */
    public static String getChunkFolderPath(String fileMd5) {
        return TEMP_FILE_PATH.getMsg() + fileMd5 + "/";
    }

    //创建临时分块文件夹目录
    public static boolean createFileFold(String fileMd5) {
        //创建上传文件，目录
        String fileFolderPath = ChunkUploadUtil.getChunkFolderPath(fileMd5);
        File fileFolder = new File(fileFolderPath);
        if (!fileFolder.exists()) {
            //创建文件夹
            return fileFolder.mkdirs();
        }
        return true;
    }

    private static void createChunkFileFolder(String fileMd5) {
        //创建上传文件目录
        String chunkFileFolderPath = getChunkFolderPath(fileMd5);
        File chunkFileFolder = new File(chunkFileFolderPath);
        if (!chunkFileFolder.exists()) {
            //创建文件夹
            chunkFileFolder.mkdirs();
        }

    }


    public static String uploadChunk(MultipartFile file, String fileMd5, Integer chunk) {
        //块文件
        File chunkfile = new File(getChunkFolderPath(fileMd5) + chunk);
        //上传的块文件

        createChunkFileFolder(fileMd5);

        InputStream inputStream = null;
        FileOutputStream outputStream = null;
        try {
            inputStream = file.getInputStream();
            outputStream = new FileOutputStream(chunkfile);
            IOUtils.copy(inputStream, outputStream);
            return getChunkFolderPath(fileMd5) + chunk;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("upload chunk file fail:{}", e.getMessage());
            throw new BaseException(CHUNK_FILE_UPLOAD_FAIL);
        } finally {
            try {
                inputStream.close();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //获取所有块文件
    public static File[] getChunkFiles(File chunkFileFolder) {
        //获取路径下的所有块文件
        File[] chunkFiles = chunkFileFolder.listFiles();
        if (chunkFiles==null||chunkFiles.length == 0) {
            return null;
        }
        //将文件数组转成list，并排序
        List<File> chunkFileList = new ArrayList<>();
        Collections.addAll(chunkFileList, chunkFiles);
        //排序
        chunkFileList.sort((o1, o2) -> {
            if (Integer.parseInt(o1.getName()) > Integer.parseInt(o2.getName())) {
                return 1;
            }
            return -1;
        });

        return chunkFileList.toArray(new File[chunkFileList.size()]);
    }

    //合并文件
    public static File mergeFile(String fileMd5, String fileExt, File[] chunkFiles) {
        try {
            //合并文件路径
            File folder = new File(ALL_FILE_PATH.getMsg());
            if (!folder.exists()) {
                folder.mkdirs();
            }
            //创建合并文件
            File mergeFile = new File(ChunkUploadUtil.getFilePath(fileMd5, fileExt));
            //合并文件存在先删除再创建
            if (mergeFile.exists()) {
                mergeFile.delete();
            }
            if (!mergeFile.createNewFile()) {
                throw new BaseException(MERGE_FILE_CREATE_FAIL);
            }

            //创建写文件对象
            FileChannel outChannel = new FileOutputStream(mergeFile, true).getChannel();

            FileChannel inChannel;
            //遍历分块文件开始合并
            for (File chunkFile : chunkFiles) {
                //读取文件缓冲区
                inChannel = new FileInputStream(chunkFile).getChannel();

                // 从inChannel中读取file.length()长度的数据，写入outChannel的start处
                outChannel.transferFrom(inChannel, outChannel.size(), inChannel.size());
                inChannel.close();
            }
            outChannel.close();
            return mergeFile;
        } catch (FileNotFoundException e) {
            log.error("merge file error:{}", e.getMessage());
            throw new BaseException(MERGE_FILE_FAIL);
        } catch (IOException e) {
            log.error("IO error:{}", e.getMessage());
            throw new BaseException(IO_ERROR);
        } finally {
            File folder = new File(getChunkFolderPath(fileMd5).substring(0, getChunkFolderPath(fileMd5).length() - 1));
            try {
                FileUtils.deleteDirectory(folder);
            } catch (IOException e) {
                log.error("delete error:{}", e.getMessage());
                throw new BaseException(DELETE_FAIL);
            }
        }

    }


}
