package com.gibs.peixunapi.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.DigestUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

@Slf4j
public class SecretUtils {

    public static void HttpRequest(String encoding, String requestURL) {
        URL url;
        HttpURLConnection connection;
        BufferedReader in = null;
        try {
            // ef5a5cf2-a105-42bb-9189-e15cec143c94
            // 05df2d3933314b4343154332
            url = new URL(requestURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "UTF-8");
            connection.setConnectTimeout(3000);//设置连接主机超时（单位：毫秒）
            connection.setReadTimeout(3000);//设置从主机读取数据超时（单位：毫秒）
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            connection.connect();

            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    //2019-8-26新增
    public static String authorization_basic(String key, String secret, Long aging) {
        String token = DigestUtils.md5DigestAsHex(secret.getBytes()) + "&" + System.currentTimeMillis();
        String encoding = Base64Unit.encode(key + ":" + token);
        return encoding;
    }

    //2019-8-28新增
    public static String setEncoding(String key, String secret, String asyncID) {
        String token = DigestUtils.md5DigestAsHex(secret.getBytes()) + "&" + System.currentTimeMillis();
        String encoding = Base64Unit.encode(key + ":" + token + ":" + asyncID);
        return encoding;
    }

    public static String setEncoding(String key, String secret, String... str) {
        String token = DigestUtils.md5DigestAsHex(secret.getBytes()) + "&" + System.currentTimeMillis();
        StringBuilder stringBuilder = new StringBuilder(key + ":" + token + ":");
        for (String s : str) {
            stringBuilder.append(s).append(":");
        }
        String encoding = Base64Unit.encode(stringBuilder.toString());
        return encoding;
    }

    public static String isEncoding(String authorization) {
        String apiKey = "e70769380ff7446fa0deb73c485422c5";
        String apiSecret = "6ada682286244b12b7d6cd56cbe07a11";

        if (authorization == null || authorization.equals("")) {
            log.error("对不起你没有权限！！");
            return "401";
        }
        String encoding = new String(Base64Unit.decode(authorization));
        String key = encoding.split(":")[0];
        String token = encoding.split(":")[1];
        String asyncID = encoding.split(":")[2];
        String secret = token.split("&")[0];
        if (apiKey.equals(key) && DigestUtils.md5DigestAsHex(apiSecret.getBytes()).equals(secret))
            return asyncID;
        return "401";
    }
    public static String isEncoding2(String authorization) {
        String apiKey = "e70769380ff7446fa0deb73c485422c5";
        String apiSecret = "6ada682286244b12b7d6cd56cbe07a11";
        if (authorization == null || authorization.equals("")) {
            System.out.println("对不起你没有权限！！");
            return "401";
        }
        String encoding = "";
        try {
            encoding = Base64Util.decode(authorization);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String key = encoding.split(":")[0];
        String token = encoding.split(":")[1];
        String userInfo = encoding.split(":")[2];
        String secret = token.split("&")[0];
        if (apiKey.equals(key) && org.springframework.util.DigestUtils.md5DigestAsHex(apiSecret.getBytes()).equals(secret))
            return userInfo;
        return "401";
    }
	public static String encodingRole(String authorization) {
		String apiKey = "e70769380ff7446fa0deb73c485422c5";
		String apiSecret = "6ada682286244b12b7d6cd56cbe07a11";

		if (authorization == null || authorization.equals("")) {
			log.error("对不起你没有权限！！");
			return "401";
		}
		String encoding = new String(Base64Unit.decode(authorization));
		String key = encoding.split(":")[0];
		String token = encoding.split(":")[1];
		String role = encoding.split(":")[3];
		String secret = token.split("&")[0];
		if (apiKey.equals(key) && DigestUtils.md5DigestAsHex(apiSecret.getBytes()).equals(secret))
			return role;
		return "401";
	}

    public static void main(String[] args) {
        String requestURL = "http://data.whunime.com/api/Data/GetPointDataByModule";
        String encoding = authorization_basic();
        HttpRequest(encoding, requestURL);
        System.out.println(encoding);
        System.out.println(authorization_basic(encoding));
    }

    public static String authorization_basic() {
        String key = "e70769380ff7446fa0deb73c485422c5";
        String secret = "6ada682286244b12b7d6cd56cbe07a11";
        String token = DigestUtils.md5DigestAsHex(secret.getBytes()) + "&" + System.currentTimeMillis();
        String encoding = Base64Unit.encode(key + ":" + token);
        return encoding;
    }

    public static int authorization_basic(String authorization) {
        // request.headers.get("authorization");
        String apiKey = "e70769380ff7446fa0deb73c485422c5";
        String apiSecret = "6ada682286244b12b7d6cd56cbe07a11";
        if (authorization == null || authorization.equals("")) {
            log.error("对不起你没有权限！！");
            return 401;
        }
        String encoding = new String(Base64Unit.decode(authorization));
        String key = encoding.split(":")[0];
        String token = encoding.split(":")[1];
        String secret = token.split("&")[0];
        if (apiKey.equals(key) && DigestUtils.md5DigestAsHex(apiSecret.getBytes()).equals(secret))
            return 200;
        return 401;
    }
}
