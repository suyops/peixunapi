package com.gibs.peixunapi.utils;

import com.baomidou.mybatisplus.extension.api.R;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.PageResult;
import com.gibs.peixunapi.result.Result;
import org.apache.poi.ss.formula.functions.T;

import java.util.ArrayList;

/**
 * @author liangjiawei
 * @date 2020/08/20/11:26
 * @Version 1.0
 * @Description:
 */
public class ResultUtil {



    public static <T> Result<T> success(ResultEnum codeMsg) {
        return new Result(codeMsg.getCode(),codeMsg.getMsg(),new ArrayList<>());
    }

    public static <T> Result<T> success(ResultEnum codeMsg, T data) {
        return new Result(codeMsg.getCode(),codeMsg.getMsg(),data);
    }

    public static <T> Result<PageData<T>> successPage(ResultEnum codeMsg) {
        return new Result(codeMsg.getCode(),codeMsg.getMsg());
    }
    public static <T> Result<PageData<T>> successPage(ResultEnum codeMsg, PageData<T> data) {
        return new Result<>(codeMsg.getCode(),codeMsg.getMsg(),data);
    }

    public static <T> Result<T> fail(ResultEnum failCodeMsg) {
        return new Result(failCodeMsg.getCode(), failCodeMsg.getMsg());
    }
    public static <T> Result<T> fail(ResultEnum failCodeMsg, Object data) {
        return new Result(failCodeMsg.getCode(), failCodeMsg.getMsg(),data);
    }

    public static <T> Result<T> error(Integer code, String msg) {
        return new Result(code, msg);
    }
    public static <T> Result<T> error(ResultEnum errorCodeMsg) {
        return new Result(errorCodeMsg.getCode(), errorCodeMsg.getMsg());
    }
    public static <T> Result<T> error(ResultEnum errorCodeMsg, Object data) {
        return new Result(errorCodeMsg.getCode(), errorCodeMsg.getMsg(),data);
    }

}
