package com.gibs.peixunapi.utils;

/**
 * @author liangjiawei
 * @date 2021/07/17/0:42
 * @Version 1.0
 * @Description:
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import net.sf.json.JSONObject;

public class SecretUtil2{
    static String apiKey = "e70769380ff7446fa0deb73c485422c5";
    static String apiSecret = "6ada682286244b12b7d6cd56cbe07a11";

    public static void HttpRequest(String encoding, String requestURL) {
        URL url;
        URLConnection connection = null;
        BufferedReader in = null;
        String result = "";
        try {
            // ef5a5cf2-a105-42bb-9189-e15cec143c94
            // 05df2d3933314b4343154332
            url = new URL(requestURL);
            connection = (HttpURLConnection) url.openConnection();
            ((HttpURLConnection) connection).setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "UTF-8");
            connection.setConnectTimeout(3000);//设置连接主机超时（单位：毫秒）
            connection.setReadTimeout(3000);//设置从主机读取数据超时（单位：毫秒）
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            connection.connect();

            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

//	//2020-12-18修改
//	public static String setEncoding(String content) {
//		try {
//			return AesUtil.encrypt(content, apiSecret);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return "";
//		}
//	}
//
//	public static String isEncoding(String authorization) {
////		String encoding = new String(Base64.decode(authorization));
////		String key = encoding.split(":")[0];
////		String token = encoding.split(":")[1];
////		String asyncID = encoding.split(":")[2];
////		String secret = token.split("&")[0];
////		if (apiKey.equals(key) && Codec.hexMD5(apiSecret).equals(secret))
////			return asyncID;
////		return "401";
//		if (authorization == null || authorization.equals("")) {
//			System.out.println("对不起你没有权限！！");
//			return "401";
//		}
//		try {
//			return AesUtil.decrypt(authorization, apiSecret);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return "401";
//		}
//	}


    //2020-12-18修改
    public static String setEncoding(HashMap<String, String> map) {
        try {
//			StringBuilder content = new StringBuilder("{");
//			content.append("\"time\":\""+new Date().getTime()+"\"");
//			for (Iterator iterator = map.keySet().iterator(); iterator.hasNext();) {
//				String key = (String) iterator.next();
//				content.append(",\""+key+"\":\""+map.get(key)+"\"");
//			}
//			content.append("}");
            String content = JSONObject.fromObject(map).toString();
            return AesUtil.encrypt(content, apiSecret);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    public static HashMap<String, String> getEncoding(String token){
        HashMap<String, String> map = new HashMap<>();
        try {
            String json = AesUtil.decrypt(token, apiSecret);
            map = (HashMap<String, String>)JSONObject.toBean(JSONObject.fromObject(json),HashMap.class);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        return map;
    }
}

