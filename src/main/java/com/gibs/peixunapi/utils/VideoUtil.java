package com.gibs.peixunapi.utils;


import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderException;
import it.sauronsoftware.jave.MultimediaInfo;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;

import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import static com.gibs.peixunapi.enums.BaseEnum.UPLOAD_PATH;

/**
 * @author liangjiawei
 * @date 2020/10/12/18:58
 * @Version 1.0
 * @Description:
 */
@Slf4j
public class VideoUtil {

    /**
     * 获取视频时长，单位为秒
     *
     * @param
     * @param video
     * @return 时长（s）
     */
    public static String getVideoTime(File video) {

        // 获取视频时长
        try{
        Encoder encoder = new Encoder();
        MultimediaInfo m = encoder.getInfo(video);
        Long ls = m.getDuration()/1000;
        int hour = (int) (ls/3600);
        int minute = (int) (ls%3600)/60;
        int second = (int) (ls-hour*3600-minute*60);
        //logger.info("视频时长为：{}时{}分{}秒", hour, minute, second);
        return ls.toString();
        } catch (EncoderException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 截取视频获得指定帧的图片
     *
     * @param contentFile   源视频文件
     * @return
     */
    public static String getVideoPic(File contentFile)  {
        try {


        FFmpegFrameGrabber ff = new FFmpegFrameGrabber(contentFile);
        log.info("ffstart");
            ff.start();
            log.info("end");
            // 截取中间帧图片(具体依实际情况而定)
            int i = 0;
            int length = ff.getLengthInFrames();
            int middleFrame = length / 2;
            Frame frame = null;
            while (i < length) {
                frame = ff.grabFrame();
                if ((i > middleFrame) && (frame.image != null)) {
                    break;
                }
                i++;
            }

            // 截取的帧图片
            Java2DFrameConverter converter = new Java2DFrameConverter();
            BufferedImage srcImage = converter.getBufferedImage(frame);
            int srcImageWidth = srcImage.getWidth();
            int srcImageHeight = srcImage.getHeight();

            // 对截图进行等比例缩放(缩略图)
            int width = 480;
            int height = (int) (((double) width / srcImageWidth) * srcImageHeight);
            BufferedImage thumbnailImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
            thumbnailImage.getGraphics().drawImage(srcImage.getScaledInstance(width, height, Image.SCALE_SMOOTH), 0, 0, null);

            String ext = "jpg";
            String saveFileName = UUIDUtil.get24UUID() + "." + ext;
            String folderPath = DateUtils.format(new Date(), "yyyyMM") + "/" + DateUtils.format(new Date(), "dd");
            File folder = new File(UPLOAD_PATH.getMsg(),folderPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            File picFile = new File(folder,saveFileName);
            ImageIO.write(thumbnailImage, "jpg", picFile);
            ff.stop();

            return "/"+folderPath + "/"  + saveFileName;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
