package com.gibs.peixunapi.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author liangjiawei
 * @date 2020/10/23/9:18
 * @Version 1.0
 * @Description:
 */
@Component
public class SpringBeanUtils implements ApplicationContextAware {
    private static ApplicationContext context;
    @Override
    public  void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringBeanUtils.context = applicationContext;
    }

//通过name获取 Bean.

    public static Object getBean(String name) {
        return context.getBean(name);
    }

//通过class获取Bean.

    public static <T> T getBean(Class<T> clazz) {
        return context.getBean(clazz);
    }

//通过name,以及Clazz返回指定的Bean

    public static <T> T getBean(String name, Class<T> clazz) {
        return context.getBean(name, clazz);
    }


}

