package com.gibs.peixunapi.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author liangjiawei
 * @date 2020/08/20/17:26
 * @Version 1.0
 * @Description: 分页Result
 */
@Data
public class PageResult<T>  extends Result {
    //返回码
    private Integer code;
    //返回描述
    private String msg;
    //返回分页数据
    private PageData pageData;

    public PageResult(){
        this.code = 0;
        this.msg = "请求成功";
    }

    public PageResult(Integer code,String msg){
        this();
        this.code = code;
        this.msg = msg;
    }
    public PageResult(Integer code,String msg,PageData pageData){
        this();
        this.code = code;
        this.msg = msg;
        this.pageData = pageData;
    }
    public PageResult(PageData pageData){
        this();
        this.pageData = pageData;
    }
}
