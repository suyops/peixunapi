package com.gibs.peixunapi.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author liangjiawei
 * @date 2020/08/20/11:09
 * @Version 1.0
 * @Description: 统一返回结果
 */
@Data
public class Result<T> implements Serializable {

    public static final String SUCCESS = "success";
    //返回码
    private Integer code;
    //返回描述
    private String msg;
    //返回数据
    private T data;


    public Result(){
        this.code = 0;
        this.msg = "请求成功";
    }

    public Result(Integer code,String msg){
        this();
        this.code = code;
        this.msg = msg;
    }
    public Result(Integer code,String msg,T data){
        this();
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public Result(T data){
        this();
        this.data = data;
    }

}
