package com.gibs.peixunapi.result;

import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/08/20/17:22
 * @Version 1.0
 * @Description:
 */
@Data
public class PageData<T> {

    private Integer total;
    private Integer page;
    private Integer limit;
    private List<T>  data;

    public PageData() {

    }
    public PageData(Integer total, Integer limit, Integer page, List<T> data) {
        this.total = total;
        this.limit = limit;
        this.page = page;
        this.data = data;
    }
    public PageData(Integer total, Integer limit, Integer page) {
        this.total = total;
        this.limit = limit;
        this.page = page;
        this.data = null;
    }




}
