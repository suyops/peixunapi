package com.gibs.peixunapi.filter;


import com.gibs.peixunapi.config.MyRequestWrapper;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author liangjiawei
 * @date 2021/03/30/9:34
 * @Version 1.0
 * @Description:
 */
@WebFilter(filterName = "httpServletRequestWrapperFilter", urlPatterns = {"/*"})
public class RequestFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        ServletRequest requestWrapper = null;

        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            //遇到post方法才对request进行包装
            String methodType = httpRequest.getMethod();
            if ("POST".equals(methodType) && !request.getContentType().contains("multipart/form-data")) {
                requestWrapper = new MyRequestWrapper((HttpServletRequest) request);
            } else if ("POST".equals(methodType) && request.getContentType().contains("multipart/form-data")) {
                MultipartResolver resolver = new CommonsMultipartResolver(httpRequest.getSession().getServletContext());
                MultipartHttpServletRequest multipartRequest = resolver.resolveMultipart(httpRequest);
                requestWrapper =  new MyRequestWrapper(multipartRequest);
                // 将转化后的 request 放入过滤链中
                request = multipartRequest;
            }
        }
        if (null == requestWrapper) {
            chain.doFilter(request, response);
        } else {
            chain.doFilter(requestWrapper, response);
        }
    }

    @Override
    public void destroy() {

    }


}
