package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.VO.show.ShowTestPaperListVO;
import com.gibs.peixunapi.model.TestPaper;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Mapper 接口
 *
 * @author
 * @since
 */
@Component
@Mapper
public interface TestPaperMapper {

    @Select(value = " <script>" +
            "select a.id as id, a.name as name from test_paper a " +
            "inner join course_test_paper as b on a.id = b.test_paper_id " +
            "where b.course_id = #{courseId}" +
            "<if test='key!=null and key!=\"\" ' > and a.name like '%${key}%' </if>" +
            "</script>")
    List<OptionVO> getOption(@Param("courseId")Integer courseId, @Param("key") String key);

    @Select("SELECT " +
            "test_paper.*  " +
            "FROM " +
            "exam_student " +
            "INNER JOIN exam_class_info ON exam_student.exam_class_info_id = exam_class_info.id " +
            "INNER JOIN exam_info ON exam_class_info.exam_info_id = exam_info.id  " +
            "INNER JOIN test_paper on test_paper.id = exam_info.test_paper_id " +
            "AND test_paper.id = exam_info.test_paper_id  " +
            "WHERE " +
            "exam_student.unique_code = #{uniqueCode}")
    TestPaper findTestPaperIdByUniqueCode(@Param("uniqueCode") String uniqueCode);


    @Select(" <script>" +
    		"SELECT " +
            "tp.id AS id, " +
            "tp.`NAME` AS `NAME`, " +
            "tp.create_time AS createTime, " +
            "( tp.radio_count + tp.check_count + tp.judge_count + tp.fill_count + tp.question_count ) AS subjectHubCount, " +
            "tp.total_score AS totalScore, " +
            "u.username AS creatorName  " +
            "FROM " +
            "test_paper AS tp " +
            "INNER JOIN course_test_paper AS ctp ON ctp.test_paper_id = tp.id " +
            "INNER JOIN course AS c ON ctp.course_id = c.id " +
            "INNER JOIN `user` AS u ON tp.creator_id_id = u.id " +
            "WHERE " +
            "c.id = #{courseId} " +
            "<if test='name!=null and name!=\"\" '> where tp.name like '%${name}%' </if>" +
            "</script>")
    List<ShowTestPaperListVO> getTestPaperList(@Param("courseId")Integer courseId, @Param("name")String key);
}
