package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.model.ActionLog;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 *  Mapper 接口
 *
 * @author 
 * @since 
 */
@Component
@Mapper
public interface ActionLogMapper {

    @Select("<script>" +
            "select * " +
            "from action_log " +
            "where 1=1 " +
            "<if test='content!=null and content!=\"\"'>and content like '%${content}%' </if> " +
            "<if test='dateFrom!=null and dateFrom!=\"\" and dateTo!=null and dateTo!=\"\"'>and create_time between #{dateFrom} and #{dateTo} </if> " +
            "<if test='dateFrom!=null and dateFrom!=\"\" and dateTo==null'><![CDATA[and create_time >= #{dateFrom} ]]></if>" +
            "<if test='dateTo!=null and dateTo!=\"\" and dateFrom==null'><![CDATA[and create_time <= #{dateTo} ]]></if>" +
            "</script>")
    List<ActionLog> getActionLog(@Param("content") String content, @Param("dateFrom") String dateFrom, @Param("dateTo") String dateTo);
}
