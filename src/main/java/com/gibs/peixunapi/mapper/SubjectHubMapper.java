package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.VO.SubjectHubVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Mapper 接口
 *
 * @author
 * @since
 */
@Component
@Mapper
public interface SubjectHubMapper {

    @Select("<script>" +
            "select " +
            "csh.id as id, " +
            "a.type as type," +
            "a.content as content, " +
            "a.answer as answer " +
            "from subject_hub as a " +
            "inner join course_subject_hub as csh on a.id = csh.subject_hub_id " +
            "where csh.course_id = #{course} " +
            "<if test='key!=null and key!=\"\" ' > and a.key like ${key} </if>" +
            "<if test='type!=null and type!=\"\" ' > and a.type = #{type} </if>" +
            "and a.status = 1 order by a.create_time" +
            "</script>")
    List<SubjectHubVO> findSubjectHubOption(@Param("course") Integer courseId, @Param("key")String key, @Param("type")String type);
}
