package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.VO.show.CourseSubjectHubVO;
import com.gibs.peixunapi.model.Exercises;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Mapper 接口
 *
 * @author
 * @since
 */
@Component
@Mapper
public interface ExercisesMapper {
    @Select("<script>" +
            " select b.id as id, b.subject_hub_id as subjectHubId, b.course_id as courseId, b.sort as sort " +
            " from exercises a inner join course_subject_hub b on a.course_subject_hub_id = b.id " +
            " where 1=1 " +

            "<choose>" +
            "<when test=\"type=='document'.toString()\"> " +
            " and a.documentation = #{id} " +
            "</when> " +
            "<when test=\"type=='video'.toString()\"> " +
            " and a.video_class = #{id} " +
            " and a.type != 1 " +
            "</when> " +
            "</choose> " +
            " order by b.create_time" +
            "</script>")
    List<CourseSubjectHubVO> getCourseSubjectHubList(@Param("id") Integer chapterId, @Param("type") String type);


    @Select("<script>" +
            " select a.* from\n" +
            "        exercises a inner join course_subject_hub b on a.course_subject_hub_id = b.id\n" +
            "        where 1=1\n" +
            "        <choose> \n" +
            "            <when test=\"type=='document'.toString()\">\n" +
            "                and a.documentation = #{id}\n" +
            "            </when>\n" +
            "            <when test=\"type=='video'.toString()\">\n" +
            "                and a.video_class = #{id}\n" +
            "            </when>\n" +
            "        </choose>\n" +
            "        order by b.create_time" +
            "</script>")
    List<Exercises> getExerciseList(@Param("id") Integer chapterId, @Param("type") String type);

    @Select("<script>" +
            "SELECT COUNT( exercises.id ) " +
            "FROM exercises " +
            "INNER JOIN course_subject_hub AS a on a.id = exercises.course_subject_hub_id " +
            "INNER JOIN subject_hub AS b on a.subject_hub_id = b.id " +
            "WHERE " +
            "<choose> " +
            "   <when test='type==\"document\".toString()'> " +
            "      exercises.documentation = #{chapterId} " +
            "   </when> " +
            "   <when test='type==\"video\".toString()'> " +
            "      exercises.video_class = #{chapterId} " +
            "   </when> " +
            "</choose> " +
            " and ( b.type = 'radio' or b.type = 'check' or b.type = 'judge' ) " +
            "</script>")
    Integer countExercisesInChapter(@Param("chapterId") Integer id, @Param("type") String courseEnum);

    @Select("SELECT " +
            "COUNT( a.id ) " +
            "FROM " +
            "( " +
            "SELECT " +
            "exercises.id " +
            "FROM " +
            "exercise_record " +
            "INNER JOIN exercises ON exercise_record.exercise_id = exercises.id " +
            "INNER JOIN video_class ON ( exercises.video_class = video_class.id AND video_class.course_id = #{course} ) " +
            "UNION ALL " +
            "SELECT " +
            "exercises.id " +
            "FROM " +
            "exercise_record " +
            "INNER JOIN exercises ON exercise_record.exercise_id = exercises.id " +
            "INNER JOIN documentation ON ( exercises.documentation = documentation.id AND documentation.course_id = #{course} ) " +
            ") a")
    Integer countByCourse(@Param("course") Integer courseId);
}
