package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.VO.show.BusinessListVO;
import com.gibs.peixunapi.VO.show.OptionVO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 *  Mapper 接口
 *
 * @author 
 * @since 
 */
@Component
@Mapper
public interface BusinessMapper  {

    @Select("<script>" +
            "select a.id as id, a.name name, a.invited_code as invitedCode, a.status status " +
            "from business as a " +
            "where 1 = 1 " +
            "<if test='name!=null and name!=\"\"' > " +
            "and a.name like '%${name}%' </if> " +
            "<if test='status!=null' > " +
            "and a.status = #{status} </if>" +
            " order by a.create_time desc " +
            "</script> ")
    List<BusinessListVO> getIdListByLikeNameAndStatus(@Param("name") String name,@Param("status") Integer status);

    @Select("select short_name as name " +
            "from business  " +
            "where 1 = 1 " +
            "order by sort ")
    List<String> findBusiness();

    @Select("select id as id, name as name,sort as sort, is_belong_syndicate as value " +
            "from business  " +
            "where status = 1 " +
            "order by sort ")
    List<OptionVO> getOptions();
}
