package com.gibs.peixunapi.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Mapper 接口
 *
 * @author
 * @since
 */
@Component
@Mapper
public interface UserRoleMapper {

    @Select("<script> " +
            "SELECT a.id " +
            "FROM " +
            "business AS d " +
            "INNER JOIN `user` AS a ON d.id = a.business_id " +
            "INNER JOIN user_role AS b ON a.id = b.user_id " +
            "INNER JOIN role AS c ON b.role_id = c.id " +
            "where d.id = #{businessId}  and a.username like '%${name}%' and a.status = 1 " +
            " <if test='audit!=null '>  and a.audit = #{audit} </if>" +
            " </script>")
    List<Integer> findByBusinessIdAndNameLike(@Param("name") String name, @Param("audit") Integer audit, @Param("businessId") Integer businessId);

    @Select(" <script>" +
            " SELECT " +
            " a.id " +
            " FROM " +
            " `user` AS a " +
            " INNER JOIN user_and_role AS b ON a.id = b.user_id " +
            " INNER JOIN role AS c ON b.role_id = c.id " +
            " WHERE 1 = 1" +
            " <if test='username !=null and username !=\"\"'> " +
            " and a.username = #{username} </if>" +
            " <if test='status !=null '> " +
            " and a.status = #{status} </if>" +
            " <if test='code !=null and code !=\"\"'> " +
            " and c.`code` in " +
            " <foreach collection=\"codeList\" item=\"codeList\" index=\"index\" open=\"(\" close=\")\" separator=\",\">\n" +
            " #{codeList} " +
            " </foreach>" +
            " </if>" +
            " </script>")
    List<Integer> findInCodeAndUsername(@Param("codeList") List<String> codeList, @Param("username") String username, @Param("status") Integer status);

    @Select("<script> " +
            "SELECT " +
            " a.id " +
            " FROM " +
            " `user` AS a " +
            " <if test='businessId!=null and businessId!=\"\"'> " +
            "INNER JOIN business AS d ON a.business_id = d.id </if> " +
            " <if test='code!=null and code!=\"\"'> " +
            " INNER JOIN user_role AS b ON a.id = b.user_id " +
            " INNER JOIN role AS c ON b.role_id = c.id </if> " +
            " WHERE 1 = 1 and a.username!='admin' " +
            " <if test='status==null '> and a.status = 1 </if> " +
            " <if test='code!=null and code!=\"\"'>  and c.code = #{code} </if> " +
            " <if test='username!=null and username!=\"\"'>  and a.username like '%${username}%' </if> " +
            " <if test='businessId!=null and businessId!=\"\"'>  and d.id = #{businessId} </if>" +
            " <if test='status!=null '>  and a.status = #{status} </if>" +
            " <if test='audit!=null '>  and a.audit = #{audit} </if>" +
            " order by a.audit " +
            " </script>")
    List<Integer> findUserIdsFour(@Param("code") String code, @Param("username") String name, @Param("businessId") Integer businessId, @Param("status") Integer status, @Param("audit") Integer audit);
}
