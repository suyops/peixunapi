package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.model.Scheduled;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/10/28/9:22
 * @Version 1.0
 * @Description:
 */
@Component
@Mapper
public interface ScheduledMapper {
    @Select("SELECT * FROM scheduled where TO_DAYS(date)-TO_DAYS(NOW()) = 0")
    List<Scheduled> getCronList();

    @Select("update scheduled set status = 0 where id = #{id}")
    List<Scheduled> save(@Param("id") Integer id);

    @Select("SELECT * FROM scheduled where id = #{id} ")
    Scheduled getCron(@Param("id")Integer id);

@Select("DELETE FROM scheduled WHERE id = #{id}  ")
    void delete(@Param("id")Integer id);
}
