package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.VO.show.PieVO;
import com.gibs.peixunapi.VO.show.CourseManageVO;
import com.gibs.peixunapi.VO.show.SubjectHubManageVO;
import com.gibs.peixunapi.model.Course;
import org.apache.ibatis.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * Mapper 接口
 *
 * @author
 * @since
 */
@Component
@Mapper
public interface CourseMapper {

    Course getById(@Param(value = "courseId") Integer courseId);

    @Select(value = " <script>" +
            " select a.id as id, a.name as name from course a " +
            " where a.end_time > #{time} " +
            "<if test='key!=null and key!=\"\" ' > and a.name like '%${key}%' </if>" +
            "</script>")
    List<OptionVO> getOptions(@Param("key") String key, @Param("time") String time);

    @Select("SELECT count(*) AS value, name AS name " +
            "FROM course GROUP BY type ")
    List<PieVO> findCoursePieData();

    @Select(" <script>" +
            "select distinct a.* " +
            "from course a " +
            "<if test=\" userId !=null \" > left join course_teacher as ct on ct.course_id = a.id </if>" +
            "where 1=1 " +
            "and a.deleted = 0 " +
            "<if test='name!=null and name!=\"\" ' > and a.name like '%${name}%' </if>" +
            "<if test=\" userId !=null \" > and (ct.teacher_id = #{userId} or a.creator = #{userId}) </if>" +
            "</script>")
    List<Course> findStudyingCourse(@Param("time") String time, @Param("name") String name, @Param("userId")Integer id);

    @Select(" <script>" +
            "SELECT distinct " +
            "a.`name` name, " +
            "a.id id, " +
            "a.file_count fileCount, " +
            "a.video_count videoCount, " +
            "a.start_time startTime, " +
            "a.end_time endTime, " +
            "a.invalid_time invalidTime, " +
            "a.hours  hour, " +
            "a.is_publish audit, " +
            "a.deleted isDelete, " +
            "d.`username`  teacherName " +
            "FROM " +
            "course AS a " +
            "INNER JOIN course_teacher AS c ON a.id = c.course_id " +
            "INNER JOIN `user` AS d ON c.teacher_id = d.id  " +
            "WHERE " +
            "1=1 " +
//            "a.is_delete = FALSE " +
            "AND c.type = '1' " +
            "<if test='audit!=null ' > AND a.is_publish = #{audit} </if>" +
            "<if test='teacherId!=null ' > AND c.teacher_id = #{teacherId} or a.creator =#{teacherId} </if>" +
            "<if test='name!=null and name!=\"\" ' > AND a.name like '%${name}%' </if>" +
            "order by a.create_time desc " +
            "</script>")
    List<CourseManageVO> getCourseList(@Param("audit") Integer isAudit, @Param("teacherId") Integer teacherId,@Param("name") String name);

    @Select(" <script>" +
            "SELECT DISTINCT " +
            "a.`name` name, " +
            "a.id id, " +
            "a.file_count fileCount, " +
            "a.video_count videoCount, " +
            "a.start_time startTime, " +
            "a.end_time endTime, " +
            "a.is_publish audit " +
            "FROM " +
            "course AS a " +
            "<if test='name!=null and name!=\"\" ' > INNER JOIN course_teacher AS c ON a.id = c.course_id " +
            " INNER JOIN `user` AS d ON c.teacher_id = d.id </if> " +
            "WHERE " +
            "a.deleted = FALSE " +
            "<if test='name!=null and name!=\"\" ' > AND a.name like '%${name}%' </if>" +
            "</script>")
    List<SubjectHubManageVO> findSubjectHubManageList(@Param("name") String name);

    @Select(" <script>" +
            "select distinct a.* from course as a " +
            "inner join course_teacher as b on a.id = b.course_id " +
            "where 1=1 " +
            "<if test='creator!=null and creator!=\"0\" ' >AND a.creator = #{creator} or b.teacher_id = #{creator}  </if>" +
            "<if test='name!=null and name!=\"\" ' > AND a.name like '%${name}%' </if>" +
            "order by a.create_time desc, a.id asc " +
            "</script>")
    List<Course> findCoursesByCreator(@Param("creator")Integer creator, @Param("name")String key);

    @Select("<script>" +
            "select DISTINCT a.* from course a " +
            "<if test='name!=null ' > " +
            "inner join course_teacher b on b.course_id = a.id  " +
            "</if>" +
            " where a.is_publish = 1 " +
            "<if test='name!=null ' > " +
            "or (a.creator =#{teacher} and b.teacher_id = #{teacher}) " +
            "</if>" +
            "ORDER BY a.create_time DESC " +
            "</script>")
    List<Course> findAllAuditCourse(Integer teacherId);
}
