package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.DTO.BusinessECharDTO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Mapper 接口
 *
 * @author
 * @since
 */
@Component
@Mapper
public interface OfferRecordMapper {

    @Select("<script>" +
            "SELECT " +
            "COUNT(b.id) VALUE, b.NAME NAME " +
            "FROM " +
            "offer_record AS a " +
            "INNER JOIN ( " +
            "SELECT " +
            "a.sort, a.NAME, b.id " +
            "FROM " +
            "business a " +
            "INNER JOIN USER b ON a.id = b.business_id " +
            "ORDER BY a.sort ) b " +
            "ON a.accepter_id = b.id " +
            "WHERE DATE_FORMAT( create_time, '%m' ) = #{month} " +
            "GROUP BY b.id " +
            "ORDER BY b.sort" +
            "</script>")
    List<BusinessECharDTO> findSeriesOfferData(@Param("month") Integer month);


    @Select("SELECT " +
            "COUNT(*) AS VALUE, " +
            "DATE_FORMAT( a.create_time, '%m' ) AS NAME " +
            "FROM " +
            "offer_record AS a " +
            "INNER JOIN ( SELECT id FROM `user` WHERE business_id = #{businessId} ) b ON a.accepter_id = b.id " +
            "WHERE " +
            "DATE_FORMAT( a.create_time, '%Y' ) = #{year} " +
            "GROUP BY " +
            "DATE_FORMAT( a.create_time, '%m' ) ")
    List<BusinessECharDTO> findSeriesOfferDataByBusiness(@Param("year")int year, @Param("businessId")Integer businessId);
}
