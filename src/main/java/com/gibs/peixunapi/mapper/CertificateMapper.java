package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.VO.show.CertificateListVO;
import com.gibs.peixunapi.VO.show.CertificateManageVO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 *  Mapper 接口
 *
 * @author 
 * @since 
 */
@Component
@Mapper
public interface CertificateMapper  {


    @Select("<script> " +
            "select a.id as id, a.name as name, b.url as url " +
            "from certificate as a  " +
            "inner join file_info as b on a.file_info_id = b.id " +
            "where a.deleted = 0 " +
            "<if test='name!=null and name!=\"\" '>and a.name like '%${name}%' </if> " +
            "order by a.create_time " +
            "</script> ")
    List<CertificateListVO> getCertificateVOList(@Param("name") String name);

@Select("<script>" +
        "SELECT  " +
        "b.id AS id, " +
        "c.`username` AS `username`, " +
        "a.create_time AS createTime, " +
        "b.expiry_date AS expiryDate, " +
        "b.is_pause AS pause, " +
        "b.status AS status, " +
        "b.unique_code AS uniqueCode, " +
        "b.valid_time as valid_time, " +
        "b.continue_hours AS continueHours, " +
        "b.apply_status as applyStatus, " +
        "d.name as businessName, " +
        "b.create_time " +
        "FROM " +
        "certificate AS a " +
        "INNER JOIN certificate_student AS b ON a.id = b.certificate_id " +
        " INNER JOIN `user` AS c ON c.id = b.user_id " +
        " INNER JOIN business AS d ON c.business_id = d.id " +
        " where 1=1 " +
        "<if test='comingExpiry!=null '> <![CDATA[ and if( (datediff( b.expiry_date, '2020-10-13' ) <= 15),(1=1),(1=2) ) ]]> </if> " +
        "<if test='expiry!=null '> and b.is_expiry = #{expiry} and b.is_pause = 1 and b.status = 1 </if> " +
        "<if test='status!=null '> and b.status = #{status} </if> " +
        "<if test='pause!=null '> and b.is_pause = #{pause} </if> " +
        "<if test='businessId!=null '> and d.id = #{businessId} </if> " +
        "<if test='key!=null and key!=\"\" '> and c.username like '%${key}%' </if> " +
        " order by b.create_time desc" +
        "</script> ")
    List<CertificateManageVO> getCertificateManageList(@Param("comingExpiry")Integer comingExpiry, @Param("date")Date date, @Param("expiry")Integer expiry, @Param("status")Integer status,
                                                       @Param("pause")Integer pause, @Param("businessId")Integer businessId,
                                                       @Param("key")String key);
}
