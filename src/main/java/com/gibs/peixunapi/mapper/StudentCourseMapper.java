package com.gibs.peixunapi.mapper;

import java.util.List;

import com.gibs.peixunapi.DTO.CourseStudyDTO;
import com.gibs.peixunapi.VO.show.CourseStudyVO;
import com.gibs.peixunapi.VO.show.SignUpStudentListVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import com.gibs.peixunapi.VO.show.StudentSignUpTypeVO;


/**
 *  Mapper 接口
 *
 * @author 
 * @since 
 */
@Component
@Mapper
public interface StudentCourseMapper  {

@Select(" <script>" +
        "SELECT c.is_sign_up, a.id, a.username, b.`name` " +
        "FROM " +
        "`user` AS a " +
        "INNER JOIN business AS b ON a.business_id = b.id " +
        "LEFT JOIN student_course AS c ON a.id = c.student_id " +
        "WHERE 1=1 " +
        "<if test='businessId!=null ' > " +
        "AND b.id = #{businessId} </if>" +
        "AND a.`status` = 1 " +
        "AND c.course_id = #{courseId} " +
        "<if test='signUp!=null ' > " +
        "AND c.is_sign_up = #{signUp} </if>" +
        "</script>")
    List<StudentSignUpTypeVO> findByCourseIdAndStudentId(@Param("courseId")Integer courseId, @Param("signUp")Integer signUp, @Param("businessId")Integer businessId);

@Select("SELECT  " +
        "u.id as userId, " +
        "sc.course_id as studentCourseId, " +
        "u.username AS username, " +
        "b.`name` AS businessName, " +
        "sc.video_progress AS videoProgress, " +
        "sc.documentation_progress AS documentProgress " +
        "FROM " +
        "`user` AS u " +
        "INNER JOIN business AS b ON u.business_id = b.id " +
        "INNER JOIN student_course AS sc ON u.id = sc.student_id " +
        "WHERE " +
        "sc.course_id = #{courseId} ")
    List<CourseStudyDTO> findCourseStudyList(@Param("courseId") Integer courseId);

    @Select("SELECT  " +
            "u.username AS username, " +
            "sc.sign_up_time as signUpTime " +
            "FROM " +
            "`user` AS u " +
            "INNER JOIN student_course AS sc ON u.id = sc.student_id " +
            "WHERE " +
            "sc.course_id = #{courseId} " +
            "order by sc.sign_up_time  ")
    List<SignUpStudentListVO> findSignUpList(@Param("courseId")Integer courseId);

    @Select(" <script>" +
            "SELECT a.is_sign_up " +
            "FROM " +
            "student_course a " +
            "<if test='type!=null and type==\"video\"'>" +
            "inner join video_class as b on b.course_id = a.course_id " +
            "</if>" +
            "<if test='type!=null and type==\"document\"'>" +
            "inner join documentation as b on b.course_id = a.course_id " +
            "</if>" +
            "WHERE a.student_id = #{user} and b.id = #{chapterId} " +
            "</script>")
    Integer ifSubscribeByChapter(@Param("chapterId")Integer chapterId, @Param("type")String type, @Param("user")Integer userId);
}
