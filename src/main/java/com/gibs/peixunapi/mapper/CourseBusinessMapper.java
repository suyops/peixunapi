package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.VO.show.CourseManageVO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 *  Mapper 接口
 *
 * @author liangjiawe
 * @since 20209/9
 */
@Component
@Mapper
public interface CourseBusinessMapper {

@Select("<script>" +
        "SELECT " +
        "course.*,course.hours as hour,IF( business.id > 0, 1, 0 ) as isSubscribe " +
        "FROM " +
        "course " +
        "LEFT JOIN business_sub_course ON course.id = business_sub_course.course_id " +
        "LEFT JOIN business ON business_sub_course.business_id = business.id  " +
        "AND business.id = #{business}" +
        "<if test='name!=null and name!=\"\"'> where course.name like '%${name}%' </if>" +
        "</script>")
    List<CourseManageVO> findByBusinessPage(@Param("business") Integer businessId, @Param("name") String name);
}
