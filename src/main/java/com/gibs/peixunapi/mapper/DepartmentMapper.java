package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.VO.show.BusinessListVO;
import com.gibs.peixunapi.VO.show.OptionVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 *  Mapper 接口
 *
 * @author 
 * @since 
 */
@Component
@Mapper
public interface DepartmentMapper {

    @Select("select id as id, name as name,sort as sort " +
            "from department  " +
            "where status = 1 " +
            "order by sort ")
    List<OptionVO> getOptions();
}
