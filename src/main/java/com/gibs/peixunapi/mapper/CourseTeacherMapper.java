package com.gibs.peixunapi.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/18/15:39
 * @Version 1.0
 * @Description:
 */
@Mapper
@Component
public interface CourseTeacherMapper {

    @Select("<script>" +
            "select distinct a.teacher_id " +
            "from course_teacher as a " +
            "<choose>" +
            "<when test=\"type=='document'.toString()\"> " +
            "inner join documentation as c on a.course_id = c.course_id " +
            "where c.id = #{chapter} " +
            "</when> " +
            "<when test=\"type=='video'.toString()\"> " +
            "inner join video_class as c on a.course_id = c.course_id " +
            "where c.id = #{chapter} " +
            "</when> " +
            "</choose> " +
            "</script>")
    List<Integer> findTeachersId(Integer chapterId, String message);
}
