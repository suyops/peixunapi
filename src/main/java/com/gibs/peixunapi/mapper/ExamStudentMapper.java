package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.DTO.BusinessECharDTO;
import com.gibs.peixunapi.VO.show.ExamSignUpStudentVO;
import com.gibs.peixunapi.VO.show.ScoreManageVO;
import com.gibs.peixunapi.VO.show.StudentInExamClassVO;
import com.gibs.peixunapi.model.ExamStudent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/17/15:17
 * @Version 1.0
 * @Description:
 */
@Mapper
@Component
public interface ExamStudentMapper {

    @Select("<script>" +
            "SELECT " +
            "COUNT(b.id) VALUE, b.NAME NAME " +
            "FROM " +
            "exam_student AS a " +
            "INNER JOIN ( " +
            "SELECT " +
            "a.sort, a.NAME, b.id " +
            "FROM " +
            "business a " +
            "INNER JOIN USER b ON a.id = b.business_id " +
            "ORDER BY a.sort ) b " +
            "ON a.student_id = b.id " +
            "WHERE DATE_FORMAT( create_time, '%m' ) = #{month} " +
            "GROUP BY b.id " +
            "ORDER BY b.sort" +
            "</script>")
    List<BusinessECharDTO> findSeriesExamData(@Param("month") Integer month);


    @Select("SELECT " +
            "COUNT(*) AS VALUE, " +
            "DATE_FORMAT( a.create_time, '%m' ) AS NAME " +
            "FROM " +
            "exam_student AS a " +
            "INNER JOIN ( SELECT id FROM `user` WHERE business_id = #{businessId} ) b ON a.student_id = b.id " +
            "WHERE " +
            "DATE_FORMAT( a.create_time, '%Y' ) = #{year} " +
            "GROUP BY " +
            "DATE_FORMAT( a.create_time, '%m' ) ")
    List<BusinessECharDTO> findSeriesExamDataByBusiness(@Param("year") int year, @Param("businessId") Integer businessId);


    @Select("SELECT " +
            "a.id as id, a.username as studentName, b.name as businessName, " +
            "d.documentation_progress as documentProgress, d.video_progress as videoProgress" +
            "FROM " +
            "`user` AS a " +
            "INNER JOIN business AS b ON a.business_id = b.id " +
            "INNER JOIN student_course AS d ON a.id = d.student_id " +
            "INNER JOIN exam_student AS c ON c.student_id = a.id " +
            "WHERE c.exam_class_info_Id = #{classId} ")
    List<ExamSignUpStudentVO> findSignUpStudentList(@Param("classId") Integer classId);

    @Select("<script>" +
            "SELECT " +
            "u.id AS id, " +
            "u.username AS username, " +
            "es.unique_code AS uniqueCode, " +
//            "sc.documentation_progress AS documentProgress, " +
//            "sc.video_progress AS videoProgress, " +
            "es.exam_status AS examStatus, " +
            "es.apply_delayed_exam AS applyDelayedExam, " +
            "es.exam_scores AS scores, " +
            "b.`name` AS businessName, " +
            "es.correct_status AS correctStatus, " +
            "eci.`code` AS `CODE`  " +
            "FROM " +
            "`user` AS u " +
//            "INNER JOIN student_course AS sc ON u.id = sc.student_id " +
            "INNER JOIN exam_student AS es ON es.student_id = u.id " +
            "INNER JOIN exam_class_info AS eci ON es.exam_class_info_id = eci.id " +
            "INNER JOIN department AS b ON b.id = u.department_id " +
            "INNER JOIN exam_info AS ei ON eci.exam_info_id = ei.id " +
//            "AND sc.course_id = ei.course_id " +
            "WHERE eci.id = #{classId} " +
            " <if test=' name!=null and name!=\"\" '> and u.username like '%${name}%' </if>" +
            "</script>")
    List<StudentInExamClassVO> findStudentList(@Param("name") String name, @Param("classId") Integer classId);

    @Select("<script>"+
            "select a.id," +
            "(@i:=@i+1) sort, " +
            "c.username," +
            "a.daily_score," +
            "a.exam_scores," +
            "d.name as businessName, " +
            "b.proportion " +
            "from exam_student as a " +
            "inner join exam_class_info as b on a.exam_class_info_id = b.id " +
            "inner join user as c on a.student_id = c.id " +
            "inner join business as d on d.id = c.business_id ,(select @i:=1) as it " +
            "where b.id = #{classId} " +
            "<if test='key!=null and key!=\"\"'>" +
            "and c.username like %${key}%  " +
            "</if>"+
            "</script>")
    List<ScoreManageVO> findByExamClass(String key, Integer classId);
}
