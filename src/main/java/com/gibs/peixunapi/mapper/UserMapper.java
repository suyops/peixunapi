package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.VO.show.AllowExamVO;
import com.gibs.peixunapi.VO.show.PieVO;
import com.gibs.peixunapi.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Mapper 接口
 *
 * @author
 * @since
 */
@Component
@Mapper
public interface UserMapper {


    @Select("<script> select id from user where status = 1 " +
            "<if test='name!=null and name!=\"\"' >" +
            " and username like '%${name}%' " +
            " </if> " +
            "</script>")
    List<Integer> findByName(@Param("name") String name);

    @Select(" <script>" +
            "select a.* from user as a " +
            " inner join user_role as b on a.id = b.user_id " +
            " inner join role as c on b.role_id = c.id " +
            " where c.code = #{code} and a.status = 1 and a.audit = 1 " +
            " <if test='username!=null and username!=\"\"' > " +
            " and a.username like '%${username}%' </if> " +
            " </script> ")
    List<User> findUserListByCode(@Param("code") String code, @Param("username") String name);

    @Select(" <script>" +
            "select a.id from user as a " +
            "inner join business as b on a.business_id = b.id " +
            "where a.status = 1" +
            "<if test='businessId!=null and businessId!=\"\"' > " +
            "and b.id = #{businessId} </if> " +
            " and a.audit = 1 "+
            " </script> ")
    List<Integer> findUserListByBusinessId(@Param("businessId") Integer businessId);

    @Select("select count( * ) as value, b.name as name " +
            "from user as a " +
            "inner join business as b on a.business_id = b.id group by b.name")
    List<PieVO> findUserPieData();

    @Select(" <script>" +
            "select a.* from user as a " +
            " inner join business as d on a.business_id = d.id " +
            " where a.status = 1 and a.audit = 1 and d.id = #{businessId} " +
            " <if test='name!=null and name!=\"\"' > " +
            " and a.username like '%${name}%' </if> " +
            " </script> ")
    List<User> findUserListByCodeAndBusiness( @Param("name") String name, @Param("businessId") Integer businessId);

    @Select(" <script>" +
            "SELECT DISTINCT " +
            "a.id," +
            "a.username" +
            " " +
            "FROM " +
            "`user` a " +
            "LEFT JOIN exam_student b ON a.id = b.student_id and b.exam_info_id =#{examId} " +
            "WHERE " +
            "b.id is null " +
            " <if test='name!=null and name!=\"\"' > " +
            " and a.username like '%${name}%' </if>"+
            " <if test='year!=null and year!=\"0\"' > " +
            " and a.entry_year = #{year} </if>"+
            " <if test='department!=null and department!=\"0\"' > " +
            " and a.department_id = #{department} </if>"+
            " </script> ")
    List<AllowExamVO> findExamList(@Param("department")Integer department, @Param("year")Integer year, @Param("name")String key, @Param("examId")Integer examId);
}
