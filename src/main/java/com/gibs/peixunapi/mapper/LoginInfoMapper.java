package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.DTO.BarDataDTO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Mapper 接口
 *
 * @author
 * @since
 */
@Component
@Mapper
public interface LoginInfoMapper {

    @Select("<script> " +
            "SELECT month, " +
            "COUNT(*) AS count " +
            "FROM " +
            "( " +
                "SELECT DATE_FORMAT( login_time, '%m' ) month " +
                "FROM login_info " +
                "WHERE DATE_FORMAT( login_time, '%Y' ) = #{year}  " +
                "<if test='idList!=null and idList!=\"\" '> " +
                "AND user_id in "  +
                "<foreach collection=\"idList\" item=\"idList\" index=\"index\" open=\"(\" close=\")\" separator=\",\"> " +
                "#{idList} " +
                "</foreach> " +
                "</if> " +
            ") a  " +
            "GROUP BY month " +
            "</script>")
    List<BarDataDTO> findUserBarDataByBusiness(@Param("idList") List<Integer> idList, @Param("year") Integer year);
}
