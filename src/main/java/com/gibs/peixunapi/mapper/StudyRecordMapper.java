package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.DTO.BarDataDTO;
import com.gibs.peixunapi.DTO.BusinessECharDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/17/10:37
 * @Version 1.0
 * @Description:
 */
@Mapper
@Component
public interface StudyRecordMapper {
    @Select("<script>" +
            "SELECT `month`, COUNT(*) AS count  " +
            "FROM " +
            "( SELECT " +
            "DATE_FORMAT( create_time, '%m' ) AS `month`  " +
            "FROM " +
            "study_record  " +
            "WHERE " +
            "DATE_FORMAT( create_time, '%Y' ) = #{year} " +
            "<if test='userId!=null' >AND student_id = #{userId} </if>" +
            "AND video_class_id is null ) a " +
            "GROUP BY `month` " +
            "</script>")
    List<BarDataDTO> findDocumentLineData(@Param("userId") Integer userId, @Param("year") int year);

    @Select("<script>" +
            "SELECT `month`, COUNT(*) AS count  " +
            "FROM " +
            "( SELECT " +
            "DATE_FORMAT( create_time, '%m' ) AS `month`  " +
            "FROM " +
            "study_record  " +
            "WHERE " +
            "DATE_FORMAT( create_time, '%Y' ) = #{year} " +
            "<if test='userId!=null' >AND student_id = #{userId} </if>" +
            "AND document_id is null ) a " +
            "GROUP BY `month` " +
            "</script>")
    List<BarDataDTO> findVideoLineData(@Param("userId") Integer userId, @Param("year") int year);

    @Select("<script>" +
            "SELECT " +
            "COUNT(c.id ) VALUE, c.name NAME " +
            "FROM " +
            "study_record AS a " +
            "INNER JOIN ( " +
                "SELECT " +
                "a.sort, a.NAME, a.id " +
                "FROM " +
                "business a " +
                "INNER JOIN `user` b ON a.id = b.business_id " +
                "ORDER BY a.sort ) c " +
            "ON a.student_id = c.id " +
            "WHERE DATE_FORMAT( create_time, '%m' ) = #{month} " +
            "GROUP BY c.id  " +
            "ORDER BY c.sort" +
            "</script>")
    List<BusinessECharDTO> findSeriesStudyData(@Param("month") Integer month);



    @Select("SELECT " +
                "COUNT(*) AS VALUE, " +
                "DATE_FORMAT( a.create_time, '%m' ) AS NAME " +
            "FROM " +
            "study_record AS a " +
                "INNER JOIN ( SELECT id FROM `user` WHERE business_id = #{businessId} ) b ON a.student_id = b.id " +
            "WHERE " +
                "DATE_FORMAT( a.create_time, '%Y' ) = #{year} " +
            "GROUP BY " +
            "DATE_FORMAT( a.create_time, '%m' ) ")
    List<BusinessECharDTO> findSeriesStudyDataByBusiness(@Param("year")Integer year,@Param("businessId")Integer businessId);
}
