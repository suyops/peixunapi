package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.DTO.SubjectHubDTO;
import com.gibs.peixunapi.model.CourseSubjectHub;
import com.gibs.peixunapi.model.SubjectHub;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 *  Mapper 接口
 *
 * @author 
 * @since 
 */
@Component
@Mapper
public interface CourseSubjectHubMapper  {

    @Select("<script>" +
            " SELECT b.*,a.sort as sort FROM " +
            " course_subject_hub a LEFT JOIN subject_hub b ON a.subject_hub_id= b.id " +
            " WHERE a.course_id = #{id} " +
            " <if test=\"type != null and type != ''\"> " +
            " AND b.type = #{type} " +
            "        </if> " +
            " ORDER BY a.sort " +
            "</script>")
    List<SubjectHubDTO> findSubjectListByCourseId(@Param("id") Integer id, @Param("type") String type);

    @Select("<script>" +
            " SELECT " +
            " a.*  " +
            " FROM " +
            " course_subject_hub AS a " +
            " INNER JOIN subject_hub AS b ON a.subject_hub_id = b.id" +
            " <choose> " +
            " <when test='type==\"document\".toString()'>" +
            " INNER JOIN documentation AS c ON a.course_id = c.course_id " +
            " </when> " +
            "  <when test='type==\"video\".toString()'> " +
            " INNER JOIN video_class AS c ON a.course_id = c.course_id " +
            " </when> " +
            " </choose> " +
            " where c.id = #{chapterId} and a.subject_hub_id = #{subjectHubId} " +
            "</script>")
    CourseSubjectHub findByChapterAndSubjectHub(@Param("chapterId")Integer chapterId, @Param("subjectHubId")Integer subjectHubId, @Param("type")String type);


    @Select("<script>" +
            " SELECT " +
            " a.*  " +
            " FROM " +
            " course_subject_hub AS a " +
            " INNER JOIN subject_hub AS b ON a.subject_hub_id = b.id" +
            " where a.course_id = #{chapterId} and a.subject_hub_id = #{subjectHubId} " +
            "</script>")
    CourseSubjectHub findByCourseAndSubjectHub(@Param("chapterId")Integer chapterId, @Param("subjectHubId")Integer subjectHubId);
}
