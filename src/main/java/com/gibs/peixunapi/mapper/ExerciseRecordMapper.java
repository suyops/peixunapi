package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.DTO.ShowExerciseRecordDTO;
import com.gibs.peixunapi.VO.show.ShowExerciseRecordVO;
import com.gibs.peixunapi.VO.SubjectHubVO;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Mapper 接口
 *
 * @author
 * @since
 */
@Component
@Mapper
public interface ExerciseRecordMapper {

    @Select("<script>" +
            "select b.id courseId, b.name courseName " +
            "    from student_course a " +
            "        left join course b on a.course_id = b.id " +
            "    where a.student_id = #{studentId} " +
            "<if test = ' courseId!=null ' > and a.course_id = #{courseId} </if>" +
            "</script>")
    ShowExerciseRecordVO getCourse(@Param("studentId") Integer studentId,
                                   @Param("courseId") Integer courseId);

    @Select("<script>" +
            "SELECT a.chapterId, a.chapterName, a.type FROM ( " +
            " SELECT  a.id AS chapterId,  a.NAME AS chapterName,  a.sort AS sort, 'document' as type " +
            " FROM  documentation a  INNER JOIN exercises b ON a.id = b.documentation  INNER JOIN exercise_record c ON c.id = b.id   " +
            "  WHERE  a.course_id = #{courseId}   AND c.student_id = #{studentId}  " +
            "  UNION ALL " +
            " SELECT " +
            "  a.id AS chapterId,  a.NAME AS chapterName,  a.sort AS sort, 'video' as type " +
            "  FROM  video_class a  INNER JOIN exercises b ON a.id = b.video_class  INNER JOIN exercise_record c ON c.id = b.id  " +
            " WHERE  a.course_id = #{courseId}  AND c.student_id = #{studentId}  " +
            " ) a ORDER BY a.sort" +
            "</script>")
    List<ShowExerciseRecordDTO> getChapterRecord(@Param("courseId") Integer courseId,
                                                 @Param("studentId") Integer studentId);


    @Select("<script>" +
            "SELECT a.chapterId, a.chapterName, a.type FROM ( " +
            " SELECT  a.id AS chapterId,  a.NAME AS chapterName,  a.sort AS sort, 'document' as type " +
            " FROM  documentation a  INNER JOIN exercises b ON a.id = b.documentation  INNER JOIN exercise_record c ON c.id = b.id   " +
            "  WHERE  1=1 " +
            "  AND c.student_id = #{studentId}  " +
            "  UNION ALL " +
            " SELECT " +
            "  a.id AS chapterId,  a.NAME AS chapterName,  a.sort AS sort, 'video' as type " +
            "  FROM  video_class a  INNER JOIN exercises b ON a.id = b.video_class  INNER JOIN exercise_record c ON c.id = b.id  " +
            " WHERE  1=1 " +
            "  AND c.student_id = #{studentId}  " +
            " ) a ORDER BY a.sort" +
            "</script>")
    List<ShowExerciseRecordDTO> getLatestChapterRecord(@Param("studentId") Integer studentId);

    @Select("<script>" +
            "SELECT a.id FROM ( " +
            " <choose> " +
            " <when test='type==\"document\".toString()'> " +
            " SELECT b.id AS id, b.sort AS sort , b.create_time, 'document' as type   " +
            " FROM exercises b  INNER JOIN exercise_record c ON c.id = b.id " +
            "  WHERE b.documentation = #{chapterId} AND c.student_id = #{studentId}  " +
            " </when> " +
            "  <when test='type==\"video\".toString()'> " +
            " SELECT b.id AS id, b.sort AS sort, b.create_time, 'video' as type  " +
            "  FROM exercises b INNER JOIN exercise_record c ON c.id = b.id " +
            " WHERE b.video_class = #{chapterId} AND c.student_id = #{studentId}  " +
            " </when>" +
            " </choose>" +
            " ) a ORDER BY b.sort" +
            "</script>")
    List<Integer> getExerciseId(@Param("chapterId") Integer chapterId,
                                @Param("studentId") Integer studentId, @Param("type") String type);


    List<SubjectHubVO> getRecord(@Param("courseSubjectId") Integer courseSubjectId);

    @Select("<script>" +
            "SELECT COUNT(exercise_record.id) " +
            "FROM exercise_record " +
            "INNER JOIN exercises ON exercise_record.exercise_id = exercises.id  " +
            "WHERE " +
            "exercise_record.student_id = #{studentId} " +
            "<choose> " +
            "   <when test='type==\"document\".toString()'> " +
            "       AND exercises.documentation = #{chapterId} " +
            "   </when> " +
            "   <when test='type==\"video\".toString()'> " +
            "       AND exercises.video_class = #{chapterId} " +
            "   </when>" +
            "</choose>" +
            "</script>")
    Integer finishExerciseCount(@Param("chapterId") Integer id, @Param("studentId") Integer userId, @Param("type") String message);

    @Select("<script>" +
            "select a.exercise_id " +
            "from exercise_record a " +
            "inner join exercises b on a.exercise_id = b.id " +
            "where " +
            "<choose> " +
            " <when test='type==\"document\".toString()'> " +
            " AND exercises.documentation = #{chapterId} " +
            " </when> " +
            " <when test='type==\"video\".toString()'> " +
            " AND exercises.video_class = #{chapterId} " +
            " </when>" +
            "</choose>" +
            "and a.student_id = #{studentId} " +
            "and a.is_right = 1 " +
            "</script>")
    List<Integer> findIdListByChapterAndStudent(@Param("chapterId")Integer chapterId, @Param("studentId")Integer id, @Param("type")String type);
}
