package com.gibs.peixunapi.mapper;

import com.gibs.peixunapi.VO.show.StudentExamCenterVO;
import com.gibs.peixunapi.model.ExamClassInfo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * Mapper 接口
 *
 * @author
 * @since
 */
@Component
@Mapper
public interface ExamClassInfoMapper {

    @Select("<script>" +
            "SELECT a.id " +
            "FROM " +
            "exam_class_info a " +
            "INNER JOIN certificate_class AS b ON a.id = b.exam_class_info_id " +
            "<if test=' name!=null and name!=\"\" ' > INNER JOIN exam_info AS c ON c.id = a.exam_info_id " +
            "INNER JOIN course AS d ON d.id = c.course_id  </if> " +
            "WHERE " +
            "a.correct_status = 1 " +
            "AND a.exam_status = 1 " +
            "AND b.pass_count >= b.issue_count " +
            "<if test=' name!=null and name!=\"\" ' > AND d.NAME LIKE '%${name}%' </if>" +
            "ORDER BY " +
            "a.announce_result_time DESC " +
            "</script>")
    List<Integer> findFinishedClass(@Param("name") String name);

    @Select("<script>" +
            "SELECT " +
            " a.id AS id, " +
            " a.`name` AS `name`, " +
//            " c.`name` AS courseName, " +,l
            " c.exam_scores AS score, " +
            " a.exam_start_time AS examStartTime, " +
            " a.exam_end_time AS examEndTime, " +
            " b.duration_time AS durationTime, " +
            " b.id AS examInfoId," +
            " IF( ( TO_DAYS( a.sign_dead_line )- TO_DAYS( #{time} )> 0 ), 0, 1 ) as deadLineStatus " +
            "FROM " +
            " exam_class_info AS a " +
            " INNER JOIN exam_info AS b ON a.exam_info_id = b.id " +
            " INNER JOIN exam_student AS c ON a.id = c.exam_class_info_id " +

//            " INNER JOIN course AS c ON b.course_id = c.id " +
            "WHERE 1=1 " +
            "and c.student_id = #{userId}" +
//            " b.course_id in " +
//            "<foreach collection=\"courseId\" item=\"courseId\" index=\"index\" open=\"(\" close=\")\" separator=\",\"> " +
//            "#{courseId} " +
//            "</foreach> " +
//            "<![CDATA[ AND a.exam_status < 1 ]]>" +
//            " <if test=' name!=null and name!=\"\" ' > AND c.name like '%${name}%' </if>" +

            " order by a.create_time desc" +
            "</script>")
    List<StudentExamCenterVO> findClassInCourse(@Param("name") String name, @Param("userId")Integer userId, @Param("time") Date time);

    @Select("<script>" +
            "select distinct a.id from exam_class_info as a " +
            "<if test=' name!=null and name!=\"\" ' >" +
            "left join exam_info as b on a.exam_info_id = b.id  " +
            "left join course as c on c.id= b.course_id " +
            "</if> " +
            "<if test=' userId!=null ' > inner join exam_teacher as et on et.exam_class_info_id = a.id and et.type = \'manage\'  </if>" +
            "where  1=1" +
            "<if test=' name!=null and name!=\"\" ' > and (c.name like '%${name}%' or a.name like' %${name}%') </if> " +
            //"<![CDATA[ and a.exam_end_time > #{time} ]]>" +
            "and a.exam_status = 0 " +
            "<if test=' userId!=null ' > and et.teacher_id = #{userId}  </if>" +
            "order by a.create_time desc" +
            "</script>")
    List<Integer> findIdByStartedExam(@Param("name")String name, @Param("time")String time, @Param("userId")Integer userId);

    @Select("<script>" +
            "select distinct a.id " +
            "from exam_class_info a " +
            "<if test=' name!=null and name!=\"\" ' > inner join exam_info as ei on ei.id = a.exam_info_id " +
            "inner join course as c on c.id = ei.course_id </if>" +
            "<if test=' userId!=null ' > inner join exam_teacher as et on et.exam_class_info_id = a.id and et.type = 'correct' </if>" +
            "where 1=1 " +
            "<if test=' name!=null and name!=\"\" ' > and (c.name like '%${name}%' or a.name like '%${name}%') </if>" +
            "<if test=' userId!=null ' > and et.teacher_id = #{userId} </if>" +
            "and a.exam_status = 1 " +
            "and a.correct_status = 0 " +
            "order by a.create_time desc " +
            "</script>")
    List<Integer> findWaitingCorrectIdList(@Param("name")String name, @Param("userId")Integer userId);
}
