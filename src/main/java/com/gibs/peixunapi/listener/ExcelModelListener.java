package com.gibs.peixunapi.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.service.*;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.gibs.peixunapi.enums.SubjectEnum.*;

/**
 * @author liangjiawei
 * @date 2020/11/11/10:54
 * @Version 1.0
 * @Description:
 */
@Slf4j
public class ExcelModelListener extends AnalysisEventListener<Map<Integer, String>> {

    private SubjectHubService subjectHubService;
    private CourseSubjectHubService courseSubjectHubService;
    private CourseService courseService;
    private TestPaperSubjectService testPaperSubjectService;
    private SubOptionService subOptionService;
    private String subjectType;

    private static int ALL_COUNT = 0;
    List<SubjectHub> subjectHubList = new ArrayList<>();

    List<CourseSubjectHub> courseSubjectHubList = new ArrayList<>();
    int sort = 0;

    private Integer id;
    private String type;

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public ExcelModelListener(SubjectHubService subjectHubService, CourseSubjectHubService courseSubjectHubService,
                              CourseService courseService, TestPaperSubjectService testPaperSubjectService,
                              SubOptionService subOptionService, Integer id, String type,String subjectType) {
        this.subjectHubService = subjectHubService;
        this.courseSubjectHubService = courseSubjectHubService;
        this.courseService = courseService;
        this.testPaperSubjectService = testPaperSubjectService;
        this.subOptionService = subOptionService;
        this.id = id;
        this.type = type;
        this.subjectType = subjectType;
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        super.invokeHeadMap(headMap, context);
    }

    @Override
    public void invoke(Map<Integer, String> sheetImport, AnalysisContext analysisContext) {
//        sheetImport.forEach((key, value) -> log.info("解析到一条数据:key:{} value:{} count:{}", key, value, ALL_COUNT));
        ALL_COUNT++;
        //创建题目
        save(sheetImport, analysisContext);
    }

    private void save(Map<Integer, String> sheetImport, AnalysisContext analysisContext) {
        SubjectHub subjectHub = new SubjectHub();
        subjectHub.setContent(sheetImport.get(1));
        subjectHub.setAnswer(sheetImport.get(2));
        if (analysisContext.readSheetHolder().getSheetNo() == 0) {
            subjectHub.setType(RADIO.getCode());
        } else if (analysisContext.readSheetHolder().getSheetNo() == 1) {
            subjectHub.setType(CHECK.getCode());
        } else if (analysisContext.readSheetHolder().getSheetNo() == 2) {
            subjectHub.setType(JUDGE.getCode());
        } else if (analysisContext.readSheetHolder().getSheetNo() == 3) {
            subjectHub.setType(FILL.getCode());
        } else {
            subjectHub.setType(QUESTION.getCode());
        }
        subjectHubService.saveImport(subjectHub);
        subjectHubList.add(subjectHub);
        //创建选项
        if (sheetImport.size() > 3) {
            for (int i = 3; i < sheetImport.size(); i++) {
                SubOption subOption = new SubOption();
                subOption.setSubjectHub(subjectHub);
                subOption.setSort(i-2);
                subOption.setContent(sheetImport.get(i));
                char ascii = (char) Integer.parseInt(Integer.toString(i-3 + 65));
                subOption.setValue(String.valueOf(ascii));
                subOptionService.saveImport(subOption);
            }
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        log.info("excel analysis complete!\nnow start to bind the subject");
        //加入课程题库
        if ("course".equals(getType())) {
            Course course = courseService.get(getId());
            for (SubjectHub hub : subjectHubList) {
                CourseSubjectHub courseSubjectHub = new CourseSubjectHub();
                courseSubjectHub.setSubjectHub(hub);
                courseSubjectHub.setCourse(course);
                courseSubjectHubList.add(courseSubjectHub);
            }
            courseSubjectHubService.createBatch(courseSubjectHubList);

        } else {
            testPaperSubjectService.saveImport(subjectHubList, getId(),subjectType);
        }
    }


}
