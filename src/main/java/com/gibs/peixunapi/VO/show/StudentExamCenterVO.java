package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/09/28/14:57
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "学生考试中心列表")
public class StudentExamCenterVO {
    /** id */
    @ApiModelProperty(value = "id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;
    /** 班级id */
    @ApiModelProperty(value = "班级id")
    private Integer classId;

    /** 培训名称 */
    @ApiModelProperty(value = "培训名称")
    private String courseName;
    
    /** 考试信息id */
    @ApiModelProperty(value = "考试信息id")
    private Integer examInfoId;

    /** 准考证号 */
    @ApiModelProperty(value = "准考证号")
    private String uniqueCode;
    
    /** 时长 */
    @ApiModelProperty(value = "时长")
    private String durationTime;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examStartTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examEndTime;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private String signUpStatus;
    /** 笔试分数 */
    @ApiModelProperty(value = "笔试分数")
    private BigDecimal score;

    /** 申请缓考  未通过：-1 未申请：0 已申请:1 已通过：2 */
    @ApiModelProperty(value = "申请缓考 通过：-1 未申请：0 已申请:1 已通过：2 ")
    private String applyDelayedExamString;

    private Integer applyDelayedExam;

    /** 是否截止  未截止：0 已截止:1  */
    @ApiModelProperty(value = "是否截止  未截止：0 已截止:1 ")
    private String deadLineStatusString;

    private Integer deadLineStatus;
}
