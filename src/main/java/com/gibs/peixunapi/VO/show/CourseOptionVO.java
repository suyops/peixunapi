package com.gibs.peixunapi.VO.show;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/10/14/1:10
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "章节选项VO类")
public class CourseOptionVO {
    /** id */
    @ApiModelProperty(value = "id",hidden = true)
    private Integer id;

    /** 排序 */
    @ApiModelProperty(value = "排序",hidden = true)
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称",hidden = true)
    private String name;

    /** 状态 */
    @ApiModelProperty(value = "状态",hidden = true)
    private Integer status;
}
