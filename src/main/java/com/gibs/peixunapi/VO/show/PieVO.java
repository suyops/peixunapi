package com.gibs.peixunapi.VO.show;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/09/16/18:05
 * @Version 1.0
 * @Description:
 */
@Data
public class PieVO {
    private Integer value;
    private String name;
}
