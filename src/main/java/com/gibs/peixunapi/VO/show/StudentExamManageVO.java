package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/09/21/11:17
 * @Version 1.0
 * @Description: 学生考试中心-成绩管理列表
 */
@Data
@ApiModel("学生考试中心-成绩管理列表")
public class StudentExamManageVO {
    /** id */
    @ApiModelProperty("id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty("序号")
    private Integer sort;

    /** 课程名称 */
    @ApiModelProperty("课程名称")
    private String courseName;

    /** 考试班级 */
    @ApiModelProperty("考试班级")
    private String className;

    /** 考试状态:缺考/已完成/申请缓考/已缓考 */
    @ApiModelProperty("考试状态:缺考/已完成/申请缓考/已缓考")
    private String examStatus;

    /** 考试成绩 */
    @ApiModelProperty("考试成绩")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
	private BigDecimal examScores;

	/** 平时成绩 */
	@ApiModelProperty("平时成绩")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
	private BigDecimal dailyScore;

    /** 最终成绩 */
    @ApiModelProperty("最终成绩")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal finalScore;
}
