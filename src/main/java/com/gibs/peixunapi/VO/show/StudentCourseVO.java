package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gibs.peixunapi.VO.show.ChapterListVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/09/17:59
 * @Version 1.0
 * @Description:
 */
@ApiModel(description = "获取最新课程列表VO")
@Data
public class StudentCourseVO {
    /** 课程id */
    @ApiModelProperty(value = "课程id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;

    /** 老师名称 */
    @ApiModelProperty(value = "老师名称")
    private String teacherName;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date startTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date endTime;
//
//    /** 截止时间 */
//    @ApiModelProperty(value = "截止时间")
//        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
//    private Date invalidTime;

    /** 状态 未报名:false 已报名:true */
    @ApiModelProperty(value = "状态 未报名:false 已报名:true")
    private Boolean signUp;

    /** 此课程学习状态 */
    @ApiModelProperty(value = "此课程学习状态")
    private String studyStatus;
    /** 是否删除 */
    @ApiModelProperty(value = "是否删除", hidden = true)
    private Boolean isDelete;

    /**
     * 是否上架 下架:0 上架:1
     */
    @ApiModelProperty(value = "是否上架 ")
    private Integer isPublish;
//
//    /** 审批状态 */
//    @ApiModelProperty(value = "审批状态")
//    private Integer audit;
//
//    /** 审批状态(中文) */
//    @ApiModelProperty(value = "审批状态(中文)")
//    private String  auditStatus;
//
//    /** 开放章节数 */
//    @ApiModelProperty(value = " 开放章节数")
//    private Integer openChapterNum;
//
//    /**完成习题最大可学习章节数 */
//    @ApiModelProperty(value = "完成习题最大可学习章节数")
//    private Integer learnChapterMaxNum;

    /** 章节信息列表 */
    @ApiModelProperty(value = " 章节信息列表")
    private List<ChapterListVO> chapterVOList;


    public StudentCourseVO() {
        this.signUp = false;
//        this.audit = 0;
    }
}
