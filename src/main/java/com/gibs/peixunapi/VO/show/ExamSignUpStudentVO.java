package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/09/21/9:06
 * @Version 1.0
 * @Description: 考生报名列表VO
 */

@Data
@ApiModel("考生报名列表VO")
public class ExamSignUpStudentVO {
    /** id */
    @ApiModelProperty(value = "id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 学生姓名 */
    @ApiModelProperty(value = "学生姓名")
    private String studentName;

    /** 企业名称 */
    @ApiModelProperty(value = "企业名称")
    private String businessName;

    /** 文档学习进度 */
    @ApiModelProperty(value = "文档学习进度")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal documentProgress;

    /** 视频学习进度 */
    @ApiModelProperty(value = "视频学习进度")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal videoProgress;
}
