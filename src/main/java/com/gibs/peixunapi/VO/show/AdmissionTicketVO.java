package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gibs.peixunapi.model.FileInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.OneToOne;
import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/10/20/16:02
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("准考证")
public class AdmissionTicketVO {

    /** 姓名 */
    @ApiModelProperty("姓名")
    private String username;

    /** 准考证码 */
    @ApiModelProperty("准考证码")
    private String uniqueCode;

    /** 单位 */
    @ApiModelProperty("单位")
    private String businessName;

    /** 岗位 */
    @ApiModelProperty("岗位")
    private String post;


    /** 证件编号 */
    @ApiModelProperty("证件编号")
    private String certificateNumber;

    /** 性别 */
    @ApiModelProperty("证件编号")
    private String sex;

    /** 头像 */
    @ApiModelProperty("证件编号")
    private String avatarUrl;

    /** 开始时间 */
    @ApiModelProperty("开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examStartTime;

    /** 结束时间 */
    @ApiModelProperty("结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examEndTime;

    /** 课程名称 */
    @ApiModelProperty("课程名称")
    private String courseName;

    /** 班级名称 */
    @ApiModelProperty("班级名称")
    private String name;

    /** 考试须知 */
    @ApiModelProperty(value = "考试须知")
    private String about;
    
    /** 考场号 */
    @ApiModelProperty(value = "考试号")
    private String code;
}
