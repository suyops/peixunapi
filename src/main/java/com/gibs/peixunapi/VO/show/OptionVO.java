package com.gibs.peixunapi.VO.show;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/08/24/10:18
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "通用多选、弹窗选项VO类")
public class OptionVO {
    /** id */
    @ApiModelProperty(value = "id",hidden = true)
    private Integer id;

    /** 排序 */
    @ApiModelProperty(value = "排序",hidden = true)
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称",hidden = true)
    private String name;

    /** 值 */
    @ApiModelProperty(value = "值",hidden = true)
    private String value;



    public OptionVO(){}
    public OptionVO(Integer id, Integer sort, String name,String value) {
        this.id = id;
        this.sort = sort;
        this.name = name;
        this.value = value;
    }
}
