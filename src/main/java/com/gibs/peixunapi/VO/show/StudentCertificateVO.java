package com.gibs.peixunapi.VO.show;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/10/13/12:10
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("学生-证书中心")
public class StudentCertificateVO {
    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("序号")
    private Integer sort;

    @ApiModelProperty("状态")
    private String status;

    @ApiModelProperty("图片地址")
    private String url;

    @ApiModelProperty("申请状态 未申请:0  已申请:1")
    private Integer applyStatus;
}
