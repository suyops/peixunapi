package com.gibs.peixunapi.VO.show;

//import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/08/27/14:56
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "试卷VO")
public class ShowTestPaperVO {

    /** id */
    @ApiModelProperty(value = "id")
    private Integer id;
    
    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String  name;

    /**
     * 考生名
     */
    @ApiModelProperty(value = "考生名")
    private String username;

    /** 总分 */
    @ApiModelProperty(value = "总分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal totalScore;
    
    /** 单选题数量 */
    @ApiModelProperty(value = "单选题数量")
    private Integer radioCount;

    /** 单选题分值 */
    @ApiModelProperty(value = "单选题分值")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal radioScore;

    /** 单选题总分值 */
    @ApiModelProperty(value = "单选题总分值")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal radioTotalScore;


    /** 单选题列表 */
    @ApiModelProperty(value = "单选题列表")
    private List<SubjectHubVO> radioList;

    /** 多选题数量 */
    @ApiModelProperty(value = "多选题数量")
    private Integer checkCount;

    /** 多选题分值 */
    @ApiModelProperty(value = "多选题分值")
        @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal checkScore;

    /** 多选题总分值 */
    @ApiModelProperty(value = "多选题总分值")
        @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal checkTotalScore;


    /** 多选题列表 */
    @ApiModelProperty(value = "多选题列表")
    private List<SubjectHubVO> checkList;

    /** 判断题数量 */
    @ApiModelProperty(value = "判断题数量")
    private Integer judgeCount;

    /** 判断题分值 */
    @ApiModelProperty(value = "判断题分值")
        @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal judgeScore;

    /** 判断题总分值 */
    @ApiModelProperty(value = "判断题总分值")
        @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal judgeTotalScore;

    /** 判断题列表 */
    @ApiModelProperty(value = "判断题列表")
    private List<SubjectHubVO> judgeList;

    /** 填空题数量 */
    @ApiModelProperty(value = "填空题数量")
    private Integer fillCount;

    /** 填空题分值 */
    @ApiModelProperty(value = "填空题分值")
        @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal fillScore;

    /** 填空题总分值 */
    @ApiModelProperty(value = "填空题总分值")
        @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal fillTotalScore;

    /** 填空题列表 */
    @ApiModelProperty(value = "填空题列表")
    private List<SubjectHubVO> fillList;

    /** 问答题数量 */
    @ApiModelProperty(value = "问答题数量")
    private Integer questionCount;

    /** 问答题分值 */
    @ApiModelProperty(value = "问答题分值")
        @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal questionScore;

    /** 问答题总分值 */
    @ApiModelProperty(value = "问答题总分值")
        @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal questionTotalScore;

    /** 问答题列表 */
    @ApiModelProperty(value = "问答题列表")
    private List<SubjectHubVO> questionList;

    public ShowTestPaperVO() {
        BigDecimal zero = new BigDecimal("0.00");
        int zeroInt = 0;
        List<SubjectHubVO> zeroList = new ArrayList<>();
        this.totalScore = zero;
        this.radioCount = zeroInt;
        this.radioScore = zero;
        this.radioTotalScore = zero;
        this.radioList = zeroList;
        this.checkCount = zeroInt;
        this.checkScore = zero;
        this.checkTotalScore = zero;
        this.checkList = zeroList;
        this.judgeCount = zeroInt;
        this.judgeScore = zero;
        this.judgeTotalScore = zero;
        this.judgeList = zeroList;
        this.fillCount = zeroInt;
        this.fillScore = zero;
        this.fillTotalScore = zero;
        this.fillList = zeroList;
        this.questionCount = zeroInt;
        this.questionScore = zero;
        this.questionTotalScore = zero;
        this.questionList = zeroList;
    }
}
