package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/10/13/22:58
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("企业管理-培训信息-订阅课程")
public class BusinessSubscribeVO {
    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "课程名称")
    private String name;


    @ApiModelProperty(value = "截止时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date invalidTime;

    @ApiModelProperty(value = "报名人数")
    private Integer subscribeCount;
}
