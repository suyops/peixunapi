package com.gibs.peixunapi.VO.show;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/09/16/18:08
 * @Version 1.0
 * @Description: 首页图表线状图格式
 */
@Data
public class LegendVO {
    private String[] data;

    public LegendVO(String[] strings) {
    }

    public LegendVO() {
    }
}
