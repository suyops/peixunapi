package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/10/09/19:37
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("通过考试学生列表")
public class PassExamStudentVO {

    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("序号")
    private Integer sort;

    @ApiModelProperty("企业单位")
    private String businessName;

    @ApiModelProperty("姓名")
    private String username;

    @ApiModelProperty("准考证")
    private String uniqueCode;

    @ApiModelProperty("得分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal finalScore;
}
