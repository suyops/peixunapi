package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/09/27/16:23
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("考务管理")
public class ExamManageVO {
    /** 课程id */
    @ApiModelProperty(value = "课程id")
    private Integer id;
    
    /** 课程名称 */
    @ApiModelProperty(value = "课程名称")
    private String courseName;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;

    /** 时长 */
    @ApiModelProperty(value = "时长")
    private String durationTime;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examStartTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examEndTime;

    /** 考试人数 */
    @ApiModelProperty(value = "考试人数")
    private Integer studentCount;

}
