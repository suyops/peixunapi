package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/10/30/18:49
 * @Version 1.0
 * @Description:
 */
@Data
public class SignUpStudentListVO {

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "序号")
    private Integer sort;

    @ApiModelProperty(value = "报名时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date signUpTime;


}
