package com.gibs.peixunapi.VO.show;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/08/24/16:58
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "练习题VO类")
public class ExerciseVO {

    /** 练习题id */
    @ApiModelProperty(value = "练习题id")
    private Integer id;
    /** 课程_题库表id */
    @ApiModelProperty(value = "课程_题库表id")
    private Integer courseSubjectHubId;
    /** 排序 */
    @ApiModelProperty(value = "排序")
    private Integer sort;
}
