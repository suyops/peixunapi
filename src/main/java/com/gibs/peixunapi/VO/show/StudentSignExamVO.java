package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/10/10/19:51
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("考试班级-查看考生列表")
public class StudentSignExamVO {
    /** id */
    @ApiModelProperty("id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty("序号")
    private Integer sort;

    /** 学生姓名 */
    @ApiModelProperty("学生姓名")
    private String username;

    /** 企业名称 */
    @ApiModelProperty("企业名称")
    private String businessName;

    /** 文档学习进度 */
    @ApiModelProperty("文档学习进度")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal documentProgress;

    /** 视频学习进度 */
    @ApiModelProperty("视频学习进度")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal videoProgress;


}
