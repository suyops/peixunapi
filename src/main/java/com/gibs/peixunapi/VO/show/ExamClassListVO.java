package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gibs.peixunapi.model.ExamInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/09/18/10:15
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "班级列表VO")
public class ExamClassListVO {

    /** id */
    @ApiModelProperty(value = "id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 课程名称 */
    @ApiModelProperty(value = "课程名称")
    private String courseName;
    
    /** 课程id */
    @ApiModelProperty(value = "课程id")
    private Integer courseId;
    /**
     * 笔试成绩比重
     */
    @ApiModelProperty(value = "笔试成绩比重")
    private BigDecimal proportion;
    /** 班级信息id */
    @ApiModelProperty(value = "班级信息id")
    private Integer examInfoId;

    /** 班级名称 */
    @ApiModelProperty(value = "班级名称")
    private String name;

    /** 考试开始时间 */
    @ApiModelProperty(value = "考试开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examStartTime;

    /** 考试结束时间 */
    @ApiModelProperty(value = "考试结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examEndTime;

    /** 时长 */
    @ApiModelProperty(value = "时长")
    private String duration;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private Integer examStatus;
    @ApiModelProperty(value = "考试状态 等待开始: -1 考试中: 0  考试完成: 1")
    private String examStatusStr;
    /** 负责老师 */
    @ApiModelProperty(value = "负责老师")
    private String teacherName;
}
