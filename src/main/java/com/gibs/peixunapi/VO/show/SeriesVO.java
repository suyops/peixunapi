package com.gibs.peixunapi.VO.show;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/09/16/18:10
 * @Version 1.0
 * @Description:
 */
@Data
public class SeriesVO {
    private String name;
    private Integer[] data;

    public SeriesVO() {
    }

    public SeriesVO(String name,  Integer[] data) {
        this.name = name;
        this.data = data;
    }
}
