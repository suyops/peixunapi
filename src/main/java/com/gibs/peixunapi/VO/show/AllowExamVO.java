package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/09/14/10:20
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "考试资格列表")
public class AllowExamVO {

    /** id */
    @ApiModelProperty(value = "id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 姓名 */
    @ApiModelProperty(value = "姓名")
    private String username;

    /** 企业名称 */
    @ApiModelProperty(value = "企业名称")
    private String businessName;
//
//    /** 文档学习进度 */
//    @ApiModelProperty(value = "文档学习进度")
//    @JsonSerialize(using = BigDecimalSerializerUtil.class)
//    private BigDecimal documentationProgress;
//
//    /** 视频学习进度 */
//    @ApiModelProperty(value = "视频学习进度")
//    @JsonSerialize(using = BigDecimalSerializerUtil.class)
//    private BigDecimal videoProgress;
}
