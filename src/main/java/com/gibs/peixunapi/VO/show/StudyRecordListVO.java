package com.gibs.peixunapi.VO.show;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/11/13/15:46
 * @Version 1.0
 * @Description:
 */
@Data
public class StudyRecordListVO {
    private String courseName;
    private String chapterName;
    private Integer chapterId;
    private String timeRecord;
}
