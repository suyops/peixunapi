package com.gibs.peixunapi.VO.show;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/09/11/14:12
 * @Version 1.0
 * @Description:
 */
@ApiModel(description = "用户列表VO")
@Data
public class UserListVO {
    /** id */
    @ApiModelProperty(value = "id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 姓名 */
    @ApiModelProperty(value = "姓名")
    private String username;

    /** 手机 */
    @ApiModelProperty(value = "手机")
    private String telephone;

    /** 所在企业 */
    @ApiModelProperty(value = "所在企业")
    private String businessName;

    /** 邮箱 */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private String status;

    /** 审查 */
    @ApiModelProperty(value = "审查")
    private String audit;

    /** 权限角色 */
    @ApiModelProperty(value = "权限")
    private String roleName;

}
