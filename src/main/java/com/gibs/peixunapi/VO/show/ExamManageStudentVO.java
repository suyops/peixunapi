package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/09/27/16:23
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("考试班级列表")
public class ExamManageStudentVO {
    /** 学生id */
    @ApiModelProperty(value = "学生id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 姓名 */
    @ApiModelProperty(value = "姓名")
    private String username;


    /** 准考证 */
    @ApiModelProperty(value = "准考证")
    private String uniqueCode;

    /** 单位 */
    @ApiModelProperty(value = "单位")
    private String businessName;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examStartTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examEndTime;

    /** 考试进度 */
    @ApiModelProperty(value = "考试进度")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal progress;

    /** 考试状态 */
    @ApiModelProperty(value = "考试状态")
    private String examStatusString;
}