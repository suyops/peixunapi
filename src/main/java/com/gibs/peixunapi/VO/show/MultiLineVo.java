package com.gibs.peixunapi.VO.show;

import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/16/18:12
 * @Version 1.0
 * @Description:
 */
@Data
public class MultiLineVo {
    private LegendVO legendVO;
    private XAxisVO xAxisVO;
    private List<SeriesVO> seriesVOList;

    public MultiLineVo( LegendVO legendVO, XAxisVO xAxisVO, List<SeriesVO> seriesVOList) {
        this.xAxisVO = xAxisVO;
        this.seriesVOList = seriesVOList;
        this.legendVO = legendVO;
    }

    public MultiLineVo() {
    }
}
