package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gibs.peixunapi.DTO.CommentReferenceDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/10/21/14:47
 * @Version 1.0
 * @Description:
 */
@Data
public class ShowCommentVO {
    /** id */
    @ApiModelProperty("id")
    private Integer id;

    /** 评论人 */
    @ApiModelProperty("评论人")
    private String username;

    /** 头像 */
    @ApiModelProperty("头像")
    private String avatarUrl;

    /** 评论时间 */
    @ApiModelProperty("评论时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date createTime;

    /** 评论内容 */
    @ApiModelProperty("评论内容")
    private String content;

    /** 引用实体 */
    @ApiModelProperty("引用实体")
    private CommentReferenceDTO referenceDTO;
}
