package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/10/22/16:22
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("")
public class CourseStudyVO {
    private Integer sort;
    private String username;
    private String businessName;
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal videoProgress;
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal documentProgress;
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal rightRate;
    private Integer countQuestion;
    private Integer examScores;
}
