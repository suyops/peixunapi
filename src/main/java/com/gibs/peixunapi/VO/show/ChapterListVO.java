package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/10/09/13:00
 * @Version 1.0
 * @Description:
 */
@Data
public class ChapterListVO {

    /** 章节id */
    @ApiModelProperty(value = "章节id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;

    /** 本章节学习进度 */
    @ApiModelProperty(value = "本章节学习进度")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal studyProgress;

    /** 本章节完成练习题数 */
    @ApiModelProperty(value = "本章节完成练习题数")
    private Integer finishExerciseCount;

    /** 本章节练习题数 */
    @ApiModelProperty(value = "本章节练习题数")
    private Integer exerciseCount;

    /** 图片 */
    @ApiModelProperty(value = "图片",hidden = true)
    private String picture;

    /** 是否删除 */
    @ApiModelProperty(value = "是否删除", hidden = true)
    private Boolean deleted;

    /**
     * 是否上架 下架:0 上架:1
     */
    @ApiModelProperty(value = "是否上架 ")
    private Integer isPublish;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private Integer status;

    public ChapterListVO() {
        this.studyProgress = BigDecimal.valueOf(0d);
        this.finishExerciseCount = 0;
        this.exerciseCount = 0;

    }
}
