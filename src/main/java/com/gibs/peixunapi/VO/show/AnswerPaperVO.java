package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/23/10:42
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("答卷")
public class AnswerPaperVO {
    /**
     * 准考证号
     */
    @ApiModelProperty(value = "准考证号")
    private String uniqueCode;

    /**
     * 考生名
     */
    @ApiModelProperty(value = "考生名")
    private String username;

    /**
     * 试卷总分
     */
    @ApiModelProperty(value = "试卷总分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal totalScore;

    /**
     * 单选题列表
     */
    @ApiModelProperty(value = "单选题列表")
    private List<AnswerSheetVO> radioList;

    /**
     * 单选题得分
     */
    @ApiModelProperty(value = "单选题得分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal radioScore;

    /**
     * 单选题得分
     */
    @ApiModelProperty(value = "单选题总分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal radioTotalScore;

    /**
     * 多选题列表
     */
    @ApiModelProperty(value = "多选题列表")
    private List<AnswerSheetVO> checkList;

    /**
     * 多选题得分
     */
    @ApiModelProperty(value = "多选题得分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal checkScore;

    /**
     * 多选题得分
     */
    @ApiModelProperty(value = "多选题总分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal checkTotalScore;

    /**
     * 判断题列表
     */
    @ApiModelProperty(value = "判断题列表")
    private List<AnswerSheetVO> judgeList;

    /**
     * 判断题得分
     */
    @ApiModelProperty(value = "判断题得分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal judgeScore;

    /**
     * 判断题得分
     */
    @ApiModelProperty(value = "判断题得分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal judgeTotalScore;

    /**
     * 填空题列表
     */
    @ApiModelProperty(value = "填空题列表")
    private List<AnswerSheetVO> fillList;

    /**
     * 填空题得分
     */
    @ApiModelProperty(value = "填空题得分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal fillScore;

    /**
     * 填空题得分
     */
    @ApiModelProperty(value = "填空题得分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal fillTotalScore;

    /**
     * 问答题列表
     */
    @ApiModelProperty(value = "问答题列表")
    private List<AnswerSheetVO> questionList;

    /**
     * 问答题得分
     */
    @ApiModelProperty(value = "问答题得分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal questionScore;

    /**
     * 问答题得分
     */
    @ApiModelProperty(value = "问答题得分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal questionTotalScore;

    public AnswerPaperVO(String uniqueCode, List<AnswerSheetVO> radioList, List<AnswerSheetVO> checkList, List<AnswerSheetVO> judgeList, List<AnswerSheetVO> fillList, List<AnswerSheetVO> questionList) {
        this.uniqueCode = uniqueCode;
        this.radioList = radioList;
        this.checkList = checkList;
        this.judgeList = judgeList;
        this.fillList = fillList;
        this.questionList = questionList;
    }

    public AnswerPaperVO() {
        this.uniqueCode = "";
        this.radioList = new ArrayList<>();
        this.checkList = new ArrayList<>();
        this.judgeList = new ArrayList<>();
        this.fillList = new ArrayList<>();
        this.questionList = new ArrayList<>();
        BigDecimal zero = new BigDecimal(0);
        this.radioScore = zero;
        this.checkScore = zero;
        this.judgeScore = zero;
        this.fillScore = zero;
        this.questionScore = zero;
        this.radioTotalScore = zero;
        this.checkTotalScore = zero;
        this.judgeTotalScore = zero;
        this.fillTotalScore = zero;
        this.questionTotalScore = zero;
    }
}
