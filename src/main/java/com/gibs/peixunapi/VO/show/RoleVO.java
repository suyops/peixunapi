package com.gibs.peixunapi.VO.show;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/08/21/15:11
 * @Version 1.0
 * @Description: 角色VO
 */
@Data
public class RoleVO {

    private String name;     //名称

    private String code;     //编码

    private Integer sort;     //排序
}
