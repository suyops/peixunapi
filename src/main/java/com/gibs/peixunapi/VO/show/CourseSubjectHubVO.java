package com.gibs.peixunapi.VO.show;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/08/28/14:42
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "课程题库接收类")
public class CourseSubjectHubVO {
    /** 课程_题库id */
    @ApiModelProperty(value = "课程_题库id")
    private Integer id;

    /** 题库id */
    @ApiModelProperty(value = "题库id")
    private Integer subjectHubId;

    /** 课程id */
    @ApiModelProperty(value = "课程id")
    private Integer courseId;

    /** 排序 */
    @ApiModelProperty(value = "排序")
    private Integer sort;
}
