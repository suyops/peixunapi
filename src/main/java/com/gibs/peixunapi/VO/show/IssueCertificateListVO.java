package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
/**
 * @author liangjiawei
 * @date 2020/10/05/9:06
 * @Version 1.0
 * @Description: 证书发放列表VO
 */

@Data
public class IssueCertificateListVO {
    /** 班级id */
    @ApiModelProperty(value = "班级id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 课程名称 */
    @ApiModelProperty(value = "课程名称")
    private String courseName;

    /** 班级名称 */
    @ApiModelProperty(value = "班级名称")
    private String name;
    
    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examStartTime;

    /** 培训人数 */
    @ApiModelProperty(value = "培训人数")
    private Integer signUpCount;

    /** 考试人数 */
    @ApiModelProperty(value = "考试人数")
    private Integer candidateCount;

    /** 通过 */
    @ApiModelProperty(value = "通过")
    private Integer passCount;
}
