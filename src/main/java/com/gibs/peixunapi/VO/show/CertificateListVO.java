package com.gibs.peixunapi.VO.show;

import lombok.Data;
/**
 * @author liangjiawei
 * @date 2020/10/04/18:12
 * @Version 1.0
 * @Description:
 */

@Data
public class CertificateListVO {

    /** id */
    private Integer id;

    /** 序号*/
    private Integer sort;

    /**名称*/
    private String name;

    /**证书样式*/
    private String url;
}
