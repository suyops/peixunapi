package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gibs.peixunapi.model.Certificate;
import com.gibs.peixunapi.model.ExamStudent;
import com.gibs.peixunapi.model.FileInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/10/10/20:47
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("个人证书")
public class PersonCertificateVO {

        /** id */
        @ApiModelProperty(value = "id(非必填)", example = "1")
        private Integer id;

        /**名称*/
        @ApiModelProperty(value = "名称",example = "xxx证书")
        private String name;

        /**课程名称*/
        @ApiModelProperty(value = "课程名称",example = "xxx证书")
        private String courseName;

        /**文件*/
        @ApiModelProperty(value = "文件id",example = "1")
        private FileInfo fileId;

        /** 继续教育课时 */
        @ApiModelProperty(" 继续教育课时 ")
        private Integer continueHours;

        /**证书*/
        @ApiModelProperty("证书")
        private FileInfo fileInfo;

        /** 学生id */
        @ApiModelProperty(" 学生id ")
        private Integer userId;

        /** 证书编码 */
        @ApiModelProperty(" 证书编码 ")
        private String uniqueCode;

        /** 到期时间 */
        @ApiModelProperty(" 到期时间 ")
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
        private Date expiryDate;

        /** 到期时间 */
        @ApiModelProperty(" 到期时间 ")
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
        private Date createTime;

        /**有效期*/
        @ApiModelProperty("有效期")
        private Integer validTime;

        /**剩余天数*/
        @ApiModelProperty("剩余天数")
        private Integer expiryDay;

        /** 暂停状态 */
        @ApiModelProperty("暂停状态:开始暂停:1 关闭暂停:0")
        private Integer pause;

        /** 吊销状态 */
        @ApiModelProperty("吊销状态: 证书激活:1 证书吊销:0")
        private Integer status;

        /** 申请状态 未申请:0  已申请:1 */
        @ApiModelProperty("申请状态 未申请:0  已申请:1")
        private Integer applyStatus;
}
