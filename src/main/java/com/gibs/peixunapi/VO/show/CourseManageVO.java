package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.PrePersist;
import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/09/27/16:23
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("课程管理列表")
public class CourseManageVO {
    /** 课程id */
    @ApiModelProperty(value = "课程id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;

    /** 课时 */
    @ApiModelProperty(value = "课时")
    private Integer hour;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date startTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date endTime;

    /** 文档课程数量 */
    @ApiModelProperty(value = "文档课程数量")
    private Integer fileCount;

    /** 视频课程数量 */
    @ApiModelProperty(value = "视频课程数量")
    private Integer videoCount;

    /** 订阅 */
    @ApiModelProperty(value = "订阅")
    private Integer isSubscribe;

    /** 订阅人数 */
    @ApiModelProperty(value = "订阅人数")
    private Integer subscribeCount;

    /** 报名人数 */
    @ApiModelProperty(value = "报名人数")
    private Integer signUpCount;

    /** 班级数量 */
    @ApiModelProperty(value = "班级数量")
    private Integer classNumber;

    /** 审查状态 */
    @ApiModelProperty(value = "审查状态")
    private Integer audit;

    /** 删除状态 */
    @ApiModelProperty(value = "删除状态")
    private Integer isDelete;

    /** 负责老师 */
    @ApiModelProperty(value = "负责老师")
    private String teacherName;

    public CourseManageVO() {
        subscribeCount = 0;
        signUpCount = 0;
        classNumber = 0;
    }

    public CourseManageVO(Integer id, Integer sort, String name, Integer hour, Date startTime, Date endTime, Integer fileCount, Integer videoCount, Integer isSubscribe, Integer subscribeCount, Integer signUpCount, Integer classNumber, Integer audit, String teacherName) {
        this.id = id;
        this.sort = sort;
        this.name = name;
        this.hour = hour;
        this.startTime = startTime;
        this.endTime = endTime;
        this.fileCount = fileCount;
        this.videoCount = videoCount;
        this.isSubscribe = isSubscribe;
        this.subscribeCount = subscribeCount;
        this.signUpCount = signUpCount;
        this.classNumber = classNumber;
        this.audit = audit;
        this.teacherName = teacherName;
    }


}
