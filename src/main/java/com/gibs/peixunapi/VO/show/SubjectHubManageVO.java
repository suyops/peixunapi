package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/10/10/17:05
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("题库管理")
public class SubjectHubManageVO {

    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("序号")
    private Integer sort;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date startTime;

    @ApiModelProperty("结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date endTime;

    @ApiModelProperty("视频数量")
    private Integer videoCount;

    @ApiModelProperty("文档数量")
    private Integer fileCount;

    @JsonIgnore
    private Integer audit;

    @ApiModelProperty("状态")
    private String status;
}
