package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.model.ExamInfo;

import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/22/9:22
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("考试班级信息")
public class ShowExamClassVO {
    /** id */
    @ApiModelProperty(value = "id",example = "1")
    private Integer id;

    /** 名称 */
    @ApiModelProperty(value = "名称",example = "a班")
    private String name;

    /** 班级人数 */
    @ApiModelProperty(value = "班级人数",example = "30")
    private Integer number;
    
    /** 培训课程名称 */
    @ApiModelProperty(value = "培训课程名称",example = "xxxx培训考试")
    private String courseName;
    
    /** 考试名称 */
    @ApiModelProperty(value = "考试名称",example = "xxxx培训考试")
    private String examInfoName;
    
    /** 考试对象 */
    @ApiModelProperty(value = "考试对象",example = "")
    private ExamInfo examInfo;

    /** 笔试成绩比重 */
    @ApiModelProperty(value = "笔试成绩比重",example = "0.6")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal proportion;

    /** 考试开始时间 */
    @ApiModelProperty(value = "考试开始时间",example = "2020-04-19 12:00")
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examStartTime;

    /** 考试结束时间 */
    @ApiModelProperty(value = "考试结束时间",example = "2020-04-19 14:00")
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examEndTime;

    /** 阅卷开始时间 */
    @ApiModelProperty(value = "阅卷开始时间",example = "2020-04-25 6:00")
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date correctStartTime;

    /** 阅卷结束时间 */
    @ApiModelProperty(value = "阅卷结束时间",example = "2020-04-30 12:00")
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date correctEndTime ;

    /** 成绩公布时间 */
    @ApiModelProperty(value = "成绩公布时间",example = "2020-04-30 14:00")
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date announceResultTime;

    /** 监考老师列表  */
    @ApiModelProperty(value = "监考老师列表")
    private List<OptionVO> manageList;

    /** 阅卷老师列表 */
    @ApiModelProperty(value = "阅卷老师列表")
    private List<OptionVO> correctList;

    /** 考场校验码 */
    @ApiModelProperty(value = "考场校验码",example = "1s2df15d")
    private String code;

    /** 考试状态 等待开始: -1 考试中: 0 阅卷中: 1 考试完成: 2*/
    @ApiModelProperty(value = "考试状态 等待开始: -1 考试中: 0 阅卷中: 1 考试完成: 2",example = "-1")
    private Integer examStatus;

}
