package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/10/23/14:21
 * @Version 1.0
 * @Description:
 */
@Data
public class ShowNoticeVO {
    private Integer id;

    /** 标题 */
    private String title;

    /** 发布人 */
    private Integer sender;

    /** 发布人 */
    private String senderName;

    /** 接收人 */
    private String accepterName;

    /** 发布时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date sendDate;

    /** 内容 */
    private String content;

    /** 链接 */
    private String link;

    /** 查看时间 */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date readTime;

    /** 查看状态 */
    private Integer read;
}
