package com.gibs.peixunapi.VO.show;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/09/16/18:08
 * @Version 1.0
 * @Description:
 */
@Data
public class XAxisVO {
    private String[] data;

    public XAxisVO() {
    }

    public XAxisVO(String[] data) {
        this.data = data;
    }
}
