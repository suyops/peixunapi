package com.gibs.peixunapi.VO.show;

import com.gibs.peixunapi.model.FileInfo;
import lombok.Data;

@Data
public class ShowCertificateVO {

    /** id */
    private Integer id;

    /**名称*/
    private String name;

    /**证书样式*/
    private FileInfo fileInfo;

    /**有效期*/
    private Integer validTime;

}
