package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/10/09/11:00
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("成绩管理列表")
public class ScoreManageVO {

    /** id */
    @ApiModelProperty("id")
    private Integer id;

    /** 排序 */
    @ApiModelProperty("排序")
    private Integer sort;

    /** 姓名 */
    @ApiModelProperty("姓名")
    private String username;

    /** 公司名称 */
    @ApiModelProperty("公司名称")
    private String businessName;

    /** 日常成绩 */
    @ApiModelProperty("日常成绩")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal dailyScore;

    /** 考试成绩 */
    @ApiModelProperty("考试成绩")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal examScores;

    /** 笔试占比 */
    @ApiModelProperty("笔试占比")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal proportion;
}
