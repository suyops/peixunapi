package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/10/10/9:04
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("证书管理")
public class CertificateManageVO {

    /** id */
    @ApiModelProperty("id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty("序号")
    private Integer sort;

    /** 姓名 */
    @ApiModelProperty("姓名")
    private String username;

    /** 单位名称 */
    @ApiModelProperty("单位名称")
    private String businessName;

    /** 证书编号 */
    @ApiModelProperty("证书编号")
    private String uniqueCode;

    /** 获取时间 */
    @ApiModelProperty("获取时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date createTime;

    /** 到期时间 */
    @ApiModelProperty("到期时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date expiryDate;

    /** 有效期 */
    @ApiModelProperty("有效期(天)")
    @JsonIgnore
    private Integer valid_time;

    /** 有效期 */
    @ApiModelProperty("有效期(天)")
    private String validTime;

    /** 剩余天数 */
    @ApiModelProperty("剩余天数")
    private Integer remainDay;

    /** 继续教育课时 */
    @ApiModelProperty("继续教育课时")
    private Integer continueHours;

    /** 暂停状态 */
    @ApiModelProperty("暂停状态:暂停:0  正常:1")
    private Integer pause;

    /** 吊销状态 */
    @ApiModelProperty("吊销状态: 吊销:0  正常:1")
    private Integer status;

    /** 申请状态 未申请:0  已申请:1 */
    @ApiModelProperty("申请状态 申请:0  正常:1")
    private Integer applyStatus;

}
