package com.gibs.peixunapi.VO.show;

import com.gibs.peixunapi.DTO.ShowExerciseRecordDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/01/10:46
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("练习题记录")
public class ShowExerciseRecordVO {
    @ApiModelProperty("课程id")
    private Integer courseId;
    @ApiModelProperty("课程名称")
    private String courseName;
    @ApiModelProperty("学生id")
    private Integer studentId;
    @ApiModelProperty("练习记录")
    private List<ShowExerciseRecordDTO> recordDTOList;
}
