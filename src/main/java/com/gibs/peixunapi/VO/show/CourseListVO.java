package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/09/09/18:19
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "课程列表VO")
public class CourseListVO {

    /** 课程id */
    @ApiModelProperty(value = "课程id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd" )
    private Date startTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd" )
    private Date endTime;

    /** 文档培训数量 */
    @ApiModelProperty(value = "文档培训数量")
    private Integer fileCount;

    /** 视频培训数量 */
    @ApiModelProperty(value = "视频培训数量")
    private Integer videoCount;
//
//    /** 订阅人数 */
//    @ApiModelProperty(value = "订阅人数")
//    private Integer subscribeCount;

    /** 报名人数 */
    @ApiModelProperty(value = "报名人数")
    private Integer signUpCount;
    /**
     * 是否删除
     */
    @ApiModelProperty(value = "是否删除")
    private boolean deleted;

    /**
     * 是否上架 下架:0 上架:1
     */
    @ApiModelProperty(value = "是否上架 ")
    private Integer isPublish;

    /** 负责老师 */
    @ApiModelProperty(value = "负责老师")
    private String teacherName;

}
