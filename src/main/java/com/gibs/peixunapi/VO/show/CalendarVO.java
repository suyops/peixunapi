package com.gibs.peixunapi.VO.show;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/11/17/10:53
 * @Version 1.0
 * @Description:
 */
@Data
public class CalendarVO {
    private List<LocalDate> examDate;

    private List<LocalDate> studyDate;
}
