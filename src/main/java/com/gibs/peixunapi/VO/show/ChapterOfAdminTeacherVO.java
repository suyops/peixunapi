package com.gibs.peixunapi.VO.show;

import com.gibs.peixunapi.VO.post.ChapterVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/24/11:57
 * @Version 1.0
 * @Description:
 */
@ApiModel("学习中心列表 ")
@Data
public class ChapterOfAdminTeacherVO {
    /** 课程id */
    @ApiModelProperty(value = " 课程id")
    private Integer courseId;

    /** 课程名称 */
    @ApiModelProperty(value = " 课程名称")
    private String name;

    /** 是否删除 */
    @ApiModelProperty(value = "是否删除", hidden = true)
    private Boolean isDelete;

    /**
     * 是否上架 下架:0 上架:1
     */
    @ApiModelProperty(value = "是否上架 ")
    private Integer isPublish;
    /** 负责老师 */
    @ApiModelProperty(value = " 负责老师")
    private String teacherName;

    /** 章节信息列表 */
    @ApiModelProperty(value = " 章节信息列表")
    private List<ChapterListVO> chapterList;

}
