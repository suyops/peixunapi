package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.model.FileInfo;
import com.gibs.peixunapi.model.Video;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/10/10/14:27
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(value = "章节信息类22",description = "")
public class ShowChapterVO {
    /** 课程id */
    @ApiModelProperty(value = "课程id")
    private Integer courseId;

    /** 章节id */
    @ApiModelProperty(value = "章节id")
    private Integer chapterId;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;

    /** 简介 */
    @ApiModelProperty(value = "简介")
    private String summary;

    /** 本章节学习进度 */
    @ApiModelProperty(value = "本章节学习进度")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal progress;

    /** 图片 */
    @ApiModelProperty(value = "图片")
    private String picture;

    /**
     * 是否上架 下架:0 上架:1
     */
    @ApiModelProperty(value = "是否上架 ")
    private Integer isPublish;

    /**
     * 是否删除
     */
    @ApiModelProperty(value = "delete", notes = "是否删除", hidden = true)
    private Boolean deleted;

    //*******************视频**********************//
    /** 视频 */
    @ApiModelProperty(value = "video", notes ="视频")
    private Video video;

    /** 已看时长 */
    @ApiModelProperty(value = "已看时长")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal timeRecord;

    //*******************文档**********************//
    /** 文件 */
    @ApiModelProperty(value = "附件id")
    private FileInfo fileInfo;
}
