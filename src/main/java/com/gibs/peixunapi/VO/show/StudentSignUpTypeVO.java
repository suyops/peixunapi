package com.gibs.peixunapi.VO.show;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/10/13/23:31
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("管理员-报名课程-学生选项列表")
public class StudentSignUpTypeVO {
    private Integer id;
    private Integer sort;
    private String username;
    private String businessName;
    private Integer signUp;
    private String signUpStatus;
}
