package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gibs.peixunapi.VO.post.BusinessVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/08/21/15:02
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "用户信息VO")
public class ShowUserVo {
    /** id */
    @ApiModelProperty(value = "id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 用户 */
    @ApiModelProperty(value = "用户")
    private String username;

    /** 手机 */
    @ApiModelProperty(value = "手机")
    private String telephone;

    /** 微信登录名 */
    @ApiModelProperty(value = "微信登录名")
    private String weChatName;

    /** 登录时间 */
    @ApiModelProperty(value = "登录时间")
    private String loginTime;
    /** 所在企业 */
    @ApiModelProperty(value = "所在企业")
    private BusinessVO business;


    /** 企业邀请码 */
    @ApiModelProperty(value = "企业邀请码")
    @JsonIgnore
    private String invitedCode;

    /** 证件类型 */
    @ApiModelProperty(value = "证件类型")
    private String certificateType;

    /** 证件编号 */
    @ApiModelProperty(value = "证件编号")
    private String certificateNumber;

    /** 邮箱 */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /** 性别 */
    @ApiModelProperty(value = "性别 男:1 女0")
    private Integer sex;

    /** 审核状态 */
    @ApiModelProperty(value = "审核状态")
    private Integer audit;

    /** 权限角色 */
    @ApiModelProperty(value = "权限角色")
    private List<RoleVO> roleVOList;







}
