package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gibs.peixunapi.model.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/10/13/10:22
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("展示课程VO")
public class ShowCourseInfoVO {
    /** 课程id */
    @ApiModelProperty(value = "课程id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 课程名称 */
    @ApiModelProperty(value = "课程名称")
    private String name;

    /** 简介 */
    @ApiModelProperty(value = "简介")
    private String summary;

    /** 课程课时 */
    @ApiModelProperty(value = "课程课时")
    private Integer hours;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date startTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date endTime;
//
//    /** 截止时间 */
//    @ApiModelProperty(value = "截止时间")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
//    private Date invalidTime;

    /** 课程标签 */
    @ApiModelProperty(value = "课程标签")
    private String type;

    /** 负责(主教老师) */
    @ApiModelProperty(value = "主教老师", name = "teacher")
    private OptionVO teacher;

    /** 助教老师 */
    @ApiModelProperty(value = "助教老师", name = "assistTeacherList")
    private List<OptionVO> assistTeacherList;

    /** 此课程学习状态 */
    @ApiModelProperty(value = "此课程学习状态")
    private String studyStatus;

    /** 文档培训数量 */
    @ApiModelProperty(value = "文档培训数量")
    private Integer fileCount;

    /** 视频培训数量 */
    @ApiModelProperty(value = "视频培训数量")
    private Integer videoCount;
//
//    /** 订阅时间 */
//    @ApiModelProperty(value = "订阅时间", hidden = true)
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
//    private Date subscribeTime;

    /** 报名时间 */
    @ApiModelProperty(value = "报名时间", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date signUpTime;

//    /** 审核状态 */
//    @ApiModelProperty(value = "审查状态", hidden = true)
//    private Integer audit;
//
//    /** 审核状态(中文) */
//    @ApiModelProperty(value = "审核状态(中文) ", hidden = true)
//    private String auditStatus;
//
//    /** 状态 未订阅:false 已订阅:true */
//    @ApiModelProperty(value = "状态 未订阅:false 已订阅:true", hidden = true)
//    private Boolean subscribe;
//
//    /** 状态 未报名:false 已报名:true */
//    @ApiModelProperty(value = "状态 未报名:false 已报名:true", hidden = true)
//    private Boolean signUp;

    /**
     * 是否删除
     */
    @ApiModelProperty(value = "是否删除")
    private boolean deleted;

    /**
     * 是否上架 下架:0 上架:1
     */
    @ApiModelProperty(value = "是否上架 ")
    private Integer isPublish;
}
