package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/09/24/19:41
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "等待阅卷的考试班级信息VO")
public class CorrectManageVO {
    /** id */
    @ApiModelProperty(value = "id")
    private Integer id;
    
    /** 班级信息id */
    @ApiModelProperty(value = "班级信息id")
    private Integer examInfoId;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 课程名称 */
    @ApiModelProperty(value = "课程名称")
    private String courseName;

    /** 班级名称 */
    @ApiModelProperty(value = "班级名称")
    private String name;

    /** 考试开始时间 */
    @ApiModelProperty(value = "考试开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examStartTime;

    /** 考试结束时间 */
    @ApiModelProperty(value = "考试结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date examEndTime;

    /** 班级人数 */
    @ApiModelProperty(value = "班级人数")
    private Integer number;

    /** 参加考试人数 */
    @ApiModelProperty(value = "参加考试人数")
    private Integer examNum;

    /** 老师名称 */
    @ApiModelProperty(value = "老师名称")
    private String teacherName;

    /** 阅卷状态 */
    @ApiModelProperty(value = "阅卷状态")
    private Integer correctStatus;

    /** 阅卷状态 */
    @ApiModelProperty(value = "阅卷状态")
    private String correctStatusString;
}
