package com.gibs.peixunapi.VO.show;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("试卷列表VO")
public class ShowTestPaperListVO {
	@ApiModelProperty("id")
	private Integer id;
	
	@ApiModelProperty("序号")
	private Integer sort;
	
	@ApiModelProperty("名称")
	private String name;
	
	@ApiModelProperty("创建时间")
	private Date createTime;
	
	@ApiModelProperty("题目数量")
	private Integer subjectHubCount;
	
	@ApiModelProperty("总分")
	@JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal totalScore;
	
	@ApiModelProperty("创建人")
    private String creatorName;
}
