package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/09/09/19:04
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "考试管理VO")
public class ExamCourseVO {
    /** 课程id */
    @ApiModelProperty(value = "课程id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date startTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date endTime;

    /** 课程报名人数 */
    @ApiModelProperty(value = "课程报名人数")
    private Integer signUpCount;

    /** 有考试资格人数 */
    @ApiModelProperty(value = "有考试资格人数")
    private Integer allowExamCount;

    /** 已考场次 */
    @ApiModelProperty(value = "已考场次")
    private Integer examCount;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private String status;

    /** 负责教师 */
    @ApiModelProperty(value = "负责教师")
    private String teacherName;
}
