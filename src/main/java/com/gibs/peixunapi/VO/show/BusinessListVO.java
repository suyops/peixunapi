package com.gibs.peixunapi.VO.show;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/09/15/17:42
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "企业信息列表VO")
public class BusinessListVO {
    /** id */
    @ApiModelProperty(value = "id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;

    /** 管理员 */
    @ApiModelProperty(value = "管理员")
    private String managerName;
//
//    /** 邀请码 */
//    @ApiModelProperty(value = "邀请码")
//    private String invitedCode;

    /** 注册人数 */
    @ApiModelProperty(value = "注册人数")
    private Integer number;

    /** 状态 */
    @ApiModelProperty(value = "状态 启用:1 禁用:0")
    private String status;
}
