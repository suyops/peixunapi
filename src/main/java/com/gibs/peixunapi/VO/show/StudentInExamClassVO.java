package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/09/21/11:17
 * @Version 1.0
 * @Description: 考试班级-查看考生列表
 */
@Data
@ApiModel("考试班级-查看考生列表")
public class StudentInExamClassVO {
    /** id */
    @ApiModelProperty("id")
    private Integer id;

    /** 序号 */
    @ApiModelProperty("序号")
    private Integer sort;

    /** 学生姓名 */
    @ApiModelProperty("学生姓名")
    private String username;

    /** 考试分数 */
    @ApiModelProperty("考试分数")
    private String scores;
    
    /** 单位名称 */
    @ApiModelProperty("单位名称")
    private String businessName;

    /** 准考证 */
    @ApiModelProperty("准考证")
    private String uniqueCode;

    /** 考试码 */
    @ApiModelProperty("考试码")
    private String code;

    /** 文档学习进度 */
    @ApiModelProperty("文档学习进度")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal documentProgress;

    /** 视频学习进度 */
    @ApiModelProperty("视频学习进度")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal videoProgress;

    /** 考试完成状态 未考试:0 已完成:1 */
    @ApiModelProperty("考试完成状态 未考试:0 已完成:1")
    private String examStatusString;

    private Integer examStatus;

    /** 阅卷完成状态:未批改:-1 批改中: 0 已批改:1 */
    @ApiModelProperty("阅卷完成状态:未批改:-1 批改中: 0 已批改:1")
    private String correctStatusString;

    private Integer correctStatus;


    /** 申请缓考  未通过：-1 未申请：0 已申请:1 已通过：2 */
    @ApiModelProperty(value = "申请缓考 通过：-1 未申请：0 已申请:1 已通过：2 ")
    private String applyDelayedExamString;

    private Integer applyDelayedExam;

}
