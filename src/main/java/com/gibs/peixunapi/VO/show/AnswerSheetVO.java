package com.gibs.peixunapi.VO.show;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/09/23/10:57
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "答卷题目展示VO")
public class AnswerSheetVO {
    /** 题目VO */
    @ApiModelProperty(value = "题目VO")
    private SubjectHubVO subjectHubVO;

    /** 分数 */
    @ApiModelProperty(value = "分数")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal score;

    /** 考生答案 */
    @ApiModelProperty(value = "考生答案")
    private String myAnswer;

    /** id */
    @ApiModelProperty(value = "id")
    private Integer id;
}
