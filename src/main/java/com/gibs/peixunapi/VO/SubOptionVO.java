package com.gibs.peixunapi.VO;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/08/24/15:42
 * @Version 1.0
 * @Description:
 */
@Data
public class SubOptionVO {
    private Integer id;
    /** 选项内容 */
    private String content;
    /** 选项值 */
    private String value;
    /** 排序 */
    private Integer sort;
}

