package com.gibs.peixunapi.VO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/08/24/15:20
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "题目VO类")
public class SubjectHubVO {

    /** 题目id */
    @ApiModelProperty(value = "题目id")
    private Integer id;

    /** 试卷题目id */
    @ApiModelProperty(value = "试卷题目id")
    private Integer testPaperSubjectHubId;

    /** 练习题id */
    @ApiModelProperty(value = "练习题id")
    private Integer exercisesId;

    /** 排序 */
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /** 类型 */
    @ApiModelProperty(value = "类型")
    private String type;

    /** 题目内容 */
    @ApiModelProperty(value = "题目内容")
    private String content;

    /** 答案 */
    @ApiModelProperty(value = "答案")
    private String answer;

    /** 填空个数 */
    @ApiModelProperty(value = "填空个数")
    private Integer fillNum;

    /** 解析 */
    @ApiModelProperty(value = "解析")
    private String analysis;

    /** 是否正确 */
    @ApiModelProperty(value = "是否正确")
    private Boolean isRight;

    /** 得分 */
    @ApiModelProperty(value = "得分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal score;

    /** 回答记录 */
    @ApiModelProperty(value = "回答记录")
    private String myAnswer;

    /** 选项列表 */
    @ApiModelProperty(value = "选项列表")
    private List<SubOptionVO> optionVOList;

    public SubjectHubVO() {
        this.optionVOList = new ArrayList<>();
        this.analysis = "";
    }
}
