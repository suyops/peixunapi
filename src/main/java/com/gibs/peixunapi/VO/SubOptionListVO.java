package com.gibs.peixunapi.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/08/26/16:41
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "批量新增题目选项VO类")
public class SubOptionListVO {
    /** 选项列表 */
    @ApiModelProperty(value = "选项列表")
    private List<SubOptionVO> subOptionVOList;
    /** 题目id */
    @ApiModelProperty(value = "题目id")
    private Integer subjectHubId;
}
