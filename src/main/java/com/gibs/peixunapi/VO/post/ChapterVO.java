package com.gibs.peixunapi.VO.post;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/09/03/9:37
 * @Version 1.0
 * @Description: 章节展示信息DTO类
 */
@Data
@ApiModel("Post章节信息VO")
public class ChapterVO {

    /** 课程id */
    @ApiModelProperty(value = "课程id")
    private Integer courseId;

    /** 章节id */
    @ApiModelProperty(value = "章节id")
    private Integer chapterId;

    /** 序号 */
    @ApiModelProperty(value = "序号")
    private Integer sort;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;

    /** 简介 */
    @ApiModelProperty(value = "简介")
    private String summary;

    /** 本章节学习进度 */
    @ApiModelProperty(value = "本章节学习进度")
    private Double studyProgress;

//    /** 审核 */
//    @ApiModelProperty(value = "审核")
//    private Integer audit;
    /** 是否删除 */
    @ApiModelProperty(value = "是否删除", hidden = true)
    private Boolean isDelete;

    /**
     * 是否上架 下架:0 上架:1
     */
    @ApiModelProperty(value = "是否上架 ")
    private Integer isPublish;
    //*******************视频**********************//
    /** 视频id */
    @ApiModelProperty(value = "视频id")
    private Integer videoId;

//    /** 挂机限制 */
//    @ApiModelProperty(value = "挂机限制")
//    private Boolean hangupLimit;
//
//    /** 题目插入时间 */
//    @ApiModelProperty(value = "题目插入时间")
//    private Double questionTime;
//
//    /** 练习题表id */
//    @ApiModelProperty(value = "练习题表id")
//    private Integer exerciseId;
//
//    /** id */
//    @ApiModelProperty(value = "练习题表id")
//    private Integer subjectHubId;

    //*******************文档**********************//

    /** 文件id */
    @ApiModelProperty(value = "文件id")
    private Integer fileInfoId;

//
//    /** 最少阅读时间 */
//    @ApiModelProperty(value = "本章节学习进度")
//    private Double minReadTime;
//
//    /** 最少翻页时间 */
//    @ApiModelProperty(value = "最少翻页时间")
//    private Double turnPageTime;
//
//    /** 翻页限制 */
//    @ApiModelProperty(value = "封面图id")
//    private Boolean isAllowTurnPage;


}
