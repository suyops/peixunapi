package com.gibs.peixunapi.VO.post;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/10/09/19:58
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("发放证书VO")
public class IssueCertificateVO {

    @ApiModelProperty(value = "模板id",example = "1")
    private Integer certificateId;

    @ApiModelProperty(value = "班级id",example = "1")
    private Integer examClassId;

    /**有效期*/
    @ApiModelProperty(value = "有效期",example = "365")
    private Integer validTime;

    @ApiModelProperty(value = "学生准考证List",example = "11231wfasd,2234g5f324f234d23,32f345234234c23,4234234,23f25,6dfsdg23")
    private List<String> list;

    @ApiModelProperty(value = "继续教育课时",example = "35")
    private Integer continueHours;

    @ApiModelProperty(value = "证书名称",example = "")
    private String name;

    @ApiModelProperty(value = "证书内容",example = "xxxx年xxxx月xxxxx日获得xxxxxx证书.xxxxxx")
    private String content;
}
