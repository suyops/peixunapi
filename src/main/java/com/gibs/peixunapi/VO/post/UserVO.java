package com.gibs.peixunapi.VO.post;

import com.gibs.peixunapi.model.FileInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/10/12/17:49
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "用户信息VO")
public class UserVO {
    /** id */
    @ApiModelProperty(value = "id",example ="1" )
    private Integer id;

    /** 用户 */
    @ApiModelProperty(value = "用户", required = true,example ="zhangsan")
    private String username;

    /** 岗位 */
    @ApiModelProperty(value = "岗位", required = true,example ="技术人员")
    private String post;

    /** 手机 */
    @ApiModelProperty(value = "手机", required = true,example ="15984227135")
    private String telephone;

    /** 所在企业 */
    @ApiModelProperty(value = "所在企业", example = "(非必须)嘻嘻嘻公司")
    private Integer businessId;

    /** 企业邀请码 */
    @ApiModelProperty(value = "企业邀请码", required = true, example = "ab32ac37")
    private String invitedCode;

    /** 证件类型 */
    @ApiModelProperty(value = "证件类型", required = true, example = "身份证")
    private String certificateType;

    /** 证件编号 */
    @ApiModelProperty(value = "证件编号", required = true, example = "440915202009154689")
    private String certificateNumber;

    /** 邮箱 */
    @ApiModelProperty(value = "邮箱", required = true, example = "xxx@qq.com")
    private String email;

    /** 性别 */
    @ApiModelProperty(value = "性别 男:1 女0", required = true, example = "1/0")
    private Integer sex;

    /** 角色 */
    @ApiModelProperty(value = "角色", required = true, example = "student/teacher")
    private String role;

    /** 密码 */
    @ApiModelProperty(value = "密码", required = true, example = "123456")
    private String password;

    /** 头像 */
    private Integer avatar;

    /** 审核 */
    private Integer audit;

}
