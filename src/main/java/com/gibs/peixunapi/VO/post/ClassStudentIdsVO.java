package com.gibs.peixunapi.VO.post;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/09/27/10:06
 * @Version 1.0
 * @Description: 添加考生列表VO
 */
@Data
@ApiModel(description = "添加考生列表VO")
public class ClassStudentIdsVO {
    @ApiModelProperty("学生ids,使用\",\"隔开")
    private String studentIds;
    @ApiModelProperty("考试班级id")
    private Integer examClassId;
}
