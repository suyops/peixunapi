package com.gibs.peixunapi.VO.post;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/08/31/14:40
 * @Version 1.0
 * @Description:
 */
@ApiModel(description = "学习记录VO")
@Data
public class StudyRecordVO {
    /** 章节习题id */
    @ApiModelProperty(value = "章节习题id")
    private Integer exerciseId;
    /** 练习题回答 */
    @ApiModelProperty(value = "练习题回答")
    private String myAnswer;
    /** 观看时长 */
    @ApiModelProperty(value = "观看时长")
    private Integer timeRecord;
}
