package com.gibs.peixunapi.VO.post;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/09/10/17:42
 * @Version 1.0
 * @Description:
 */
@ApiModel(description = "考试班级信息其他")
@Data
public class ExamClassVO {
    /** id */
    @ApiModelProperty(value = "id(非必填)")
    private Integer id;

    /** id */
    @ApiModelProperty(value = "课程Id")
    private Integer courseId;

    /** 名称 */
    @ApiModelProperty(value = "名称")
    private String name;

    /** 考试id */
    @ApiModelProperty(value = "考试id")
    private Integer examInfoId;

    /** 考试开始时间 */
    @ApiModelProperty(value = "考试开始时间")
    private String examStartTime;

    /** 考试开始时间 */
    @ApiModelProperty(value = "考试开始时间")
    private String examEndTime;

    /** 阅卷开始时间 */
    @ApiModelProperty(value = "阅卷开始时间")
    private String correctStartTime;

    /** 阅卷结束时间 */
    @ApiModelProperty(value = "阅卷结束时间")
    private String correctEndTime ;

    /** 成绩公布时间 */
    @ApiModelProperty(value = "成绩公布时间")
    private String announceResultTime;

    /** 监考老师id列表 使用","隔开 */
    @ApiModelProperty(value = "监考老师id列表用\",\"隔开")
    private String manageTeacher;

    /** 阅卷老师列表 使用","隔开 */
    @ApiModelProperty(value = "阅卷老师id列表用\",\"隔开")
    private String correctTeacher;

    /** 考试须知 */
    @ApiModelProperty(value = "考试须知")
    private String about;

    @ApiModelProperty(value = "总分")
    private BigDecimal totalScore;

    @ApiModelProperty(value = "合格分数")
    private BigDecimal passScore;
    @ApiModelProperty(value = "笔试成绩比重")
    private BigDecimal proportion;
    @ApiModelProperty(value = "时长")
    private String durationTime;

    @ApiModelProperty(value = "试卷id")
    private Integer testPaperId;

}
