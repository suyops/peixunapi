package com.gibs.peixunapi.VO.post;

import com.gibs.peixunapi.VO.SubjectHubVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/08/28/15:56
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "试卷编辑时接收VO类")
public class TestPaperVO {
    /** 题目详情VO类集合 */
    @ApiModelProperty(value = "题目详情VO类集合")
    private List<SubjectHubVO> subjectHubVOList;
    /** 试卷id */
    @ApiModelProperty(value = "试卷id")
    private Integer testPaperId;
    /** 考试id */
    @ApiModelProperty(value = "考试id")
    private Integer examInfoId;
}
