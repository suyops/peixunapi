package com.gibs.peixunapi.VO.post;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/09/27/10:12
 * @Version 1.0
 * @Description:
 */
@Data
public class StudentAnswerVO {
    String uniqueCode;
    Integer testPaperSubjectId;
    String studentAnswer;
}
