package com.gibs.peixunapi.VO.post;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/08/31/16:42
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "新增学习记录VO类")
public class RecordVO {
    /** 学员id */
    @ApiModelProperty(value = "学员id")
    private Integer studentId;
    /** 学习记录VO类 */
    @ApiModelProperty(value = "学习记录VO类")
    List<StudyRecordVO> recordVOList;
}
