package com.gibs.peixunapi.VO.post;

import com.gibs.peixunapi.VO.SubOptionVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/10/13/16:12
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("post题目")
public class PostSubjectHubVO {

    /** 题目id */
    @ApiModelProperty(value = "题目id")
    private Integer id;

    /** 章节id */
    @ApiModelProperty(value = "章节id")
    private Integer chapterId;

    /** 章节类型 */
    @ApiModelProperty(value = "章节类型")
    private String chapterType;

    /** 排序 */
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /** 类型 */
    @ApiModelProperty(value = "类型")
    private String type;

    /** 题目内容 */
    @ApiModelProperty(value = "题目内容")
    private String content;

    /** 答案 */
    @ApiModelProperty(value = "答案")
    private String answer;

    /** 选项列表 */
    @ApiModelProperty(value = "选项列表")
    private List<SubOptionVO> optionVOList;
}
