package com.gibs.peixunapi.VO.post;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gibs.peixunapi.model.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/01/17:13
 * @Version 1.0
 * @Description: 课程VO类
 */
@Data
@ApiModel(description = "课程VO类")
public class CourseInfoVO {
    /** 课程id */
    @ApiModelProperty(value = "课程id",example = "1")
    private Integer id;

    /** 序号 */
    @ApiModelProperty(value = "序号",example = "1")
    private Integer sort;

    /** 课程名称 */
    @ApiModelProperty(value = "课程名称",example = "a课程")
    private String name;

    /** 简介 */
    @ApiModelProperty(value = "简介",example = "a课程简介巴拉巴拉哔哩哔哩白萝卜")
    private String summary;

    /** 创建者 */
    @ApiModelProperty(value = "创建者",example = "1")
    private Integer creator;

    /** 课程课时 */
    @ApiModelProperty(value = "课程课时",example = "45")
    private Integer hours;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd" )
    private String startTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd" )
    private String endTime;
//
//    /** 截止时间 */
//    @ApiModelProperty(value = "截止时间",example = "2020-09-24 10:13:48")
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    @JsonFormat(pattern = "yyyy-MM-dd" )
//    private Date invalidTime;

    /** 课程标签 */
    @ApiModelProperty(value = "课程标签",example = "装配/计算/算法")
    private String type;

    /** 负责(主教老师) */
    @ApiModelProperty(value = "主教老师", name = "teacherId",example = "7")
//    @JsonIgnore
    private Integer teacherId;

    @ApiModelProperty(value = "主教老师", name = "teacher", hidden = true)
    private User teacher;

    /** 助教老师 */
    @ApiModelProperty(value = "助教老师", name = "assistTeacherIdList",example = "9/7,9")
    private List<Integer> assistTeachersIdList;

    @ApiModelProperty(value = "助教老师", name = "assistTeacherList",hidden = true)
    private List<User> assistTeacherList;

    /** 此课程学习状态 */
    @ApiModelProperty(value = "此课程学习状态", hidden = true)
    private String studyStatus;

    /** 文档培训数量 */
    @ApiModelProperty(value = "文档培训数量",example = "10")
    private Integer fileCount;

    /** 视频培训数量 */
    @ApiModelProperty(value = "视频培训数量",example = "10")
    private Integer videoCount;

    /** 是否删除 */
    @ApiModelProperty(value = "是否删除", hidden = true)
    private Boolean isDelete;

    /**
     * 是否上架 下架:0 上架:1
     */
    @ApiModelProperty(value = "是否上架 ")
    private Integer isPublish;
//    /** 订阅人数 */
//    @ApiModelProperty(value = "订阅人数", hidden = true)
//    private Integer subscribeCount;

    /** 报名人数 */
    @ApiModelProperty(value = "报名人数", hidden = true)
    private Integer signUpCount;

    /** 审核状态 */
    @ApiModelProperty(value = "审查状态", hidden = true)
    private Integer audit;
//
//    /** 订阅时间 */
//    @ApiModelProperty(value = "订阅时间", hidden = true)
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    @JsonFormat(pattern = "yyyy-MM-dd" )
//    private Date subscribeTime;
//
//    /** 状态 未订阅:false 已订阅:true */
//    @ApiModelProperty(value = "状态 未订阅:false 已订阅:true", hidden = true)
//    private Boolean subscribe;

    /** 报名时间 */
    @ApiModelProperty(value = "报名时间", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd" )
    private String signUpTime;

    /** 状态 未报名:false 已报名:true */
    @ApiModelProperty(value = "状态 未报名:false 已报名:true", hidden = true)
    private Boolean signUp;
}
