package com.gibs.peixunapi.VO.post;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/10/23/16:17
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("消息接收VO")
public class NoticeVO {

    /** 标题 */
	@ApiModelProperty(value = "标题")
    private String title;

    /** 发布人 */
    @ApiModelProperty(value = "发布")
    private Integer sender;

    /** 接收人 */
    @ApiModelProperty(value = "接收人 用,分隔")
    private String acceptors;

    /** 接收公司 */
    @ApiModelProperty(value = "接收公司 用,分隔")
    private String businessIds;

    /** 接收角色 */
    @ApiModelProperty(value = "接收角色 用,分隔")
    private String roles;

    /** 内容 */
    @ApiModelProperty(value = "内容")
    private String content;

    /** 链接 */
    @ApiModelProperty(value = "链接")
    private String link;

}
