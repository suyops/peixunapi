package com.gibs.peixunapi.VO.post;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/08/21/15:11
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel(description = "企业信息VO")
public class BusinessVO {

    /** id */
     @ApiModelProperty(value = "id")
    private Integer id;

     /** 名称 */
     @ApiModelProperty(value = "名称", required = true, example = "一建集团有限公司")
    private String name;

     /** 简称 */
     @ApiModelProperty(value = "简称", required = true, example = "集团")
    private String shortName;

     /** 管理员 */
     @ApiModelProperty(value = "管理员", hidden = true)
    private String managerName;

    /** 管理员id */
    @ApiModelProperty(value = "管理员id",allowEmptyValue = true)
    private Integer managerId;

//     /** 发票抬头 */
//     @ApiModelProperty(value = "发票抬头", required = true, example = "广州市建筑科学研究院有限公司")
//    private String invoiceTitle;
//
//     /** 纳税人识别号 */
//     @ApiModelProperty(value = "纳税人识别号", required = true, example = "3s1df2as1df2asd1fasdfa121321")
//    private String taxpayerNumber;
//
//     /** 开户银行 */
//     @ApiModelProperty(value = "开户银行", required = true, example = "xxx银行")
//    private String bankName;
//
//     /** 银行账号 */
//     @ApiModelProperty(value = "银行账号", required = true, example = "21321542321353543135")
//    private String bankAccount;
//
//     /** 邀请码 */
//     @ApiModelProperty(value = "邀请码" ,hidden = true)
//    private String invitedCode;

     /** 排序 */
     @ApiModelProperty(value = "排序", hidden = true)
    private Integer sort;

    /** 是否属于集团旗下 */
    @ApiModelProperty(value = "是否属于集团旗下")
    private Integer isBelongSyndicate;
}
