package com.gibs.peixunapi.VO.post;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/09/10/14:45
 * @Version 1.0
 * @Description:
 */
@ApiModel(description = "考试信息VO")
@Data
public class ExamInfoVO {

    @ApiModelProperty(value = "考试名称",example = "xxx培训考试")
    private String name;

    @ApiModelProperty(value = "课程id",example = "1")
    private Integer courseId;

    @ApiModelProperty(value = "总分",example = "100")
    private BigDecimal totalScore;

    @ApiModelProperty(value = "合格分数",example = "60")
    private BigDecimal passScore;

    @ApiModelProperty(value = "时长",example = "120")
    private String durationTime;

    @ApiModelProperty(value = "试卷id",example = "1")
    private Integer testPaperId;
}
