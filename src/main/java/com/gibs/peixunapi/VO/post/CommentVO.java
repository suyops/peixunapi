package com.gibs.peixunapi.VO.post;

import com.gibs.peixunapi.model.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/10/21/14:07
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("message")
public class CommentVO {

    /** 评论内容 */
    @ApiModelProperty("评论内容")
    private String content;

    /** 类型 讨论:1 提问:2 */
    @ApiModelProperty("类型 讨论:1 提问:2")
    private Integer type;

    /** 接收人 */
    @ApiModelProperty("接收人")
    private Integer accepter;

    /** 引用评论id */
    @ApiModelProperty("引用评论id")
    private Integer reference;
}
