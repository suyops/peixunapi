package com.gibs.peixunapi.VO.post;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/10/09/11:20
 * @Version 1.0
 * @Description: 平时分VO类
 */
@Data
@ApiModel(description = "平时分VO类")
public class
DailyScoreVO {
    @ApiModelProperty(value = "id",example = "1")
    private Integer id;

    @ApiModelProperty(value = "平时分",example = "50")
    private String dailyScore;

    @ApiModelProperty(value = "笔试成绩占比重",example = "0.7")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal proportion;
}
