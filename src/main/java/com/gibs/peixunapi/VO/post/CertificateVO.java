package com.gibs.peixunapi.VO.post;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CertificateVO {
    /** id */
    @ApiModelProperty(value = "id(非必填)", example = "1")
    private Integer id;

    /**名称*/
    @ApiModelProperty(value = "名称",example = "xxx证书")
    private String name;

    /**文件id*/
    @ApiModelProperty(value = "文件id",example = "1")
    private Integer fileId;


}
