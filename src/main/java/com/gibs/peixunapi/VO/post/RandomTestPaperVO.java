package com.gibs.peixunapi.VO.post;

import com.gibs.peixunapi.DTO.TestPaperNumberScoreDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/10/11/19:08
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("随机生成试卷")
public class RandomTestPaperVO {
    @ApiModelProperty("考试班级id")
    private Integer classId;

    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("题目类型,分值,数量")
    private List<TestPaperNumberScoreDTO> numberScore;

}
