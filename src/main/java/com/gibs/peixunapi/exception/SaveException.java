package com.gibs.peixunapi.exception;

import com.gibs.peixunapi.enums.ResultEnum;

import static com.gibs.peixunapi.enums.ResultEnum.SAVE_FAIL;

/**
 * @author liangjiawei
 * @date 2020/09/18/17:25
 * @Version 1.0
 * @Description:
 */
public class SaveException extends RuntimeException {
    private Integer code;

    private String msg;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public SaveException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
    }

    public SaveException(String locate, Exception e) {
        super(SAVE_FAIL.getMsg());
        this.code = SAVE_FAIL.getCode();
        this.msg = SAVE_FAIL.getMsg() + "  locate:" + locate + "reason:" + e.getMessage();
    }

    public SaveException(ResultEnum resultEnum, Throwable cause) {
        super(resultEnum.getCode().toString(), cause);
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg() + ":" + cause.getMessage();
    }
}
