package com.gibs.peixunapi.exception;

/**
 * @author liangjiawei
 * @date 2020/09/23/20:46
 * @Version 1.0
 * @Description:
 */
public class NoDataException extends BaseException {
    private Integer code;
    private String msg;

    @Override
    public Integer getCode() {
        return code;
    }
    @Override
    public void setCode(Integer code) {
        this.code = code;
    }
    @Override
    public String getMsg() {
        return msg;
    }
    @Override
    public void setMsg(String msg) {
        this.msg = msg;
    }

    public NoDataException() {

//        super();
            this.code = 0;

    }


    public NoDataException(String message, String msg) {
        super(message);
        this.msg = msg;
    }

    public NoDataException(String msg) {
//        super(msg);
        this.msg = msg;
    }

    public NoDataException(Throwable cause, Integer code, String msg) {
        super(msg, cause);
        this.code = code;
        this.msg = msg;
    }
}
