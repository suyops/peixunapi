package com.gibs.peixunapi.exception;

import com.gibs.peixunapi.enums.ResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.JDBCException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author liangjiawei
 * @date 2020/08/20/11:14
 * @Version 1.0
 * @Description:
 */
@Slf4j
public class BaseException extends RuntimeException {
    private Integer code;
    private String msg;
    private String threadMsg;

    public BaseException(String msg, Throwable cause) {

        this.msg = msg + "  EXCEPTION:" + cause;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getThreadMsg() {
        return threadMsg;
    }

    public BaseException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
        this.threadMsg = null;

    }

    public BaseException() {
        super();
    }

    public BaseException(ResultEnum resultEnum, String cause) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg() + "  EXCEPTION:" + cause;
        this.threadMsg = null;
    }

    public BaseException(String errorMsg) {
        super(errorMsg);
        this.msg = errorMsg;
        this.threadMsg = null;

    }

    public BaseException(ResultEnum resultEnum, StackTraceElement stackTraceElement) {
        super(resultEnum.getCode().toString());
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
        this.threadMsg = "{" + stackTraceElement.getClassName() + "}:{" + stackTraceElement.getMethodName() + "}-{" + stackTraceElement.getLineNumber() + "}";
    }
    public BaseException(ResultEnum resultEnum, Throwable e) {
this.code = resultEnum.getCode();
        if (e instanceof BaseException) {
            BaseException base = (BaseException) e;
            this.msg = resultEnum.getMsg()+"!"+base.getMsg();
        }else if (e.getCause() instanceof JDBCException){
            JDBCException sql = (JDBCException) e.getCause();
            this.msg = sql.getSQLException().getSQLState()+sql.getSQLException().getMessage();
        }
            else{
            StringWriter sw = new StringWriter();
            try (PrintWriter pw = new PrintWriter(sw)) {
                if (e.getCause() != null) {
                    e.getCause().printStackTrace(pw);
                }else{
                    e.printStackTrace(pw);
                }
                sw.toString().split("\r\n\t", 2);
                this.msg = sw.toString().split("\r\n\t", 2)[0];
            }
        }
        this.threadMsg = "{"+ Arrays.stream(e.getStackTrace()).map(StackTraceElement::toString).collect(Collectors.joining("\r\n"))+"}";
    }

}
