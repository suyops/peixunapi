package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "wrongExercises")
public class WrongExercises extends BaseModel {	//错题库
	private Integer student;     //学生id
	@ManyToOne(cascade = CascadeType.REFRESH)
	private ExerciseRecord exerciseRecode;     //练习题id


}

