package com.gibs.peixunapi.model;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/11/11/10:20
 * @Version 1.0
 * @Description:
 */
@Data
public class SheetImport {
    @ExcelProperty(index = 0)
    private Integer sort;
    @ExcelProperty(index = 1)
    private String title;
    @ExcelProperty(index = 2)
    private String answer;

    private String[] options;
}
