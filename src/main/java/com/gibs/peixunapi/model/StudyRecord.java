package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

/**
 * @author liangjiawei
 * @date 2020/09/17/9:46
 * @Version 1.0
 * @Description: 学习之路记录
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class StudyRecord extends BaseModel {

    /** 学生id */
    private Integer studentId;

    /** 课程id */
    private Integer courseId;

    /** 文档id */
    private Integer documentId;

    /** 视频课程id */
    private Integer videoClassId;

    /** 动作 */
    private String action;
}
