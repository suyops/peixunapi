package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * //证书模板
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "certificate")
public class Certificate extends BaseModel {

	/**名称*/
	private String name;

	/**证书样式*/
	@OneToOne(cascade = CascadeType.REFRESH)
	private FileInfo fileInfo;

	/**创建人*/
	private Integer creator;

	/**是否删除*/
	private Integer isDelete;

	@PrePersist
	public void create(){
		isDelete = 0;
	}
}

