package com.gibs.peixunapi.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.gibs.peixunapi.utils.UUIDUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * //证书模板_学生
 *
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "certificate_student")
public class CertificateStudent extends BaseModel {

    /**
     * 证书模板
     */
    @ManyToOne(cascade = CascadeType.REFRESH)
    private Certificate certificate;

    /**
     * 继续教育课时
     */
    private Integer continueHours;

    /**
     * 学生考试id
     */
    @OneToOne(cascade = CascadeType.REFRESH)
    private ExamStudent examStudent;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称", example = "xxx证书")
    private String name;

    /**
     * 证书
     */
    @OneToOne(cascade = CascadeType.REFRESH)
    private FileInfo fileInfo;

    /**
     * 学生id
     */
    private Integer userId;

    /**
     * 证书编码
     */
    private String uniqueCode;

    /**
     * 到期时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date expiryDate;

    /**
     * 有效期
     */
    private Integer validTime;

    /**
     * 是否到期 到期:0  正常:1
     */
    @Column(name = "is_expiry")
    private Integer expiry;

    /**
     * 是否暂停 暂停:0  正常:1
     */
    @Column(name = "is_pause")
    private Integer pause;

    /**
     * 申请状态 未申请:0  申请:1
     */
    private Integer applyStatus;

    public CertificateStudent() {
        expiry = 1;
        pause = 1;
        applyStatus = 0;
    }

    @PrePersist
    public void create() {
        uniqueCode = UUIDUtil.get12UUID() + LocalDateTime.now(ZoneOffset.of("+8")).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }
}
