package com.gibs.peixunapi.model;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author liangjiawei
 * @date 2020/09/02/9:26
 * @Version 1.0
 * @Description:
 */

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class CourseTestPaper extends BaseModel {
	private Integer courseId;
	private Integer testPaperId;
}
