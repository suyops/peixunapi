package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * 考试负责老师
 *
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "exam_teacher")
public class ExamTeacher extends BaseModel {

	/** 考试班级id */
	private Integer examClassInfoId;

	/** 老师id */
	@ManyToOne(cascade = CascadeType.REFRESH)
    private User teacher;

	/** 类型 考务老师:manage  阅卷老师: correct */
	private String type;


}

