package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

/**
 * @author liangjiawei
 * @date 2020/12/01/9:34
 * @Version 1.0
 * @Description:
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class SmsRecord extends BaseModel{
    /** 验证码信息 */
    private String message;
    /** 验证码 */
    private String code;
    /** 手机号码 */
    private String telephone;
    /** 接收者 */
    private Integer receiver;
}
