package com.gibs.peixunapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gibs.peixunapi.utils.UUIDUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 考试班级信息
 *
 * @author liangjiawei
 */
@ApiModel(description = "考试班级信息")
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "exam_class_info")
public class ExamClassInfo extends BaseModel {

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 班级人数
     */
    @ApiModelProperty(value = "班级人数")
    private Integer number;

    /**
     * 考试id
     */
    @ApiModelProperty(value = "考试id")
    @OneToOne
    private ExamInfo examInfo;

    /**
     * 考场校验码
     */
    @ApiModelProperty(value = "考场校验码")
    private String code;

    /**
     * 笔试成绩比重
     */
    @ApiModelProperty(value = "笔试成绩比重")
    private BigDecimal proportion;
//
//    /**
//     * 截止报名日期
//     */
//    @ApiModelProperty(value = "截止报名日期")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private Date signDeadLine;

    /**
     * 考试开始时间
     */
    @ApiModelProperty(value = "考试开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date examStartTime;

    /**
     * 考试结束时间
     */
    @ApiModelProperty(value = "考试结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date examEndTime;

    /**
     * 阅卷开始时间
     */
    @ApiModelProperty(value = "阅卷开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date correctStartTime;

    /**
     * 阅卷结束时间
     */
    @ApiModelProperty(value = "阅卷结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date correctEndTime;

    /**
     * 成绩公布时间
     */
    @ApiModelProperty(value = "成绩公布时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date announceResultTime;

    /**
     * 考试状态 等待开始: -1 考试中: 0  考试完成: 1
     */
    @ApiModelProperty(value = "考试状态 等待开始: -1 考试中: 0  考试完成: 1")
    private Integer examStatus;

    /**
     * 阅卷状态 等待开始: -1 阅卷中: 0 阅卷完成:1
     */
    @ApiModelProperty(value = "阅卷状态 等待开始: -1 阅卷中: 0 阅卷完成:1")
    private Integer correctStatus;

    @ApiModelProperty(value = "创建人")
    @ManyToOne(cascade = CascadeType.REFRESH)
    private User creator;

    public ExamClassInfo() {

        number = 0;
        correctStatus = -1;
        code = UUIDUtil.get8UUID().toUpperCase();
    }

    @PrePersist
    public void create() {
        setStatus(-1);
        examStatus = -1;
        correctStatus = -1;
    }
}

