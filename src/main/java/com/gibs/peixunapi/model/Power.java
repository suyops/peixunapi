package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "power")
public class Power extends BaseModel {	//权限
	private String code;     //编号
	private String name;     //名称
	private Integer sort;     //排序
	private String remark;     //备注


}

