package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "exercise_record",uniqueConstraints = @UniqueConstraint(columnNames = {"student_id","exercise_id"}))

public class ExerciseRecord extends BaseModel {	//习题记录
	@ManyToOne(cascade = CascadeType.REFRESH)
	private User student;     //学生id
	@ManyToOne(cascade = CascadeType.REFRESH)
	private Exercises exercise;     //习题id
	private String myAnswer;     //答案
	private Boolean isRight;     //是否正确
	private Boolean isKeep;     //是否收藏


}

