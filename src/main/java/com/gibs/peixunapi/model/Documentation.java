package com.gibs.peixunapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * 文档课程
 *
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "documentation")
public class Documentation extends BaseModel {
    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 简介
     */
    @ApiModelProperty(value = "简介")
    private String summary;

    /**
     * 课程id
     */
    @ApiModelProperty(value = "course", notes = "课程实体类", hidden = true)
    @ManyToOne(cascade = CascadeType.REFRESH)
    private Course course;

    /**
     * 文件
     */
    @ApiModelProperty(value = "fileInfo", notes = "文件实体类", hidden = true)
    @OneToOne(cascade = CascadeType.REFRESH)
    private FileInfo fileInfo;

    /**
     * 是否上架 下架:0 上架:1
     */
    @ApiModelProperty(value = "是否上架 ")
    private Integer isPublish;

    /**
     * 是否删除
     */
    @ApiModelProperty(value = "delete", notes = "是否删除", hidden = true)
    private Boolean deleted;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "creatorId", notes = "创建人", hidden = true)
    private Integer creatorId;
    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;

    @PrePersist
    public void create() {
        super.onCreate();
        isPublish = 1;
        deleted = false;
    }

}

