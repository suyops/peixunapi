package com.gibs.peixunapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.util.Date;

/**
 * 课程订阅
 * @author liangjiawei
 */
@ApiModel(description = "课程订阅")
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "business_sub_course")
public class  CourseBusiness extends BaseModel {

	/** 企业id */
	@ApiModelProperty(value = "企业id")
	private Integer businessId;

	/** 课程id */
	@OneToOne
	@ApiModelProperty(value = "课程id")
	private Course course;

	/** 订阅时间 */
	@ApiModelProperty(value = "订阅时间")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Date subscribeTime;

	/** 订阅人id */
	@ApiModelProperty(value = "订阅人id")
	private Integer creatorId;

	/** 未订阅:0 订阅:1 */
	@ApiModelProperty(value = "未订阅:0 订阅:1")
	private Integer status;


}

