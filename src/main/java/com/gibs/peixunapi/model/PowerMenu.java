package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * @author liangjiawei
 * @date 2020/09/14/19:04
 * @Version 1.0
 * @Description:
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class PowerMenu extends BaseModel {
    private Integer powerId;
    @ManyToOne(cascade = CascadeType.REFRESH)
    private Menu menu;     //绑定菜单

}
