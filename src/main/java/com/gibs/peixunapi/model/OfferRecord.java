package com.gibs.peixunapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "OfferRecord")
public class OfferRecord extends BaseModel {	//证书发放记录
	@ManyToOne(cascade = CascadeType.REFRESH)
	private Certificate certificate;     //证书模板
	private String educationalRequire;     //继续教育学时要求
	private String content;     //证书内容
	@ManyToOne(cascade = CascadeType.REFRESH)
	private User creator;     //发放人
	@ManyToOne(cascade = CascadeType.REFRESH)
	private Course course;     //课程id
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Date offerTime;     //发放时间
	private String uniqueCode;     //证书校验码
	@OneToOne(cascade = CascadeType.REFRESH)
	private User accepter;     //接收人Id


}

