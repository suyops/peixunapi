package com.gibs.peixunapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

/**
 * 课程
 *
 * @author liangjiawei
 */
@ApiModel(description = "课程")
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "course")
public class Course extends BaseModel {

    /**
     * 课程名称
     */
    @ApiModelProperty(value = "课程名称")
    private String name;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /**
     * 简介
     */
    @ApiModelProperty(value = "简介")
    private String summary;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    private Integer creator;

    /**
     * 课程总课时
     */
    @ApiModelProperty(value = "课程总课时")
    private Integer hours;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDate startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDate endTime;

//    /**
//     * 截止时间
//     */
//    @ApiModelProperty(value = "截止时间")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private LocalDate invalidTime;

    /**
     * 课程类别
     */
    @ApiModelProperty(value = "课程类别")
    private String type;

    /**
     * 文档培训数量
     */
    @ApiModelProperty(value = "文档培训数量")
    private Integer fileCount;

    /**
     * 视频培训数量
     */
    @ApiModelProperty(value = "视频培训数量")
    private Integer videoCount;

//    /**
//     * 审查状态 未通过:-1 申请中:0 通过:1
//     */
//    @ApiModelProperty(value = "审查状态 ")
//    private Integer audit;

    /**
     * 是否删除
     */
    @ApiModelProperty(value = "是否删除")
    private boolean deleted;

    /**
     * 是否上架 下架:0 上架:1
     */
    @ApiModelProperty(value = "是否上架 ")
    private Integer isPublish;

    /**
     * 习题进度限制
     */
    @ApiModelProperty(value = "习题进度限制")
    private Integer learnChapterMaxNum;

    /**
     * 习题进度限制状态
     */
    @ApiModelProperty(value = "习题进度限制状态")
    private Boolean learnRestrict;

    public Course() {
        deleted = false;
        learnRestrict = false;
        isPublish=1;
    }
//    @PrePersist
//    public void create() {
//        audit = -1;
//    }
//

}

