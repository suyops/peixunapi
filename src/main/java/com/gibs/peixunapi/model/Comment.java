package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

/**
 * @author liangjiawei
 * 学习评论
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "comment",indexes = {@Index(columnList = "subscribeChannel")})
public class Comment extends BaseModel {

	/** 订阅频道: {video/document} + {id} */
	private String subscribeChannel;

	/** 评论人 */
	@ManyToOne(cascade = CascadeType.REFRESH)
	private User creator;

	/** 评论内容 */
	private String content;

	/** 类型 讨论:1 提问:2*/
	private Integer type;

	/** 接收人 */
	private Integer accepter;

	/** 引用讨论 */
	private Integer reference;

	/** 是否删除 */
	private boolean isDelete;

	@PrePersist
	public void create(){
		isDelete = false;
	}
}

