package com.gibs.peixunapi.model;

import com.gibs.peixunapi.enums.CourseEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author liangjiawei
 * @date 2020/09/02/9:26
 * @Version 1.0
 * @Description:
 */

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "course_teacher")
public class CourseTeacher extends BaseModel {
    private Integer courseId;
    @ManyToOne
    private User teacher;
    private Integer type;

    public CourseTeacher() {
        super();
    }


    public CourseTeacher(Integer courseId, User teacher, CourseEnum type) {
        this.courseId = courseId;
        this.teacher = teacher;
        this.type = type.getCode();
    }
}
