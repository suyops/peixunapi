package com.gibs.peixunapi.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "Menu")
@ApiModel("菜单")
public class Menu extends BaseModel {
	/** 菜单名 */
	@ApiModelProperty("菜单名")
	private String name;
	/** 链接 */
	@ApiModelProperty("链接")
	private String url;
	/** 图标 */
	@ApiModelProperty("图标")
	private String icon;
	/** 排序 */
	@ApiModelProperty("排序")
	private Integer sort;
	/** 父级id */
	@ApiModelProperty("父级id")
	private Integer parentId;
	/** 备注 */
	@ApiModelProperty("备注")
	private String remark;

}

