package com.gibs.peixunapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 *
 * 登录信息
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "login_info")
public class LoginInfo extends BaseModel {

	/** 用户id */
	private Integer userId;

	/** 登录时间 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Date loginTime;

	/** 设备类型  PC:1 移动端:2*/
	private Integer deviceType;

	/** ip地址 */
	private String IPAddress;

	/** 离线时间 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Date offLineTime;

	/** 累积上线时长 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Double onLineTime;

	/** 登录次数 */
	private Integer loginCount;

	public LoginInfo(Integer userId, Date loginTime, Integer deviceType, String IPAddress) {
		this.userId = userId;
		this.loginTime = loginTime;
		this.deviceType = deviceType;
		this.IPAddress = IPAddress;
	}

	public LoginInfo() {
		loginCount = 1;
		onLineTime = 0d;

	}

}

