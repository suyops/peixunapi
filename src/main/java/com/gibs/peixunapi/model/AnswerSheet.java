package com.gibs.peixunapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 答卷
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "answer_sheet")
@Data
public class AnswerSheet extends BaseModel {

	/** 学生考试信息id */
	@OneToOne(cascade = CascadeType.REFRESH)
	private ExamStudent examStudent;

	/** 试卷题目id */
	@ManyToOne(cascade = CascadeType.REFRESH)
	private TestPaperSubject testPaperSubject;

	/** 填写回答 */
	private String myAnswer;

	/** 得分 */
	private BigDecimal score;

	/** 批改人 */
	@ManyToOne(cascade = CascadeType.REFRESH)
	private User markMan;

	/** 批改时间 */
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Date markTime;

	public AnswerSheet() {
		this.myAnswer = "";
		score = new BigDecimal(0);
	}


}

