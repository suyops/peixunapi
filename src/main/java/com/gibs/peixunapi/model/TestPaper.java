package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

/**
 * 试卷
 *
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "test_paper")
public class TestPaper extends BaseModel {

    /**
     * 名称
     */
    private String name;

    /**
     * 总分
     */
    private BigDecimal totalScore;

    /**
     * 单选题数量
     */
    private Integer radioCount;

    /**
     * 单选题分值
     */
    private BigDecimal  radioScore;

    /**
     * 单选后备题
     */
    private String radioBak;

    /**
     * 多选题数量
     */
    private Integer checkCount;

    /**
     * 多选题分值
     */
    private BigDecimal  checkScore;

    /**
     * 多选后备题
     */
    private String checkBak;

    /**
     * 判断题数量
     */
    private Integer judgeCount;

    /**
     * 判断题分值
     */
    private BigDecimal  judgeScore;

    /**
     * 判断后备题
     */
    private String judgeBak;

    /**
     * 填空题数量
     */
    private Integer fillCount;

    /**
     * 填空题分值
     */
    private BigDecimal fillScore;

    /**
     * 填空后备题
     */
    private String fillBak;

    /**
     * 问答题数量
     */
    private Integer questionCount;

    /**
     * 问答题分值
     */
    private BigDecimal  questionScore;

    /**
     * 问答后备题
     */
    private String questionBak;

    /**
     * 创建人
     */
    @ManyToOne(cascade = CascadeType.REFRESH)
    private User creatorId;

    public TestPaper() {
        BigDecimal zero = new BigDecimal(0);
        this.totalScore = zero;
        this.radioCount = 0;
        this.radioScore = zero;
        this.checkCount = 0;
        this.checkScore = zero;
        this.judgeCount = 0;
        this.judgeScore = zero;
        this.fillCount = 0;
        this.fillScore = zero;
        this.questionCount = 0;
        this.questionScore = zero;
    }
}

