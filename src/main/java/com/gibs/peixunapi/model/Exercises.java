package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 * 章节习题
 *
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "Exercises")
public class Exercises extends BaseModel {
    /** 视频章节id */
	private Integer videoClass;

	/** 视频练习题类型 防挂机:1  */
    private Integer type;

    /** 文档章节id */
    private Integer documentation;

    /** 课程题库id */
    @ManyToOne(cascade = CascadeType.REFRESH)
    private CourseSubjectHub courseSubjectHub;

	/** 排序 */
    private Integer sort;

}

