package com.gibs.peixunapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 学习进度
 *
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "study_progress",uniqueConstraints = @UniqueConstraint(columnNames = {"student_id","video_class_id"}))
public class StudyProgress extends BaseModel {
	/** 学生 */
	@ApiModelProperty(value = "学生")
	@ManyToOne
	private User student;

	/** 章节 */
	@ApiModelProperty(value = "章节")
	@OneToOne
	private VideoClass videoClass;

	/** 章节 */
	@ApiModelProperty(value = "章节")
	@OneToOne
	private Documentation documentation;

	/** 进度 */
	@ApiModelProperty(value = "进度")
	private BigDecimal progress;

	/** 视频观看时长 */
	@ApiModelProperty(value = "视频观看时长")
	private BigDecimal timeRecord;

	/** 视频是否答题 */
	@ApiModelProperty(value = "视频是否答题")
	private Integer isVideoPass;


	public StudyProgress() {
		this.timeRecord = BigDecimal.valueOf(0.00d);
		this.progress = BigDecimal.valueOf(0.00d);
		this.isVideoPass = 0;
	}
}

