package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "UserRole")
public class UserRole extends BaseModel {	//用户角色关联表
	private Integer userId;     //用户id.

	private Integer roleId;     //角色id


}

