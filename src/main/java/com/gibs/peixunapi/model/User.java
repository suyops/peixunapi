package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Arrays;
import java.util.UUID;

/**
 * 用户
 *
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "user")
public class User extends BaseModel {

    /**
     * 用户名
     */
    private String username;

    /**
     * 岗位
     */
    private String post;

    /**
     * 手机
     */
    private String telephone;

    /**
     * 密码
     */
    private String password;

    /**
     * 微信登录名
     */
    private String weChatName;

    /**
     * 微信id
     */
    private String wechatOpenId;

    /**
     * 所在企业
     */
    @ManyToOne(cascade = CascadeType.REFRESH)
    private Business business;

    /**
     * 证件类型
     */
    private String certificateType;

    /**
     * 证件编号
     */
    private String certificateNumber;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 审核状态  未通过:0 通过:1
     */
    private Integer audit;

    /**
     * uuid
     */
    private String uuid;

    /**
     * 入职年份
     */
    private String entryYear;
    /**
     * 部门
     */
    private Integer departmentId;

    /**
     * 头像
     */
    @OneToOne(cascade = CascadeType.REFRESH)
    private FileInfo avatar;


    @Transient
    private String role;

    public User() {
        this.audit = 0;
    }

    @PrePersist
    public void create() {
        if (uuid == null) {
            UUID id = UUID.randomUUID();
            uuid = Arrays.stream(id.toString().split("-")).reduce("", String::concat);
        }
    }
}

