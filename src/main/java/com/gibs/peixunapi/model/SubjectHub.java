package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "SubjectHub")
public class SubjectHub extends BaseModel {	//题目中心
	private String type;     //类型
	private String content;
	private String answer;
	private String analysis;

}

