
package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.UUID;

/**
 *
 * 企业
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "business")
public class Business extends BaseModel {

	/** 名称 */
	private String name;

	/** 简称 */
	private String shortName;

	/** 管理员 */
	private Integer managerId;

	/** 发票抬头 */
	private String invoiceTitle;

	/** 纳税人识别号 */
	private String taxpayerNumber;

	/** 开户银行 */
	private String bankName;

	/** 银行账号 */
	private String bankAccount;

	/** 邀请码 */
	private String invitedCode;

	/** 排序 */
	private Integer sort;

	/** 是否属于集团旗下 */
	private Integer isBelongSyndicate;

	@PrePersist
	public void create(){
		UUID id=UUID.randomUUID();
		String[] idd=id.toString().split("-");
		invitedCode =idd[0];
	}

}

