package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author liangjiawei
 * @date 2020/10/22/14:43
 * @Version 1.0
 * @Description:
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Scheduled extends BaseModel{

    private String taskName;
    /** 定时任务类名 */
    private String className;
    /** 定时任务方法名 */
    private String methodName;
    /** cron表达式 */
    private String cron;
    /** 任务描述 */
    private String comment;
    /** 任务参数 */
    private String paramId;
    /** 任务日期 */
    private String date;
    /** 是否暂停 */
    private Integer isEnable;

    public Scheduled() {
    }
}
