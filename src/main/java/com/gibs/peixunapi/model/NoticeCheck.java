package com.gibs.peixunapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "NoticeCheck")
public class NoticeCheck extends BaseModel {	//消息查看
	@OneToOne(cascade = CascadeType.REFRESH)
	private Notice notice;     //消息id

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Date checkDate;     //查看时间
	private Integer checkStatus;     //查看状态
	@ManyToOne(cascade = CascadeType.REFRESH)
	private User user;     //用户id


}

