package com.gibs.peixunapi.model;

import javassist.runtime.Inner;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "test_paper_subject",
        uniqueConstraints = @UniqueConstraint(columnNames = {"test_paper_id", "subject_hub_id"}))
public class TestPaperSubject extends BaseModel {    //试卷题目
    @ManyToOne(cascade = CascadeType.REFRESH)
    private TestPaper testPaper;     //试卷id
    @OneToOne(cascade = CascadeType.REFRESH)
    private SubjectHub subjectHub;     //题目id
    
    private Integer creator;     //创建人
    private Integer sort;        //排序
 }

