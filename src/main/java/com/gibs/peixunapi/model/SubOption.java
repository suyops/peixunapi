package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
//import com.gibs.validator.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "SubOption")
public class SubOption extends BaseModel {	//题库选项
	@ManyToOne(cascade = CascadeType.REFRESH)
	private SubjectHub subjectHub;     //题库id
	private String content;     //选项内容
	private String value;     //选项值
	private Integer sort;     //排序
}

