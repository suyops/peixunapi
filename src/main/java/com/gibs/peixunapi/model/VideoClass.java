package com.gibs.peixunapi.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * 视频课程
 *
 * @author liangjiawei
 */
@ApiModel(description = "视频课程实体类")
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "video_class")
public class VideoClass extends BaseModel {

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 简介
     */
    @ApiModelProperty(value = "简介")
    private String summary;

//	/** 挂机限制 */
//	@ApiModelProperty(value = "挂机限制")
//	private Boolean hangupLimit;

//	/** 题目插入时间 */
//	@ApiModelProperty(value = "题目插入时间")
//	private Double questionTime;

    /**
     * 课程
     */
    @ManyToOne(cascade = CascadeType.REFRESH)
    @ApiModelProperty(value = "course", notes = "课程实体类", required = false, hidden = true)
    private Course course;

    /**
     * 视频
     */
    @OneToOne(cascade = CascadeType.REFRESH)
    @ApiModelProperty(value = "video", notes = "视频", required = false, hidden = true)
    private Video video;
//
//	/** 审核 */
//	@ApiModelProperty(value = "审核")
//	private Integer audit;

    /**
     * 是否上架 下架:0 上架:1
     */
    @ApiModelProperty(value = "是否上架 ")
    private Integer isPublish;

	/**
	 * 是否删除
	 */
	@ApiModelProperty(value = "是否删除")
	private boolean deleted;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Integer sort;

    @PrePersist
    public void create() {
		deleted = false;
        isPublish = 1;
    }
}

