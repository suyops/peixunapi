package com.gibs.peixunapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import javax.persistence.*;


/**
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "actionLog")
public class ActionLog extends BaseModel {    //操作日志

    private String uri;     //操作路径


    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date actionTime;     //操作时间

    private String type;

    private String content;     //操作内容

    private String method;     //请求方法

    private String requestParam;     //请求参数

    private String responseParam;     //请求参数

    private Integer userId;

    private String ip;

    private String userName;

    private String module;


}

