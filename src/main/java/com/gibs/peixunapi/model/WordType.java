package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "WordType")
public class WordType extends BaseModel {	//字典类型
	private String name;     //名称
	private String code;     //编码
	private String description;     //描述
	private Integer sort;     //排序
	private String type;     //类型

}

