package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 视频
 * @author liangjiawei
 * @date
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "video")
public class Video extends BaseModel {

	/** 名称 */
	private String name;

	/** 封面图id */
	@OneToOne(cascade = CascadeType.REFRESH)
	private FileInfo coverImage;

	/** 视频地址 */
	@OneToOne(cascade = CascadeType.REFRESH)
	private FileInfo url;

	/** 时长 */
	private Integer duration;

	/** 类型 */
	private String type;


}

