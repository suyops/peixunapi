package com.gibs.peixunapi.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * 考试
 *
 * @author liangjiawei
 */
@ApiModel(description = "考试")
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "exam_info")
public class ExamInfo extends BaseModel {

    /** 课程id */
    @ApiModelProperty(value = "课程id")
	@ManyToOne(cascade = CascadeType.REFRESH)
	private Course course;

    /** 总分 */
    @ApiModelProperty(value = "总分")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal totalScore;

    /** 合格分数 */
    @ApiModelProperty(value = "合格分数")
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal passScore;

    /** 时长 */
    @ApiModelProperty(value = "时长")
    private String durationTime;

    /** 试卷id */
    @ApiModelProperty(value = "试卷id")
	@OneToOne(cascade = CascadeType.REFRESH)
	private TestPaper testPaper;

    /** 创建人 */
    @ApiModelProperty(value = "创建人")
	@ManyToOne(cascade = CascadeType.REFRESH)
	private User creator;

    /** 考试须知 */
    @ApiModelProperty(value = "考试须知")
    private String about;
}

