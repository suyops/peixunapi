package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * @author liangjiawei
 * @date 2020/09/14/19:17
 * @Version 1.0
 * @Description:
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class RolePower extends BaseModel{
    private Integer roleId;
    @ManyToOne(cascade = CascadeType.REFRESH)
    private Power power;     //权限

}
