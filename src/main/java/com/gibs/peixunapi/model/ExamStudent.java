package com.gibs.peixunapi.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gibs.peixunapi.utils.BigDecimalSerializerUtil;
import com.gibs.peixunapi.utils.UUIDUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * 学生考试信息
 *
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "exam_student")
public class ExamStudent extends BaseModel {

    /**
     * 学生id
     */
    @ManyToOne(cascade = CascadeType.REFRESH)
    private User student;

    /**
     * 考试班级id
     */
    @ManyToOne(cascade = CascadeType.REFRESH)
    private ExamClassInfo examClassInfo;

    /**
     * 考试id
     */
    private Integer examInfoId;

    /**
     * 课程id
     */
    private Integer courseId;

    /**
     * 准考证号
     */
    private String uniqueCode;

    /**
     * 考试完成状态 未考试:-1 考试中:0  已完成:1
     */
    private Integer examStatus;

    /**
     * 参加考试状态 未参加:0 已参加:1
     */
    @Column(name = "is_take_exam")
    private Integer takeExam;

    /**
     * 考试成绩
     */
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal examScores;

    /**
     * 平时成绩
     */
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal dailyScore;

    /**
     * 最终成绩
     */
    @JsonSerialize(using = BigDecimalSerializerUtil.class)
    private BigDecimal finalScore;

    /**
     * 申请缓考  未通过：-1 未申请：0 已申请:1 已通过：2
     */
    private Integer applyDelayedExam;

    /**
     * 批改状态 未批改:-1 批改中: 0 已批改:1
     */
    private Integer correctStatus;

    /**
     * 是否通过考试  未通过:0 通过:1
     */
    private Integer isPass;

    @PrePersist
    public void create() {
        correctStatus = -1;
        examStatus = 0;
        applyDelayedExam = 0;
        examStatus = -1;
        takeExam = 0;
        isPass = 0;
        finalScore = BigDecimal.valueOf(0d);
        uniqueCode = UUIDUtil.get12UUID() + LocalDateTime.now(ZoneOffset.of("+8")).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
    }
}

