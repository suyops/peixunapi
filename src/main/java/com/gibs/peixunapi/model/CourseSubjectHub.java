package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "CourseSubjectHub")
public class CourseSubjectHub extends BaseModel {	//课程_题库

	@ManyToOne(cascade = CascadeType.REFRESH)
	private SubjectHub subjectHub;     //题库id

	@ManyToOne(cascade = CascadeType.REFRESH)
	private Course course;     //课程id

	private Integer sort;     //排序
}

