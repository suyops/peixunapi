package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;

/**
 * @author liangjiawei
 * @date 2020/10/09/10:43
 * @Version 1.0
 * @Description:
 */

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "certificate_class")
public class CertificateClass extends BaseModel {

    /**班级id*/
    private Integer examClassInfoId;
    /**发放次数*/
    private Integer issueCount;
    /**通过人数*/
    private Integer passCount;

    public CertificateClass() {
        issueCount = 0;
        passCount = 0;
    }
}
