package com.gibs.peixunapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 学生_课程
 * @author liangjiawei
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "student_course")
public class StudentCourse extends BaseModel {

	/** 学生id */
	@ManyToOne
	private User student;

	/** 课程id */
	@ManyToOne
	private Course course;

	/** 订阅时间 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Date subscribeTime;

	/** 报名时间 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Date signUpTime;

	/** 文档学习进度 */
	private BigDecimal documentationProgress;

	/** 视频学习进度 */
	private BigDecimal videoProgress;

	/** 状态 未订阅:false 已订阅:true */
	@Column(name = "is_subscribe")
	private Boolean subscribe;

	/** 状态 未报名:false 已报名:true */
	@Column(name = "is_sign_up")
	private Boolean signUp;

	/** 此课程学习状态  未完成:-2 未开始:-1 学习中:0 已结束:1 */
	private Integer studyStatus;

	/** 考试资格  已获得：1 未获得：0*/
	@Column(name = "is_allow_exam")
	private boolean allowExam;

	@PrePersist
	public void create(){
		documentationProgress = new BigDecimal("0.00");
		videoProgress = new BigDecimal("0.00");
		studyStatus = -1;
		signUp = false;
		allowExam = false;
	}
}

