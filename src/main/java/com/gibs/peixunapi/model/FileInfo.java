package com.gibs.peixunapi.model;

import com.gibs.peixunapi.config.Myconfig;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "fileInfo")
public class FileInfo extends BaseModel {	//文件
	private String fileMd5;
	private String name;     //名称
	private String type;     //类型
	private String url;     //地址


	public String getUrl() {
		return Myconfig.UPLOAD_FILE_PATH+url;
	}
	public String getPath() {
		return url;
	}
}

