package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "role")
public class Role extends BaseModel {	//角色
	private String name;     //名称
	private String code;     //编码
	private String sort;     //排序
}

