package com.gibs.peixunapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author liangjiawei
 * @Description: 消息通知
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "notice")
public class Notice extends BaseModel {

	/** 标题 */
	private String title;

	/** 发布人 */
	private Integer sender;

	/** 接收人 */
	private Integer accepter;

	/** 发布时间 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Date sendDate;

	/** 内容 */
	private String content;

	/** 链接 */
	private String link;

	/** 查看时间 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
	private Date readTime;

	/** 查看状态 */
	@Column(name = "is_read")
	private Integer read;

	@PrePersist
	public void create(){
		sendDate = new Date();
		read = 0;
	}


}

