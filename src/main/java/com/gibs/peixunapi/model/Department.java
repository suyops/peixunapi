package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

/**
 * @author liangjiawei
 * @date 2022/03/22/18:47
 * @Version 1.0
 * @Description: 部门
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class Department extends BaseModel {
    private String name;
    private Integer sort;
    private Integer isUsed;
}
