package com.gibs.peixunapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
//@Table(name = "Word")
public class Word extends BaseModel {	//数据字典
	private String name;     //名称
	private String code;     //编码
	private String description;     //描述
	/** 值 */
	private String value;
	private Integer sort;     //排序
	private String categoryCode;     //分类编码
	/** 分类说明 */
	private String category;


}

