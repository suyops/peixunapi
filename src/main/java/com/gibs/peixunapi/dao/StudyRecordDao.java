package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.StudyRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author liangjiawei
 * @date 2020/09/17/10:39
 * @Version 1.0
 * @Description:
 */

@Repository
public interface StudyRecordDao extends JpaRepository<StudyRecord, Integer>, JpaSpecificationExecutor<StudyRecord> {
    @Query(value = "SELECT " +
            " a.* " +
            "FROM " +
            " study_record as a, " +
            " ( SELECT id, MAX( create_time ) create_time FROM study_record WHERE student_id = ?1 GROUP BY video_class_id, document_id ) b " +
            "WHERE " +
            "a.student_id = ?1 " +
            "AND a.create_time = b.create_time " +
            "ORDER BY a.create_time DESC",nativeQuery = true)
    Page<StudyRecord> findByStudent(Integer userId, Pageable pageable);
}
