package com.gibs.peixunapi.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.gibs.peixunapi.model.StudentCourse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.User;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(rollbackFor = Exception.class)
public interface UserDao extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

	@Query("select a from User as a where a.id in ?1")
	Page<User> findPageByIdIn(List<Integer> idList, Pageable pageable);

	@Query("select a from User as a where a.id in ?1")
	List<User> findListByIdIn(List<Integer> idList);

	@Query("select a from User as a where a.username like :username and a.status = 1")
	Page<User> findUsersByUsernameContaining(@Param("username") String username, Pageable pageable1);

	@Modifying(clearAutomatically = true)
	@Query("update User set audit = 1, updateTime =?2 where status = 1 and id in ?1 ")
	Integer auditBatchUserByIdList(List<Integer> idList, Date date);

	@Query("from User where business.id = ?1 and status = 1 ")
	List<User> findListByBusinessID(Integer businessId);

	@Query("from User where username = ?1 ")
	User findByUsername(String username);

	@Query("select count(User) from User where business.id = ?1 ")
	Long countByBusinessID(Integer businessId);

	@Query("select a from User as a where a.uuid = ?1 ")
	User findByUUID(String uuid);

	@Query("select a.managerId from Business as a where a.id = ?1 ")
	Integer findBusinessManager(Integer id);

	@Modifying
	@Query("update User set status = -1 where id = :id ")
	void deleteUser(@Param("id") Integer id);

	@Query("select a from User as a where a.status = 1 ")
	Page<User> findAllByStatus(Pageable pageable);

	@Query("select a from User as a where a.telephone = ?1")
	User findByTelephone(String telephone);

	@Modifying
	@Query(value = "update user set business_id = null where business_id =?1 and status < 1",nativeQuery = true)
    void unBindDeleteUserInBusiness(Integer businessId);



	@Query(value = "SELECT DISTINCT " +
			"a.* " +
			"FROM " +
			"`user` a " +
			"LEFT JOIN exam_student b ON a.id = b.student_id " +
			"AND b.exam_status = 0 AND b.correct_status = -1 and b.exam_class_info_id != ?2 " +
			"WHERE " +
			"b.course_id =?1 " +
			"and b.is_pass= 0 " +
			"GROUP BY b.student_id ",nativeQuery = true)
	List<User> findNoPassExam(Integer courseId, Integer examClassId);
}

