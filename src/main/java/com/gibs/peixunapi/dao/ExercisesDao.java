package com.gibs.peixunapi.dao;

import java.util.List;

import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.model.CourseSubjectHub;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.Exercises;

@Repository
public interface ExercisesDao extends JpaRepository<Exercises, Integer>, JpaSpecificationExecutor<Exercises> {

	@Query(value = "SELECT e.id FROM course_subject_hub a inner join subject_hub b on a.subject_hub_id = b.id inner join exercises e on a.id = e.course_subject_hub_id " +
			"WHERE e.video_class = ?1 and ( b.type = 'radio' or b.type = 'check' or b.type = 'judge' ) and e.type != 1 " +
			"ORDER BY a.id ", nativeQuery = true)
	List<Integer> findIdOfVideoExercise(Integer chapterId);

	@Query(value = "SELECT e.id FROM course_subject_hub a inner join subject_hub b on a.subject_hub_id = b.id inner join exercises e on a.id = e.course_subject_hub_id " +
			"WHERE e.documentation = ?1 and ( b.type = 'radio' or b.type = 'check' or b.type = 'judge' or b.type = 'fill' ) " +
			"ORDER BY a.id ", nativeQuery = true)
	List<Integer> findIdOfDocumentExercise(Integer chapterId);

	@Query(value = "SELECT a.* " +
			"FROM exercises a " +
			" INNER JOIN course_subject_hub b ON a.course_subject_hub_id = b.id " +
			" INNER JOIN subject_hub sh ON b.subject_hub_id = sh.id " +
			" LEFT JOIN exercise_record g ON g.exercise_id = a.id AND g.student_id = ?3 AND g.is_right = 1 " +
			"WHERE 1 = 1 " +
			" AND a.documentation = ?1 " +
			" AND ( sh.type = 'radio' OR sh.type = 'check' OR sh.type = 'judge' ) " +
			" AND g.id IS NULL " +
			"ORDER BY b.create_time limit ?2",nativeQuery = true)
	List<Exercises> getDocumentExerciseList(Integer chapterId, Integer number, Integer userId);

	@Query(value = "SELECT a.* " +
			"FROM exercises a " +
			" INNER JOIN course_subject_hub b ON a.course_subject_hub_id = b.id " +
			" INNER JOIN subject_hub sh ON b.subject_hub_id = sh.id " +
			" LEFT JOIN exercise_record g ON g.exercise_id = a.id AND g.student_id = ?3 AND g.is_right = 1 " +
			"WHERE 1 = 1 " +
			" AND a.video_class = ?1 and a.type != 1 " +
			" AND ( sh.type = 'radio' OR sh.type = 'check' OR sh.type = 'judge' ) " +
			" AND g.id IS NULL " +
			"ORDER BY b.create_time limit ?2",nativeQuery = true)
	List<Exercises> getVideoExerciseList(Integer chapterId, Integer number,Integer userId);

	@Query(value = "select a.* from exercises a where a.id in ?1 order by FIND_IN_SET(a.id,?2)",nativeQuery = true)
	List<Exercises> findByIdIn(List<Integer> idList,String ids);

	@Modifying
	@Query(value = "delete from exercises where video_class = ?1 limit 1",nativeQuery = true)
	void deleteByVideoClass(Integer chapterId);

	@Query("select a from Exercises as a where a.videoClass = ?1 and a.type = 1")
	List<Exercises> findHangUpSubject(Integer chapterId);
}

