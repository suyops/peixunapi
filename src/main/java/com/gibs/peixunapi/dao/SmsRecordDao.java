package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.SmsRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author liangjiawei
 * @date 2020/12/01/9:40
 * @Version 1.0
 * @Description:
 */
public interface SmsRecordDao extends JpaRepository<SmsRecord, Integer>, JpaSpecificationExecutor<SmsRecord> {
}
