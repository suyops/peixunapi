package com.gibs.peixunapi.dao;

import java.util.List;

import com.gibs.peixunapi.model.SubjectHub;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.TestPaperSubject;

@Repository
public interface TestPaperSubjectDao extends JpaRepository<TestPaperSubject, Integer>, JpaSpecificationExecutor<TestPaperSubject> {

    @Query("from TestPaperSubject where testPaper.id = ?1 order by subjectHub.type,sort asc")
    List<TestPaperSubject> findSubjectHubListByTestPaperId(Integer testPaperId);

    @Modifying(clearAutomatically = true)
    @Query(value = "update test_paper_subject set subject_hub_id = ?1 where test_paper_id = ?2 and subject_hub_id = ?3", nativeQuery = true)
    Integer updateTestPaperSubject(Integer newSubjectHubId, Integer testPaperId, Integer oldSubjectHubId);
}

