package com.gibs.peixunapi.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.Role;

@Repository
public interface RoleDao extends JpaRepository<Role, Integer>, JpaSpecificationExecutor<Role> {

	@Query("select id from Role where code in ?1")
	List<Integer> findByCodeIn(List<String> code);

	@Query("select id from Role where code = ?1")
	Integer findByCode(String code);

	@Query("from Role where id in ?1")
	List<Role> findByIdIn(List<Integer> idList);

}

