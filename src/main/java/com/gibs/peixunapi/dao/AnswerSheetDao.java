package com.gibs.peixunapi.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.AnswerSheet;

@Repository
public interface AnswerSheetDao extends JpaRepository<AnswerSheet, Integer>, JpaSpecificationExecutor<AnswerSheet> {

    @Query("select a from AnswerSheet as a where a.examStudent.id = ?1 order by a.testPaperSubject.subjectHub.type")
    List<AnswerSheet> findByCorrectStatus(Integer examStudentId);

    @Query("select a.score from AnswerSheet as a where a.examStudent.id = ?1 ")
    List<Double> findScoreByExamStudent(Integer examStudentId);

    @Query("select count(a.id) from AnswerSheet as a where a.examStudent.id = ?1 ")
    Long countTopic(Integer examStudentId);

    @Query(value = "select a.* from answer_sheet as a where a.exam_student_id= ?1 and a.test_paper_subject_id = ?2 order by a.create_time limit 1",nativeQuery = true)
    AnswerSheet findByExamStudentAndTestPaperSubject(Integer examStudentId, Integer testPaperSubjectId);
}

