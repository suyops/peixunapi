package com.gibs.peixunapi.dao;

import java.util.List;

import com.gibs.peixunapi.model.TestPaper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.ExamInfo;

@Repository
public interface ExamInfoDao extends JpaRepository<ExamInfo, Integer>, JpaSpecificationExecutor<ExamInfo> {

	@Query("select a from ExamInfo as a inner join ExamClassInfo as b on a.id = b.examInfo.id where b.id = :examClassId")
	ExamInfo findTestPaperByExamClassId(@Param("examClassId") Integer examClassId);

	List<ExamInfo> findExamInfosByTestPaper_Id(Integer testPaperId);

	@Modifying
	@Query("update ExamInfo set testPaper.id = :testPaperId where id = :id")
    void bindTestPaper(@Param("id")Integer id, @Param("testPaperId") Integer testPaperId);
}
