package com.gibs.peixunapi.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.WordType;

@Repository
public interface WordTypeDao extends JpaRepository<WordType, Integer>, JpaSpecificationExecutor<WordType> {

}

