package com.gibs.peixunapi.dao;

import java.util.List;

import com.gibs.peixunapi.model.Documentation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.VideoClass;

@Repository
public interface VideoClassDao extends JpaRepository<VideoClass, Integer>, JpaSpecificationExecutor<VideoClass> {

	@Query("from VideoClass where course.id = ?1 and isPublish = 1 and deleted = 0 order by sort")
	List<VideoClass> findByCourseId(Integer courseId);

	@Modifying
	@Query("update VideoClass set isPublish = ?2 where id = ?1 ")
	void changeStatus(Integer id, Integer status);



	@Modifying
	@Query("update VideoClass set isPublish = ?2 where id = ?1 ")
	void auditChapter(Integer id, Integer audit);

	@Modifying
	@Query("update VideoClass set isPublish = ?2 where course.id = ?1 ")
	void auditChapterByCourse(Integer id, Integer audit);

	@Modifying
	@Query(value = "update video_class set video_id = ?2 where id = ?1 ",nativeQuery = true)
    void changeVideo(Integer videoClassId, Integer videoId);

	@Modifying
	@Query("update VideoClass set isPublish = 0 where id = ?1")
    void applyAudit(Integer courseId);

	@Query("select count (id) from VideoClass where course.id = ?1 and isPublish = 1")
    Long countByCourse(Integer id);

	@Modifying
	@Query("update VideoClass set deleted = 1 where id = ?1")
	void deleteChapter(Integer chapterId);
}

