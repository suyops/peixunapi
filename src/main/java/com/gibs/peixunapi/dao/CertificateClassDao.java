package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.CertificateClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author liangjiawei
 * @date 2020/10/09/19:20
 * @Version 1.0
 * @Description:
 */

@Repository
public interface CertificateClassDao extends JpaRepository<CertificateClass, Integer>, JpaSpecificationExecutor<CertificateClass> {

    @Query("from CertificateClass where examClassInfoId = ?1")
    CertificateClass findByClass(Integer examClassId);

    @Query("select count(id) from CertificateClass where examClassInfoId = ?1")
    Integer countByClass(Integer examClassId);
}
