package com.gibs.peixunapi.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.Business;

@Repository
public interface BusinessDao extends JpaRepository<Business, Integer>, JpaSpecificationExecutor<Business> {
	@Query("from Business where invitedCode = ?1")
	Business findByInvitedCode(String invitedCode);

	@Query("select count (id) from User where business.id = ?1")
    Integer countByBusiness(Integer businessId);
}

