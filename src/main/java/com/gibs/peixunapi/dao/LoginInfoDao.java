package com.gibs.peixunapi.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.LoginInfo;

@Repository
public interface LoginInfoDao extends JpaRepository<LoginInfo, Integer>, JpaSpecificationExecutor<LoginInfo> {
	LoginInfo findFirstByUserIdOrderByLoginTimeDesc(Integer id);

}

