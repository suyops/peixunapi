package com.gibs.peixunapi.dao;

import java.util.List;

import com.gibs.peixunapi.model.CourseBusiness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseBusinessDao extends JpaRepository<CourseBusiness, Integer>, JpaSpecificationExecutor<CourseBusiness> {

    @Query("from CourseBusiness a where a.businessId = ?1 order by a.subscribeTime desc ")
    List<CourseBusiness> findByBusiness(Integer id);

    @Query("from CourseBusiness a where a.businessId = :business and a.creatorId = :course ")
    CourseBusiness findByBusinessAndCourse(@Param("business") Integer businessId, @Param("course") Integer courseId);
}

