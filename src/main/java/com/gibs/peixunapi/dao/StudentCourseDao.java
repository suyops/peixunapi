package com.gibs.peixunapi.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.StudentCourse;

@Repository
public interface StudentCourseDao extends JpaRepository<StudentCourse, Integer>, JpaSpecificationExecutor<StudentCourse> {

    @Query("select a from StudentCourse a where a.student.id = ?1 order by a.createTime desc ")
    List<StudentCourse> findByStudentAndSubscribe(Integer studentId);

    @Query("select a from StudentCourse a  where a.student.id = ?1 and a.signUp = ?2 order by a.signUpTime desc")
    List<StudentCourse> findByStudentAndSignUp(Integer studentId, Boolean isSubscribe);

    Long countByCourse_IdAndSignUpIsTrue(Integer courseId);

    Long countByCourse_IdAndSubscribeIsTrue(Integer courseId);

    @Query("from StudentCourse where course.id = ?1 and student.id=?2")
    StudentCourse findByCourseIdAndStudentId(Integer courseId, Integer studentId);

    @Query("select count(id) from StudentCourse where documentationProgress > ?1 " +
            "and videoProgress = ?1 and course.id = ?2")
    Long countFinishStudyByCourseId(BigDecimal progress, Integer courseId);

    @Query(value = "SELECT DISTINCT " +
            "a.* " +
            "FROM " +
            "student_course a " +
            "inner join `user` as u on a.student_id = u.id " +
            "LEFT JOIN exam_student b ON a.student_id = b.student_id " +
            "WHERE " +
            "a.course_id =?1 " +
            "AND a.study_status = 0 " +
            "and b.id is null " +
            " and u.username like %?2% ",nativeQuery = true)
    List<StudentCourse> getExamList(Integer courseId, String key);
    @Query(value = "SELECT DISTINCT " +
            "a.* " +
            "FROM " +
            "student_course a " +
            "inner join `user` as u on a.student_id = u.id " +
            "LEFT JOIN exam_student b ON a.student_id = b.student_id " +
            "AND b.exam_status = 0 AND b.correct_status = -1 and b.exam_class_info_id != ?2 " +
            "WHERE " +
            "a.is_allow_exam = TRUE " +
            "AND a.course_id =?1 " +
            "AND a.study_status = 0 " +
            "and b.is_pass= 0 " +
            " GROUP BY a.student_id ",nativeQuery = true)
    List<StudentCourse> getNoPassExam(Integer courseId, Integer examClassId, String key);

    @Query("select count(id) from StudentCourse where course.id = ?1 and student.business.id = ?2 and signUp = true order by signUpTime ")
    Long countSignUpByCourseAndBusiness(Integer courseId, Integer business);

    @Query("select count(id) from StudentCourse where course.id = ?1 and student.business.id = ?2 and subscribe = true order by subscribeTime ")
    Long countSubscribeByCourseAndBusiness(Integer courseId, Integer business);

    @Modifying
    @Query("update StudentCourse set studyStatus = 0 where course.id = ?1 and subscribe  = true and signUp = true ")
    void startCourse(Integer courseId);

    @Modifying
    @Query("update StudentCourse set studyStatus = 1 where course.id = ?1 ")
    void finishCourse(Integer courseId);

    @Query("select a from StudentCourse as a inner join Documentation as b on a.course.id = b.course.id where b.id = ?1 and a.student.id=?2")
    StudentCourse findByDocumentAndStudent(Integer chapterId, Integer userId);

    @Query("select a from StudentCourse as a inner join VideoClass as b on a.course.id = b.course.id where b.id = ?1 and a.student.id=?2")
    StudentCourse findByVideoAndStudent(Integer chapterId, Integer userId);

    @Query(value = "select distinct sc.* " +
            "from student_course as sc " +
            "inner join course as c on sc.course_id = c.id " +
            "where sc.student_id = ?1 " +
            "and (c.start_time between ?2 and ?3 or c.end_time between ?2 and ?3) " +
            "GROUP BY sc.course_id order by c.start_time",nativeQuery = true)
    List<StudentCourse> findStudyDate(Integer userId, String format, String format1);


}

