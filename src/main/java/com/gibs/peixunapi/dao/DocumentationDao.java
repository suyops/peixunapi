package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.Documentation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentationDao extends JpaRepository<Documentation, Integer>, JpaSpecificationExecutor<Documentation> {

    @Query("from Documentation where course.id = ?1 and isPublish = 1  and deleted = false order by sort")
    List<Documentation> findByCourseId(Integer courseId);

    @Modifying
    @Query("update Documentation set isPublish = ?2 where id = ?1 ")
    void changeStatus(Integer id, Integer status);

    @Modifying
    @Query("update Documentation set isPublish = ?2 where id = ?1 ")
    void auditChapter(Integer id, Integer audit);

    @Modifying
    @Query("update Documentation set isPublish = ?2 where course.id = ?1 ")
    void auditChapterByCourse(Integer id, Integer audit);

    @Modifying
    @Query("update Documentation set isPublish = 0 where id = ?1")
    void applyAudit(Integer courseId);

    @Query("select count (id) from Documentation where course.id = ?1 and isPublish = 1 and deleted = false ")
    Long countByCourse(Integer id);

    @Modifying
    @Query("update Documentation set deleted = 1 where id = ?1")
    void deleteChapter(Integer chapterId);
}

