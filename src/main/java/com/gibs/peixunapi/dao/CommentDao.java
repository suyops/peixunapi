package com.gibs.peixunapi.dao;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.Comment;

@Repository
public interface CommentDao extends JpaRepository<Comment, Integer>, JpaSpecificationExecutor<Comment> {

    @Query("from Comment where subscribeChannel = :channel and type = :comment order by createTime desc ")
    List<Comment> findBySubscribeChannel(@Param("channel") String subscribeChannel, @Param("comment") Integer comment);

    @Query("select count(a.id) from Comment as a where a.creator.id = :user and a.subscribeChannel like %:course")
    Long questionCountByCourse(@Param("user") Integer userId, @Param("course") Integer courseId);

    @Query("select a from Comment as a where a.accepter= :user ")
    List<Comment> getPersonQuestion(@Param("user") Integer id);
}

