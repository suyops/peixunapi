package com.gibs.peixunapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.gibs.peixunapi.model.CourseTestPaper;

@Repository
public interface CourseTestPaperDao extends JpaRepository<CourseTestPaper, Integer>, JpaSpecificationExecutor<CourseTestPaper> {

}
