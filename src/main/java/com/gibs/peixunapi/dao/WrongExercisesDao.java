package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.Exercises;
import com.gibs.peixunapi.model.WrongExercises;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/10/12/22:46
 * @Version 1.0
 * @Description:
 */
@Repository
public interface WrongExercisesDao extends JpaRepository<WrongExercises, Integer>, JpaSpecificationExecutor<WrongExercises> {
    @Query("from WrongExercises where student = ?1")
    List<WrongExercises> findByStudent(Integer studentId);

    @Query("select a from WrongExercises as a inner join ExerciseRecord as b on a.exerciseRecode.id = b.id " +
            "inner join Exercises as c on c.id = b.exercise.id where c.id = :exerciseId and a.student = :studentId")
    WrongExercises findByStudentAndExercise(@Param("exerciseId") Integer exercisesId, @Param("studentId") Integer student);
}
