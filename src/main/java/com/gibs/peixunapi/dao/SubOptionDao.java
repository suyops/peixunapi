package com.gibs.peixunapi.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.SubOption;

@Repository
public interface SubOptionDao extends JpaRepository<SubOption, Integer>, JpaSpecificationExecutor<SubOption> {
	List<SubOption> findSubOptionsBySubjectHubId(Integer subjectHubId);

	@Query("from SubOption where subjectHub.id = ?1 order by sort")
	List<SubOption> findByIdOrderBySort(Integer id);
}

