package com.gibs.peixunapi.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.NoticeCheck;

@Repository
public interface NoticeCheckDao extends JpaRepository<NoticeCheck, Integer>, JpaSpecificationExecutor<NoticeCheck> {


}

