package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.Scheduled;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/10/28/18:38
 * @Version 1.0
 * @Description:
 */
@Repository
public interface ScheduledDao extends JpaRepository<Scheduled, Integer>, JpaSpecificationExecutor<Scheduled> {
    @Query(value = "select a.* from scheduled a where a.date = ?1",nativeQuery = true)
    List<Scheduled> findTodaysJob(String now);

    @Query(value = "select a.* from scheduled a order by a.create_time desc ",nativeQuery = true)
    List<Scheduled> getAll();

    Scheduled findFirstByTaskName(String taskName);
}
