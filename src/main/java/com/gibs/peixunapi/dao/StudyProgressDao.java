package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.StudyProgress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudyProgressDao extends JpaRepository<StudyProgress, Integer>, JpaSpecificationExecutor<StudyProgress> {


    @Query("select timeRecord from StudyProgress where videoClass.id = ?1 and student.id = ?2")
    Double findTimeRecord(Integer videoClassId, Integer studentId);

    @Query(value = "select a.* from study_progress as a where a.video_class_id= ?1 and a.student_id = ?2 order by a.create_time desc limit 1 ", nativeQuery = true)
    StudyProgress findByVideoClass_IdAndStudent_Id(Integer videoClassId, Integer studentId);

    @Query(value = "select a.* from study_progress as a where a.documentation_id= ?1 and a.student_id = ?2 order by a.create_time desc limit 1 ", nativeQuery = true)
    StudyProgress findByDocumentation_IdAndStudent_Id(Integer documentId, Integer studentId);

    @Query("select count (id) from StudyProgress where documentation.course.id = ?1 and student.id = ?2")
    Long countFinishDocument(Integer id, Integer userId);

    @Query("select count (id) from StudyProgress where videoClass.course.id = ?1 and progress >99.99d and student.id = ?2")
    Long countFinishVideoClass(Integer id, Integer userId);

    @Query("select a.progress from StudyProgress a where a.videoClass.course.id = ?1 and a.student.id = ?2")
    List<Double> findVideoProgressByCourseAndUser(Integer courseId, Integer userId);
}

