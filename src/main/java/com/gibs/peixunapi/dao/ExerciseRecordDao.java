package com.gibs.peixunapi.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.ExerciseRecord;

@Repository
public interface ExerciseRecordDao extends JpaRepository<ExerciseRecord, Integer>, JpaSpecificationExecutor<ExerciseRecord> {


    @Query("select count(a.id) from ExerciseRecord as a " +
            "inner join Exercises as b on a.exercise.id = b.id " +
            "where a.student.id = :studentId and b.documentation = :chapterId")
    Integer countRecordByStudentAndDocument(@Param("studentId") Integer studentId, @Param("chapterId") Integer chapterId);

    @Query("from ExerciseRecord where exercise.id = :exerciseId and student.id = :studentId")
    ExerciseRecord findByExercisesAndStudent(@Param("exerciseId") Integer exerciseId, @Param("studentId") Integer studentId);

    @Query("select distinct a from ExerciseRecord a where  a.student.id = :studentId  group by a.exercise.documentation, a.exercise.videoClass order by a.createTime desc")
    Page<ExerciseRecord> findLatestRecord(@Param("studentId") Integer userId, Pageable pageable);

    @Query(value = "select count(a.id)" +
            "from " +
            "(select a.id from exercise_record as a " +
            "inner join exercises as b on b.id=a.exercise_id " +
            "inner join video_class as c on c.id = b.video_class " +
            "where a.student_id = ?1 and c.course_id = ?2 " +
            "union all " +
            "select a.id from exercise_record as a " +
            "inner join exercises as b on b.id=a.exercise_id " +
            "inner join documentation as c on c.id = b.documentation " +
            "where a.student_id = ?1 and c.course_id = ?2 ) a" +
            "",nativeQuery = true)
    Long countRightStudentAndCourse(Integer userId, Integer courseId);
}

