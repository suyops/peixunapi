package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.CourseTeacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/02/9:49
 * @Version 1.0
 * @Description:
 */
@Repository
public interface CourseTeacherDao  extends JpaRepository<CourseTeacher, Integer>, JpaSpecificationExecutor<CourseTeacher> {
    @Query("from CourseTeacher where courseId=?1 and type =?2")
    List<CourseTeacher> findByCourseIdAndType(Integer courseId, Integer type);
}
