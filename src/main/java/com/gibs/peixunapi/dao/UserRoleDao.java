package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleDao extends JpaRepository<UserRole, Integer>, JpaSpecificationExecutor<UserRole> {


    @Query("select distinct roleId from UserRole where userId = ?1 ")
    List<Integer> findRoleIdByUserId(Integer id);

    @Modifying
    @Query("delete from UserRole  where userId = :userId" )
    void deleteByUser(@Param("userId")Integer userId);

    @Query("select userId from UserRole where roleId = ?1")
    List<Integer> findUserByRoleId(Integer roleId);
}

