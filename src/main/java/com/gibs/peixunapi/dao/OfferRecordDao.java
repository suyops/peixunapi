package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.OfferRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface OfferRecordDao extends JpaRepository<OfferRecord, Integer>, JpaSpecificationExecutor<OfferRecord> {

}

