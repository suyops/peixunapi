package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.RolePower;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/14/19:18
 * @Version 1.0
 * @Description:
 */
@Repository
public interface RolePowerDao extends JpaRepository<RolePower, Integer>, JpaSpecificationExecutor<RolePower> {

    @Query("from RolePower where roleId = ?1")
    List<RolePower> findByRoleId(Integer roleId);
}
