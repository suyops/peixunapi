package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.ExamTeacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamTeacherDao extends JpaRepository<ExamTeacher, Integer>, JpaSpecificationExecutor<ExamTeacher> {

	@Query("from ExamTeacher where examClassInfoId = ?1 and type = ?2")
    List<ExamTeacher> findByClassAndType(Integer classId, String type);

	@Modifying
	@Query(value = "delete from exam_teacher where exam_class_info_id = ?1 and type = ?2", nativeQuery = true)
	void deleteByClassId(Integer classId, String type);
}

