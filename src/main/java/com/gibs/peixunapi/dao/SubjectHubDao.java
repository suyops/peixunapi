package com.gibs.peixunapi.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.SubjectHub;

@Repository
public interface SubjectHubDao extends JpaRepository<SubjectHub, Integer>, JpaSpecificationExecutor<SubjectHub> {

	@Query(value = "select a.* from subject_hub as a  where a.id in ?1 order by FIND_IN_SET(a.id,?2)",nativeQuery = true)
	List<SubjectHub> findSubjectHubsByIdIn(List<Integer> idList, String ids);

	@Query("select count( a.id ) from SubjectHub as a inner join CourseSubjectHub as b " +
			" on a.id = b.subjectHub.id where b.course.id = ?1 and a.type = ?2")
    Long countByCourseAndType(Integer courseId, String message);



}

