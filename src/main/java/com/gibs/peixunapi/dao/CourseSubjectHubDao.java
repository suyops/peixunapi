package com.gibs.peixunapi.dao;

import java.util.List;

import com.gibs.peixunapi.model.Exercises;
import com.gibs.peixunapi.model.SubjectHub;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.CourseSubjectHub;

@Repository
public interface CourseSubjectHubDao extends JpaRepository<CourseSubjectHub, Integer>, JpaSpecificationExecutor<CourseSubjectHub> {
    @Query(value = "SELECT a.id " +
            "FROM course_subject_hub a " +
            "inner join subject_hub b on a.subject_hub_id = b.id " +
            "WHERE a.course_id = ?1 and b.type = ?2 " +
            "ORDER BY a.id ", nativeQuery = true)
    List<Integer> findIdListByCourseAndType(Integer courseId, String type);

    @Query(value = "SELECT DISTINCT a.*  " +
            "FROM course_subject_hub a " +
            "INNER JOIN subject_hub b ON a.subject_hub_id = b.id  " +
            "WHERE " +
            " (b.type = 'radio' and a.course_id = ?1 ) " +
            " OR (b.type = 'check' and a.course_id = ?1 ) " +
            " OR (b.type = 'judge' and a.course_id = ?1 ) " +
            "ORDER BY a.id", nativeQuery = true)
    List<CourseSubjectHub> findHangUpSubjectChooseList(Integer courseId);

    @Query(value = "select * from course_subject_hub where id in ?1 order by FIND_IN_SET(id,?2)", nativeQuery = true)
    List<CourseSubjectHub> findByIdIn(List<Integer> idList, String ids);

    @Query("from CourseSubjectHub where course.id = ?1 ")
    List<CourseSubjectHub> findByCourse(Integer link);

}

