package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.PowerMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/14/19:11
 * @Version 1.0
 * @Description:
 */
@Repository
public interface PowerMenuDao extends JpaRepository<PowerMenu, Integer>, JpaSpecificationExecutor<PowerMenu> {
    @Query("from PowerMenu where powerId in ?1 order by null")
    List<PowerMenu> findByPowerIdIn(List<Integer> powerIds);
}
