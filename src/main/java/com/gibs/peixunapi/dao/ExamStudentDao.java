package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.ExamStudent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamStudentDao extends JpaRepository<ExamStudent, Integer>, JpaSpecificationExecutor<ExamStudent> {


    Integer countByExamClassInfo_Id(Integer classId);

    @Query("select a from ExamStudent as a inner join ExamClassInfo as b " +
            "on a.examClassInfo.id = b.id " +
            "where (a.student.username like %?1% or a.student.business.name like %?1%) and b.id = ?2")
    List<ExamStudent> findByExamClass(String key, Integer classId);

    @Query("select a from ExamStudent as a where a.student.id = ?1 and a.examClassInfo.id = ?2")
    ExamStudent findByStudentAndClass(Integer studentId, Integer id);

    @Query("select a from ExamStudent as a where a.uniqueCode = ?1")
    ExamStudent findByUniqueCode(String uniqueCode);

    @Query(value = "select a.* from exam_student as a " +
            "where a.exam_status = 1 and a.correct_status = 0 and a.exam_class_info_id= ?1 order by a.update_time limit 1", nativeQuery = true)
    ExamStudent findCorrectingStudent(Integer classId);

    @Query(value = "select a.* from exam_student as a " +
            "where a.exam_status = 1 and a.correct_status = -1 and a.exam_class_info_id= ?1 order by a.update_time limit 1", nativeQuery = true)
    ExamStudent findCorrectStudent(Integer classId);

    @Query(value = "SELECT count( a.id ) FROM exam_student a " +
            "WHERE a.exam_class_info_id = ?1 and a.is_take_exam = 1 ", nativeQuery = true)
    Integer countNoExamNumber(Integer classId);

    @Query("select a from ExamStudent a where (a.student.username like %?1% or a.student.business.name like %?1%) and a.examClassInfo.id = ?2 ")
    Page<ExamStudent> findExamManageStudent(String key, Integer classId, Pageable pageable);

    @Query("select a from ExamStudent a where a.student.id = ?1 and a.takeExam = 1")
    Page<ExamStudent> findFinishExam(Integer studentId, Pageable pageable);

    @Query(value = "SELECT count( a.id ) FROM exam_student a " +
            " inner join exam_class_info as b on a.exam_class_info_id = b.id " +
            " inner join exam_info as c on b.exam_info_id = c.id " +
            "WHERE a.exam_class_info_id = ?1 " +
            "and a.is_take_exam = 1 " +
            "and a.exam_status = 1 " +
            "and a.correct_status = 1 " +
            "and a.final_score >= c.pass_score", nativeQuery = true)
    Integer countPassExamNumber(Integer classId);

    @Query(value = "SELECT a.* FROM exam_student a " +
            " inner join exam_class_info as b on a.exam_class_info_id = b.id " +
            " inner join exam_info as c on b.exam_info_id = c.id " +
            " LEFT JOIN certificate_student f ON (a.id = f.exam_student_id and a.exam_class_info_id=?1) " +
            "WHERE a.exam_class_info_id = ?1 " +
            "and a.is_take_exam = 1 " +
            "and a.exam_status = 1 " +
            "and a.correct_status = 1 " +
            "AND f.id IS NULL " +
            "and a.final_score >= c.pass_score", nativeQuery = true)
    List<ExamStudent> findPassExamStudent(Integer classId);

    @Modifying
    @Query("update ExamStudent set applyDelayedExam = 2 where student.id in ?1 and examClassInfo.id = ?2")
    void DelayedBatch(List<Integer> ids, Integer examClassId);

    @Modifying
    @Query("update ExamStudent set examStatus = 0 where examClassInfo.id = ?1")
    void startExam(Integer classId);

    @Query("select a.uniqueCode from ExamStudent as a where a.examClassInfo.id = ?1 and a.examStatus = 0")
    List<String> findUnFinish(Integer classId);

    @Modifying
    @Query("update ExamStudent set examStatus = 1 where examClassInfo.id = ?1")
    void completeExam(Integer classId);


    @Modifying
    @Query("update ExamStudent set correctStatus = 0 where examClassInfo.id = ?1")
    void correctStart(Integer classId);

    @Query("select a.uniqueCode from ExamStudent as a where a.examClassInfo.id = ?1 and a.correctStatus = 0")
    List<String> findUnCorrect(Integer classId);

    @Modifying
    @Query("update ExamStudent set correctStatus = 1 where examClassInfo.id = ?1")
    void correctFinish(Integer classId);


    @Query(value = "SELECT " +
            "es.exam_scores " +
            "FROM " +
            "exam_student AS es " +
            "CROSS JOIN exam_class_info AS eci " +
            "CROSS JOIN exam_info AS ei  " +
            "WHERE " +
            "es.exam_class_info_id = eci.id  " +
            "AND eci.exam_info_id = ei.id  " +
            "AND ei.course_id = ?2 " +
            "AND es.student_id = ?1 " +
            "ORDER BY " +
            "es.exam_scores DESC, " +
            "es.create_time DESC " +
            "LIMIT 1", nativeQuery = true)
    Integer findLatestPassExamScore(@Param("user") Integer userId, @Param("course") Integer courseId);

}

