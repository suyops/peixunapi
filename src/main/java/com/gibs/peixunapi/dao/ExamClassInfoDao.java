package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.ExamClassInfo;
import com.gibs.peixunapi.model.ExamInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ExamClassInfoDao extends JpaRepository<ExamClassInfo, Integer>, JpaSpecificationExecutor<ExamClassInfo> {

    @Query("select id from ExamClassInfo where examStatus = -1 and examInfo.id in ?1 order by null")
    List<?> isExamClassStart(List<ExamInfo> examInfoId);

    @Query("select count(a.id ) from ExamClassInfo as a inner join ExamInfo as b " +
            "on a.examInfo.id = b.id where a.examStatus >0 and b.id = ?1 ")
    Integer countFinishClass(Integer courseId);

    @Query("select count(a.id) from ExamClassInfo as a inner join ExamInfo as b " +
            "on a.examInfo.id = b.id where b.id = ?1 ")
    Integer countClass(Integer courseId);


    @Query("select a from ExamClassInfo a where (a.examInfo.course.name like %?1% or a.name like %?1%) order by a.id desc ")
    Page<ExamClassInfo> findByExamInfo(String key, Pageable pageable);

    @Query("select distinct a from ExamClassInfo a " +
            "inner join CourseTeacher as ct on ct.courseId = a.examInfo.course.id " +
            " where (a.examInfo.course.name like %?1% or a.name like %?1%) and a.creator.id = ?2 and ct.teacher.id =?2  order by a.id desc ")
    Page<ExamClassInfo> findByExamInfoAndUser(String key,Integer userId, Pageable pageable);
    @Query("select distinct a from ExamClassInfo a " +
            "inner join ExamStudent as ct on ct.examClassInfo.id = a.id " +
            " where ct.student.id = ?1  order by a.id desc ")
    Page<ExamClassInfo> findByStudent(Integer userId, Pageable pageable);
    @Query("select a.id from ExamClassInfo a where a.code = ?1 ")
    Integer findByCode(String code);

    @Query(value = "select a.* from exam_class_info a where a.id in ?1 order by FIND_IN_SET(a.id,?2)", nativeQuery = true)
    List<ExamClassInfo> findByIdIn(List<Integer> idList, String join);

    @Query(value = "select a.* from exam_class_info a " +
            "inner join exam_student b on b.exam_class_info_id = a.id " +
            "where b.student_id = ?1 and a.exam_start_time between ?2 and ?3 order by a.exam_start_time ",nativeQuery = true)
    List<ExamClassInfo> findPersionalExamBetween(Integer userId, String today, String maxDate);
}

