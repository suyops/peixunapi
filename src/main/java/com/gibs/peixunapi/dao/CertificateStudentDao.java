package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.CertificateStudent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/10/09/20:22
 * @Version 1.0
 * @Description:
 */

@Repository
public interface CertificateStudentDao extends JpaRepository<CertificateStudent, Integer>, JpaSpecificationExecutor<CertificateStudent> {

    @Query("select a from CertificateStudent as a where a.uniqueCode = ?1")
    CertificateStudent findByUniqueCode(String uniqueCode);

    @Query("select a from CertificateStudent as a where a.userId = ?1 ")
    List<CertificateStudent> findByUser(Integer userId);

    @Query(value = "select a.* from certificate_student as a where TO_DAYS(a.expiry_date) - TO_DAYS(?1) >= ?2",nativeQuery = true)
    List<CertificateStudent> findComingExpiry(Date time,Integer date);

    @Modifying
    @Query(value = "update certificate_student set is_expiry = 0 where TO_DAYS(?1) - TO_DAYS(expiry_date) >=0 and is_pause = 1 and status = 1 ",nativeQuery = true)
    void setExpiry(Date time);

    @Query(value = "select a.* from certificate_student as a where a.is_pause = 0 and is_expiry = 1",nativeQuery = true)
    List<CertificateStudent> getPause();
}

