package com.gibs.peixunapi.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.FileInfo;

@Repository
public interface FileInfoDao extends JpaRepository<FileInfo, Integer>, JpaSpecificationExecutor<FileInfo> {

    @Query("from FileInfo where name = ?1")
    FileInfo findDocumentPicture(String str);

    @Query("from FileInfo where  fileMd5 = ?1")
    FileInfo findByMd5(String fileMd5);
}

