package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.Power;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PowerDao extends JpaRepository<Power, Integer>, JpaSpecificationExecutor<Power> {

}

