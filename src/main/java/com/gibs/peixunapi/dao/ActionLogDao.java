package com.gibs.peixunapi.dao;

import com.gibs.peixunapi.model.ActionLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ActionLogDao extends JpaRepository<ActionLog, Integer>, JpaSpecificationExecutor<ActionLog> {

}

