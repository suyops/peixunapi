package com.gibs.peixunapi.dao;

import java.util.Date;
import java.util.List;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.Course;

@Repository
public interface CourseDao extends JpaRepository<Course, Integer>, JpaSpecificationExecutor<Course> {


    @Query(value = "select a.* from course a where a.deleted=false and a.is_publish = 1",nativeQuery = true)
    Page<Course> findByEndTimeAndAudit(String time, Pageable pageable);

    @Modifying
    @Query("update Course set learnChapterMaxNum = ?2,learnRestrict = ?3 where id = ?1")
    void updateLearnRestrict(Integer courseId, Integer learnChapterMaxNum, boolean b);

    @Modifying
    @Query("update Course set isPublish = 0 where id = ?1")
    void applyAudit(Integer courseId);
}

