package com.gibs.peixunapi.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.gibs.peixunapi.model.Notice;

@Repository
public interface NoticeDao extends JpaRepository<Notice, Integer>, JpaSpecificationExecutor<Notice> {

	@Query("from Notice where accepter = :accepter")
	List<Notice> findByAccepter(@Param("accepter") Integer userId);

	@Modifying
	@Query("delete from Notice where accepter = ?1 and read = 1 and readTime is not null")
	void deleteReadNotice(Integer id);
}

