package com.gibs.peixunapi.Interceptor;

import com.gibs.peixunapi.config.MyRequestWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * @author liangjiawei
 * @date 2021/03/30/10:04
 * @Version 1.0
 * @Description:
 */
@Slf4j
@Configuration
public class ParamsInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String test = "";
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            if (request instanceof MyRequestWrapper) {
                // 签名处理过程 start....
                test = ((MyRequestWrapper) request).getBodyStr();
            }
        }
       log.info(
                "URI:{}\t\t\n " +
                        "Method:{}\t\t\n " +
                        "ContentType:{}\t\t\n" +
                        "Query:{}\t\t\n" +
                        "Body:{}",
                request.getRequestURI(),
                request.getMethod(),
                request.getContentType(),
                request.getQueryString()==null?"":request.getQueryString().replaceAll("\\$\\{",""),
                test==null?"":test.replaceAll("\\$\\{",""));



        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
    }

    private String getBodyString(HttpServletRequest request) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStream inputStream = null;
        BufferedReader reader = null;
        try {
            inputStream = request.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
            String line = "";
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString().trim();
    }

}
