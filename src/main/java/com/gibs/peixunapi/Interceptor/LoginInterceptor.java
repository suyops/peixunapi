package com.gibs.peixunapi.Interceptor;

import com.alibaba.fastjson.JSON;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.UserService;
import com.gibs.peixunapi.utils.ResultUtil;
import com.gibs.peixunapi.utils.SecretUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;

import static com.gibs.peixunapi.enums.ResultEnum.DATA_NOT_FIND;
import static com.gibs.peixunapi.enums.ResultEnum.REQUEST_ERROR;
import static com.gibs.peixunapi.enums.RoleEnum.STUDENT;

/**
 * @author liangjiawei
 * @date 2020/10/11/15:59
 * @Version 1.0
 * @Description:
 */
@Slf4j
@Configuration
public class LoginInterceptor implements HandlerInterceptor {

    @Autowired
    private UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //用循环遍历所有请求头，并通过 getHeader() 方法获取一个指定名称的头字段
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Type", "text/html;charset=UTF-8");
        if (!handler.getClass().isAssignableFrom(HandlerMethod.class)) {
            System.out.println("cat cast handler to HandlerMethod.class");
            return true;
        }

        Result result;
        if (request.getHeader("authorization") == null) {
            log.error("请求失败，没携带加密信息");
            result = ResultUtil.fail(REQUEST_ERROR);
            response.getWriter().write(JSON.toJSONString(result));
            return false;
        }

        String uuid = SecretUtils.isEncoding(request.getHeader("authorization"));
        String enterRole = request.getHeader("role");
        log.info("Auth拦截:{}", new Date());
        User user = userService.findByUUID(uuid);
        user.setRole(enterRole);
        //判断加密内容是否匹配
        if (user == null) {
            log.error("找不到该用户");
            result = ResultUtil.fail(DATA_NOT_FIND);
            response.getWriter().write(JSON.toJSONString(result));
            return false;
        }
        request.setAttribute("user", user);

        return true;

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
