package com.gibs.peixunapi.handle;

import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.utils.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

import static com.gibs.peixunapi.enums.ResultEnum.*;

/**
 * @author liangjiawei
 * @date 2020/08/20/11:18
 * @Version 1.0
 * @Description:
 */


@ControllerAdvice
@Slf4j
public class GlobalExceptionHandle {

    /**
     * 处理自定义的业务异常
     *
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = BaseException.class)
    @ResponseBody
    public Result<T> baseExceptionHandler(HttpServletRequest req, BaseException e) {
        log.error("发生业务异常！原因是：{}\n方法名:{}\t第{}行\n详细:{}", e.getMsg(), e.getStackTrace()[0].getClassName() + "," + e.getStackTrace()[0].getClassName(), e.getStackTrace()[0].getLineNumber(), e.getStackTrace());
        if (e != null) {
            ResultUtil.success(DATA_NOT_FIND,new ArrayList<>());
        }
        return ResultUtil.error(e.getCode(), e.getMsg());
    }

    @ExceptionHandler(value = SaveException.class)
    @ResponseBody
    public Result<T> saveExceptionHandler(HttpServletRequest req, SaveException e) {
        log.error("发生业务异常！原因是：{}\n方法名:{}\t第{}行\n详细:{}", e.getMsg(), e.getStackTrace()[0].getClassName() + "," + e.getStackTrace()[0].getClassName(), e.getStackTrace()[0].getLineNumber(), e.getStackTrace());
        return ResultUtil.error(SAVE_FAIL.getCode(), SAVE_FAIL.getMsg());
    }

    @ExceptionHandler(value = NoDataException.class)
    @ResponseBody
    public Result<PageData<T>> noDataExceptionHandler(HttpServletRequest req, NoDataException e) {
        if ("page".equals(e.getMsg())) {
            return ResultUtil.successPage(EMPTY_DATA, new PageData(0, 10, 0, new ArrayList<>()));
        }
        return ResultUtil.success(EMPTY_DATA);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    @ResponseBody
    public Result<T> entityNotFoundException(HttpServletRequest request, EntityNotFoundException e) {
        log.error("发生业务异常！原因是：{}\n方法名:{}\t第{}行\n详细:{}", e.getMessage(), e.getStackTrace()[0].getClassName() + "," + e.getStackTrace()[0].getClassName(), e.getStackTrace()[0].getLineNumber(), e.getStackTrace());

        return ResultUtil.error(DATA_NOT_FIND);
    }

    @ResponseBody
    @ExceptionHandler(NullPointerException.class)
    public Result<T> errorHandler(NullPointerException ex) {
        log.error("发生业务异常！原因是：{}\n方法名:{}\t第{}行\n详细:{}", ex.getMessage(), ex.getStackTrace()[0].getClassName() + "," + ex.getStackTrace()[0].getClassName(), ex.getStackTrace()[0].getLineNumber(), ex.getStackTrace());

        return ResultUtil.error(UNKNOWN_ERROR);
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result<T> handleException(HttpServletRequest request, Exception e) {
        if (e instanceof BaseException) {
            baseExceptionHandler(request, (BaseException) e);
        } else if (e instanceof EntityNotFoundException) {
            entityNotFoundException(request, (EntityNotFoundException) e);
        } else if (e instanceof NoDataException) {
            noDataExceptionHandler(request, (NoDataException) e);
        } else {
            log.error("发生业务异常！原因是：{}\n方法名:{}\t第{}行\n详细:{}", e.getMessage(), e.getStackTrace()[0].getClassName() + "," + e.getStackTrace()[0].getClassName(), e.getStackTrace()[0].getLineNumber(), e.getStackTrace());

            return ResultUtil.error(SERVER_ERROR);
        }
        return null;
    }
}
