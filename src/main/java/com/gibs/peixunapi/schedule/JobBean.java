package com.gibs.peixunapi.schedule;

import com.gibs.peixunapi.model.Scheduled;
import com.gibs.peixunapi.utils.SpringBeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author liangjiawei
 * @date 2020/10/29/18:40
 * @Version 1.0
 * @Description:
 */
@Slf4j
public class JobBean extends QuartzJobBean {


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) {
        Scheduled scheduled = (Scheduled) jobExecutionContext.getJobDetail().getJobDataMap().get("method");
        String beanName = scheduled.getClassName();
        //手动从Spring容器中获取所需要的bean
        log.info("{}", scheduled);
//        String returnType = "beanEntity.getReturnType()";
        String methodName = scheduled.getMethodName();
        String beanParams = scheduled.getParamId();
        try {
            //通过反射获取指定的公有方法
            Object targetBean = SpringBeanUtils.getBean(beanName);
            //获取方法
            Method targetMethod;
            if (StringUtils.isNotBlank(scheduled.getParamId())) {
                targetMethod = targetBean.getClass().getMethod(methodName, Integer.class);
                //调用方法
                targetMethod.invoke(targetBean, Integer.parseInt(beanParams));
            } else {
                targetMethod = targetBean.getClass().getMethod(methodName);
                //调用方法
                targetMethod.invoke(targetBean);
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}
