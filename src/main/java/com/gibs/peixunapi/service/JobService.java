package com.gibs.peixunapi.service;

import com.gibs.peixunapi.model.Scheduled;
import org.quartz.SchedulerException;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/10/28/18:25
 * @Version 1.0
 * @Description:
 */
public interface JobService {


    /**
     * 修改定时任务启用状态 0 启用 1 停用
     *
     * @param id
     * @throws Exception
     */
    void isEnableScheduleJob(Integer id) throws Exception;

    /**
     * 暂停/恢复 定时任务
     *
     * @param id
     * @throws Exception
     */
    void pauseScheduleJob(Integer id) throws Exception;

    /**
     * 删除定时任务
     *
     * @param id
     * @throws Exception
     */
    void deleteScheduleJob(Integer id) throws Exception;

    /**
     * 立即运行定时任务
     *
     * @param id
     * @throws Exception
     */
    void runScheduleJob(Integer id) throws Exception;


    /**
     * 新增任务
     *
     * @param scheduleJob
     * @throws Exception
     */
    void addOrUpdateJob(Scheduled scheduleJob) throws Exception;

    void updateJob(Scheduled schedule);

    /**
     * Scheduler 删除指定定时任务
     *
     * @param scheduleJob
     * @throws Exception
     */
    void stopJob(Scheduled scheduleJob) throws SchedulerException;

    /**
     * 删除全部定时任务
     *
     * @throws SchedulerException
     */
    void stopAllJob() throws SchedulerException;

    /**
     * Scheduler 暂停定时任务
     *
     * @param scheduleJob
     * @throws Exception
     */
    void pauseJob(Scheduled scheduleJob) throws SchedulerException;

    /**
     * Scheduler 恢复定时任务
     *
     * @param scheduleJob
     * @throws Exception
     */
    void resumejob(Scheduled scheduleJob) throws SchedulerException;

    /**
     * 立即执行一个job
     *
     * @param scheduleJob
     * @throws SchedulerException
     */
    void runAJobNow(Scheduled scheduleJob) throws SchedulerException;

    /**
     * 获取所有计划中的任务列表
     *
     * @return
     * @throws SchedulerException
     */
    List<Scheduled> allDoingSchedule() throws SchedulerException;

    /**
     * 获取所有执行中的任务列表
     *
     * @return
     * @throws SchedulerException
     */
    List<Scheduled> getRunningJob() throws SchedulerException;

    /**
     * 暂停所有任务
     *
     * @throws SchedulerException
     */
    void pauseAllJobs() throws SchedulerException;

    /**
     * 恢复所有任务
     *
     * @throws SchedulerException
     */
    void resumeAllJobs() throws SchedulerException;

    void addTodayJobs();

    /**
     * 获取数据库中所有任务列表
     *
     * @return
     */
    List<Scheduled> allSchedule();

    /**
     * 修改任务的开始时间
     */
    void editJob(Scheduled scheduled);

    /**
     * 根据任务名称获取任务
     *
     * @return
     */
    Scheduled getJob(String taskName);
}
