package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.VO.post.PostSubjectHubVO;
import com.gibs.peixunapi.VO.show.ShowTestPaperVO;
import com.gibs.peixunapi.enums.SubjectEnum;
import com.gibs.peixunapi.model.SubjectHub;

import java.util.List;
import java.util.Map;

public interface SubjectHubService {

    /**
     * 新增题目
     *
     * @param object 题目VO对象
     * @return
     */
    SubjectHub create(SubjectHubVO object);

    /**
     * 编辑单条题目
     *
     * @param object
     */
    void edit(PostSubjectHubVO object);

    /**
     * 删除单条题目
     *
     * @param id id
     * @return
     */
    void delete(Integer id);

    /**
     * 马上新增并返回实体id
     *
     * @param object 题库VO类
     * @return Subject.id
     */
    Integer persist(SubjectHubVO object);

    /**
     * 根据题库idList返回题目VO实体类列表
     *
     * @param idList 题库idi列表
     * @return 题库VO类列表
     */
    List<SubjectHubVO> getVOListByIdList(List<Integer> idList);

    /**
     * 查询该课程下不同类型的题目数量
     *
     * @param courseId 课程名
     * @param enums    题目类型
     * @return
     */
    Integer countByCourseAndType(Integer courseId, SubjectEnum enums);

    /**
     * 获取单条题目VO
     *
     * @param id
     * @return
     */
    SubjectHubVO getVO(Integer id);

    /**
     * 获取单条题目
     *
     * @param id
     * @return
     */
    SubjectHub get(Integer id);

    /**
     * 保存导入题目
     *
     * @param subjectHub
     * @return
     */
    SubjectHub saveImport(SubjectHub subjectHub);

    /**
     * 查询该课程下题库的题目选项列表
     * @param courseId
     * @param key
     * @param type
     * @return
     */
    List<SubjectHubVO> getSubjectHubOption(Integer courseId, String key, String type);

    /**
     * 整理提醒
     * @param subjectHubVOMap
     * @return
     */
    ShowTestPaperVO sortSubject(Map<String, List<SubjectHubVO>> subjectHubVOMap);
}

