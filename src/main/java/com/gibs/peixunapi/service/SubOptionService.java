package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.SubOptionVO;
import com.gibs.peixunapi.model.SubOption;
import com.gibs.peixunapi.model.SubjectHub;
import com.gibs.peixunapi.result.Result;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SubOptionService {

    /**
     * 新增题目时新增选项,直接传递题目实体类,减少数据库I/O操作,用于subjectHubService调用
     *
     * @param objectList 题目选项VO类集合
     * @param subjectHub 题目实体类
     * @return
     */
    Result create(List<SubOptionVO> objectList, SubjectHub subjectHub);


    /**
     * 编辑单条选项
     * @param voList
     * @param subjectHubId
     * @return
     */
    Result edit(List<SubOptionVO> voList, Integer subjectHubId);

    /**
     * 删除单条选项
     *
     * @param id 选项id
     * @return
     */
    Result delete(Integer id);

    Result get(Integer id);

    /**
     * 根据题目id来获取选项列表
     *
     * @param subjectHubId 题目id
     * @return
     */
    Result getList(Integer subjectHubId);


    /**
     * 获取选项VO
     * @param subjectHubId
     * @return
     */
    List<SubOptionVO> getSubOptions(Integer subjectHubId);

    /**
     * 保存导入数据
     * @param subOption
     */
    void saveImport(SubOption subOption);
}

