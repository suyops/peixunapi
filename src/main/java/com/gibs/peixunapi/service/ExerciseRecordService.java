package com.gibs.peixunapi.service;

import com.gibs.peixunapi.DTO.ShowExerciseRecordDTO;
import com.gibs.peixunapi.VO.post.StudyRecordVO;
import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.VO.show.ShowExerciseRecordVO;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.model.ExerciseRecord;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;

import java.util.List;

public interface ExerciseRecordService {


    /**
     * 新增学员用户的该章节练习题回答记录
     *
     * @param studentId    学生用户id
     * @param recordVOList 练习题记录VO类
     * @return
     */
    Result create(Integer studentId, List<StudyRecordVO> recordVOList);

    /**
     * 获取学员用户的练习记录
     *
     * @param studentId 学生用户id
     * @param courseId  课程id
     * @param chapterId 章节id
     * @param type      题目类型
     * @param current   页数
     * @param limit     最大条数
     * @return 统一处理结果
     */
    PageData<ShowExerciseRecordDTO> getListPage(Integer studentId, Integer courseId, Integer chapterId, String type, Integer current, Integer limit);

    /**
     * 获取该学生在此章节的习题记录个数
     *
     * @param student  学生id
     * @param exercise 练习题id
     * @return
     */
    Integer countStudentAndDocument(Integer student, Integer exercise);

    /**
     * 获取该学生完成此章节练习题数量
     *
     * @param id
     * @param userId
     * @param document
     * @return
     */
    Integer getFinishExerciseCountByStudent(Integer id, Integer userId, CourseEnum document);

    /**
     * 查询练习记录
     *
     * @param exerciseId 练习题
     * @param studentId  学生id
     * @return
     */
    ExerciseRecord getByExercisesAndStudent(Integer exerciseId, Integer studentId);

    /**
     * 查询最新练习记录
     *
     * @param userId
     * @param page
     * @param limit
     * @return
     */
    PageData<OptionVO> getLatestListPage(Integer userId, Integer page, Integer limit);

    /**
     * 查询学生此课程的正确答题数
     *
     * @param userId
     * @param courseId
     * @return
     */
    Integer countRightStudentAndCourse(Integer userId, Integer courseId);
}

