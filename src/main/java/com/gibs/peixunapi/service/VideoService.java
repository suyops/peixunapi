package com.gibs.peixunapi.service;

import com.gibs.peixunapi.model.Video;

public interface VideoService {

    /**
     * 保存视频
     *
     * @param object
     * @return
     */
    Video save(Video object);

    /**
     * 获取视频
     *
     * @param id id
     * @return
     */
    Video get(Integer id);

}

