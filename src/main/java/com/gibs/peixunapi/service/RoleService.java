package com.gibs.peixunapi.service;

import com.gibs.peixunapi.model.Menu;

import java.util.List;

public interface RoleService {

    /**
     * 根据code获取id
     *
     * @param code
     * @return
     */
    Integer getRoleIdByCode(String code);


    /**
     * 获取菜单
     *
     * @param role 角色id
     * @return
     */
    List<Menu> getMenu(String role);

    /**
     * 更该用户角色
     *
     * @param roles  角色code ,
     * @param userId 用户id
     */
    void changeRole(String roles, Integer userId);
}

