package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.post.DailyScoreVO;
import com.gibs.peixunapi.VO.show.AdmissionTicketVO;
import com.gibs.peixunapi.VO.show.PassExamStudentVO;
import com.gibs.peixunapi.VO.show.ScoreManageVO;
import com.gibs.peixunapi.VO.show.StudentInExamClassVO;
import com.gibs.peixunapi.model.ExamStudent;
import com.gibs.peixunapi.result.PageData;

import java.util.List;

public interface ExamStudentService {

    ExamStudent get(Integer id);

    /**
     * 批量添加学生进去考试班级
     *
     * @param studentIds  学生ids,用","隔开
     * @param examClassId 考试场次id
     * @return
     */
    Boolean addStudentToExam(String studentIds, Integer examClassId);

    /**
     * 获取该场次考生数量
     *
     * @param classId 考试场次id
     * @return
     */
    Integer getClassNumber(Integer classId);

    /**
     * 查询该报名该场考试学生列表
     *
     * @param classId 考试场次id
     * @param page    页数
     * @param limit   最大条数
     * @return
     */
    PageData getSignUpList(Integer classId, Integer page, Integer limit);

    /**
     * 学员自主报名考试
     *
     * @param classId   班级id
     * @param studentId 学生id
     * @return
     */
    Boolean signUpClass(Integer classId, Integer studentId);

    /**
     * 删除考生
     *
     * @param studentIds  学生id
     * @param examClassId 班级id
     * @return
     */
    boolean deleteStudentToExam(String studentIds, Integer examClassId);

    /**
     * 获取考生列表
     *
     * @param name    学生姓名 搜索关键字
     * @param classId 考试班级id
     * @param page    页数
     * @param limit   最大条数
     * @return
     */
    PageData<StudentInExamClassVO> getStudentListInExamClass(String name, Integer classId, Integer page, Integer limit);

    /**
     * 考试入场校验
     *
     * @param code      考场编码
     * @param studentId 学生id
     * @return
     */
    void checkExamCode(String code, Integer studentId);

    /**
     * 查询考生考试信息
     *
     * @param classId   课程id
     * @param studentId 学生id
     * @return
     */
    StudentInExamClassVO getExamStudent(Integer classId, Integer studentId);

    /**
     * 根据学生id以及考试班级id精确查找
     *
     * @param classId
     * @param studentId
     * @return
     */
    ExamStudent getByStudentAndClass(Integer classId, Integer studentId);

    /**
     * 根据准考证获得学生考试信息
     *
     * @param uniqueCode 准考证
     * @return
     */
    ExamStudent getByUniqueCode(String uniqueCode);

    /**
     * 考试交卷
     *
     * @param uniqueCode 准考证
     */
    void submitPaper(String uniqueCode);

    /**
     * 申请缓考
     *
     * @param studentId   学生id
     * @param examClassId 考试场次id
     */
    void applyForDelayed(Integer studentId, Integer examClassId);

    /**
     * 管理员操作批量缓考
     *
     * @param studentIds  学生ids
     * @param examClassId 考试场次id
     */
    void applyForDelayedBatch(String studentIds, Integer examClassId);

    /**
     * 审批缓考
     *
     * @param ids         学生id
     * @param examClassId 考试场次id
     * @param delayed
     */
    void checkDelayed(String ids, Integer examClassId, Integer delayed);

    /**
     * 查找等待考卷的考生信息
     *
     * @param classId 考试班级id
     * @return
     */
    ExamStudent getCorrect(Integer classId);

    /**
     * 完成学生考试阅卷状态,并自动生成笔试成绩
     *
     * @param uniqueCode 准考证号
     * @return
     */
    boolean finishCorrect(String uniqueCode);

    /**
     * 获取实际参加考试人数
     *
     * @param classId 考试场次id
     * @return
     */
    Integer countExamNumber(Integer classId);

    /**
     * 考务管理-获取所有进行进行中的考试信息
     *
     * @param key
     * @param classId
     * @param page
     * @param limit
     * @return
     */
    PageData getExamManageStudentList(String key, Integer classId, Integer page, Integer limit);

    /**
     * 根据班级id学生id获取学生目前考试状态
     *
     * @param classId   班级id
     * @param studentId 学生id
     * @return
     */
    String getStatusByExamClassAndStudent(Integer classId, Integer studentId);

    /**
     * 根据学生id获取学生所有课程成绩
     *
     * @param studentId 用户id
     * @param page      页数
     * @param limit     最大条数
     * @return
     */
    PageData getStudentScoreManage(Integer studentId, Integer page, Integer limit);

    /**
     * 成绩管理列表
     *
     * @param key
     * @param classId 考试班级id
     * @return
     */
    List<ScoreManageVO> getScoreManageList(String key, Integer classId);

    /**
     * 编辑考生平时分数
     *
     * @param vo 平时分VO
     * @return
     */
    Integer editDailyScore(DailyScoreVO vo);


    /**
     * 获取通过考试人数
     *
     * @param classId 考试场次id
     * @return
     */
    Integer countPassExamNumber(Integer classId);

    /**
     * 获取通过考试学生列表
     *
     * @param classId 考试场次id
     * @return
     */
    List<PassExamStudentVO> getPassExamStudent(Integer classId);

    /**
     * 获取电子准考证相关信息
     *
     * @param userId
     * @param classId
     * @return
     */
    AdmissionTicketVO getAdmissionTicket(Integer userId, Integer classId);

    /**
     * 获取最近一次最考分考试
     *
     * @param userId
     * @param courseId
     * @return
     */
    Integer getLatestPassExamScore(Integer userId, Integer courseId);


}

