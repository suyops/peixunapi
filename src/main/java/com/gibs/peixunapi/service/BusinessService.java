package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.post.BusinessVO;
import com.gibs.peixunapi.VO.show.BusinessListVO;
import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.model.Business;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;

import java.util.List;

public interface BusinessService {

    /**
     * 根据前端传来的VO类新建企业信息
     *
     * @param business BusinessVO类
     * @return
     */
    Result create(BusinessVO business);

    /**
     * 根据前端传来的VO类更新企业信息
     *
     * @param business BusinessVO类
     * @return
     */
    Result save(BusinessVO business);

    /**
     * 删除企业
     * @param id id
     * @return
     */
    Boolean delete(Integer id);

    /**
     * 根据id查找企业信息VO
     *
     * @param id id
     * @return
     */
    BusinessVO getVO(Integer id);

    /**
     * 根据id查找企业信息
     *
     * @param id id
     * @return
     */
    Business get(Integer id);

    /**
     * 根据邀请码查询
     * @param invitedCode
     * @return
     */
    Business getByInvitedCode(String invitedCode);

    /**
     * 模糊查询企业信息列表
     *
     * @param name 企业名称
     * @param status 状态
     * @param page 页数
     * @param limit 最大条数
     * @return
     */
    PageData<BusinessListVO> getListPage(String name, Integer status, Integer page, Integer limit);

    /**
     * 公司选项列表
     * @param key
     * @return
     */
    List<OptionVO> getOptions(String key);

    /**
     * 修改公司状态
     *
     * @param businessId 公司id
     * @param status 状态
     */
    void changeStatus(Integer businessId, Integer status);

    /**
     * 计算公司人数
     *
     * @param businessId
     * @return
     */
    Integer countByBusiness(Integer businessId);

    /**
     * 保存管理员
     *
     * @param business
     */
    void saveManager(Business business);

    List<OptionVO> getDepartmentOptions();
}

