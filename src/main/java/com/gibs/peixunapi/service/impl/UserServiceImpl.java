package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.show.*;
import com.gibs.peixunapi.VO.post.BusinessVO;
import com.gibs.peixunapi.VO.post.UserVO;
import com.gibs.peixunapi.dao.UserDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.mapper.UserMapper;
import com.gibs.peixunapi.model.Business;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.DigestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.CourseEnum.ASSIST_TEACHER;
import static com.gibs.peixunapi.enums.CourseEnum.TEACHER;
import static com.gibs.peixunapi.enums.ResultEnum.*;
import static com.gibs.peixunapi.enums.RoleEnum.BUSINESS_ADMIN;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {

    final static String DEFAULT_PASSWORD = "123456";

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserService userService;
    @Autowired
    private BusinessService businessService;

    @Autowired
    private FileInfoService fileInfoService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result create(UserVO vo) {

        if (userDao.findByUsername(vo.getUsername()) != null) {
            throw new BaseException(USER_ALREADY_EXISTS);
        }
        if (userDao.findByTelephone(vo.getTelephone()) != null) {
            throw new BaseException(TELEPHONE_ALREADY_EXISTS);
        }
        User user = new User();
        vo.setId(null);
        ConverterUtil.copyNotBlankProperties(vo, user);
        Business business;
        //公司id以及邀请码二选一必填
        vo.setBusinessId(1);
        if (StringUtils.isNotBlank(vo.getInvitedCode()) && vo.getBusinessId() == null) {
            //公司邀请码为自主注册账户
            business = businessService.getByInvitedCode(vo.getInvitedCode());
        } else if (vo.getBusinessId() != null && StringUtils.isBlank(vo.getInvitedCode())) {
            //公司id为管理员创建此账户
            business = businessService.get(vo.getBusinessId());
            user.setAudit(1);
        } else {
            throw new BaseException(BUSINESS_MUST_POST);
        }

        user.setBusiness(business);
        if (vo.getAvatar() != null) {
            user.setAvatar(fileInfoService.get(vo.getAvatar()));
        }
        //密码加密
        user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
        try {
            User newOne = userDao.save(user);
            //绑定关系
            if (!vo.getRole().isEmpty() && vo.getRole().contains(",")) {
                List<String> codes = Arrays.stream(vo.getRole().split(",")).collect(Collectors.toList());
                for (String code : codes) {
                    Integer roleId = roleService.getRoleIdByCode(code);
                    userRoleService.create(newOne.getId(), roleId);
                    //绑定管理员
                    if (code.equals(BUSINESS_ADMIN.getCode())) {
                        business.setManagerId(newOne.getId());
                        businessService.saveManager(business);
                    }
                }
            } else if (!vo.getRole().isEmpty()) {
                Integer roleId = roleService.getRoleIdByCode(vo.getRole());
                userRoleService.create(newOne.getId(), roleId);
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(CREATE_FAIL, e);
        }
        return ResultUtil.success(CREATE_SUCCESS);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result create(User object,String role,Integer businessId) {
        try {
            Business business = businessService.get(businessId);
            object.setAudit(1);
            User newOne = userDao.save(object);
            //绑定关系
            if (!role.isEmpty() && role.contains(",")) {
                List<String> codes = Arrays.stream(role.split(",")).collect(Collectors.toList());
                for (String code : codes) {
                    Integer roleId = roleService.getRoleIdByCode(code);
                    userRoleService.create(newOne.getId(), roleId);
                    //绑定管理员
                    if (code.equals(BUSINESS_ADMIN.getCode())) {
                        business.setManagerId(newOne.getId());
                        businessService.saveManager(business);
                    }
                }
            } else if (!role.isEmpty()) {
                Integer roleId = roleService.getRoleIdByCode(role);
                userRoleService.create(newOne.getId(), roleId);
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(CREATE_FAIL, e);
        }
        return ResultUtil.success(CREATE_SUCCESS);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public Result save(UserVO showUserVO) {
        try {

            User user = userService.get(showUserVO.getId());
            //排除null值数据
            showUserVO.setPassword(user.getPassword());
            showUserVO.setId(user.getId());
            ConverterUtil.copyInSpecifyProperties(showUserVO, user, "username", "post", "telephone", "certificateType", "certificateNumber", "email", "sex");

            if (showUserVO.getBusinessId() != null && !showUserVO.getBusinessId().equals(user.getBusiness().getId())) {
                Business business = businessService.get(showUserVO.getBusinessId());
                user.setBusiness(business);
            }
            if (showUserVO.getAvatar() != null) {
                user.setAvatar(fileInfoService.get(showUserVO.getAvatar()));
            }
            if (!StringUtils.isBlank(showUserVO.getRole())) {
                userRoleService.deleteAll(user.getId());
                roleService.changeRole(showUserVO.getRole(), user.getId());
            }

            userDao.save(user);
            return ResultUtil.success(UPDATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(UPDATE_FAIL, e);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void delete(Integer id) {
        try {
            userDao.deleteUser(id);
            userRoleService.deleteAll(id);

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(DELETE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    public Result deleteBatch(List<Integer> id) {
        try {
            id.forEach(i -> userDao.deleteUser(i));

            return ResultUtil.success(DELETE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(DELETE_FAIL, e);
        }
    }

    @Override
    public User get(Integer id) {
        return userDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Result getVO(Integer id) {
        User user = get(id);

        ShowUserVo showUserVo = new ShowUserVo();
        ConverterUtil.copyProperties(user, showUserVo, "password", "loginTime", "audit", "status");

        BusinessVO businessVO = new BusinessVO();
        ConverterUtil.copyInSpecifyProperties(user.getBusiness(), businessVO, "id", "name", "shortName");
        showUserVo.setBusiness(businessVO);

        List<RoleVO> roleVOList = userRoleService.getRoleVOListByUserId(user.getId());
        showUserVo.setRoleVOList(roleVOList);

        return ResultUtil.success(FIND_SUCCESS, showUserVo);
    }


    @Override
    public Result getList(String name) {
        try {

            List<Integer> idList = userMapper.findByName(name);

            List<User> list = userService.findByIdIn(idList);

            List<ShowUserVo> VOList = new ArrayList<>();
            for (User user : list) {
                ShowUserVo showUserVo = new ShowUserVo();
                ConverterUtil.copyProperties(user, showUserVo, "loginTime", "audit", "status",
                        "certificateType", "certificateNumber");

                BusinessVO businessVO = new BusinessVO();
                ConverterUtil.copyInSpecifyProperties(user.getBusiness(), businessVO, "name", "shortName");
                showUserVo.setBusiness(businessVO);

                List<RoleVO> roleVOList = userRoleService.getRoleVOListByUserId(user.getId());
                showUserVo.setRoleVOList(roleVOList);


                VOList.add(showUserVo);
            }
            if (Collections.isEmpty(list)) {
                throw new NoDataException();
            }

            return ResultUtil.success(FIND_SUCCESS, VOList);
        } catch (Exception e) {
            return ResultUtil.error(FIND_FAIL, e);
        }
    }


    @Override
    public Result getUserList(String username, Integer businessId, Integer audit, Integer page, Integer limit) {
        List<Integer> userIdList = userRoleService.getUserIdList(username, businessId, audit, page, limit);

        Pageable pageable = PageRequest.of(page - 1, limit);
        Page<User> pageData = userService.findByIdIn(userIdList, pageable);

        if (pageData.getContent().isEmpty()) {
            throw new NoDataException("page");
        }

        List<UserListVO> voList = new ArrayList<>();
        for (User user : pageData.getContent()) {
            UserListVO vo = new UserListVO();
            ConverterUtil.copyProperties(user, vo, "sort");
            vo.setSort((pageData.getContent().indexOf(user) + 1) * 10 * page);
            vo.setBusinessName(user.getBusiness().getName());

            String roleName = userRoleService.getRoleNameByUserId(user.getId());
            vo.setRoleName(roleName);

            vo.setAudit(user.getAudit() == 1 ? "通过" : "未审核");
            vo.setStatus(user.getStatus() == 1 ? "正常" : "禁用");

            voList.add(vo);
        }

        int total = (int) pageData.getTotalElements();
        PageData<UserListVO> data = new PageData<>(total, limit, page, voList);
        return ResultUtil.successPage(SUCCESS, data);
    }

    @Override
    public Result getUserList(String username, Integer businessId, String code, Integer status, Integer audit, Integer page, Integer limit) {

        PageInfo<Integer> pageData = userRoleService.getUserIdList(username, code, businessId, status, audit, page, limit);
        List<Integer> userIdList = pageData.getList();
        List<User> list = userService.findByIdIn(userIdList);
        if (list.isEmpty()) {
            throw new NoDataException("page");
        }

        List<UserListVO> voList = new ArrayList<>();
        for (User user : list) {
            UserListVO vo = new UserListVO();
            ConverterUtil.copyProperties(user, vo, "sort");
            vo.setSort((list.indexOf(user) + 1) * limit * (page - 1));
            String roleName = userRoleService.getRoleNameByUserId(user.getId());

            vo.setBusinessName(user.getBusiness().getName());
            vo.setAudit(user.getAudit() == 1 ? "通过" : "未审核");
            vo.setStatus(user.getStatus() == 1 ? "正常" : "禁用");
            vo.setRoleName(roleName);
            voList.add(vo);
        }

        int total = (int) pageData.getTotal();
        PageData<UserListVO> data = new PageData<>(total, limit, page, voList);
        return ResultUtil.successPage(SUCCESS, data);
    }

    @Override
    public Result getOptions(String key, Integer page, Integer limit) {
        List<User> userList;
        Page<User> pageData;
        Pageable pageable = PageRequest.of(page - 1, limit);

        if (!StringUtils.isEmpty(key)) {
            pageData = userDao.findUsersByUsernameContaining(key, pageable);
        } else {
            pageData = userDao.findAllByStatus(pageable);
        }
        if (pageData.getContent().size() == 0) {
            throw new NoDataException("page");
        }

        userList = pageData.getContent();
        List<OptionVO> vo = new ArrayList<>();

        int count = 1;
        for (User object : userList) {
            OptionVO optionVO = new OptionVO(object.getId(), count++, object.getUsername(), null);
            vo.add(optionVO);
        }

        PageData<OptionVO> data = new PageData<>((int) pageData.getTotalElements(), limit, page, vo);
        return ResultUtil.successPage(SUCCESS, data);

    }

    @Override
    @Modifying(clearAutomatically = true)
    public Result audit(List<Integer> idList) {
        try {
            userDao.auditBatchUserByIdList(idList, new Date());
            return ResultUtil.success(UPDATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(UPDATE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    public void changePassword(Integer id, String password) {
        User user = get(id);
        user.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
        try {
            userDao.save(user);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }


    }

    @Override
    public List<User> getListByBusiness(Integer businessId) {

        return userDao.findListByBusinessID(businessId);
    }

    @Override
    public Result getTeacherOption(String name) {
        List<User> userList = userMapper.findUserListByCode(TEACHER.getMessage(), name);
        if (Collections.isEmpty(userList)) {
            throw new NoDataException();
        }
        List<OptionVO> voList = new ArrayList<>(userList.size());
        for (User user : userList) {
            OptionVO vo = new OptionVO();
            ConverterUtil.copyProperties(user, vo);
            vo.setName(user.getUsername());
            vo.setSort(userList.indexOf(user) + 1);
            voList.add(vo);
        }
        return ResultUtil.success(FIND_SUCCESS, voList);
    }

    @Override
    public Result getAssistTeacherOption(String name) {
        List<User> userList = userMapper.findUserListByCode(ASSIST_TEACHER.getMessage(), name);
        if (Collections.isEmpty(userList)) {
            throw new NoDataException();
        }
        List<OptionVO> voList = new ArrayList<>(userList.size());
        for (User user : userList) {
            OptionVO vo = new OptionVO();
            ConverterUtil.copyProperties(user, vo);
            vo.setName(user.getUsername());
            vo.setSort(userList.indexOf(user) + 1);
            voList.add(vo);
        }
        return ResultUtil.success(FIND_SUCCESS, voList);
    }

    @Override
    public Result getTeacherOptionPage(String name, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<User> pageData = new PageInfo<>(userMapper.findUserListByCode(TEACHER.getMessage(), name));
        if (pageData.getList().isEmpty()) {
            throw new NoDataException("page");
        }
        List<User> list = pageData.getList();
        List<OptionVO> voList = new ArrayList<>(list.size());
        for (User user : list) {
            OptionVO vo = new OptionVO();
            ConverterUtil.copyProperties(user, vo);
            vo.setSort(list.indexOf(user) + 1);
            voList.add(vo);
        }
        PageData<OptionVO> data = new PageData<>((int) pageData.getTotal(), limit, page, voList);
        return ResultUtil.successPage(FIND_SUCCESS, data);
    }

    @Override
    public List<User> findByIdIn(List<Integer> idList) {
        return userDao.findListByIdIn(idList);
    }

    @Override
    public Page<User> findByIdIn(List<Integer> idList, Pageable pageable) {
        return userDao.findPageByIdIn(idList, pageable);
    }

    @Override
    public Integer countByBusinessId(Integer businessId) {
        return userDao.countByBusinessID(businessId).intValue();
    }

    @Override
    @Modifying(clearAutomatically = true)
    public void resetPassword(Integer id) {

        User user = get(id);
        user.setPassword(DigestUtils.md5DigestAsHex(DEFAULT_PASSWORD.getBytes()));
        try {
            userDao.save(user);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void changeStatus(Integer id, Integer status) {
        User user = get(id);
        user.setStatus(status);
        try {
            userDao.save(user);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public Result getStaffOption(String name, Integer businessId) {
        List<User> userList = userMapper.findUserListByCodeAndBusiness(name, businessId);
        if (Collections.isEmpty(userList)) {
            throw new NoDataException();
        }
        List<OptionVO> voList = new ArrayList<>(userList.size());
        for (User user : userList) {
            OptionVO vo = new OptionVO();
            vo.setId(user.getId());
            vo.setSort(userList.indexOf(user) + 1);
            vo.setName(user.getUsername());
            voList.add(vo);
        }
        return ResultUtil.success(FIND_SUCCESS, voList);
    }

    @Override
    public User findByUUID(String uuid) {
        return userDao.findByUUID(uuid);
    }

    @Override
    public User getAttributeUser() {
        ServletRequestAttributes servletRequest = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequest.getRequest();
        return (User) request.getAttribute("user");
    }

    @Override
    public Integer getManagerByBusiness(Integer id) {
        return userDao.findBusinessManager(id);
    }


    @Override
    public User getByUserName(String username) {
        User user = userDao.findByUsername(username);
        if (user == null) {
            throw new BaseException(NO_USER);
        }
        return user;
    }

    @Override
    public User getByTelephone(String telephone) {
        User user = userDao.findByTelephone(telephone);
        if (user == null) {
            throw new BaseException(NO_USER);
        }
        return user;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void forgetPassword(String passWord) {
        User attribute = userService.getAttributeUser();
        User user = userService.get(attribute.getId());
        user.setPassword(DigestUtils.md5DigestAsHex(passWord.getBytes()));
        try {
            userDao.save(user);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(OPERATING_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void unBindDeleteUserInBusiness(Integer businessId) {
        try{
        userDao.unBindDeleteUserInBusiness(businessId);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(DELETE_FAIL, e);
        }
    }

    @Override
    public List<AllowExamVO> getExamList(Integer department, Integer year, String key, Integer examId) {
        return userMapper.findExamList(department,year,key,examId);
    }

    @Override
    public List<User> getNoPassExam(Integer courseId, Integer examClassId) {
            return userDao.findNoPassExam(courseId, examClassId);
    }

    @Override
    public List<User> getIdIn( List<Integer> ids) {
        return userDao.findAllById(ids);
    }
}

