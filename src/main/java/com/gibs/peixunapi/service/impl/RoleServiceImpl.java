package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.dao.PowerMenuDao;
import com.gibs.peixunapi.dao.RoleDao;
import com.gibs.peixunapi.dao.RolePowerDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.enums.RoleEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.service.BusinessService;
import com.gibs.peixunapi.service.RoleService;
import com.gibs.peixunapi.service.UserRoleService;
import com.gibs.peixunapi.service.UserService;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.ResultEnum.DATA_NOT_FIND;
import static com.gibs.peixunapi.enums.RoleEnum.BUSINESS_ADMIN;


/**
 * @author liangjiawei
 */
@Service
@Slf4j
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private RolePowerDao rolePowerDao;
    @Autowired
    private PowerMenuDao powerMenuDao;
    @Autowired
    private UserService userService;
    @Autowired
    private BusinessService businessService;

    @Override
    public Integer getRoleIdByCode(String code) {
        Integer result = roleDao.findByCode(code);
        if (result == null) {
            throw new BaseException(DATA_NOT_FIND);
        }
        return result;
    }

    @Override
    public List<Menu> getMenu(String role) {
        String code = RoleEnum.getCode(role);
        Integer roleId = getRoleIdByCode(code);

        List<RolePower> rolePowerList = rolePowerDao.findByRoleId(roleId);
        if (Collections.isEmpty(rolePowerList)) {
            throw new BaseException(DATA_NOT_FIND);
        }
        List<Integer> powerIds = rolePowerList.stream().map(i -> i.getPower().getId()).collect(Collectors.toList());
        List<PowerMenu> powerMenuList = powerMenuDao.findByPowerIdIn(powerIds);
        if (Collections.isEmpty(powerMenuList)) {
            throw new BaseException(DATA_NOT_FIND);
        }
        return powerMenuList.stream().map(PowerMenu::getMenu).collect(Collectors.toList());

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void changeRole(String roles, Integer userId) {
        try {
            if (!StringUtils.isBlank(roles)) {
                //获取codeID
                User user = userService.get(userId);
                if (roles.contains(BUSINESS_ADMIN.getCode())) {

                    Business business = user.getBusiness();
                    business.setManagerId(userId);
                    businessService.saveManager(business);
                }else{
                    if (user.getId().equals(user.getBusiness().getManagerId())) {
                        Business business = user.getBusiness();
                        business.setManagerId(null);
                        businessService.saveManager(business);
                    }
                }
                if (roles.contains(",")) {
                    List<String> codeList = Arrays.stream(roles.split(",")).collect(Collectors.toList());
                    List<Integer> codeIdList = roleDao.findByCodeIn(codeList);
                    userRoleService.createBatch(codeIdList, userId);
                } else {
                    Integer role = roleDao.findByCode(roles);
                    userRoleService.create(userId, role);
                }

            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }
}

