package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.post.ChapterVO;
import com.gibs.peixunapi.VO.show.ChapterListVO;
import com.gibs.peixunapi.VO.show.ShowChapterVO;
import com.gibs.peixunapi.dao.VideoClassDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.model.Course;
import com.gibs.peixunapi.model.StudyProgress;
import com.gibs.peixunapi.model.Video;
import com.gibs.peixunapi.model.VideoClass;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.CourseEnum.VIDEO;
import static com.gibs.peixunapi.enums.ResultEnum.*;

@Service
@Slf4j

public class VideoClassServiceImpl implements VideoClassService {

    @Autowired
    private VideoClassDao videoClassDao;
    @Autowired
    private CourseService courseService;
    @Autowired
    private VideoService videoService;
    @Autowired
    private ExercisesService exercisesService;
    @Autowired
    private StudyProgressService studyProgressService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result create(ChapterVO chapterVO) {
        VideoClass videoClass = new VideoClass();
        ConverterUtil.copyProperties(chapterVO, videoClass);

        Course course = courseService.get(chapterVO.getCourseId());
        Video video = videoService.get(chapterVO.getVideoId());


        videoClass.setCourse(course);
        videoClass.setVideo(video);
        try {
            videoClassDao.save(videoClass);
            //绑定练习题
            return ResultUtil.success(ResultEnum.CREATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL, e);
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public Result save(ChapterVO chapterVO) {
        VideoClass videoClass = videoClassDao.findById(chapterVO.getVideoId()).orElseThrow(EntityNotFoundException::new);
        Course course;
        Video video;

        ConverterUtil.copyNotNullProperties(chapterVO, videoClass);

        if (!videoClass.getCourse().getId().equals(chapterVO.getCourseId())) {
            course = courseService.get(chapterVO.getCourseId());
            videoClass.setCourse(course);
        }
        if (!videoClass.getVideo().getId().equals(chapterVO.getVideoId())) {
            video = videoService.get(chapterVO.getVideoId());
            videoClass.setVideo(video);
        }
        try {
            videoClassDao.save(videoClass);

            return ResultUtil.success(ResultEnum.UPDATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(UPDATE_FAIL, e);
        }
    }

    @Override
    public VideoClass get(Integer id) {
        return videoClassDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public ShowChapterVO getVO(Integer id, Integer userId, String type) {
        VideoClass videoClass = videoClassDao.findById(id).orElseThrow(EntityNotFoundException::new);
        ShowChapterVO vo = new ShowChapterVO();
        ConverterUtil.copyProperties(videoClass, vo);
        vo.setChapterId(videoClass.getId());
        vo.setVideo(videoClass.getVideo());

//        vo.setSubjectHub(exercisesService.getHangUpSubjectVO(videoClass.getId()));
//        if (videoClass.getAudit() == 1) {
//            vo.setAuditString("通过");
//        }
//        if (videoClass.getAudit() == 0) {
//            vo.setAuditString("未通过");
//        }
//        if (videoClass.getStatus() == 1) {
//            vo.setStatusString("开放");
//        }
//        if (videoClass.getStatus() == 0) {
//            vo.setStatusString("关闭");
//        }
        StudyProgress studyProgress = studyProgressService.getStudyProgress(videoClass.getId(), userId, VIDEO);

        if (studyProgress != null) {
            vo.setProgress(studyProgress.getProgress());
            vo.setTimeRecord(studyProgress.getTimeRecord());
//            vo.setIsVideoPass(studyProgress.getIsVideoPass());
        }
        return vo;
    }

    @Override
    public List<ChapterListVO> studentGetList(Integer courseId, Integer userId) {
        List<VideoClass> videoList = videoClassDao.findByCourseId(courseId);
        if (Collections.isEmpty(videoList)) {
            return null;
        }
        List<ChapterListVO> voList = new ArrayList<>(videoList.size());
        for (VideoClass video : videoList) {
            ChapterListVO vo = new ChapterListVO();
            ConverterUtil.copyProperties(video, vo);
            StudyProgress studyProgress = studyProgressService.getStudyProgress(video.getId(), userId, VIDEO);
            if (studyProgress.getId() != null) {
                vo.setStudyProgress(studyProgress.getProgress());
            }
            vo.setPicture(video.getVideo().getCoverImage() == null ? "" : video.getVideo().getCoverImage().getUrl());
            voList.add(vo);
        }
        return voList;

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void auditByCourse(Integer audit, Integer courseId) {
        try {
            videoClassDao.auditChapterByCourse(courseId, audit);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(UPDATE_FAIL, e);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public Result editVideoClass(ChapterVO chapterVO) {

        VideoClass videoClass = videoClassDao.findById(chapterVO.getChapterId()).orElseThrow(EntityNotFoundException::new);
        ConverterUtil.copyProperties(chapterVO, videoClass, "id", "audit");
        videoClass.setVideo(videoService.get(chapterVO.getVideoId()));

        try {
            videoClassDao.save(videoClass);
//            if (chapterVO.getSubjectHubId() != null && chapterVO.getSubjectHubId() != 0) {
//                Exercises old = exercisesService.getVideoBindedExercises(videoClass.getId());
//                exercisesService.changeHangUpSubject(chapterVO.getChapterId(), videoClass.getCourse().getId(),
//                        chapterVO.getSubjectHubId(), VIDEO);
//            }

            return ResultUtil.success(ResultEnum.UPDATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(UPDATE_FAIL, e);
        }

    }

    @Override
    public List<ShowChapterVO> getList(Integer courseId) {
        List<VideoClass> videoList = videoClassDao.findByCourseId(courseId);
        List<ShowChapterVO> chapterVOList = new ArrayList<>(videoList.size());
        for (VideoClass video : videoList) {
            ShowChapterVO chapterVO = new ShowChapterVO();
            ConverterUtil.copyProperties(video, chapterVO);
            chapterVO.setVideo(video.getVideo());
            chapterVO.setChapterId(video.getId());
//            chapterVO.setAudit(video.getAudit());
            chapterVO.setSort(video.getSort());
            chapterVO.setCourseId(courseId);
            chapterVO.setPicture(video.getVideo().getCoverImage() == null ? "" : video.getVideo().getCoverImage().getUrl());
            chapterVOList.add(chapterVO);
        }
        return chapterVOList;
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void linkClass(Integer courseId, Integer linkCourseId) {
        List<VideoClass> videoClassList = videoClassDao.findByCourseId(courseId);

        if (Collections.isEmpty(videoClassList)) {
            throw new BaseException(DATA_NOT_FIND);
        }

        List<VideoClass> newList = videoClassList.stream().map(videoClass -> {
            VideoClass temp = new VideoClass();
            ConverterUtil.copyProperties(videoClass, temp,
                    "id", "creatorId", "audit", "createTime", "updateTime");
            return temp;
        }).collect(Collectors.toList());
        try {
            videoClassDao.saveAll(newList);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }

    @Override
    public List<VideoClass> findByCourseId(Integer courseId) {
        List<VideoClass> list = videoClassDao.findByCourseId(courseId);
        if (Collections.isEmpty(list)) {
            new ArrayList<>();
        }
        return list;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void changeChapterStatus(Integer id, Integer status) {
        try {
            videoClassDao.changeStatus(id, status);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void auditChapter(Integer id, Integer audit) {
        try {
            videoClassDao.auditChapter(id, audit);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void changeVideo(Integer videoClassId, Integer videoId) {
        try {
            videoClassDao.changeVideo(videoClassId, videoId);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void applyAudit(Integer courseId) {
        try {
            videoClassDao.applyAudit(courseId);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }

    @Override
    public Integer countByCourse(Integer id) {
        return videoClassDao.countByCourse(id).intValue();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void deleteChapter(Integer chapterId) {
        try {
            videoClassDao.deleteChapter(chapterId);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(DELETE_FAIL, e);
        }
    }


}

