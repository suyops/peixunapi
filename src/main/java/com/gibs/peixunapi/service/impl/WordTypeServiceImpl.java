package com.gibs.peixunapi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import javax.validation.Valid;

import org.springframework.transaction.annotation.Transactional;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.utils.ResultUtil;
import com.gibs.peixunapi.dao.WordTypeDao;
import com.gibs.peixunapi.service.WordTypeService;
import com.gibs.peixunapi.model.WordType;

@Service
@Slf4j
public class WordTypeServiceImpl implements WordTypeService{

}

