package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.SubOptionVO;
import com.gibs.peixunapi.dao.SubOptionDao;
import com.gibs.peixunapi.dao.SubjectHubDao;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.model.SubOption;
import com.gibs.peixunapi.model.SubjectHub;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.SubOptionService;
import com.gibs.peixunapi.service.SubjectHubService;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.gibs.peixunapi.enums.ResultEnum.*;


@Service
@Slf4j
public class SubOptionServiceImpl implements SubOptionService {

    @Autowired
    private SubOptionDao subOptionDao;
    @Autowired
    private SubjectHubDao subjectHubDao;
    @Autowired
    private SubjectHubService subjectHubService;

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public Result create(List<SubOptionVO> objectList, SubjectHub subjectHub) {
        if (objectList.size() < 1 || subjectHub == null) {
            throw new BaseException(PARAM_ERROR);
        }
        List<SubOption> subOptionList = new ArrayList<>(objectList.size());
        for (SubOptionVO subOptionVO : objectList) {
            SubOption subOption = new SubOption();
            ConverterUtil.copyNotBlankProperties(subOptionVO, subOption);
            subOption.setStatus(1);
            subOption.setCreateTime(new Date());
            subOption.setSubjectHub(subjectHub);
            subOptionList.add(subOption);
        }
        try {
            subOptionDao.saveAll(subOptionList);
            return ResultUtil.success(CREATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(CREATE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public Result edit(List<SubOptionVO> voList, Integer subjectHubId) {

        List<SubOption> subOption = subOptionDao.findByIdOrderBySort(subjectHubId);
        try {
            for (int i = 0; i < (Math.max(voList.size(), subOption.size())); i++) {
                //如果题库的选项多了
                if (subOption.size() > voList.size()) {
                    if (i <= voList.size() - 1) {
                        subOption.get(i).setContent(voList.get(i).getContent());
                        subOption.get(i).setValue(voList.get(i).getValue());
                        subOptionDao.save(subOption.get(i));
                    } else {
                        subOptionDao.delete(subOption.get(i));
                    }
                }
                //如果题库的选项少了
                if (subOption.size() < voList.size()) {
                    if (i <= subOption.size() - 1) {
                        subOption.get(i).setContent(voList.get(i).getContent());
                        subOption.get(i).setValue(voList.get(i).getValue());
                        subOptionDao.save(subOption.get(i));
                    } else {
                        SubOption newOne = new SubOption();
                        newOne.setValue(voList.get(i).getValue());
                        newOne.setContent(voList.get(i).getContent());
                        newOne.setSort(i);
                        newOne.setSubjectHub(subjectHubService.get(subjectHubId));
                        subOptionDao.save(newOne);
                    }
                }
//            如果题库的选项一样
                if (voList.size() == subOption.size()) {
                    subOption.get(i).setContent(voList.get(i).getContent());
                    subOption.get(i).setValue(voList.get(i).getValue());
                    subOptionDao.save(subOption.get(i));
                }
            }
            return ResultUtil.success(UPDATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(UPDATE_FAIL, e);
        }
    }

    @Override
    public Result delete(Integer id) {
        try {
            subOptionDao.deleteById(id);
            return ResultUtil.success(DELETE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(DELETE_FAIL, e);
        }
    }

    @Override
    public Result get(Integer id) {
        try {

            return ResultUtil.success(FIND_SUCCESS);
        } catch (Exception e) {
            return ResultUtil.error(SERVER_ERROR, e);
        }
    }

    @Override
    public Result getList(Integer subjectHubId) {
        try {
            List<SubOption> subOptionList = subOptionDao.findSubOptionsBySubjectHubId(subjectHubId);
            if (Collections.isEmpty(subOptionList)) {
                throw new BaseException(DATA_NOT_FIND);
            }
            List<SubOptionVO> subOptionVOList = new ArrayList<>(subOptionList.size());
            subOptionList.forEach(subOption -> {
                SubOptionVO subOptionVO = new SubOptionVO();
                ConverterUtil.copyProperties(subOption, subOptionVO);
                subOptionVOList.add(subOptionVO);
            });
            return ResultUtil.success(FIND_SUCCESS, subOptionVOList);
        } catch (Exception e) {
            throw new BaseException(DATA_NOT_FIND, e);
        }
    }

    @Override
    public List<SubOptionVO> getSubOptions(Integer subjectHubId) {
        List<SubOption> subOptionList = subOptionDao.findSubOptionsBySubjectHubId(subjectHubId);
        if (Collections.isEmpty(subOptionList)) {
            return new ArrayList<>();
        }
        List<SubOptionVO> subOptionVOList = new ArrayList<>(subOptionList.size());
        subOptionList.forEach(subOption -> {
            SubOptionVO subOptionVO = new SubOptionVO();
            ConverterUtil.copyProperties(subOption, subOptionVO);
            subOptionVOList.add(subOptionVO);
        });
        return subOptionVOList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveImport(SubOption subOption) {
        try {
            subOptionDao.save(subOption);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL, e);
        }
    }
}

