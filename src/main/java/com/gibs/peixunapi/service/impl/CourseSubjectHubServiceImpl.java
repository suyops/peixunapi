package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.DTO.SubjectHubDTO;
import com.gibs.peixunapi.VO.SubOptionVO;
import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.dao.CourseSubjectHubDao;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.mapper.CourseSubjectHubMapper;
import com.gibs.peixunapi.model.Course;
import com.gibs.peixunapi.model.CourseSubjectHub;
import com.gibs.peixunapi.model.SubjectHub;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import com.google.common.base.Joiner;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.gibs.peixunapi.enums.CourseEnum.DOCUMENT;
import static com.gibs.peixunapi.enums.CourseEnum.VIDEO;
import static com.gibs.peixunapi.enums.ResultEnum.CREATE_FAIL;
import static com.gibs.peixunapi.enums.ResultEnum.UNKNOWN_ERROR;

@Slf4j

@Service
public class CourseSubjectHubServiceImpl implements CourseSubjectHubService {

    @Autowired
    private CourseSubjectHubDao courseSubjectHubDao;
    @Autowired
    private CourseSubjectHubMapper courseSubjectHubMapper;
    @Autowired
    private SubOptionService subOptionService;
    @Autowired
    private DocumentationService documentationService;
    @Autowired
    private VideoClassService videoClassService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private ExercisesService exercisesService;

    @Override
    public CourseSubjectHub get(Integer id) {
        return courseSubjectHubDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public CourseSubjectHub createInChapter(SubjectHub subjectHub, Integer chapterId, CourseEnum courseEnum) {
        Course course = new Course();
        try {
            if (DOCUMENT.equals(courseEnum)) {
                course = documentationService.get(chapterId).getCourse();
            }
            if (VIDEO.equals(courseEnum)) {
                course = videoClassService.get(chapterId).getCourse();
            }
            CourseSubjectHub courseSubjectHub = new CourseSubjectHub();
            courseSubjectHub.setCourse(course);
            courseSubjectHub.setSubjectHub(subjectHub);

            courseSubjectHub = courseSubjectHubDao.save(courseSubjectHub);
            return courseSubjectHub;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void linkSubjectHubByCourseId(Integer link, Integer beLink) {
        Course course = courseService.get(beLink);
        List<CourseSubjectHub> courseSubjectHubList = courseSubjectHubDao.findByCourse(link);
        courseSubjectHubList.forEach(i -> i.setCourse(course));
        try {
            courseSubjectHubDao.saveAll(courseSubjectHubList);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL);
        }
    }

    @Override
    public Result getSubjectList(Integer courseId, String type) {
        List<SubjectHubDTO> subjectHubList = courseSubjectHubMapper.findSubjectListByCourseId(courseId, type);
        if (Collections.isEmpty(subjectHubList)) {
            throw new NoDataException();
        }
        List<SubjectHubVO> subjectHubVOList = new ArrayList<>(subjectHubList.size());
        for (SubjectHubDTO subjectHub : subjectHubList) {
            SubjectHubVO temp = new SubjectHubVO();
            ConverterUtil.copyProperties(subjectHub, temp, "analysis", "knowPoint");
            if ("fill".equals(subjectHub.getType())) {
                if (subjectHub.getAnswer().contains(",")) {
                    String[] strs = subjectHub.getAnswer().split(",");
                    temp.setFillNum(strs.length);
                } else if (subjectHub.getAnswer().contains("，")) {
                    String[] strs = subjectHub.getAnswer().split("，");
                    temp.setFillNum(strs.length);
                }else {
                    temp.setFillNum(1);
                }
            }
            List<SubOptionVO> subOptionVOList = subOptionService.getSubOptions(subjectHub.getId());
            if (!Collections.isEmpty(subjectHubList)) {
                temp.setOptionVOList(subOptionVOList);
            }
            subjectHubVOList.add(temp);
        }
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, subjectHubVOList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<CourseSubjectHub> getRandomByCourseOfType(Integer courseId, String type, Integer limit) {
        List<Integer> idList = courseSubjectHubDao.findIdListByCourseAndType(courseId, type);
        if (Collections.isEmpty(idList)) {
            throw new BaseException(CREATE_FAIL);
        }
        List<Integer> randomList = new ArrayList<>();
        int times = limit <= idList.size() ? limit : idList.size();
        for (Integer i = 0; i < times; i++) {
            int num;
            do {
                num = (int) Math.floor(Math.random() * idList.size());
            }
            while (randomList.contains(num));
            randomList.add(idList.get(num));
            idList.remove(num);
        }
        if (Collections.isEmpty(randomList)) {
            return null;
        }
        return courseSubjectHubDao.findByIdIn(randomList, Joiner.on(",").join(randomList));
    }

    @Override
    public CourseSubjectHub getBySubjectHub(Integer chapter, Integer subjectHubId, CourseEnum enums) {
        return courseSubjectHubMapper.findByChapterAndSubjectHub(chapter, subjectHubId, enums.getMessage());
    }


    @Override
    public CourseSubjectHub getByCourseAndSubjectHub(Integer courseId, Integer subjectHubId) {
        return courseSubjectHubMapper.findByCourseAndSubjectHub(courseId, subjectHubId);
    }

    @Override
    public List<CourseSubjectHub> findHangUpSubjectChooseList(Integer courseId) {
        return courseSubjectHubDao.findHangUpSubjectChooseList(courseId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)

    public void createBatch(List<CourseSubjectHub> courseSubjectHubList) {
        try {
            courseSubjectHubDao.saveAll(courseSubjectHubList);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL);
        }
    }

    @Override
    public List<CourseSubjectHub> getByIdList(List<Integer> idList) {
        List<CourseSubjectHub> list = courseSubjectHubDao.findByIdIn(idList, Joiner.on(",").join(idList));
        if (CollectionUtils.isEmpty(list)) {
            throw new BaseException(UNKNOWN_ERROR);
        }
        return list;
    }

    @Override
    public Map<String, List<SubjectHubVO>> getCourseSubjectHub(Integer courseId) {
        List<CourseSubjectHub> list = courseSubjectHubDao.findByCourse(courseId);
        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
        Map<String, List<SubjectHubVO>> result = new HashMap<>(6);

        /* 循环遍历转换为VO类 放入Map中返回结果*/
        for (int i = 0; i < list.size(); i++) {
            SubjectHub subjectHub = list.get(i).getSubjectHub();
            SubjectHubVO subjectHubVO = new SubjectHubVO();

            ConverterUtil.copyProperties(subjectHub, subjectHubVO, "knowPoint", "analysis");
            if ("fill".equals(subjectHub.getType())) {
                if (subjectHub.getAnswer().contains(",")) {
                    String[] strs = subjectHub.getAnswer().split(",");
                    subjectHubVO.setFillNum(strs.length);
                } else if (subjectHub.getAnswer().contains("，")) {
                    String[] strs = subjectHub.getAnswer().split("，");
                    subjectHubVO.setFillNum(strs.length);
                }else {
                    subjectHubVO.setFillNum(1);
                }
            }
            subjectHubVO.setOptionVOList(subOptionService.getSubOptions(subjectHub.getId()));

            /*如果下一个type不同那么结束收集此类型完毕*/
            if (list.get(i).getSubjectHub().getType() != null) {
                if (i + 1 <= list.size()) {
                    if (!result.containsKey(list.get(i).getSubjectHub().getType())) {
                        result.put(list.get(i).getSubjectHub().getType(), new ArrayList<>());
                    }
                }
                result.get(list.get(i).getSubjectHub().getType()).add(subjectHubVO);
            }
        }
        return result;
    }
}

