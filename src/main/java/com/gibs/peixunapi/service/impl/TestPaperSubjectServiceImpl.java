package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.DTO.SubjectHubDTO;
import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.VO.post.TestPaperVO;
import com.gibs.peixunapi.dao.TestPaperSubjectDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.ResultEnum.*;
import static com.gibs.peixunapi.enums.SubjectEnum.*;

@Service
@Slf4j
public class TestPaperSubjectServiceImpl implements TestPaperSubjectService {

    @Autowired
    private TestPaperSubjectDao testPaperSubjectDao;
    @Autowired
    private SubjectHubService subjectHubService;
    @Autowired
    private TestPaperService testPaperService;
    @Autowired
    private UserService userService;
    @Autowired
    private CourseSubjectHubService courseSubjectHubService;
    @Autowired
    private SubOptionService subOptionService;

    @Override
    public TestPaperSubject get(Integer id) {
        return testPaperSubjectDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Map<String, List<SubjectHubVO>> getMapByTestPaper(Integer testPaperId) {
        List<TestPaperSubject> testPaperList = testPaperSubjectDao.findSubjectHubListByTestPaperId(testPaperId);

        List<SubjectHubDTO> subjectHubList = testPaperList.stream().map(i -> {
            SubjectHubDTO dto = new SubjectHubDTO();
            ConverterUtil.copyProperties(i.getSubjectHub(), dto);
            dto.setId(i.getSubjectHub().getId());
            dto.setAnalysis(i.getSubjectHub().getAnalysis() == null ? "" : i.getSubjectHub().getAnalysis());
            dto.setTestPaperSubjectHubId(i.getId());
            if ("fill".equals(i.getSubjectHub().getType())) {
                if (i.getSubjectHub().getAnswer().contains(",")) {
                    String[] strs = i.getSubjectHub().getAnswer().split(",");
                    dto.setFillNum(strs.length);
                } else if (i.getSubjectHub().getAnswer().contains("，")) {
                    String[] strs = i.getSubjectHub().getAnswer().split("，");
                    dto.setFillNum(strs.length);
                } else {
                    dto.setFillNum(1);
                }
            }
            return dto;
        }).collect(Collectors.toList());

        if (Collections.isEmpty(subjectHubList)) {
            throw new NoDataException();
        }
        Map<String, List<SubjectHubVO>> result = new HashMap<>();

        /* 循环遍历转换为VO类 放入Map中返回结果*/
        for (int i = 0; i < subjectHubList.size(); i++) {
            SubjectHubDTO subjectHub = subjectHubList.get(i);
            SubjectHubVO subjectHubVO = new SubjectHubVO();
            ConverterUtil.copyProperties(subjectHub, subjectHubVO, "knowPoint");
            subjectHubVO.setOptionVOList(subOptionService.getSubOptions(subjectHub.getId()));

            /*如果下一个type不同那么结束收集此类型完毕*/
            if (subjectHubList.get(i).getType() != null) {
                if (i + 1 <= subjectHubList.size()) {
                    if (!result.containsKey(subjectHubList.get(i).getType())) {
                        result.put(subjectHubList.get(i).getType(), new ArrayList<>());
                    }
                }
                result.get(subjectHubList.get(i).getType()).add(subjectHubVO);
            }
        }
        return result;
    }


    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public TestPaper switchTestPaperSubject(List<SubjectHubVO> oldSubjectHubVOList, TestPaperVO newTestPaperVO) {

        //1.对比更新题目列表
        //把原先试卷列表转换成hashMap
        Map<Integer, SubjectHubVO> oldToNewMap = oldSubjectHubVOList.stream()
                .collect(Collectors.toMap(SubjectHubVO::getId, Function.identity()));
        //替换原有题目列表 使其成为新题目列表
        newTestPaperVO.getSubjectHubVOList().forEach(newOne -> {
            //新增题目版本
            Integer subjectId = subjectHubService.persist(newOne);
            //替换题目
            if (oldToNewMap.containsKey(newOne.getId())) {
                oldToNewMap.remove(newOne.getId());
                oldToNewMap.replace(subjectId, newOne);
            }
        });
        //2.更新试卷题目
        //新增试卷
        List<SubjectHubVO> newList = new ArrayList<>(oldToNewMap.values());
        TestPaper newTestPaper = testPaperService.copy(newList, newTestPaperVO.getTestPaperId());

        //根据新试卷id ,新增试卷题目,绑定关系
        List<SubjectHub> subjectHubList = oldToNewMap.entrySet().stream()
                .map(e -> {
                    SubjectHubVO subjectHubVO = e.getValue();
                    subjectHubVO.setId(e.getKey());
                    SubjectHub subjectHub = new SubjectHub();
                    ConverterUtil.copyNotNullProperties(subjectHubVO, subjectHub);
                    return subjectHub;
                })
                .collect(Collectors.toList());
        //新建保存列表
        List<TestPaperSubject> saveList = new ArrayList<>();
        subjectHubList.forEach(e -> {
            TestPaperSubject testPaperSubject = new TestPaperSubject();
            testPaperSubject.setTestPaper(newTestPaper);
            testPaperSubject.setSubjectHub(e);
            User user = userService.getAttributeUser();
            testPaperSubject.setCreator(user.getId());
            saveList.add(testPaperSubject);
        });
        try {
            testPaperSubjectDao.saveAll(saveList);
            return newTestPaper;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }

    }

    @Override
    public Result updateTestPaperSubject(List<SubjectHubVO> oldSubjectHubVOList, TestPaperVO newTestPaperVO) {
//1.对比更新题目列表
        //把原先试卷列表转换成hashMap
        Map<Integer, SubjectHubVO> oldToNewMap = oldSubjectHubVOList.stream()
                .collect(Collectors.toMap(SubjectHubVO::getId, Function.identity()));
        //替换原有题目列表 使其成为新题目列表
        newTestPaperVO.getSubjectHubVOList().forEach(newOne -> {
            //新增题库题目
            Integer newSubjectId = subjectHubService.persist(newOne);
            //替换考试题目
            if (oldToNewMap.containsKey(newOne.getId())) {
                Integer column = testPaperSubjectDao.updateTestPaperSubject(newSubjectId, newTestPaperVO.getTestPaperId(), newOne.getId());
                if (column == 0) {
                    throw new BaseException(UNKNOWN_ERROR);
                }
            }
        });
        return ResultUtil.success(UPDATE_SUCCESS);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void randomSubject(TestPaper testPaper, Integer courseId, Integer user) {
        List<TestPaperSubject> list = new ArrayList<>();
        if (testPaper.getRadioCount() > 0) {
            list.addAll(random(RADIO.getCode(), testPaper.getRadioCount(), courseId, testPaper, user));
        }
        if (testPaper.getCheckCount() > 0) {
            list.addAll(random(CHECK.getCode(), testPaper.getCheckCount(), courseId, testPaper, user));
        }
        if (testPaper.getJudgeCount() > 0) {
            list.addAll(random(JUDGE.getCode(), testPaper.getJudgeCount(), courseId, testPaper, user));
        }
        if (testPaper.getFillCount() > 0) {
            list.addAll(random(FILL.getCode(), testPaper.getFillCount(), courseId, testPaper, user));
        }
        if (testPaper.getQuestionCount() > 0) {
            list.addAll(random(QUESTION.getCode(), testPaper.getQuestionCount(), courseId, testPaper, user));
        }
        if (!CollectionUtils.isEmpty(list)) {
            try {
                testPaperSubjectDao.saveAll(list);
            } catch (Exception e) {
                e.printStackTrace();
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                throw new SaveException(ResultEnum.SAVE_FAIL, e);
            }
        } else {
            throw new BaseException(NO_SUBJECT_TO_RANDOM);
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveImport(List<SubjectHub> subjectHubList, Integer id, String subjectType) {
        TestPaper testPaper = testPaperService.get(id);
        User user = userService.getAttributeUser();
//        User user = userService.get(2);
        List<TestPaperSubject> list = new ArrayList<>(subjectHubList.size());
        for (SubjectHub subjectHub : subjectHubList) {
            TestPaperSubject testPaperSubject = new TestPaperSubject();
            testPaperSubject.setSubjectHub(subjectHub);
            testPaperSubject.setTestPaper(testPaper);
            testPaperSubject.setCreator(user.getId());
            testPaperSubject.setSort(subjectHubList.indexOf(subjectHub) + 1);
            list.add(testPaperSubject);
        }
        try {
            testPaperSubjectDao.saveAll(list);
            if (RADIO.getCode().equals(subjectType)) {
                testPaper.setRadioCount(subjectHubList.size());
            } else if (CHECK.getCode().equals(subjectType)) {
                testPaper.setCheckCount(subjectHubList.size());
            } else if (JUDGE.getCode().equals(subjectType)) {
                testPaper.setJudgeCount(subjectHubList.size());
            } else if (FILL.getCode().equals(subjectType)) {
                testPaper.setFillCount(subjectHubList.size());
            } else if (QUESTION.getCode().equals(subjectType)) {
                testPaper.setQuestionCount(subjectHubList.size());
            }
            testPaper.setTotalScore(BigDecimal.valueOf(
                    (testPaper.getRadioCount() * testPaper.getRadioScore().intValue()) +
                    (testPaper.getCheckCount() * testPaper.getCheckScore().intValue()) +
                    (testPaper.getJudgeCount() * testPaper.getJudgeScore().intValue()) +
                    (testPaper.getFillCount() * testPaper.getFillScore().intValue()) +
                    (testPaper.getQuestionCount() * testPaper.getQuestionScore().intValue()))
            );
            testPaperService.saveCount(testPaper);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public List<TestPaperSubject> getTestPaperSubject(Integer id) {
        List<TestPaperSubject> list = testPaperSubjectDao.findSubjectHubListByTestPaperId(id);
        return list;

    }

    private List<TestPaperSubject> random(String type, Integer limit, Integer courseId, TestPaper testPaper, Integer user) {
        List<CourseSubjectHub> list = courseSubjectHubService.getRandomByCourseOfType(courseId, type, limit);
        if (Collections.isEmpty(list)) {
            return new ArrayList<>();
        }
        List<SubjectHub> subjectHubList = list.stream().map(CourseSubjectHub::getSubjectHub).collect(Collectors.toList());
        List<TestPaperSubject> result = new ArrayList<>(list.size());
        for (SubjectHub subjectHub : subjectHubList) {
            TestPaperSubject testPaperSubject = new TestPaperSubject();
            testPaperSubject.setSubjectHub(subjectHub);
            testPaperSubject.setTestPaper(testPaper);
            testPaperSubject.setCreator(user);
            testPaperSubject.setSort(subjectHubList.indexOf(subjectHub));
            result.add(testPaperSubject);
        }
        return result;
    }


}

