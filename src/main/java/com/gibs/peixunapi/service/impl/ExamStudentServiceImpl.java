package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.post.DailyScoreVO;
import com.gibs.peixunapi.VO.show.*;
import com.gibs.peixunapi.dao.ExamStudentDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.mapper.ExamStudentMapper;
import com.gibs.peixunapi.model.ExamClassInfo;
import com.gibs.peixunapi.model.ExamStudent;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.DateUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.ExamEnum.*;
import static com.gibs.peixunapi.enums.LinkEnum.EXAM_MANAGE;
import static com.gibs.peixunapi.enums.LinkEnum.SYSTEM_CALL;
import static com.gibs.peixunapi.enums.ResultEnum.*;
import static com.gibs.peixunapi.enums.RoleEnum.GROUP_ADMIN;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class ExamStudentServiceImpl implements ExamStudentService {

    @Autowired
    private ExamStudentDao examStudentDao;
    @Autowired
    private ExamStudentMapper examStudentMapper;
    @Autowired
    private ExamClassInfoService examClassInfoService;
    @Autowired
    private UserService userService;
    @Autowired
    private AnswerSheetService answerSheetService;
    @Autowired
    private ExamStudentService examStudentService;
    @Autowired
    private NoticeService noticeService;

    @Override
    public ExamStudent get(Integer id) {
        return examStudentDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addStudentToExam(String studentIds, Integer examClass) {
        ExamClassInfo classInfo = examClassInfoService.get(examClass);
        //当date1大于date2时，返回false，当小于等于时，返回true；
//        if (new Date().after(classInfo.getSignDeadLine())) {
//            throw new BaseException(PASS_SIGN_UP_DEADLINE);
//        }
        List<Integer> idList = new ArrayList<>();
        if (studentIds.contains(",")) {
            idList = Arrays.stream(studentIds.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        } else {
            idList.add(Integer.parseInt(studentIds));
        }
        List<User> studentList = userService.getIdIn(idList);
        List<ExamStudent> list = new ArrayList<>(idList.size());
        for (User user : studentList) {
            ExamStudent examStudent = new ExamStudent();
            examStudent.setStudent(user);
            examStudent.setExamClassInfo(classInfo);
            examStudent.setDailyScore(BigDecimal.ZERO);
            list.add(examStudent);
        }
        try {
            examStudentDao.saveAll(list);
            /*消息通知*/
            /*添加消息主体*/
            String content = "您好，您将于" + DateUtils.toDateZNString(classInfo.getExamStartTime())
                    + "进行" + classInfo.getName() + "考试，请您尽快确认报名。如果需要缓考，请通过申请缓考的方式向相关管理员提出。";
            noticeService.sendNoticeToUser("考试邀请", content, EXAM_MANAGE.getLink(), idList, SYSTEM_CALL.getNum());
            return true;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(UPDATE_FAIL, e);
        }

    }

    @Override
    public Integer getClassNumber(Integer classId) {
        return examStudentDao.countByExamClassInfo_Id(classId);
    }

    @Override
    public PageData getSignUpList(Integer classId, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<ExamSignUpStudentVO> dataPage = new PageInfo<>(examStudentMapper.findSignUpStudentList(classId));
        if (dataPage.getTotal() == 0) {
            throw new NoDataException("page");
        }
        List<ExamSignUpStudentVO> dataList = dataPage.getList();
        dataList.forEach(i -> i.setSort((dataList.indexOf(i) + 1) + limit * page));

        return new PageData<>((int) dataPage.getTotal(), limit, page, dataList);
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public Boolean signUpClass(Integer classId, Integer studentId) {
        ExamClassInfo examClassInfo = examClassInfoService.get(classId);
        User student = userService.get(studentId);
        //当date1大于date2时，返回false，当小于等于时，返回true；
//        if (new Date().after(examClassInfo.getSignDeadLine())) {
//            throw new BaseException(PASS_SIGN_UP_DEADLINE);
//        }

        ExamStudent examStudent = new ExamStudent();
        examStudent.setStudent(student);
        examStudent.setDailyScore(BigDecimal.ZERO);
        examStudent.setExamClassInfo(examClassInfo);
        try {
            examStudentDao.save(examStudent);
//            noticeService.sendNoticeToUser("报名成功", "", EXAM_MANAGE.getLink(), studentId.toString()
//                    , SYSTEM_CALL.getNum());
            return true;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteStudentToExam(String studentIds, Integer examClassId) {

        try {
            List<Integer> idList = new ArrayList<>();

            if (studentIds.contains(",")) {
                idList = Arrays.stream(studentIds.split(",")).map(Integer::parseInt).collect(Collectors.toList());
            } else {
                idList.add(Integer.parseInt(studentIds));
            }
            idList.forEach(i -> {
                ExamStudent examStudent = examStudentDao.findByStudentAndClass(i, examClassId);
                if (examStudent == null) {
                    throw new BaseException(DATA_NOT_FIND);
                }
                examStudentDao.delete(examStudent);
            });
            return true;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(DELETE_FAIL, e);
        }
    }

    @Override
    public PageData<StudentInExamClassVO> getStudentListInExamClass(String name, Integer classId, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<StudentInExamClassVO> dataPage = new PageInfo<>(examStudentMapper.findStudentList(name, classId));
        if (dataPage.getTotal() == 0) {
            throw new NoDataException("page");
        }
        List<StudentInExamClassVO> dataList = dataPage.getList();
        dataList.forEach(i -> {
            i.setSort((dataList.indexOf(i) + 1) + limit * page);
            if (i.getExamStatus() == 1) {
                i.setExamStatusString("已完成");
            } else {
                i.setExamStatusString("未考试");
            }
            if (i.getCorrectStatus() == -1) {
                i.setCorrectStatusString("未批改");
            } else if (i.getCorrectStatus() == 0) {
                i.setCorrectStatusString("批改中");
            } else {
                i.setCorrectStatusString("已批改");
            }
            if (i.getApplyDelayedExam() == -1) {
                i.setApplyDelayedExamString("未通过");
            } else if (i.getApplyDelayedExam() == 0) {
                i.setApplyDelayedExamString("未申请");
            } else if (i.getApplyDelayedExam() == 1) {
                i.setApplyDelayedExamString("已申请");
            } else {
                i.setApplyDelayedExamString("已通过");
            }
        });
        return new PageData<>((int) dataPage.getTotal(), limit, page, dataList);
    }

    @Override
    public void checkExamCode(String code, Integer studentId) {

        Integer classId = examClassInfoService.findByCode(code);
        ExamStudent examStudent = examStudentDao.findByStudentAndClass(studentId, classId);

        if (examStudent == null) {
            throw new BaseException(ResultEnum.NOT_ALLOW_TO_EXAM);
        }

        examStudent.setTakeExam(1);
        try {
            examStudentDao.save(examStudent);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public StudentInExamClassVO getExamStudent(Integer classId, Integer studentId) {
        ExamStudent examStudent = examStudentDao.findByStudentAndClass(studentId, classId);
        if (examStudent == null) {
            throw new BaseException(DATA_NOT_FIND);
        }
        StudentInExamClassVO vo = new StudentInExamClassVO();
        ConverterUtil.copyProperties(examStudent, vo, "sort", "id");
        vo.setId(examStudent.getStudent().getId());
        vo.setApplyDelayedExam(examStudent.getApplyDelayedExam());
        return vo;
    }

    @Override
    public ExamStudent getByStudentAndClass(Integer classId, Integer studentId) {
        return examStudentDao.findByStudentAndClass(studentId, classId);
    }

    @Override
    public ExamStudent getByUniqueCode(String uniqueCode) {
        ExamStudent examStudent = examStudentDao.findByUniqueCode(uniqueCode);
        if (examStudent == null) {
            throw new BaseException(CODE_NOT_EXIST);
        }
        return examStudent;
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void submitPaper(String uniqueCode) {
        ExamStudent examStudent = getByUniqueCode(uniqueCode);
        examStudent.setExamStatus(1);
        try {
            examStudentDao.save(examStudent);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void applyForDelayed(Integer studentId, Integer examClassId) {
        ExamStudent examStudent = examStudentDao.findByStudentAndClass(studentId, examClassId);
        if (examStudent == null) {
            throw new BaseException(DATA_NOT_FIND);
        }
        examStudent.setApplyDelayedExam(1);
        try {
            examStudentDao.save(examStudent);

            /*消息通知*/
            String sb = "您好，您组织" + DateUtils.toDateZNString(examStudent.getExamClassInfo().getExamStartTime()) + "的" +
                    examStudent.getExamClassInfo().getExamInfo() + "认证考试，当前有学员申请缓考。请及时予以办理。";
            noticeService.sendNoticeToRole("缓考申请通知", sb, EXAM_MANAGE.getLink(), GROUP_ADMIN.getCode()
                    , SYSTEM_CALL.getNum());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public void applyForDelayedBatch(String studentIds, Integer examClassId) {

        List<Integer> ids = new ArrayList<>();
        if (studentIds.contains(",")) {
            ids = Arrays.stream(studentIds.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        } else {
            ids.add(Integer.parseInt(studentIds));
        }
        try {
            examStudentDao.DelayedBatch(ids, examClassId);

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void checkDelayed(String ids, Integer examClassId, Integer delayed) {
        List<Integer> idList = new ArrayList<>();
        if (ids.contains(",")) {
            idList = Arrays.stream(ids.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        } else {
            idList.add(Integer.parseInt(ids));
        }

        List<ExamStudent> list = new ArrayList<>();
        Map<Integer, String> contentMap = new HashMap<>();
        idList.forEach(i -> {
            ExamStudent examStudent = examStudentDao.findByStudentAndClass(i, examClassId);
            if (examStudent == null) {
                throw new BaseException(DATA_NOT_FIND);
            }
            examStudent.setApplyDelayedExam(delayed);
            list.add(examStudent);

            /*添加通知主体*/
            String content = "您好，您申请" + DateUtils.toDateZNString(examStudent.getExamClassInfo().getExamStartTime()) +
                    "进行的" + examStudent.getExamClassInfo().getName() + "的缓考申请，管理员已经同意。请留意下次考试的通知，尽快参加考试。";
            contentMap.put(i, content);
        });
        try {
            examStudentDao.saveAll(list);
            noticeService.sendNoticeListToUser("申请缓考结果", contentMap, EXAM_MANAGE.getLink(), SYSTEM_CALL.getNum());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public ExamStudent getCorrect(Integer classId) {
        //获取正在批改的学生 防止中途离开缺该漏改
        ExamStudent examStudent = examStudentDao.findCorrectingStudent(classId);
        if (examStudent == null) {
            //获取未批改的学生
            examStudent = examStudentDao.findCorrectStudent(classId);
        }
        if (examStudent == null) {
            return null;
        }
        try {
            examStudent.setCorrectStatus(0);
            examStudent = examStudentDao.save(examStudent);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
        return examStudent;
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public boolean finishCorrect(String uniqueCode) {
        //修改状态
        ExamStudent examStudent = examStudentDao.findByUniqueCode(uniqueCode);
        examStudent.setCorrectStatus(1);
        //考试分数
        examStudent.setExamScores(answerSheetService.calculateScore(examStudent.getId()));
        try {
            examStudentDao.save(examStudent);

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
        return examStudentService.getCorrect(examStudent.getExamClassInfo().getId()) != null;
    }

    @Override
    public Integer countExamNumber(Integer classId) {
        return examStudentDao.countNoExamNumber(classId);
    }

    @Override
    public PageData getExamManageStudentList(String key, Integer classId, Integer page, Integer limit) {
        Pageable pageable = PageRequest.of(page - 1, limit);
        Page<ExamStudent> pageData = examStudentDao.findExamManageStudent(key, classId, pageable);
        List<ExamStudent> list = pageData.getContent();
        if (Collections.isEmpty(list)) {
            throw new NoDataException("page");
        }
        ExamClassInfo examClassInfo = examClassInfoService.get(classId);
        List<ExamManageStudentVO> voList = new ArrayList<>(list.size());
        for (ExamStudent examStudent : list) {
            ExamManageStudentVO vo = new ExamManageStudentVO();
            ConverterUtil.copyProperties(examStudent, vo);
            vo.setBusinessName(examStudent.getStudent().getBusiness().getName());
            vo.setProgress(answerSheetService.getStudentProgress(examStudent.getId(), examStudent.getExamClassInfo().getExamInfo()));
            vo.setUsername(examStudent.getStudent().getUsername());
            vo.setExamStartTime(examClassInfo.getExamStartTime());
            vo.setExamEndTime(examClassInfo.getExamEndTime());
            String examStatus = examStudentService.getStatusByExamClassAndStudent(examClassInfo.getId(), examStudent.getStudent().getId());
            vo.setExamStatusString(StringUtils.isBlank(examStatus) ? "未报名" : examStatus);
            voList.add(vo);
        }
        return new PageData<>((int) pageData.getTotalElements(), limit, page, voList);
    }

    @Override
    public String getStatusByExamClassAndStudent(Integer classId, Integer studentId) {
        ExamStudent examStudent = examStudentDao.findByStudentAndClass(studentId, classId);
        if (examStudent == null) {
            return "";
        }
        if (examStudent.getExamStatus() == 0) {
            return "待考试";
        } else if (examStudent.getExamStatus() == 1) {
            return "已考试";
        }
        return null;
    }

    @Override
    public PageData getStudentScoreManage(Integer studentId, Integer page, Integer limit) {
        //1.获取学生考试成绩
        Pageable pageable = PageRequest.of(page - 1, limit);
        Page<ExamStudent> pageData = examStudentDao.findFinishExam(studentId, pageable);
        List<ExamStudent> examStudentList = pageData.getContent();
        if (Collections.isEmpty(examStudentList)) {
            throw new NoDataException();
        }
        List<StudentExamManageVO> voList = new ArrayList<>(examStudentList.size());
        for (ExamStudent examStudent : examStudentList) {
            StudentExamManageVO vo = new StudentExamManageVO();
            vo.setId(examStudent.getId());
            vo.setSort((examStudentList.indexOf(examStudent) + 1) + (page - 1) * 10);
            vo.setClassName(examStudent.getExamClassInfo().getName());
            vo.setCourseName(examStudent.getExamClassInfo().getExamInfo().getCourse().getName());
            vo.setExamScores(Optional.ofNullable(examStudent.getExamScores()).orElse(BigDecimal.valueOf(0d)));
            vo.setDailyScore(Optional.ofNullable(examStudent.getDailyScore()).orElse(BigDecimal.valueOf(0d)));
            //是否缓考
            if (examStudent.getApplyDelayedExam() <= 0) {
                //是否参加并完成考试并完成阅卷
                if (examStudent.getTakeExam() == 1 && examStudent.getExamStatus() == 1 && examStudent.getCorrectStatus() == 1) {
                    BigDecimal b1 = Optional.ofNullable(examStudent.getExamScores()).orElse(BigDecimal.valueOf(0d));
                    BigDecimal b2 = Optional.ofNullable(examStudent.getDailyScore()).orElse(BigDecimal.valueOf(0d));
                    vo.setFinalScore(b1.multiply(Optional.ofNullable(examStudent.getExamClassInfo().getProportion()).orElse(BigDecimal.valueOf(0d))).add(b2));
                    vo.setExamStatus(FINISH_EXAM.getMessage());
                } //是否参加并完成考试并阅卷中
                else if (examStudent.getTakeExam() == 1 && examStudent.getExamStatus() == 1 && examStudent.getCorrectStatus() == 0) {
                    vo.setFinalScore(BigDecimal.valueOf(0d));
                    vo.setExamStatus(CORRECTING.getMessage());
                } //是否参加并未完成考试
                else if (examStudent.getTakeExam() == 1 && examStudent.getExamStatus() == 0) {
                    vo.setFinalScore(BigDecimal.valueOf(0d));
                    vo.setExamStatus(NOT_FINISH_EXAM.getMessage());
                } //是否参加考试
                else if (examStudent.getTakeExam() == 0) {
                    vo.setFinalScore(BigDecimal.valueOf(0d));
                    vo.setExamStatus(NOT_TAKE_EXAM.getMessage());
                }
            } else {
                vo.setFinalScore(BigDecimal.valueOf(0d));
                vo.setExamStatus(DELAYED_EXAM.getMessage());
            }
            voList.add(vo);
        }

        return new PageData<>((int) pageData.getTotalElements(), limit, page, voList);
    }

    @Override
    public List<ScoreManageVO> getScoreManageList(String key, Integer classId) {
        List<ScoreManageVO> list = examStudentMapper.findByExamClass(key, classId);
        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }

        return list;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public Integer editDailyScore(DailyScoreVO vo) {
        ExamStudent student = examStudentService.get(vo.getId());
        Integer classId = student.getExamClassInfo().getId();
        //平时分
//        student.setDailyScore(new BigDecimal(vo.getDailyScore()));
        student.setDailyScore(BigDecimal.ZERO);
        //生成最终分数
        BigDecimal b1 = new BigDecimal(vo.getDailyScore());
        BigDecimal b2 = student.getExamScores();
        student.setFinalScore(b1
                .multiply(student.getExamClassInfo().getProportion()).add(b2));
        if (student.getFinalScore().compareTo(student.getExamClassInfo().getExamInfo().getPassScore()) > 0) {
            student.setIsPass(1);
        }
        try {
            examStudentDao.save(student);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
        return classId;
    }

    @Override
    public Integer countPassExamNumber(Integer classId) {
        return examStudentDao.countPassExamNumber(classId);
    }

    @Override
    public List<PassExamStudentVO> getPassExamStudent(Integer classId) {
//        List<Integer> idList = examStudentDao.getIssuedIdList(classId);

        List<ExamStudent> list = examStudentDao.findPassExamStudent(classId);

        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
        List<PassExamStudentVO> voList = new ArrayList<>(list.size());
        for (ExamStudent examStudent : list) {
            PassExamStudentVO vo = new PassExamStudentVO();
            ConverterUtil.copyProperties(examStudent, vo);
            vo.setBusinessName(examStudent.getStudent().getBusiness().getName());
            vo.setSort(voList.indexOf(vo) + 1);
            vo.setUsername(examStudent.getStudent().getUsername());
            voList.add(vo);
        }
        return voList;
    }

    @Override
    public AdmissionTicketVO getAdmissionTicket(Integer userId, Integer classId) {
        ExamStudent examStudent = examStudentDao.findByStudentAndClass(userId, classId);
        if (examStudent == null) {
            throw new BaseException(DATA_NOT_FIND);
        }
        AdmissionTicketVO vo = new AdmissionTicketVO();
        ConverterUtil.copyProperties(examStudent, vo);
        ConverterUtil.copyProperties(examStudent.getExamClassInfo(), vo);
        vo.setAbout(examStudent.getExamClassInfo().getExamInfo().getAbout());
        vo.setUsername(examStudent.getStudent().getUsername());
        vo.setBusinessName(examStudent.getStudent().getBusiness().getName());
        vo.setAvatarUrl(examStudent.getStudent().getAvatar() == null ? "" : examStudent.getStudent().getAvatar().getUrl());
        vo.setSex(examStudent.getStudent().getSex() == 1 ? "男" : "女");
        vo.setCertificateNumber(examStudent.getStudent().getCertificateNumber());
        vo.setCourseName(examStudent.getExamClassInfo().getExamInfo().getCourse().getName());
        vo.setPost(examStudent.getStudent().getPost());
        vo.setCode(examStudent.getExamClassInfo().getCode());
        return vo;
    }

    @Override
    public Integer getLatestPassExamScore(Integer userId, Integer courseId) {
        Integer score = examStudentDao.findLatestPassExamScore(userId, courseId);
        return score == null ? 0 : score;
    }


}

