package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.post.CourseInfoVO;
import com.gibs.peixunapi.VO.show.*;
import com.gibs.peixunapi.dao.CourseDao;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.mapper.CourseMapper;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.EntityNotFoundException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.CourseEnum.DOCUMENT;
import static com.gibs.peixunapi.enums.CourseEnum.VIDEO;
import static com.gibs.peixunapi.enums.LinkEnum.COURSE_MANAGE;
import static com.gibs.peixunapi.enums.LinkEnum.SYSTEM_CALL;
import static com.gibs.peixunapi.enums.ResultEnum.*;
import static com.gibs.peixunapi.enums.RoleEnum.GROUP_ADMIN;
import static com.gibs.peixunapi.enums.RoleEnum.STUDENT;
import static com.gibs.peixunapi.enums.ScheduledEnum.FINISH_COURSE;
import static com.gibs.peixunapi.enums.ScheduledEnum.START_COURSE;

/**
 * @author liangjiawei
 */
@Slf4j
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseDao courseDao;
    @Autowired
    private CourseService courseService;
    @Autowired
    private CourseTeacherService courseTeacherService;
    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private StudentCourseService studentCourseService;
    @Autowired
    private DocumentationService documentationService;
    @Autowired
    private VideoClassService videoClassService;
    @Autowired
    private ExamClassInfoService examClassInfoService;
    @Autowired
    private UserService userService;
    @Autowired
    private ExercisesService exercisesService;
    @Autowired
    private CourseBusinessService courseBusinessService;
    @Autowired
    private NoticeService noticeService;
    @Autowired
    private FileInfoService fileInfoService;
    @Autowired
    private ScheduledService scheduledService;
    @Autowired
    private StudyProgressService studyProgressService;


    @Override
    public Course get(Integer id) {
        return courseDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result create(CourseInfoVO courseInfoVO) {

        //保存课程
        Course course = new Course();
        ConverterUtil.copyProperties(courseInfoVO, course, "assistTeacherList", "teacher", "audit");
        course.setStartTime(LocalDate.parse(courseInfoVO.getStartTime()));
        course.setEndTime(LocalDate.parse(courseInfoVO.getEndTime()));
        try {
            User creator = userService.getAttributeUser();
            course.setCreator(creator.getId());
            Course result = courseDao.save(course);

            User teacher = userService.get(courseInfoVO.getTeacherId());
            //保存负责老师
            courseTeacherService.saveManage(result.getId(), teacher);
            //保存助教老师
            if (courseInfoVO.getAssistTeachersIdList() != null && !courseInfoVO.getAssistTeachersIdList().isEmpty()) {
                courseTeacherService.saveAssist(result.getId(), courseInfoVO.getAssistTeachersIdList());
            }
            return ResultUtil.success(CREATE_SUCCESS, result.getId());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }


    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public Result auditCourse(Integer audit, Integer courseId) {
        Course course = courseDao.findById(courseId).orElseThrow(EntityNotFoundException::new);
//        course.setAudit(audit);
        try {
            courseDao.save(course);
            videoClassService.auditByCourse(audit, courseId);
            documentationService.auditDocument(audit, courseId);

            StringBuilder sb = new StringBuilder("您好,您提交的培训").append(course.getName()).append("申请已经审批,审批结果是");
            if (audit == -1) {
                sb.append("不通过。");
                noticeService.sendNoticeToRole("培训审核申请结果", "", COURSE_MANAGE.getLink(),
                        GROUP_ADMIN.getCode(), SYSTEM_CALL.getNum());
            } else {
                noticeService.sendNoticeToUser("培训审核申请结果", "", COURSE_MANAGE.getLink(),
                        course.getCreator().toString(), SYSTEM_CALL.getNum());
                scheduledService.create("startCourse" + courseId, START_COURSE, courseId.toString(),new Date(Timestamp.valueOf(course.getStartTime().atTime(LocalTime.MIDNIGHT)).getTime()) );
                scheduledService.create("finishCourse" + courseId, FINISH_COURSE, courseId.toString(),new Date(Timestamp.valueOf(course.getEndTime().atTime(LocalTime.MIDNIGHT)).getTime())  );
            }
            return ResultUtil.success(UPDATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }

    }


    @Override
    public ShowCourseInfoVO getCourseInfo(Integer courseId, String type) {

        Course course = courseDao.findById(courseId).orElseThrow(EntityNotFoundException::new);
        ShowCourseInfoVO vo = new ShowCourseInfoVO();
        ConverterUtil.copyProperties(course, vo, "isDelete", "createTime", "updateTime");
        //设置负责老师名单
        vo.setTeacher(courseTeacherService.getTeacherVO(courseId));
        //设置助教名单
        vo.setAssistTeacherList(courseTeacherService.getAssistantOption(courseId));
        //如果学生身份进入则添加学习信息
        if (Strings.isNotBlank(type) && STUDENT.getCode().equals(type)) {
            User user = userService.getAttributeUser();
            StudentCourse studentCourse = studentCourseService.getByCourseAndStudent(courseId, user.getId());
            ConverterUtil.copyProperties(studentCourse, vo, "status", "id");
//            if (studentCourse.getCourse().getAudit() == 1) {
//                vo.setAuditStatus("已通过");
//            }
//            if (studentCourse.getCourse().getAudit() == 0) {
//                vo.setAuditStatus("未通过");
//            }
//            if (studentCourse.getSubscribe()) {
                if (studentCourse.getSignUp()) {
                    if (studentCourse.getStudyStatus() == -2) {
                        vo.setStudyStatus("未完成");
                    }
                    if (studentCourse.getStudyStatus() == -1) {
                        vo.setStudyStatus("未开始");
                    }
                    if (studentCourse.getStudyStatus() == 0) {
                        vo.setStudyStatus("学习中");
                    }
                    if (studentCourse.getStudyStatus() == 1) {
                        vo.setStudyStatus("已结束");
                    }
                } else {
                    vo.setStudyStatus("未报名");
                }
//            } else {
//                vo.setStudyStatus("未订阅");
//            }
        }
        return vo;
    }

    @Override
    public List<StudentCourseVO> getStudentCourseList(Integer studentId, CourseEnum courseEnum) {
        List<StudentCourse> list = studentCourseService.getStudentSubscribeList(studentId, true);
        List<StudentCourseVO> result = new ArrayList<>();

        for (StudentCourse studentCourse : list) {
            StudentCourseVO vo = new StudentCourseVO();
            ConverterUtil.copyProperties(studentCourse, vo);
            ConverterUtil.copyProperties(studentCourse.getCourse(), vo, "studyStatus", "signUp");
            vo.setId(studentCourse.getCourse().getId());
            vo.setSort(list.indexOf(studentCourse) + 1);
            vo.setTeacherName(courseTeacherService.getTeacher(studentCourse.getCourse().getId()).getTeacher().getUsername());
//            vo.setAudit(studentCourse.getCourse().getAudit());
//
//            if (studentCourse.getCourse().getAudit() == 1) {
//                vo.setAuditStatus("已通过");
//            }
//            if (studentCourse.getCourse().getAudit() == 0) {
//                vo.setAuditStatus("未通过");
//            }
                if (studentCourse.getSignUp()) {
                    if (studentCourse.getStudyStatus() == -2) {
                        vo.setStudyStatus("未完成");
                    }
                    if (studentCourse.getStudyStatus() == -1) {
                        vo.setStudyStatus("未开始");
                    }
                    if (studentCourse.getStudyStatus() == 0) {
                        vo.setStudyStatus("学习中");
                    }
                    if (studentCourse.getStudyStatus() == 1) {
                        vo.setStudyStatus("已结束");
                    }
                } else {
                    vo.setStudyStatus("未报名");
                }
//            } else {
//                vo.setStudyStatus("未订阅");
//            }
            List<ChapterListVO> chapterList = new ArrayList<>();
            if (courseEnum == CourseEnum.DOCUMENT) {
                chapterList = documentationService.studentGetList(studentCourse.getCourse().getId(), studentId);
            } else if (courseEnum == VIDEO) {
                chapterList = videoClassService.studentGetList(studentCourse.getCourse().getId(), studentId);
            }
            if (Collections.isEmpty(chapterList)) {
                chapterList = null;
        }
            vo.setChapterVOList(chapterList);
//            //TODO 设置数据字典 最大阅读章节数
//            vo.setLearnChapterMaxNum(5);
//
//            //没有报名 只能预览
//            if (!studentCourse.getSignUp()) {
//                //TODO 设置数据字典 开放章节数
//                vo.setOpenChapterNum(2);
//            } else if (chapterList != null) {
//                //TODO 设置数据字典
//                vo.setOpenChapterNum(chapterList.size());
//            }

            result.add(vo);
        }
        return result;
    }

    @Override
    public Result getSubjectHubCourseList(Integer page, Integer limit, Integer teacherId, String key) {
        PageHelper.startPage(page, limit);
        PageInfo<Course> pageData = new PageInfo<>(courseMapper.findCoursesByCreator(teacherId, key));
        List<Course> list = pageData.getList();
        if (Collections.isEmpty(list)) {
            throw new NoDataException("page");
        }
        List<CourseListVO> voList = new ArrayList<>();
        for (Course course : list) {
            CourseListVO vo = new CourseListVO();
            ConverterUtil.copyProperties(course, vo, "sort");
            vo.setSort((list.indexOf(course) + 1) * (page - 1) * 10);
            vo.setTeacherName(courseTeacherService.getTeacher(course.getId()).getTeacher().getUsername());
            voList.add(vo);
        }
        return ResultUtil.successPage(FIND_SUCCESS,
                new PageData<>((int) pageData.getTotal(), limit,
                        page, voList));

    }

    @Override
    public PageData<CourseListVO> getTestPaperCourseList(Integer page, Integer limit, Integer teacherId, String key) {
        PageHelper.startPage(page, limit);
        PageInfo<Course> pageData = new PageInfo<>(courseMapper.findCoursesByCreator(teacherId, key));
        List<Course> list = pageData.getList();
        if (Collections.isEmpty(list)) {
            throw new NoDataException("page");
        }
        List<CourseListVO> voList = new ArrayList<>();
        for (Course course : list) {
            CourseListVO vo = new CourseListVO();
            ConverterUtil.copyProperties(course, vo);
            vo.setSort((list.indexOf(course) + 1) * (page - 1) * 10);
            vo.setSignUpCount(studentCourseService.countSignUpNumberByCourse(course.getId()));
//            vo.setSubscribeCount(studentCourseService.countSubscribeNumberByCourse(course.getId()));
            vo.setTeacherName(courseTeacherService.getTeacher(course.getId()).getTeacher().getUsername());
            voList.add(vo);
        }
        return
                new PageData<>((int) pageData.getTotal(), limit,
                        page, voList);
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public Result editCourse(CourseInfoVO courseInfoVO) {
        try {
            Course course = courseDao.findById(courseInfoVO.getId()).orElseThrow(EntityNotFoundException::new);
            ConverterUtil.copyInSpecifyProperties(courseInfoVO, course,
                    "name", "hours", "startTime", "endTime", "invalidTime", "summary", "fileCount", "videoCount", "type");
            courseDao.save(course);
            return ResultUtil.success(UPDATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public List<ShowChapterVO> getList(Integer courseId, CourseEnum courseEnum) {
        List<ShowChapterVO> chapterList = new ArrayList<>();
        if (courseEnum == CourseEnum.DOCUMENT) {
            chapterList = documentationService.getList(courseId);
        } else if (courseEnum == VIDEO) {
            chapterList = videoClassService.getList(courseId);
        }
        return chapterList;
    }

    @Override
    public PageData<CourseListVO> getLatestCourseList(Integer page, Integer limit) {
        Pageable pageable = PageRequest.of(page - 1, limit, Sort.by("create_time"));
        Date time = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");

        Page<Course> pageData = courseDao.findByEndTimeAndAudit(sdf.format(time), pageable);

        List<CourseListVO> voList = getCourseListVOList(page, pageData);
        return new PageData<>((int) pageData.getTotalElements(), limit, page, voList);
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public Result linkCourse(Integer courseId, Integer linkCourseId) {
        try {
            documentationService.linkClass(courseId, linkCourseId);
            videoClassService.linkClass(courseId, linkCourseId);
            return ResultUtil.success(OPERATING_SUCCESS);
        } catch (Exception e) {
            throw new BaseException(LINK_FAIL);
        }
    }

    @Override
    public Result getExamCourseVOInUndone(String name, Integer page, Integer limit) {
        Date time = new Date();
        PageHelper.startPage(page, limit);
        User user = userService.getAttributeUser();
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        Integer userId = user.getId();
        if(user.getRole().toLowerCase().contains("admin")){
            userId=null;
        }
        PageInfo<Course> pageData = new PageInfo<>(courseMapper.findStudyingCourse(sdf.format(time), name, userId));
        List<Course> list = pageData.getList();
        if (pageData.getList().isEmpty()) {
            throw new NoDataException("page");
        }
        List<ExamCourseVO> voList = new ArrayList<>(pageData.getList().size());
        for (Course course : list) {
            ExamCourseVO vo = new ExamCourseVO();
            ConverterUtil.copyProperties(course, vo, "sort", "status");
            //报名人数
            vo.setSignUpCount(studentCourseService.countSignUpNumberByCourse(course.getId()));
            //有考试资格人数
            vo.setAllowExamCount(studentCourseService.countFinishStudy(course.getId()));
            //培训负责老师
            vo.setTeacherName(courseTeacherService.getTeacher(course.getId()).getTeacher().getUsername());
            vo.setExamCount(examClassInfoService.countFinishClass(course.getId()));

            voList.add(vo);
        }
        PageData<ExamCourseVO> data =
                new PageData<>((int) pageData.getTotal(), limit, page, voList);
        return ResultUtil.success(FIND_SUCCESS, data);
    }

    @Override
    public Result getOptionList(String key) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        List<OptionVO> voList = courseMapper.getOptions(key, sdf.format(new Date()));
        voList.forEach(optionVO -> optionVO.setSort(voList.indexOf(optionVO)));
        return ResultUtil.success(FIND_SUCCESS, voList);
    }

    @Override
    public  PageData<CourseManageVO> getCourseManageList(Integer isAudit, Integer teacherId, String name, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<CourseManageVO> pageInfo = new PageInfo<>(courseMapper.getCourseList(isAudit, teacherId, name));

        List<CourseManageVO> voList = pageInfo.getList();
        if (Collections.isEmpty(voList)) {
            throw new NoDataException("page");
        }

        for (CourseManageVO vo : voList) {
            vo.setSubscribeCount(studentCourseService.countSubscribeNumberByCourse(vo.getId()));
            vo.setSignUpCount(studentCourseService.countSignUpNumberByCourse(vo.getId()));
            vo.setClassNumber(examClassInfoService.countClass(vo.getId()));
            vo.setSort((voList.indexOf(vo) + 1) * limit * page - 1);

        }
        return new PageData<>((int) pageInfo.getTotal(), limit, page, voList);

    }

    private List<CourseListVO> getCourseListVOList(Integer page, Page<Course> pageData) {
        List<Course> list = pageData.getContent();
        if (Collections.isEmpty(list)) {
            throw new NoDataException("page");
        }
        List<CourseListVO> voList = new ArrayList<>(list.size());
        for (Course course : list) {
            CourseListVO vo = new CourseListVO();
            ConverterUtil.copyProperties(course, vo);
//            vo.setSubscribeCount(studentCourseService.countSubscribeNumberByCourse(course.getId()));
            vo.setSort((list.indexOf(course) + 1) * page * 10);
            vo.setTeacherName(courseTeacherService.getTeacher(course.getId()).getTeacher().getUsername());
            voList.add(vo);
        }
        return voList;
    }

    @Override
    public List<ChapterOfAdminTeacherVO> findAllAudit(Integer teacherId) {
        List<Course> list = courseMapper.findAllAuditCourse(teacherId);

        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
        List<ChapterOfAdminTeacherVO> voList = new ArrayList<>(list.size());
        for (Course course : list) {
            ChapterOfAdminTeacherVO vo = new ChapterOfAdminTeacherVO();
//            if (course.getAudit() == 1 && !course.getEndTime().before(new Date())) {
//                vo.setStatus("已审批");
//            }
//            if (course.getAudit() == 1 && course.getEndTime().before(new Date())) {
//                vo.setStatus("已结束");
//            }
//            if (course.getAudit() == 0) {
//                vo.setStatus("待审批");
//            }
            vo.setName(course.getName());
            vo.setCourseId(course.getId());
            vo.setTeacherName(courseTeacherService.getTeacher(course.getId()).getTeacher().getUsername());
            voList.add(vo);
        }
        return voList;
    }

    @Override
    public List<ChapterOfAdminTeacherVO> getPassChapterList(String type, Integer teacherId) {
        List<ChapterOfAdminTeacherVO> voList = courseService.
                findAllAudit(teacherId);

        if (VIDEO.getMessage().equals(type)) {
            for (ChapterOfAdminTeacherVO vo : voList) {
                List<VideoClass> list = videoClassService.findByCourseId(vo.getCourseId());
                vo.setChapterList(new ArrayList<>(list.size()));
                for (VideoClass videoClass : list) {
                    ChapterListVO vo2 = new ChapterListVO();
                    ConverterUtil.copyProperties(videoClass, vo2);
                    vo2.setPicture(videoClass.getVideo().getCoverImage() == null ? "" : videoClass.getVideo().getCoverImage().getUrl());
                    Integer exerciseCount = exercisesService.countExercisesInChapter(videoClass.getId(), DOCUMENT);
                    vo2.setExerciseCount(exerciseCount);
                    vo.getChapterList().add(vo2);
                }
            }
        }
        if (CourseEnum.DOCUMENT.getMessage().equals(type)) {
            for (ChapterOfAdminTeacherVO vo : voList) {
                List<Documentation> list = documentationService.findByCourseId(vo.getCourseId());
                vo.setChapterList(new ArrayList<>(list.size()));
                for (Documentation document : list) {
                    ChapterListVO vo2 = new ChapterListVO();
                    ConverterUtil.copyProperties(document, vo2);
                    vo2.setPicture(fileInfoService.getDocumentPicture());
                    Integer exerciseCount = exercisesService.countExercisesInChapter(document.getId(), DOCUMENT);
                    vo2.setExerciseCount(exerciseCount);
                    vo.getChapterList().add(vo2);
                }
            }
        }
        return voList;
    }

    @Override
    public PageData getSubjectHubManageList(String name, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<SubjectHubManageVO> pageData = new PageInfo<>(courseMapper.findSubjectHubManageList(name));
        List<SubjectHubManageVO> list = pageData.getList();
        if (Collections.isEmpty(list)) {
            throw new NoDataException("page");
        }
        return new PageData<>((int) pageData.getTotal(), limit, page, list);
    }

    @Override
    public List<StartingCourseVO> getStartingCourseList(Integer id) {
        Date time = new Date();
        Pageable pageable = PageRequest.of(0, 5, Sort.by("create_time"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");

        Page<Course> pageData = courseDao.findByEndTimeAndAudit(sdf.format(time), pageable);
        List<Course> list = pageData.getContent();
        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
        List<StartingCourseVO> voList = new ArrayList<>(list.size());
        for (Course course : list) {
            StartingCourseVO vo = new StartingCourseVO();
            ConverterUtil.copyProperties(course, vo);
            vo.setSignUpCount(studentCourseService.countSignUpNumberByCourse(course.getId()));
            vo.setSubscribeCount(studentCourseService.countSubscribeNumberByCourse(course.getId()));
            vo.setSort((list.indexOf(course) + 1));
            voList.add(vo);
        }
        return voList;
    }

    @Override
    public void setLearnRestrict(Integer courseId, Integer learnChapterMaxNum, Integer learnRestrict) {
        try {
            courseDao.updateLearnRestrict(courseId, learnChapterMaxNum, (learnRestrict == 1));
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public List<CourseOptionVO> getOptionListByCourse(Integer courseId, String type) {
        List<CourseOptionVO> voList = new ArrayList<>();
        if (VIDEO.getMessage().equals(type)) {
            List<VideoClass> videoList = videoClassService.findByCourseId(courseId);
            for (VideoClass videoClass : videoList) {
                CourseOptionVO vo = new CourseOptionVO();
                ConverterUtil.copyProperties(videoClass, vo);
                voList.add(vo);
            }
        }
        if (DOCUMENT.getMessage().equals(type)) {
            List<Documentation> documentList = documentationService.findByCourseId(courseId);
            for (Documentation documentation : documentList) {
                CourseOptionVO vo = new CourseOptionVO();
                ConverterUtil.copyProperties(documentation, vo);
                voList.add(vo);
            }
        }
        return voList;
    }

    @Override
    public List<BusinessSubscribeVO> businessSubscribeList(Integer businessId) {
        List<CourseBusiness> list = courseBusinessService.getByBusiness(businessId);
        List<BusinessSubscribeVO> voList = new ArrayList<>(list.size());
        for (CourseBusiness courseBusiness : list) {
            BusinessSubscribeVO vo = new BusinessSubscribeVO();
            ConverterUtil.copyProperties(courseBusiness, vo);
            vo.setName(courseBusiness.getCourse().getName());
//            vo.setInvalidTime(courseBusiness.getCourse().getInvalidTime());
            vo.setSubscribeCount(studentCourseService.countSignUpNumberByCourse(courseBusiness.getCourse().getId()));
            voList.add(vo);
        }
        return voList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addAssistTeacher(Integer courseId, String ids) {

        List<Integer> idList = new ArrayList<>();
        if (ids.contains(",")) {
            idList = Arrays.stream(ids.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        } else {
            idList.add(Integer.parseInt(ids));
        }
        //保存助教老师
        courseTeacherService.saveAssist(courseId, idList);
        noticeService.sendNoticeToUser("加入课程", "", COURSE_MANAGE.getLink(),
                ids, SYSTEM_CALL.getNum());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void applyAudit(Integer courseId) {
        try {
            courseDao.applyAudit(courseId);

            videoClassService.applyAudit(courseId);
            documentationService.applyAudit(courseId);
            noticeService.sendNoticeToRole("培训审核申请", "您有新的培训审批申请,请尽快处理。", COURSE_MANAGE.getLink(),
                    GROUP_ADMIN.getCode(), SYSTEM_CALL.getNum());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }

    }

    @Override
    public Result deleteCourse(Integer delete, Integer courseId) {
        Course course = courseDao.findById(courseId).orElseThrow(EntityNotFoundException::new);
        course.setDeleted(1 == delete);
        try {
            courseDao.save(course);
            videoClassService.auditByCourse(delete == 1 ? 0 : 1, courseId);
            documentationService.auditDocument(delete == 1 ? 0 : 1, courseId);
            return ResultUtil.success(DELETE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }
}

