package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.DTO.ShowExerciseRecordDTO;
import com.gibs.peixunapi.VO.post.StudyRecordVO;
import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.VO.show.ShowExerciseRecordVO;
import com.gibs.peixunapi.dao.ExerciseRecordDao;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.mapper.ExerciseRecordMapper;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ResultUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Transactional(rollbackFor = Exception.class)
@Service
@Slf4j
public class ExerciseRecordServiceImpl implements ExerciseRecordService {

    @Autowired
    private ExerciseRecordDao exerciseRecordDao;
    @Autowired
    private ExerciseRecordMapper exerciseRecordMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private ExercisesService exercisesService;
    @Autowired
    private SubjectHubService subjectHubService;
    @Autowired
    private ExerciseRecordService exerciseRecordService;
    @Autowired
    private DocumentationService documentationService;
    @Autowired
    private VideoClassService videoClassService;
    @Autowired
    private StudyProgressService studyProgressService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result create(Integer studentId, List<StudyRecordVO> recordVOList) {
        List<ExerciseRecord> list = new ArrayList<>(recordVOList.size());
        try {
            for (StudyRecordVO recordVO : recordVOList) {
                ExerciseRecord exerciseRecord = exerciseRecordService.getByExercisesAndStudent(recordVO.getExerciseId(), studentId);
                User student = userService.get(studentId);
                exerciseRecord.setStudent(student);
                Exercises exercises = exercisesService.get(recordVO.getExerciseId());
                exerciseRecord.setExercise(exercises);
                exerciseRecord.setMyAnswer(recordVO.getMyAnswer());

                exerciseRecord = exerciseRecordDao.saveAndFlush(exerciseRecord);
                String answer = recordVO.getMyAnswer();
                String right = exercises.getCourseSubjectHub().getSubjectHub().getAnswer();
                if ("fill".equals(exercises.getCourseSubjectHub().getSubjectHub().getType())) {
                    right = right.replaceAll("[,，]", "");
                }
                if (answer.equals(right)) {
                    exerciseRecord.setIsRight(true);
                } else {
                    exerciseRecord.setIsRight(false);
                    exercisesService.addWrongExercises(exerciseRecord, student.getId());

                }
                list.add(exerciseRecord);
            }


            return ResultUtil.success(ResultEnum.CREATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.CREATE_FAIL, e);
        }
    }

    @Override
    public PageData<ShowExerciseRecordDTO> getListPage(Integer studentId, Integer courseId, Integer chapterId, String key, Integer page, Integer limit) {
        ShowExerciseRecordVO showerVO = exerciseRecordMapper.getCourse(studentId, courseId);
        PageHelper.startPage(page, limit);
        PageInfo<ShowExerciseRecordDTO> pageInfo =
                new PageInfo<>(exerciseRecordMapper.getChapterRecord(showerVO.getCourseId(), studentId));

        List<ShowExerciseRecordDTO> list = pageInfo.getList();
        if (Collections.isEmpty(list)) {
            throw new NoDataException("page");
        }
        for (ShowExerciseRecordDTO dto : list) {
            List<Integer> idList = exerciseRecordMapper.getExerciseId(dto.getChapterId(), studentId, dto.getType());
            dto.setSubjectHubVOList(subjectHubService.getVOListByIdList(idList));
        }
        showerVO.setRecordDTOList(list);
        return new PageData<>((int) pageInfo.getTotal(), limit, page, list);

    }

    @Override
    public Integer countStudentAndDocument(Integer student, Integer chapter) {
        return exerciseRecordDao.countRecordByStudentAndDocument(student, chapter);
    }

    @Override
    public Integer getFinishExerciseCountByStudent(Integer id, Integer userId, CourseEnum courseEnum) {
        return exerciseRecordMapper.finishExerciseCount(id, userId, courseEnum.getMessage());
    }

    @Override
    public ExerciseRecord getByExercisesAndStudent(Integer exerciseId, Integer studentId) {
        ExerciseRecord exerciseRecord = exerciseRecordDao.findByExercisesAndStudent(exerciseId, studentId);
        if (exerciseRecord == null) {
            return new ExerciseRecord();
        }
        return exerciseRecord;
    }

    @Override
    public PageData<OptionVO> getLatestListPage(Integer userId, Integer page, Integer limit) {

        Pageable pageable = PageRequest.of(page - 1, limit);
        Page<ExerciseRecord> pageData = exerciseRecordDao.findLatestRecord(userId, pageable);

        List<ExerciseRecord> list = pageData.getContent();
        if (CollectionUtils.isEmpty(list)) {
            throw new NoDataException("page");
        }
        List<OptionVO> voList = new ArrayList<>(list.size());
        for (ExerciseRecord exerciseRecord : list) {
            if (exerciseRecord.getExercise().getDocumentation() != null) {
                Documentation documentation = documentationService.get(exerciseRecord.getExercise().getDocumentation());
                String key = documentation.getCourse().getName() + "\t" + documentation.getName();
                OptionVO vo = new OptionVO(documentation.getId(), list.indexOf(exerciseRecord) + 1, key, "");
                voList.add(vo);
            }
            if (exerciseRecord.getExercise().getVideoClass() != null) {
                VideoClass videoClass = videoClassService.get(exerciseRecord.getExercise().getVideoClass());
                String key = videoClass.getCourse().getName() + "\t" + videoClass.getName();
                BigDecimal value = studyProgressService.getStudyProgress(videoClass.getId(), userId, CourseEnum.VIDEO).getProgress();
                OptionVO vo = new OptionVO(videoClass.getId(), list.indexOf(exerciseRecord) + 1, key, value.toString());
                voList.add(vo);

            }
        }
        return
                new PageData((int) pageData.getTotalElements(), limit, page, voList);

    }

    @Override
    public Integer countRightStudentAndCourse(Integer userId, Integer courseId) {
        Long count = exerciseRecordDao.countRightStudentAndCourse(userId, courseId);
        return count == null ? 0 : count.intValue();
    }

}

