package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.dao.FileInfoDao;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.model.Certificate;
import com.gibs.peixunapi.model.CertificateStudent;
import com.gibs.peixunapi.model.FileInfo;
import com.gibs.peixunapi.model.Video;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.FileInfoService;
import com.gibs.peixunapi.service.VideoService;
import com.gibs.peixunapi.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.persistence.EntityNotFoundException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.gibs.peixunapi.enums.BaseEnum.*;
import static com.gibs.peixunapi.enums.ResultEnum.*;


@Service
@Slf4j
public class FileInfoServiceImpl implements FileInfoService {

    @Autowired
    private FileInfoDao fileInfoDao;
    @Autowired
    private VideoService videoService;
    @Autowired
    private FileInfoService fileInfoService;

    //上传文件的根目录
    String uploadPath = UPLOAD_PATH.getMsg();

    final static String FILE_EXT = "jpeg,pdf,jpg,png,docx,doc,xlsx,xls,mp4,jar";

    @Override
    public FileInfo get(Integer id) {
        return fileInfoDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public String getDocumentPicture() {
        FileInfo fileInfo = fileInfoDao.findDocumentPicture(CourseEnum.DOCUMENT.getMessage());

        return fileInfo == null ? "" : fileInfo.getUrl();
    }

    @Override
    public Result uploadFile(MultipartFile file) throws Exception {
        String fileName = file.getOriginalFilename();
        String ext = Objects.requireNonNull(fileName).substring(fileName.lastIndexOf(".") + 1);
        FileInfo fileInfo = new FileInfo();
        if (FILE_EXT.contains(ext)) {
            String filePath = UploadUtils.uploadCustom(file);

            fileInfo.setName(fileName);
            fileInfo.setUrl(filePath);
            fileInfo.setType(ext);
        } else {
            return ResultUtil.fail(FILE_TYPE_ERROR);
        }
        try {
            fileInfo = fileInfoDao.save(fileInfo);

            HashMap<String, String> map = new HashMap<>(5);
            map.put("path", fileInfo.getPath());
            map.put("name", fileInfo.getName());
            map.put("id", String.valueOf(fileInfo.getId()));
            map.put("filePath", fileInfo.getUrl());

            return ResultUtil.success(UPDATE_SUCCESS, map);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(UPDATE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result uploadFileMulti(MultipartFile[] files) throws Exception {

        List<FileInfo> fileInfos = new ArrayList<>();
        for (MultipartFile file : files) {
            String fileName = file.getOriginalFilename();
            String ext = Objects.requireNonNull(fileName).substring(fileName.lastIndexOf(".") + 1);
            if (FILE_EXT.contains(ext)) {
                String filePath = UploadUtils.uploadCustom(file);
                FileInfo fileInfo = new FileInfo();
                fileInfo.setName(fileName);
                fileInfo.setUrl(filePath);
                fileInfo.setType(ext);
                fileInfos.add(fileInfo);
            } else {
                return ResultUtil.fail(FILE_TYPE_ERROR);
            }
        }
        fileInfos = fileInfoDao.saveAll(fileInfos);

        List<HashMap<String, String>> data = new ArrayList<>();
        for (FileInfo fileInfo : fileInfos) {
            HashMap<String, String> map = new HashMap<>(5);
            map.put("path", fileInfo.getPath());
            map.put("name", fileInfo.getName());
            map.put("id", String.valueOf(fileInfo.getId()));
            map.put("filePath", fileInfo.getUrl());
            data.add(map);
        }
        return ResultUtil.success(UPDATE_SUCCESS, data);
    }

    @Override
    public FileInfo videoPic(File video) {
        FileInfo fileInfo = new FileInfo();
        String filePath = VideoUtil.getVideoPic(video);
        fileInfo.setName(UUIDUtil.get32UUID());
        fileInfo.setUrl(filePath);
        fileInfo.setType("jpg");
        try {
            fileInfo = fileInfoDao.save(fileInfo);

            return fileInfo;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }


    /**
     * 根据文件md5得到文件路径
     * 规则：
     * 一级目录：md5的第一个字符
     * 二级目录：md5的第二个字符
     * 三级目录：md5
     * 文件名：md5+文件扩展名
     *
     * @param fileMd5 文件md5值
     * @return 文件路径
     */


    @Override
    public Result checkFile(String fileMd5, String fileName, String fileExt) {

        //   1. 获取文件的路径
        String filePath = ChunkUploadUtil.getFilePath(fileMd5, fileExt);
        File file = new File(filePath);

        //   2. 查询数据库文件是否存在
        FileInfo data = fileInfoDao.findByMd5(fileMd5);

        //   文件存在直接返回
        if (file.exists() && data != null && data.getName().replace(".mp4","").equals(fileName)) {
            return ResultUtil.success(SUCCESS,data.getId());
            //            throw new BaseException(UPLOAD_FILE_EXIST);
        }
        //下面说明文件并未上传
        boolean fileFold = ChunkUploadUtil.createFileFold(fileMd5);
        if (!fileFold) {
            //上传文件目录创建失败
            throw new BaseException(UPLOAD_PATH_CREATE_FAIL);
        }
        return ResultUtil.success(SUCCESS);
    }


    @Override
    public Boolean checkChunk(String fileMd5, Integer chunk, Integer chunkSize) {
        //得到块文件所在路径
        String chunkFileFolderPath = ChunkUploadUtil.getChunkFolderPath(fileMd5);
        //块文件的文件名称以1,2,3..序号命名，没有扩展名
        System.out.println(chunkFileFolderPath + chunk);
        File chunkFile = new File(chunkFileFolderPath + chunk);
        return chunkFile.exists();
    }

    @Override
    public String uploadChunk(MultipartFile file, String fileMd5, Integer chunk) {
        if (file == null) {
            throw new BaseException(UPLOAD_FILE_ISNULL);
        }
        //创建块文件目录
        return ChunkUploadUtil.uploadChunk(file, fileMd5, chunk);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HashMap<String, String> mergeChunks(String fileMd5, String fileName, Long fileSize, String fileExt) {
        fileExt = fileExt.replace(".", "");
        //获取块文件的路径
        String chunkFileFolderPath = ChunkUploadUtil.getChunkFolderPath(fileMd5);
        File chunkFileFolder = new File(chunkFileFolderPath);
        if (!chunkFileFolder.exists()) {
            throw new BaseException(UPLOAD_FILE_ISNULL);
        }
        //获取块文件，此列表是已经排好序的列表
        File[] chunkFiles = ChunkUploadUtil.getChunkFiles(chunkFileFolder);
        //合并文件
        File file = ChunkUploadUtil.mergeFile(fileMd5, fileExt, chunkFiles);



        //将文件信息保存到数据库
        FileInfo fileInfo = new FileInfo();
        fileInfo.setName(fileName);
        fileInfo.setUrl(ChunkUploadUtil.getFileRelativePath(fileMd5, fileExt));
        fileInfo.setType(fileExt);
        fileInfo.setFileMd5(fileMd5);
        try {
            fileInfo = fileInfoDao.save(fileInfo);

            String time = VideoUtil.getVideoTime(file);

            FileInfo coverImage = fileInfoService.videoPic(file);

            HashMap<String, String> map = new HashMap<>(5);

            if (fileExt.matches(".*mp4|.*flv|.*MP4|.*FLV")) {
                Video video = new Video();
                video.setCoverImage(coverImage);
                video.setUrl(fileInfo);
                video.setName(fileInfo.getName());
                video.setDuration(Integer.parseInt(Objects.requireNonNull(time)));

                video = videoService.save(video);
                map.put("path", fileInfo.getPath());
                map.put("name", video.getName());
                map.put("id", String.valueOf(video.getId()));
                map.put("filePath", fileInfo.getUrl());
            } else {
                map.put("path", fileInfo.getPath());
                map.put("name", file.getName());
                map.put("id", String.valueOf(fileInfo.getId()));
                map.put("filePath", fileInfo.getUrl());
            }
            return map;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addWatermark(List<CertificateStudent> list, Certificate certificate, String name, String content, String foot) {


        File file = new File(UPLOAD_PATH.getMsg() + certificate.getFileInfo().getPath());
        try {
            for (CertificateStudent certificateStudent : list) {

                //保存文件
                BufferedImage outputStream = GraphicsUtil.MarkImageByText(file, null, certificateStudent.getExamStudent().getStudent().getUsername(), content, foot, DateUtils.toDateZNString(new Date()), certificateStudent.getUniqueCode(), certificateStudent.getName());

                String fileName = certificateStudent.getUniqueCode() +
                        "." + certificate.getFileInfo().getType();

                File folder = new File(CERTIFICATE_FILE_PATH.getMsg());
                if(!folder.exists()) {
                    folder.mkdirs();
                }
                File certificateFile = new File(CERTIFICATE_FILE_PATH.getMsg(), fileName);
                ImageIO.write(outputStream, certificate.getFileInfo().getType(), certificateFile);

                //fileInfo
                FileInfo fileInfo = new FileInfo();
                fileInfo.setFileMd5(fileName);
                fileInfo.setName(certificateStudent.getExamStudent().getStudent().getUsername() + "-" + certificate.getName());
                fileInfo.setUrl("/certificate/" + fileName);
                fileInfo.setType(certificate.getFileInfo().getType());
                fileInfoDao.save(fileInfo);
            }
        } catch (IOException e) {
            throw new SaveException(ResultEnum.IO_ERROR, e);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }


}

