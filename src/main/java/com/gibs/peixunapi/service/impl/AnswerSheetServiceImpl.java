package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.VO.show.AnswerPaperVO;
import com.gibs.peixunapi.VO.show.AnswerSheetVO;
import com.gibs.peixunapi.VO.show.ShowTestPaperVO;
import com.gibs.peixunapi.dao.AnswerSheetDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.gibs.peixunapi.enums.ResultEnum.*;
import static com.gibs.peixunapi.enums.SubjectEnum.*;


@Slf4j
@Service
public class AnswerSheetServiceImpl implements AnswerSheetService {

    @Autowired
    private AnswerSheetDao answerSheetDao;
    @Autowired
    private ExamStudentService examStudentService;
    @Autowired
    private TestPaperSubjectService testPaperSubjectService;
    @Autowired
    private UserService userService;
    @Autowired
    private AnswerSheetService answerSheetService;
    @Autowired
    private SubOptionService subOptionService;
    @Autowired
    private TestPaperService testPaperService;
    @Autowired
    private ExamClassInfoService examClassInfoService;


    @Override
    public AnswerSheet get(Integer id) {
        return answerSheetDao.findById(id).orElseThrow(EntityNotFoundException::new);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void fillTheAnswerPaper(String uniqueCode, Integer testPaperSubjectId, String studentAnswer) {

        ExamStudent examStudent = examStudentService.getByUniqueCode(uniqueCode);
        TestPaperSubject testPaperSubject = testPaperSubjectService.get(testPaperSubjectId);

        AnswerSheet answerSheet = answerSheetService.findByExamStudentAndTestPaperSubject(examStudent.getId(), testPaperSubject.getId());
        answerSheet.setExamStudent(examStudent);
        answerSheet.setTestPaperSubject(testPaperSubject);
        answerSheet.setMyAnswer(studentAnswer);

//       判断对错
        SubjectHub subjectHub = testPaperSubject.getSubjectHub();
        //排除主观题
//        if (!FILL.getCode().equals(subjectHub.getType()) || !QUESTION.getCode().equals(subjectHub.getType())) {
            if ( !QUESTION.getCode().equals(subjectHub.getType())) {

                String right = subjectHub.getAnswer();
                if (FILL.getCode().equals(subjectHub.getType())){
                    right = right.replaceAll("[,，]", "");
                }
            if (right.equals(studentAnswer)) {
                //判断题目类型分数
                if (CHECK.getCode().equals(subjectHub.getType())) {
                    answerSheet.setScore(testPaperSubject.getTestPaper().getCheckScore());
                } else if (RADIO.getCode().equals(subjectHub.getType())) {
                    answerSheet.setScore(testPaperSubject.getTestPaper().getRadioScore());
                } else if (JUDGE.getCode().equals(subjectHub.getType())) {
                    answerSheet.setScore(testPaperSubject.getTestPaper().getJudgeScore());
                } else if (FILL.getCode().equals(subjectHub.getType())) {
                    answerSheet.setScore(testPaperSubject.getTestPaper().getJudgeScore());
                }
            }
        }

        try {
            answerSheetDao.save(answerSheet);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public Result haveNextPaper(Integer classId) {
        //1获取未批改
        ExamStudent examStudent = examStudentService.getCorrect(classId);
        if (examStudent == null) {
            return ResultUtil.success(ALL_TEST_PAPER_CORRECT);
        }
        return ResultUtil.success(SUCCESS);
    }

    @Override
    public Result getAnswerPaper(Integer classId) {
        //1获取未批改
        ExamStudent examStudent = examStudentService.getCorrect(classId);
        if (examStudent == null) {
            return ResultUtil.success(ALL_TEST_PAPER_CORRECT);
        }
        //2设置VO
        AnswerPaperVO answerPaperVO = new AnswerPaperVO();
        answerPaperVO.setUniqueCode(examStudent.getUniqueCode());
        answerPaperVO.setUsername(examStudent.getStudent().getUsername());
        ExamClassInfo classInfo = examClassInfoService.get(classId);
        TestPaper testPaper = classInfo.getExamInfo().getTestPaper();
        answerPaperVO.setTotalScore(testPaper.getTotalScore());
        //3获取题目
        List<TestPaperSubject> testPaperSubjectList = testPaperSubjectService.getTestPaperSubject(testPaper.getId());

        if (Collections.isEmpty(testPaperSubjectList)) {

            return ResultUtil.success(EMPTY_DATA, answerPaperVO);
        }
        for (TestPaperSubject testPaperSubject : testPaperSubjectList) {
            AnswerSheet answerSheet = answerSheetDao.findByExamStudentAndTestPaperSubject(examStudent.getId(), testPaperSubject.getId());
            if (answerSheet == null) {
                answerSheet = new AnswerSheet();
            }
            AnswerSheetVO sheetVO = new AnswerSheetVO();
            ConverterUtil.copyInSpecifyProperties(answerSheet, sheetVO, "id", "myAnswer");
            //复制题目
            SubjectHub subjectHub = testPaperSubject.getSubjectHub();
            SubjectHubVO subjectHubVO = new SubjectHubVO();
            ConverterUtil.copyProperties(subjectHub, subjectHubVO);
            subjectHubVO.setTestPaperSubjectHubId(testPaperSubject.getId());

            //复制选项
            subjectHubVO.setOptionVOList(subOptionService.getSubOptions(subjectHub.getId()));
            sheetVO.setSubjectHubVO(subjectHubVO);
            sheetVO.getSubjectHubVO().setSort(testPaperSubject.getSort());
            BigDecimal zero = new BigDecimal(0);
            if (RADIO.getCode().equals(subjectHub.getType())) {
                subjectHubVO.setScore(testPaperSubject.getTestPaper().getRadioScore());
                sheetVO.setScore(answerSheet.getScore());
                answerPaperVO.getRadioList().add(sheetVO);
                answerPaperVO.setRadioTotalScore(multiply(testPaper.getRadioScore() == null ? zero : testPaper.getRadioScore(), testPaper.getRadioCount()));
                if ((answerSheet.getScore() == null ? zero : answerSheet.getScore()).compareTo(zero) > 0) {
                    answerPaperVO.setRadioScore(add(answerPaperVO.getRadioScore() == null ? zero : answerPaperVO.getRadioScore(), answerSheet.getScore()));
                }
            } else if (CHECK.getCode().equals(subjectHub.getType())) {
                subjectHubVO.setScore(testPaperSubject.getTestPaper().getCheckScore());
                sheetVO.setScore(answerSheet.getScore());
                answerPaperVO.getCheckList().add(sheetVO);
                answerPaperVO.setCheckTotalScore(multiply(testPaper.getCheckScore() == null ? zero : testPaper.getCheckScore(), testPaper.getCheckCount()));
                if ((answerSheet.getScore() == null ? zero : answerSheet.getScore()).compareTo(zero) > 0) {
                    answerPaperVO.setCheckScore(add(answerPaperVO.getCheckScore() == null ? zero : answerPaperVO.getCheckScore(), answerSheet.getScore()));
                }
            } else if (JUDGE.getCode().equals(subjectHub.getType())) {
                subjectHubVO.setScore(testPaperSubject.getTestPaper().getJudgeScore());
                sheetVO.setScore(answerSheet.getScore());
                answerPaperVO.getJudgeList().add(sheetVO);
                answerPaperVO.setJudgeTotalScore(multiply(testPaper.getJudgeScore() == null ? zero : testPaper.getJudgeScore(), testPaper.getJudgeCount()));
                if ((answerSheet.getScore() == null ? zero : answerSheet.getScore()).compareTo(zero) > 0) {
                    answerPaperVO.setJudgeScore(add(answerPaperVO.getJudgeScore() == null ? zero : answerPaperVO.getJudgeScore(), answerSheet.getScore()));
                }
            } else if (FILL.getCode().equals(subjectHub.getType())) {
                subjectHubVO.setScore(testPaperSubject.getTestPaper().getFillScore());
                sheetVO.setScore(answerSheet.getScore());
                answerPaperVO.getFillList().add(sheetVO);
                answerPaperVO.setFillTotalScore(multiply(testPaper.getFillScore() == null ? zero : testPaper.getFillScore(), testPaper.getFillCount()));
                if ((answerSheet.getScore() == null ? zero : answerSheet.getScore()).compareTo(zero) > 0) {
                    answerPaperVO.setFillScore(add(answerPaperVO.getFillScore() == null ? zero : answerPaperVO.getFillScore(), answerSheet.getScore()));
                }
            } else if (QUESTION.getCode().equals(subjectHub.getType())) {
                subjectHubVO.setScore(testPaperSubject.getTestPaper().getQuestionScore());
                sheetVO.setScore(answerSheet.getScore());
                answerPaperVO.getQuestionList().add(sheetVO);
                answerPaperVO.setQuestionTotalScore(multiply(testPaper.getQuestionScore() == null ? zero : testPaper.getQuestionScore(), testPaper.getQuestionCount()));
                if ((answerSheet.getScore() == null ? zero : answerSheet.getScore()).compareTo(zero) > 0) {
                    answerPaperVO.setQuestionScore(add(answerPaperVO.getQuestionScore() == null ? zero : answerPaperVO.getQuestionScore(), answerSheet.getScore()));
                }

            }
        }
        return ResultUtil.success(FIND_SUCCESS, answerPaperVO);
    }

    /**
     * 乘法运算
     */
    private BigDecimal multiply(BigDecimal m1, Integer m2) {
        BigDecimal p2 = new BigDecimal(m2);
        return m1.multiply(p2);
    }

    /**
     * 乘法运算
     */
    private BigDecimal add(BigDecimal m1, BigDecimal m2) {
        return m1.add(m2);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void correct(Integer answerSheetId, BigDecimal score, Integer markManId) {
        AnswerSheet answerSheet = get(answerSheetId);

            User markMan = userService.get(markManId);
            answerSheet.setScore(score);
            answerSheet.setMarkMan(markMan);
            answerSheet.setMarkTime(new Date());
            try {
                answerSheetDao.save(answerSheet);
            } catch (Exception e) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                throw new SaveException(ResultEnum.SAVE_FAIL, e);
            }
    }

    @Override
    public BigDecimal calculateScore(Integer examStudentId) {

        List<Double> scoreList = answerSheetDao.findScoreByExamStudent(examStudentId);
        if (Collections.isEmpty(scoreList)) {
            throw new EntityNotFoundException();
        }
        BigDecimal result = new BigDecimal(0);
        for (Double d : scoreList) {
            result = result.add(new BigDecimal(d));
        }
        return result;
    }

    @Override
    public BigDecimal getStudentProgress(Integer examStudentId, ExamInfo examInfo) {
        Long finishTopic = answerSheetDao.countTopic(examStudentId);
        TestPaper testPaper = examInfo.getTestPaper();
        testPaper.setRadioCount(testPaper.getRadioCount() == null ? 0 : testPaper.getRadioCount());
        testPaper.setCheckCount(testPaper.getCheckCount() == null ? 0 : testPaper.getCheckCount());
        testPaper.setFillCount(testPaper.getFillCount() == null ? 0 : testPaper.getFillCount());
        testPaper.setJudgeCount(testPaper.getJudgeCount() == null ? 0 : testPaper.getJudgeCount());
        testPaper.setQuestionCount(testPaper.getQuestionCount() == null ? 0 : testPaper.getQuestionCount());
        Integer all = testPaper.getRadioCount() + testPaper.getCheckCount() + testPaper.getFillCount() + testPaper.getJudgeCount() + testPaper.getQuestionCount();

        return all == 0 ? BigDecimal.valueOf(0d) : BigDecimal.valueOf(finishTopic).divide(BigDecimal.valueOf(all));
    }


    @Override
    public AnswerSheet findByExamStudentAndTestPaperSubject(Integer examStudentId, Integer testPaperSubjectId) {
        AnswerSheet answerSheet = answerSheetDao.findByExamStudentAndTestPaperSubject(examStudentId, testPaperSubjectId);
        if (answerSheet == null) {
            return new AnswerSheet();
        }
        return answerSheet;
    }

    @Override
    public ShowTestPaperVO watchAnswerPaper(String uniqueCode) {
        ExamStudent examStudent = examStudentService.getByUniqueCode(uniqueCode);
        TestPaper testPaperId = testPaperService.getTestPaperIdByUniqueCode(uniqueCode);
        Map<String, List<SubjectHubVO>> map = testPaperSubjectService.getMapByTestPaper(testPaperId.getId());
        for (Map.Entry<String, List<SubjectHubVO>> stringListEntry : map.entrySet()) {
            List<SubjectHubVO> list = stringListEntry.getValue();
            for (SubjectHubVO subjectHubVO : list) {
                AnswerSheet answerSheet = answerSheetDao.findByExamStudentAndTestPaperSubject(examStudent.getId(), subjectHubVO.getTestPaperSubjectHubId());
                if (answerSheet != null) {
                    subjectHubVO.setMyAnswer(answerSheet.getMyAnswer());
                }
            }
        }
        ShowTestPaperVO data = testPaperService.getVOList(testPaperId, map);
        data.setUsername(examStudent.getStudent().getUsername());
        return data;
    }
}

