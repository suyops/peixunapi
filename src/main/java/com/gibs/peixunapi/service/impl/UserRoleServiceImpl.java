package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.show.RoleVO;
import com.gibs.peixunapi.dao.RoleDao;
import com.gibs.peixunapi.dao.UserRoleDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.mapper.UserRoleMapper;
import com.gibs.peixunapi.model.Role;
import com.gibs.peixunapi.model.UserRole;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.RoleService;
import com.gibs.peixunapi.service.UserRoleService;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.ResultEnum.CREATE_SUCCESS;

@Service
@Slf4j
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleDao userRoleDao;
    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private RoleService roleService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result create(Integer userId, Integer roleId) {

        UserRole userAndRole = new UserRole();
        userAndRole.setRoleId(roleId);
        userAndRole.setUserId(userId);
        try {
            userRoleDao.save(userAndRole);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.CREATE_FAIL, e);
        }
        return ResultUtil.success(CREATE_SUCCESS);
    }


    @Override
    public List<RoleVO> getRoleVOListByUserId(Integer userId) {
        List<Integer> roleIdList = userRoleDao.findRoleIdByUserId(userId);
        if (Collections.isEmpty(roleIdList)) {
//            throw new NoDataException();
            return new ArrayList<>();
        }
        List<Role> roleList = roleDao.findByIdIn(roleIdList);
        List<RoleVO> voList = new ArrayList<>(roleIdList.size());
        for (Role role : roleList) {
            RoleVO roleVO = new RoleVO();
            ConverterUtil.copyProperties(role, roleVO, "sort");
            roleVO.setSort(roleList.indexOf(role) + 1);
            voList.add(roleVO);
        }
        return voList;
    }

    @Override
    public String getRoleNameByUserId(Integer userId) {
        List<Integer> roleIdList = userRoleDao.findRoleIdByUserId(userId);
        if (Collections.isEmpty(roleIdList)) {
            return "";
        }
        List<Role> roleList = roleDao.findByIdIn(roleIdList);
        return roleList.stream().map(Role::getName).collect(Collectors.joining(","));
    }

    @Override
    public List<Integer> getUserIdList(String name, Integer businessId, Integer audit, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<Integer> pageData = new PageInfo<>(userRoleMapper.findByBusinessIdAndNameLike(name, audit, businessId));
        List<Integer> list = pageData.getList();
        if (Collections.isEmpty(list)) {
            throw new NoDataException("page");
        }
        return list;
    }

    @Override
    public PageInfo<Integer> getUserIdList(String name, String code, Integer businessId, Integer status, Integer audit, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);

        PageInfo<Integer> pageData = new PageInfo<>(userRoleMapper.findUserIdsFour(code, name, businessId, status, audit));
        List<Integer> list = pageData.getList();
        if (Collections.isEmpty(list)) {
            throw new NoDataException("page");
        }
        return pageData;
    }
    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void deleteAll(Integer userId) {
        try {
            userRoleDao.deleteByUser(userId);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);

        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void createBatch(List<Integer> roleIdList, Integer userId) {
        List<UserRole> list = new ArrayList<>(roleIdList.size());
        for (Integer i : roleIdList) {
            UserRole userRole = new UserRole();
            userRole.setRoleId(i);
            userRole.setUserId(userId);
            list.add(userRole);
        }
        try {
            userRoleDao.saveAll(list);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }


    @Override
    public List<Integer> getUserByRole(String code) {
        Integer roleId = roleService.getRoleIdByCode(code);
        if (roleId == null) {
            return null;
        }
        List<Integer> userIdList = userRoleDao.findUserByRoleId(roleId);
        return userIdList;
    }
}

