package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.DTO.TestPaperNumberScoreDTO;
import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.VO.post.RandomTestPaperVO;
import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.VO.show.ShowTestPaperListVO;
import com.gibs.peixunapi.VO.show.ShowTestPaperVO;
import com.gibs.peixunapi.dao.CourseTestPaperDao;
import com.gibs.peixunapi.dao.TestPaperDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.mapper.TestPaperMapper;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import com.gibs.peixunapi.utils.UUIDUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.ResultEnum.DATA_NOT_FIND;
import static com.gibs.peixunapi.enums.ResultEnum.FIND_SUCCESS;
import static com.gibs.peixunapi.enums.SubjectEnum.*;

@Service
@Slf4j
public class TestPaperServiceImpl implements TestPaperService {

    @Autowired
    private TestPaperDao testPaperDao;
    @Autowired
    private CourseTestPaperDao courseTestPaperDao;
    @Autowired
    private TestPaperMapper testPaperMapper;
    @Autowired
    private SubjectHubService subjectHubService;
    @Autowired
    private UserService userService;
    @Autowired
    private TestPaperSubjectService testPaperSubjectService;
    @Autowired
    private ExamClassInfoService examClassInfoService;
    @Autowired
    private ExamInfoService examInfoService;


    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public TestPaper copy(List<SubjectHubVO> subjectHubVOList, Integer testPaperId) {

        List<SubjectHub> subjectHubList = subjectHubVOList.stream().map(e -> {
            SubjectHub subjectHub = new SubjectHub();
            ConverterUtil.copyProperties(e, subjectHub);
            return subjectHub;
        }).collect(Collectors.toList());

        Map<String, Integer> subjectCountMap = new HashMap<>();
        /* 循环遍历转换为VO类 放入Map中返回结果*/
        for (SubjectHub subjectHub : subjectHubList) {
            /*如果下一个type不同那么结束收集此类型完毕*/
            if (subjectHub.getType() != null) {
//                if (subjectHubList.get(i).getType().equals(subjectHubList.get(i + 1).getType())) {
                if (!subjectCountMap.containsKey(subjectHub.getType())) {

                    subjectCountMap.put(subjectHub.getType(), 0);
                } else {
                    Integer num = subjectCountMap.get(subjectHub.getType());
                    subjectCountMap.put(subjectHub.getType(), num + 1);
                }
            }
        }
        //获取原有试卷题目信息
        TestPaper testPaper = testPaperDao.findById(testPaperId).orElseThrow(EntityNotFoundException::new);
        //设置新信息
        testPaper.setRadioCount(subjectCountMap.get("radio"));
        testPaper.setCheckCount(subjectCountMap.get("check"));
        testPaper.setJudgeCount(subjectCountMap.get("judge"));
        testPaper.setFillCount(subjectCountMap.get("fill"));
        testPaper.setQuestionCount(subjectCountMap.get("question"));
        testPaper.setId(null);
        User user = userService.getAttributeUser();
        testPaper.setCreatorId(user);
        try {
            TestPaper result = testPaperDao.save(testPaper);

            return result;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public TestPaper get(Integer id) {
        return testPaperDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Result getOptions(Integer courseId, String key) {
        List<OptionVO> voList = testPaperMapper.getOption(courseId, key);
        voList.forEach(optionVO -> optionVO.setSort(voList.indexOf(optionVO)));
        return ResultUtil.success(FIND_SUCCESS, voList);
    }


    @Override
    public ShowTestPaperVO getVOList(TestPaper testPaper, Map<String, List<SubjectHubVO>> subjectHubVOMap) {
        ShowTestPaperVO vo = new ShowTestPaperVO();
        ConverterUtil.copyProperties(testPaper, vo);
        for (Map.Entry<String, List<SubjectHubVO>> entry : subjectHubVOMap.entrySet()) {
            if ("radio".equals(entry.getKey())) {
                vo.setRadioList(entry.getValue());
                vo.setRadioTotalScore(vo.getRadioScore().multiply(BigDecimal.valueOf(vo.getRadioCount())));
            }
            if ("check".equals(entry.getKey())) {
                vo.setCheckList(entry.getValue());
                vo.setCheckTotalScore(vo.getCheckScore().multiply(BigDecimal.valueOf(vo.getCheckCount())));
            }
            if ("judge".equals(entry.getKey())) {
                vo.setJudgeList(entry.getValue());
                vo.setJudgeTotalScore(vo.getJudgeScore().multiply(BigDecimal.valueOf(vo.getJudgeCount())));
            }
            if ("fill".equals(entry.getKey())) {
                vo.setFillList(entry.getValue());
                vo.setFillTotalScore(vo.getFillScore().multiply(BigDecimal.valueOf(vo.getFillCount())));
            }
            if ("question".equals(entry.getKey())) {
                vo.setQuestionList(entry.getValue());
                vo.setQuestionTotalScore(vo.getQuestionScore().multiply(BigDecimal.valueOf(vo.getQuestionCount())));
            }
        }
        return vo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer randomTestPaper(RandomTestPaperVO randomVO) {
        List<TestPaperNumberScoreDTO> scoreDTOS = randomVO.getNumberScore();
        Integer radioCount;
        Integer checkCount;
        Integer judgeCount;
        Integer fillCount;
        Integer questionCount;

        TestPaper testPaper = new TestPaper();
        User user = userService.getAttributeUser();
//        Boolean isBigger = false;
        testPaper.setCreatorId(user);
        testPaper.setName(StringUtils.isBlank(randomVO.getName())
                ? LocalDateTime.now(ZoneOffset.of("+8")).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))
                : randomVO.getName());
        ExamClassInfo examClass = examClassInfoService.get(randomVO.getClassId());
        //统计最大允许随机题目数量以及设定分值
        for (TestPaperNumberScoreDTO scoreDTO : scoreDTOS) {
            if (scoreDTO.getNumber() != null && scoreDTO.getNumber() > 0) {
                if (RADIO.getCode().equals(scoreDTO.getType())) {
                    radioCount = subjectHubService.countByCourseAndType(examClass.getExamInfo().getCourse().getId(), RADIO);
                    if (scoreDTO.getNumber() > radioCount) {
                        scoreDTO.setNumber(radioCount);
//                        isBigger = true;
                    }
                    testPaper.setRadioScore(scoreDTO.getScore());
                    testPaper.setRadioCount(scoreDTO.getNumber());
                }
                if (CHECK.getCode().equals(scoreDTO.getType())) {
                    checkCount = subjectHubService.countByCourseAndType(examClass.getExamInfo().getCourse().getId(), CHECK);
                    if (scoreDTO.getNumber() > checkCount) {
                        scoreDTO.setNumber(checkCount);
//                        isBigger = true;
                    }
                    testPaper.setCheckScore(scoreDTO.getScore());
                    testPaper.setCheckCount(scoreDTO.getNumber());
                }
                if (JUDGE.getCode().equals(scoreDTO.getType())) {
                    judgeCount = subjectHubService.countByCourseAndType(examClass.getExamInfo().getCourse().getId(), JUDGE);
                    if (scoreDTO.getNumber() > judgeCount) {
                        scoreDTO.setNumber(judgeCount);
//                        isBigger = true;
                    }
                    testPaper.setJudgeScore(scoreDTO.getScore());
                    testPaper.setJudgeCount(scoreDTO.getNumber());
                }
                if (FILL.getCode().equals(scoreDTO.getType())) {
                    fillCount = subjectHubService.countByCourseAndType(examClass.getExamInfo().getCourse().getId(), FILL);
                    if (scoreDTO.getNumber() > fillCount) {
                        scoreDTO.setNumber(fillCount);
//                        isBigger = true;
                    }
                    testPaper.setFillScore(scoreDTO.getScore());
                    testPaper.setFillCount(scoreDTO.getNumber());
                }
                if (QUESTION.getCode().equals(scoreDTO.getType())) {
                    questionCount = subjectHubService.countByCourseAndType(examClass.getExamInfo().getCourse().getId(), QUESTION);
                    if (scoreDTO.getNumber() > questionCount) {
                        scoreDTO.setNumber(questionCount);
//                        isBigger = true;
                    }
                    testPaper.setQuestionScore(scoreDTO.getScore());
                    testPaper.setQuestionCount(scoreDTO.getNumber());
                }
            }
        }

        try {
            //保存试卷
            int score;
            score = (testPaper.getRadioCount()*testPaper.getRadioScore().intValue())+
                    (testPaper.getCheckCount()*testPaper.getCheckScore().intValue())+
                    (testPaper.getJudgeCount()*testPaper.getJudgeScore().intValue())+
                    (testPaper.getFillCount()*testPaper.getFillScore().intValue())+
                    (testPaper.getQuestionCount()*testPaper.getQuestionScore().intValue());
            testPaper.setTotalScore(BigDecimal.valueOf(score));

            testPaper = testPaperDao.save(testPaper);
            examClass.getExamInfo().setTotalScore(BigDecimal.valueOf(score));
            examInfoService.save(examClass.getExamInfo());
            //绑定试卷
            examInfoService.bindTestPaper(examClass.getExamInfo().getId(), testPaper.getId());
//            CourseTestPaper courseTestPaper = new CourseTestPaper();
//            courseTestPaper.setCourseId(examClass.getExamInfo().getCourse().getId());
//            courseTestPaper.setTestPaperId(testPaper.getId());
//            courseTestPaperDao.save(courseTestPaper);
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }

        try {
            //随机生成试题
            testPaperSubjectService.randomSubject(testPaper, examClass.getExamInfo().getCourse().getId(), user.getId());
                return testPaper.getId();
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            if (!(e instanceof BaseException)) {
                throw new BaseException(ResultEnum.RANDOM_ERROR, e);
            }
        }
        return null;
    }

    @Override
    public TestPaper getTestPaperIdByUniqueCode(String uniqueCode) {
        TestPaper testPaper = testPaperMapper.findTestPaperIdByUniqueCode(uniqueCode);
        if (testPaper == null) {
            throw new BaseException(DATA_NOT_FIND);
        }
        return testPaper;
    }

    @Override
    public PageData<ShowTestPaperListVO> getTestPaperList(Integer courseId, String key, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<ShowTestPaperListVO> pageData = new PageInfo<>(testPaperMapper.getTestPaperList(courseId, key));
        List<ShowTestPaperListVO> list = pageData.getList();
        if (CollectionUtils.isEmpty(list)) {
            throw new NoDataException("page");
        }
        for (ShowTestPaperListVO vo : list) {
            vo.setSort((list.indexOf(vo) + 1) * (page - 1) * limit);
        }

        return new PageData<>((int) pageData.getTotal(), limit, page, list);
    }

    @Override
    public Integer create(Integer courseId, String name, BigDecimal radioScore, BigDecimal checkScore,
                          BigDecimal judgeScore, BigDecimal fillScore, BigDecimal questionScore) {
        User user = userService.getAttributeUser();
        TestPaper testPaper = new TestPaper();
        testPaper.setName(name == null ? UUIDUtil.get32NumberUUID().toString() : name);
        BigDecimal total = radioScore.add(checkScore).add(judgeScore).add(fillScore).add(questionScore);
        testPaper.setTotalScore(total);
        testPaper.setRadioScore(radioScore);
        testPaper.setCheckScore(checkScore);
        testPaper.setJudgeScore(judgeScore);
        testPaper.setFillScore(fillScore);
        testPaper.setQuestionScore(questionScore);
        testPaper.setCreatorId(user);

        try {
            testPaper = testPaperDao.save(testPaper);
            CourseTestPaper courseTestPaper = new CourseTestPaper();
            courseTestPaper.setCourseId(courseId);
            courseTestPaper.setTestPaperId(testPaper.getId());
            courseTestPaperDao.save(courseTestPaper);
            return testPaper.getId();
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveCount(TestPaper testPaper) {
        try {
            testPaperDao.save(testPaper);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }

    }
}

