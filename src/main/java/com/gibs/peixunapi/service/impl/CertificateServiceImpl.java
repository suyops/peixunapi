package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.post.CertificateVO;
import com.gibs.peixunapi.VO.post.IssueCertificateVO;
import com.gibs.peixunapi.VO.show.*;
import com.gibs.peixunapi.dao.CertificateClassDao;
import com.gibs.peixunapi.dao.CertificateDao;
import com.gibs.peixunapi.dao.CertificateStudentDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.mapper.CertificateMapper;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.DateUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.EntityNotFoundException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.LinkEnum.*;
import static com.gibs.peixunapi.enums.ResultEnum.CERTIFICATE_NOT_EXIST;
import static com.gibs.peixunapi.enums.ResultEnum.SAVE_FAIL;

@Service
@Slf4j
public class CertificateServiceImpl implements CertificateService {

    @Autowired
    private CertificateDao certificateDao;
    @Autowired
    private CertificateClassDao certificateClassDao;
    @Autowired
    private CertificateStudentDao certificateStudentDao;
    @Autowired
    private CertificateService certificateService;
    @Autowired
    private CertificateMapper certificateMapper;
    @Autowired
    private FileInfoService fileInfoService;
    @Autowired
    private ExamStudentService examStudentService;
    @Autowired
    private UserService userService;
    @Autowired
    private NoticeService noticeService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void create(CertificateVO vo) {
        Certificate certificate = new Certificate();
        ConverterUtil.copyProperties(vo, certificate, "fileInfo");
        User user = userService.getAttributeUser();
        certificate.setCreator(user.getId());

        FileInfo fileInfo = fileInfoService.get(vo.getFileId());
        certificate.setFileInfo(fileInfo);
        try {
            certificateDao.save(certificate);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    @Override
    public void save(CertificateVO vo) {
        Certificate certificate = certificateService.get(vo.getId());
        ConverterUtil.copyProperties(vo, certificate, "fileInfo", "isDelete");
        if (!certificate.getFileInfo().getId().equals(vo.getFileId())) {
            FileInfo fileInfo = fileInfoService.get(vo.getFileId());
            certificate.setFileInfo(fileInfo);
        }

        try {
            certificateDao.save(certificate);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }




    @Override
    public Certificate get(Integer id) {
        return certificateDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }


    @Override
    public PageData getCertificateList(String name, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<CertificateListVO> pageData = new PageInfo<>(certificateMapper.getCertificateVOList(name));
        List<CertificateListVO> voList = pageData.getList();
        if (Collections.isEmpty(voList)) {
            throw new NoDataException("page");
        }
        voList.forEach(i -> i.setSort((voList.indexOf(i) + 1) + (page - 1) * 10));

        return new PageData<>((int) pageData.getTotal(), limit, page, voList);
    }

    @Override
    public ShowCertificateVO getCertificate(Integer id) {
        Certificate certificate = get(id);
        ShowCertificateVO vo = new ShowCertificateVO();
        ConverterUtil.copyProperties(certificate, vo);
        return vo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createCertificateClass(Integer classId) {
        // Integer number = examStudentService.countPassExamNumber(classId);
        CertificateClass certificateClass = new CertificateClass();
        certificateClass.setExamClassInfoId(classId);
        try {
            certificateClassDao.save(certificateClass);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer issueCertificate(IssueCertificateVO vo) {
        ExamClassInfo examClassInfo = null;
        LocalDateTime time = LocalDateTime.now();
        LocalDateTime expiryDate = time.plusDays(vo.getValidTime().longValue());
        if (vo.getList().isEmpty()) {
            return null;
        } else {
            List<CertificateStudent> list = new ArrayList<>();
            Certificate certificate = certificateService.get(vo.getCertificateId());
            for (String s : vo.getList()) {
                CertificateStudent certificateStudent = new CertificateStudent();
                certificateStudent.setCertificate(certificate);
                ExamStudent examStudent = examStudentService.getByUniqueCode(s);
                if (examClassInfo == null) {
                    examClassInfo = examStudent.getExamClassInfo();
                }
                certificateStudent.setExamStudent(examStudent);
                certificateStudent.setUserId(examStudent.getStudent().getId());
                certificateStudent.setExpiryDate(Date.from(expiryDate.atZone(ZoneId.systemDefault()).toInstant()));
                ConverterUtil.copyInSpecifyProperties(vo, certificateStudent, "validTime", "continueHours");
                certificateStudent.setName(certificate.getName());

                list.add(certificateStudent);
            }
            try {
                list = certificateStudentDao.saveAll(list);
                // 添加水印
                fileInfoService.addWatermark(list,certificate, certificate.getName(), vo.getContent(),"xxxxgongsi");
                /*消息通知*/
                String content = "您好。您于" +
                        DateUtils.toDateZNString(examClassInfo.getExamStartTime()) +
                        "参加的" +
                        examClassInfo.getName() +
                        "培训认证考试通过，管理员已于" +
                        DateUtils.toDateZNString(new Date()) +
                        "向您颁发" +
                        vo.getName() +
                        "证书，该证书有效期为" +
                        vo.getValidTime() +
                        "天。到期时间为" +
                        DateUtils.toDateZNString(Date.from(expiryDate.atZone(ZoneId.systemDefault()).toInstant())) +
                        "。如若中间证书被挂起，时效将暂停计算。";
                noticeService.sendNoticeToUser("证书颁发", content, CHECK_MY_CERTIFICATE.getLink(),
                        list.stream().map(CertificateStudent::getExamStudent).map(ExamStudent::getStudent).map(User::getId)
                                .collect(Collectors.toList()),
                        SYSTEM_CALL.getNum());
                return list.size();
            } catch (Exception e) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                throw new SaveException(ResultEnum.SAVE_FAIL, e);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void addIssueCount(Integer examClassId, Integer number) {

        CertificateClass certificateClass = certificateClassDao.findByClass(examClassId);
        if (certificateClass == null) {
            throw new BaseException(ResultEnum.DATA_NOT_FIND);
        }
        Integer add = number + certificateClass.getIssueCount();
        certificateClass.setIssueCount(add);
        try {
            certificateClassDao.save(certificateClass);

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }

    @Override
    public PageData getCertificateManageList(Integer expiry, Integer status, Integer pause, Integer comingExpiry, Integer businessId, String key, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<CertificateManageVO> pageData = new PageInfo<>(certificateMapper.getCertificateManageList(comingExpiry, new Date(), expiry, status, pause, businessId, key));
        List<CertificateManageVO> list = pageData.getList();
        if (Collections.isEmpty(list)) {
            throw new NoDataException("page");
        }
        for (CertificateManageVO vo : list) {
            vo.setSort((list.indexOf(vo) + 1) * limit * (page - 1));
            LocalDateTime time = vo.getExpiryDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            LocalDateTime now = LocalDateTime.now();
            vo.setRemainDay((int) Duration.between(now, time).toDays());
            vo.setValidTime(vo.getValidTime() + "天");
        }
        return new PageData((int) pageData.getTotal(), limit, page, list);
    }

    @Override
    public void checkCertificate(String uniqueCode) {
        CertificateStudent id = certificateStudentDao.findByUniqueCode(uniqueCode);
        if (id == null) {
            throw new BaseException(CERTIFICATE_NOT_EXIST);
        }
    }

    @Override
    public PersonCertificateVO getCertificateStudent(Integer id) {
        CertificateStudent certificateStudent = certificateStudentDao.findById(id).orElseThrow(EntityNotFoundException::new);
        if (certificateStudent == null) {
            throw new BaseException(CERTIFICATE_NOT_EXIST);
        }

        PersonCertificateVO vo = new PersonCertificateVO();
        ConverterUtil.copyProperties(certificateStudent, vo);
        vo.setCourseName(certificateStudent.getExamStudent().getExamClassInfo().getExamInfo().getCourse().getName());
        LocalDateTime deadline = vo.getExpiryDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        vo.setExpiryDay((int) Duration.between(LocalDateTime.now(), deadline).toDays());
        return vo;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void pause(String uniqueCode, Integer status) {
        CertificateStudent certificateStudent = certificateStudentDao.findByUniqueCode(uniqueCode);
        if (certificateStudent == null) {
            throw new BaseException(CERTIFICATE_NOT_EXIST);
        }
        certificateStudent.setPause(status);
        try {
            certificateStudentDao.save(certificateStudent);
            /*消息通知*/
            String content = "您好。您于" + DateUtils.toDateZNString(certificateStudent.getCreateTime()) +
                    "获得的" + certificateStudent.getName() +
                    "，证书编号为" +
                    certificateStudent.getUniqueCode() +
                    ":。该证书当前已经被" +
                    userService.getAttributeUser().getUsername() +
                    "管理员挂起，挂起期间，证书时效将暂停。待管理员恢复后继续计算证书时效。";
            noticeService.sendNoticeToUser("暂停证书", content, CHECK_MY_CERTIFICATE.getLink(),
                    certificateStudent.getExamStudent().getStudent().getId().toString(),
                    SYSTEM_CALL.getNum());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void revoke(String uniqueCode, Integer status) {
        CertificateStudent certificateStudent = certificateStudentDao.findByUniqueCode(uniqueCode);
        if (certificateStudent == null) {
            throw new BaseException(CERTIFICATE_NOT_EXIST);
        }
        certificateStudent.setStatus(status);
        try {
            certificateStudentDao.save(certificateStudent);
            /*消息通知*/
            String content = "您好。您于" + DateUtils.toDateZNString(certificateStudent.getCreateTime()) +
                    "获得的" + certificateStudent.getName() +
                    "，证书编号为" +
                    certificateStudent.getUniqueCode() +
                    ":。该证书当前已经被" +
                    userService.getAttributeUser().getUsername() +
                    "管理员吊销。了解详情请到本系统证书中心页面查询。";
            noticeService.sendNoticeToUser("证书吊销", content, CHECK_MY_CERTIFICATE.getLink(),
                    certificateStudent.getExamStudent().getStudent().getId().toString(),
                    SYSTEM_CALL.getNum());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL);
        }
    }

    @Override
    public List<OptionVO> getOptionList() {
        List<Certificate> list = certificateDao.findAll();
        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
        List<OptionVO> voList = new ArrayList<>(list.size());
        list.forEach(i -> {
            OptionVO vo = new OptionVO();
            ConverterUtil.copyProperties(i, vo);
            vo.setSort(list.indexOf(i) + 1);
            voList.add(vo);
        });
        return voList;
    }

    @Override
    public List<StudentCertificateVO> getStudentCertificate() {
        User user = userService.getAttributeUser();
        List<CertificateStudent> list = certificateStudentDao.findByUser(user.getId());
        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
        List<StudentCertificateVO> voList = new ArrayList<>(list.size());

        for (CertificateStudent certificateStudent : list) {
            StudentCertificateVO vo = new StudentCertificateVO();
            ConverterUtil.copyProperties(certificateStudent, vo);
            voList.add(vo);
        }
        return voList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void renewalBatch(List<String> uniqueCode) {
        List<CertificateStudent> list = new ArrayList<>(uniqueCode.size());
        Map<Integer, String> contentIdMap = new HashMap<>(list.size());

        for (String s : uniqueCode) {
            CertificateStudent certificateStudent = certificateStudentDao.findByUniqueCode(s);
            if (certificateStudent == null) {
                throw new BaseException(CERTIFICATE_NOT_EXIST);
            }
            Integer day = certificateStudent.getValidTime();
            LocalDateTime time = certificateStudent.getExpiryDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            LocalDateTime expiryDate = time.plusDays(day);
            certificateStudent.setExpiryDate(Date.from(expiryDate.atZone(ZoneId.systemDefault()).toInstant()));
            certificateStudent.setApplyStatus(0);
            list.add(certificateStudent);

            /*消息通知*/
            String content = "您好。您于" + certificateStudent.getCreateTime() + "获得的" +
                    certificateStudent.getName() + "，由于继续教育要求已经满足。该证书当前已经被" +
                    userService.getAttributeUser().getUsername() + "管理员续期，有效期延长" +
                    certificateStudent.getValidTime() + "天。到期时间为" + certificateStudent.getExpiryDate() + "。";
            contentIdMap.put(certificateStudent.getUserId(), content);
        }

        try {
            certificateStudentDao.saveAll(list);
            /*消息通知*/
            noticeService.sendNoticeListToUser("证书续期", contentIdMap, CHECK_MY_CERTIFICATE.getLink(), SYSTEM_CALL.getNum());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void applyRenewal(String uniqueCode) {
        CertificateStudent certificateStudent = certificateStudentDao.findByUniqueCode(uniqueCode);
        certificateStudent.setApplyStatus(1);

        try {
            certificateStudentDao.save(certificateStudent);

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL);
        }
        noticeService.sendNoticeToBusinessCount("申请证书续期", "", MANAGE_CERTIFICATE.getLink(),
                (certificateStudent.getExamStudent().getStudent().getBusiness().getId()),
                SYSTEM_CALL.getNum());
    }

    @Override
    public Boolean CertificateClassExist(Integer classId) {
        return certificateClassDao.countByClass(classId) > 0;
    }
}

