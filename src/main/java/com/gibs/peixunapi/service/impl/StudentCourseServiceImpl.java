package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.DTO.CourseStudyDTO;
import com.gibs.peixunapi.VO.show.AllowExamVO;
import com.gibs.peixunapi.VO.show.CourseStudyVO;
import com.gibs.peixunapi.VO.show.SignUpStudentListVO;
import com.gibs.peixunapi.VO.show.StudentSignUpTypeVO;
import com.gibs.peixunapi.dao.StudentCourseDao;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.mapper.StudentCourseMapper;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.CourseEnum.*;
import static com.gibs.peixunapi.enums.LinkEnum.STUDENT_COURSE_MANAGE;
import static com.gibs.peixunapi.enums.LinkEnum.SYSTEM_CALL;
import static com.gibs.peixunapi.enums.ResultEnum.*;

@Service
@Slf4j
public class StudentCourseServiceImpl implements StudentCourseService {

    @Autowired
    private StudentCourseDao studentCourseDao;
    @Autowired
    private UserService userService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private ExamStudentService examStudentService;
    @Autowired
    private StudentCourseMapper studentCourseMapper;
    @Autowired
    private ExercisesService exercisesService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private ExerciseRecordService exerciseRecordService;
    @Autowired
    private DocumentationService documentationService;
    @Autowired
    private VideoClassService videoClassService;
    @Autowired
    private StudyProgressService studyProgressService;
    @Autowired
    private NoticeService noticeService;
    @Autowired
    private StudentCourseService studentCourseService;

    @Override
    public List<StudentCourse> getStudentSubscribeList(Integer studentId, Boolean isSubscribe) {
        List<StudentCourse> list = studentCourseDao.findByStudentAndSubscribe(studentId);
        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
        return list;
    }

    @Override
    public List<StudentCourse> getStudentSignUpList(Integer studentId, Boolean isSignUp) {
        List<StudentCourse> list = studentCourseDao.findByStudentAndSignUp(studentId, isSignUp);
        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
        return list;
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public Result subscribeCourse(Integer studentId, Integer courseId) {
        //先检查是否已存在
        if (studentCourseDao.findByCourseIdAndStudentId(courseId, studentId) != null) {
            return ResultUtil.success(SUBSCRIBE_EXIST);
        }

        StudentCourse studentCourse = setStudentCourse(studentId, courseId);
        studentCourse.setStudyStatus(judgeStudyStatus(studentCourse));
        try {
            studentCourseDao.save(studentCourse);
            return ResultUtil.success(SUBSCRIBE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(DELETE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void subscribeCourse(List<Integer> studentIds, Integer courseId) {
        Integer status = null;
        List<StudentCourse> list = new ArrayList<>();
        for (Integer studentId : studentIds) {
            StudentCourse studentCourse = setStudentCourse(studentId, courseId);
            if (status == null) {
                status = judgeStudyStatus(studentCourse);
            } else {
                studentCourse.setStudyStatus(status);
            }
            list.add(studentCourse);
        }
        try {
            studentCourseDao.saveAll(list);

            /*消息通知*/
            String content = "企业管理员帮您订阅了培训:" + courseService.get(courseId).getName() + "。如若需要了解培训详情，请点击查看";
            noticeService.sendNoticeToUser("订阅通知", content,
                    STUDENT_COURSE_MANAGE.getLink(),
                    list.stream().map(StudentCourse::getStudent).map(User::getId).collect(Collectors.toList()),
                    SYSTEM_CALL.getNum());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(DELETE_FAIL, e);
        }
    }


    private StudentCourse setStudentCourse(Integer studentId, Integer courseId) {
        User student = userService.get(studentId);
        Course course = courseService.get(courseId);

        StudentCourse studentCourse = new StudentCourse();
        studentCourse.setCourse(course);
        studentCourse.setStudent(student);
        studentCourse.setSubscribe(true);
        Date date = new Date();
        studentCourse.setSubscribeTime(date);
        return studentCourse;
    }

    private Integer judgeStudyStatus(StudentCourse studentCourse) {
        LocalDateTime nowTime = LocalDateTime.now();
        LocalDateTime start = studentCourse.getCourse().getStartTime().atTime(LocalTime.MIDNIGHT) ;
        LocalDateTime end =studentCourse.getCourse().getEndTime().atTime(LocalTime.MIDNIGHT) ;
        if (nowTime.isBefore(start)) {
            return NOT_START.getCode();
        } else if (nowTime.isAfter(start) && nowTime.isBefore(end)) {
            return ON_STUDY.getCode();
        } else {
            throw new BaseException(UNKNOWN_ERROR);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public Result signUpCourse(Integer studentId, Integer courseId) {
        StudentCourse studentCourse = studentCourseDao.findByCourseIdAndStudentId(courseId, studentId);
        //是否已订阅
//        if ((studentCourse == null) || (!studentCourse.getSubscribe() && studentCourse.getSubscribeTime() == null)) {
//            return ResultUtil.fail(SUBSCRIBE_NOT_EXIST);
//        }
        //是否已报名
        if (studentCourse.getSignUp() && studentCourse.getSignUpTime() != null) {
            return ResultUtil.fail(SIGN_UP_EXIST);
        }

        studentCourse.setSubscribe(true);
        studentCourse.setSubscribeTime(new Date());
        studentCourse.setSignUp(true);
        studentCourse.setSignUpTime(new Date());
        studentCourse.setAllowExam(true);
        try {
            studentCourseDao.save(studentCourse);
            /*消息通知*/
//            noticeService.sendNoticeToUser("课程报名",
//                    "您好。您报名参加" + DateUtils.toDateZNString(studentCourse.getCourse().getStartTime()) +
//                            "开展的" + studentCourse.getCourse().getName() + "培训。当前课程报名成功，请等待本次培训开始后再进行相应课程学习。",
//                    COURSE_MANAGE.getLink(), studentId.toString(), SYSTEM_CALL.getNum());
//            if (studentCourse.getStudent().getBusiness().getManagerId() == null) {
//                throw new BaseException(NO_MANAGER);
//            }else{
//                noticeService.sendNoticeToUser("学员报名",
//                        "您好，当前已有学员报名参加" + DateUtils.toDateZNString(studentCourse.getCourse().getStartTime()) +
//                                "开展的" + studentCourse.getCourse().getName() + "培训。在管理中心订阅管理中可以查看以及管理培训报名名单。",
//                        COURSE_MANAGE.getLink(), studentCourse.getStudent().getBusiness().getManagerId().toString(), SYSTEM_CALL.getNum());
//            }
            return ResultUtil.success(OPERATING_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(OPERATING_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void signUpCourse(List<Integer> studentIds, Integer courseId) {
        for (Integer studentId : studentIds) {
            studentCourseService.subscribeCourse(studentId, courseId);
            studentCourseService.signUpCourse(studentId, courseId);
        }

    }


    @Override
    public Integer countFinishStudy(Integer courseId) {
        BigDecimal progress = BigDecimal.valueOf(99.9d);
        return studentCourseDao.countFinishStudyByCourseId(progress, courseId).intValue();
    }

    @Override
    public List<AllowExamVO> getAllowExamList(Integer department,Integer year, String key, Integer examId) {
        List<AllowExamVO> list;
        //拥有考试资格未参加过考试 TODO增加年份选择
        list = userService.getExamList(department,year,key,examId);
        //拥有考试资格未通过上次考试
//        list.addAll(userService.getNoPassExam(courseId, examClassId));
        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
        return list;
    }

    @Override
    public Integer countSignUpNumberByCourse(Integer courseId) {
        return studentCourseDao.countByCourse_IdAndSignUpIsTrue(courseId).intValue();
    }

    @Override
    public Integer countSignUpByCourseAndBusiness(Integer courseId, Integer business) {
        return studentCourseDao.countSignUpByCourseAndBusiness(courseId, business).intValue();
    }

    @Override
    public Integer countSubscribeNumberByCourse(Integer courseId) {
        return studentCourseDao.countByCourse_IdAndSubscribeIsTrue(courseId).intValue();
    }

    @Override
    public Integer countSubscribeByCourseAndBusiness(Integer courseId, Integer business) {
        return studentCourseDao.countSubscribeByCourseAndBusiness(courseId, business).intValue();
    }

    @Override
    public StudentCourse getByCourseAndStudent(Integer courseId, Integer studentId) {
        return studentCourseDao.findByCourseIdAndStudentId(courseId, studentId);
    }

    @Override
    public List<StudentSignUpTypeVO> getStudentListByCourseOrBusiness(Integer courseId, Integer signUp, Integer businessId) {
        List<StudentSignUpTypeVO> list = studentCourseMapper.findByCourseIdAndStudentId(courseId, signUp, businessId);
        if (CollectionUtils.isEmpty(list)) {
            throw new NoDataException();
        }
        for (StudentSignUpTypeVO vo : list) {
            vo.setSort(list.indexOf(vo) + 1);
        }
        return list;
    }

    @Override
    public PageData<CourseStudyVO> getCourseStudyList(Integer courseId, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<CourseStudyDTO> pageData = new PageInfo<>(studentCourseMapper.findCourseStudyList(courseId));
        List<CourseStudyDTO> list = pageData.getList();
        if (CollectionUtils.isEmpty(list)) {
            throw new NoDataException("page");
        }
        Integer all = exercisesService.countByCourse(courseId);
        List<CourseStudyVO> data = new ArrayList<>(list.size());
        for (CourseStudyDTO dto : list) {
            CourseStudyVO vo = new CourseStudyVO();
            ConverterUtil.copyProperties(dto, vo);
            vo.setSort((list.indexOf(dto) + 1) * (page - 1) * limit);
            BigDecimal rightCount = BigDecimal.valueOf(exerciseRecordService.countRightStudentAndCourse(dto.getUserId(), courseId));
            if (all == 0) {
                vo.setRightRate(BigDecimal.valueOf(0d));
            } else {
                vo.setRightRate(rightCount.divide(BigDecimal.valueOf(all)).multiply(BigDecimal.valueOf(100d)));
            }
            vo.setCountQuestion(commentService.questionCount(dto.getUserId(), courseId));
            vo.setExamScores(examStudentService.getLatestPassExamScore(dto.getUserId(), courseId));
            data.add(vo);
        }
        return new PageData<>((int) pageData.getTotal(), limit, page, data);
    }

    @Override
    public void isFinishCourse(Integer userId, Integer chapter, String type) {
        Course course = null;
        StudentCourse studentCourse = null;
        if (DOCUMENT.getMessage().equals(type)) {
            //获取本课程所有文档数量
            //获取已经学习的文档数量studyprogress数量
            Documentation documentation = documentationService.get(chapter);
            course = documentation.getCourse();
            studentCourse = studentCourseDao.findByCourseIdAndStudentId(course.getId(), userId);

            Integer documentCount = documentationService.countByCourse(course.getId());
//            if (new BigDecimal(documentCount).compareTo(new BigDecimal("100")) > -1) {
                studentCourse.setAllowExam(true);
//            }
        } else {
            VideoClass videoClass = videoClassService.get(chapter);
            course = videoClass.getCourse();
            studentCourse = studentCourseDao.findByCourseIdAndStudentId(course.getId(), userId);

            Integer videoClassCount = videoClassService.countByCourse(course.getId());
//            if (new BigDecimal(videoClassCount).compareTo(new BigDecimal("100")) > -1) {
                studentCourse.setAllowExam(true);
//            }
        }
        try {
            studentCourseDao.save(studentCourse);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(OPERATING_FAIL, e);
        }
    }

    @Override
    public List<SignUpStudentListVO> getSignUpList(Integer courseId) {
        List<SignUpStudentListVO> voList = studentCourseMapper.findSignUpList(courseId);
        if (CollectionUtils.isEmpty(voList)) {
            throw new NoDataException();
        }
        voList.forEach(i -> i.setSort(voList.indexOf(i) + 1));
        return voList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void updateCourseProgress(Integer chapterId, Integer userId, CourseEnum courseEnum) {
        StudentCourse studentCourse = null;
        if (DOCUMENT.equals(courseEnum)) {
            studentCourse = studentCourseDao.findByDocumentAndStudent(chapterId, userId);
            Integer all = documentationService.countByCourse(studentCourse.getCourse().getId());
            Integer read = studyProgressService.countFinishDocument(studentCourse.getCourse().getId(), userId);
            studentCourse.setDocumentationProgress(new BigDecimal((read.doubleValue() / all.doubleValue()) * 100d).setScale(2, BigDecimal.ROUND_DOWN));

        } else if (VIDEO.equals(courseEnum)) {
            studentCourse = studentCourseDao.findByVideoAndStudent(chapterId, userId);
            Integer all = videoClassService.countByCourse(studentCourse.getCourse().getId());
            List<Double> studyProgressList = studyProgressService.getVideoProgressByCourseAndStudent(studentCourse.getCourse().getId(), userId);
            Double progress = new Double("0");
            for (Double d : studyProgressList) {
                progress += d;
            }
            studentCourse.setVideoProgress(new BigDecimal((progress / (all.doubleValue() * 100d)) * 100d).setScale(2, BigDecimal.ROUND_DOWN));
        }
        if (studentCourse != null) {
            studentCourseDao.save(studentCourse);
        }
    }

    @Override
    public List<LocalDate> getStudyDate(Integer userId, LocalDate today, LocalDate maxDate) {
        List<StudentCourse> list = studentCourseDao.findStudyDate(userId,
                today.atTime(0, 0).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                maxDate.atTime(0, 0).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        List<LocalDate> studyDateList = new ArrayList<>();
        int days = 0;
        LocalDate start;
        LocalDate end;
        for (StudentCourse studentCourse : list) {
             start = studentCourse.getCourse().getStartTime() ;
             end = studentCourse.getCourse().getEndTime() ;

            if (start.isBefore(today) && end.isAfter(today)) {
                days = Period.between(today, end).getDays();
                addDays(days, today, studyDateList);
            } else if (start.isAfter(today) || start.isEqual(today)) {
                days = Period.between(start, end).getDays();
                addDays(days, start, studyDateList);
            }
        }
        return studyDateList;
    }

    @Override
    public boolean ifSubscribeByChapter(Integer chapterId, String type, Integer userId) {
        return studentCourseMapper.ifSubscribeByChapter(chapterId,type, userId)==1;
    }

    private List<LocalDate> addDays(int days, LocalDate date, List<LocalDate> studyDateList) {
        List<LocalDate> list = new ArrayList<>(days);
        int count = days;
        if (!CollectionUtils.isEmpty(studyDateList)) {
            if (studyDateList.get(studyDateList.size() - 1).isAfter(date)) {
                count = days - Period.between(studyDateList.get(studyDateList.size() - 1), date).getDays();
            }
            if (studyDateList.get(studyDateList.size() - 1).isEqual(date)) {
                count = days - 1;
            }
            date = studyDateList.get(studyDateList.size() - 1);
        }

        for (int i = 0; i < count; i++) {
            list.add(date.plusDays(i));
        }
        studyDateList.addAll(list);
        return studyDateList;
    }
}


