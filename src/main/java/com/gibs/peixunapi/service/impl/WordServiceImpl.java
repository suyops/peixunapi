package com.gibs.peixunapi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import javax.validation.Valid;

import org.springframework.transaction.annotation.Transactional;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.utils.ResultUtil;
import com.gibs.peixunapi.dao.WordDao;
import com.gibs.peixunapi.service.WordService;
import com.gibs.peixunapi.model.Word;

@Service
@Slf4j
public class WordServiceImpl implements WordService{

	@Autowired
	private WordDao wordDao;

	@Transactional
@Override
	public Result create(@Valid Word object) {
		try {
		
//			objectDao.save(object);
			return ResultUtil.success(ResultEnum.CREATE_SUCCESS);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResultUtil.error(ResultEnum.SERVER_ERROR);
		}
	}

	@Transactional
@Override
	public Result save(@Valid Word object) {
		try {
			
//			Word word = objectDao.findOne(object.getId());
//			ConverterUtil.copyNotNullProperties(object,word);
//			objectDao.save(word);
			return ResultUtil.success(ResultEnum.UPDATE_SUCCESS);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResultUtil.error(ResultEnum.SERVER_ERROR);
		}
	}

@Override
	public Result delete(Integer id) {
		try {
//			String[] ids = id.split(",");
//			for (String str : ids) {
//				objectDao.delete(Integer.valueOf(str));
//			}
			return ResultUtil.success(ResultEnum.DELETE_SUCCESS);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResultUtil.error(ResultEnum.SERVER_ERROR);
		}
	}


@Override
	public Result  get(Integer id) {
		try {
//			HashMap<String, Object> data = new HashMap<String, Object>();
//			Word object = objectDao.findOne(id);
//			if (object == null) {
//				Result(false, Status.FAILD, "查找失败", null);
//			}
//			HashMap<String,Object> map = new HashMap();
//			map.put("id", object.getId());
//			map.put("name", object.getName()==null?"":object.getName());
//			map.put("code", object.getCode()==null?"":object.getCode());
//			map.put("description", object.getDescription()==null?"":object.getDescription());
//			map.put("sort", object.getSort()==null?"":object.getSort());
//			HashMap<String,Object> wordType_id = new HashMap<String,Object>();
//			wordType_id.put("name", object.getWordType_id()==null?"":object.getWordType_id().getName());
//			wordType_id.put("id", object.getWordType_id()==null?"":object.getWordType_id().getId());
//			map.put("wordType_id", wordType_id);
//			data.put("object",map);

			return ResultUtil.success(ResultEnum.FIND_SUCCESS);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResultUtil.error(ResultEnum.SERVER_ERROR);
		}
	}

	@Override
	public Result  getList(String key) {
		try {
//			List<Word> list = new ArrayList<>();
//			if (!StringUtils.isEmpty(key)) {
//				list = objectDao.findLike("%" + key + "%");
//			} else {
//				list = objectDao.findAll();
//			}
//			if (list.size() == 0) {
//				PageResult(true, Status.OK, "无数据", new ArrayList(), 0L,0,0);
//			}
//			List<Object> data = new ArrayList<>();
//			for(Word object : list){
//				HashMap<String,Object> map = new HashMap();
//				map.put("id", object.getId());
//				map.put("name", object.getName()==null?"":object.getName());
//				map.put("code", object.getCode()==null?"":object.getCode());
//				map.put("description", object.getDescription()==null?"":object.getDescription());
//				map.put("sort", object.getSort()==null?"":object.getSort());
//				HashMap<String,Object> wordType_id = new HashMap<String,Object>();
//				wordType_id.put("name", object.getWordType_id()==null?"":object.getWordType_id().getName());
//				wordType_id.put("id", object.getWordType_id()==null?"":object.getWordType_id().getId());
//				map.put("wordType_id", wordType_id);
//				data.add(map);
//			}
			return ResultUtil.success(ResultEnum.FIND_SUCCESS);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResultUtil.error(ResultEnum.SERVER_ERROR);
		}
	}

@Override
	public Result getListPage(String key, Integer current, Integer limit) {
		try {
//			List<Word> list = new ArrayList<>();
//			limit = limit == null ? 10 : limit;
//			current = current == null ? 1 : current;
//			int offset = (current - 1) * limit;
//			Long total = null;
//			if (!StringUtils.isEmpty(key)) {
//				list = objectDao.findPageLike("%" + key + "%", offset, limit);
//				total = objectDao.countLike("%" + key + "%");
//			} else {
//				list = objectDao.findPage(offset, limit);
//				total = objectDao.count();
//			}
//			if (total == 0) {
//				PageResult(true, Status.OK, "无数据", new ArrayList(), 0L,0,0);
//			}
//			List<Object> data = new ArrayList<>();
//			for(Word object : list){
//				HashMap<String,Object> map = new HashMap();
//				map.put("id", object.getId());
//				map.put("name", object.getName()==null?"":object.getName());
//				map.put("code", object.getCode()==null?"":object.getCode());
//				map.put("description", object.getDescription()==null?"":object.getDescription());
//				map.put("sort", object.getSort()==null?"":object.getSort());
//				HashMap<String,Object> wordType_id = new HashMap<String,Object>();
//				wordType_id.put("name", object.getWordType_id()==null?"":object.getWordType_id().getName());
//				wordType_id.put("id", object.getWordType_id()==null?"":object.getWordType_id().getId());
//				map.put("wordType_id", wordType_id);
//				data.add(map);
//			}
//			PageData pageData = new PageData(total, limit, current);
			return ResultUtil.successPage(ResultEnum.SUCCESS);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResultUtil.error(ResultEnum.SERVER_ERROR);
		}
	}

	

@Override
	public Result getOptions(String key) {
		try {
//			List<Word> list = new ArrayList<>();
//			if (!StringUtils.isEmpty(key)) {
//				list = objectDao.findLike("%" + key + "%");
//			} else {
//				list = objectDao.findAll();
//			}
//			List<Object> data = new ArrayList<>();
//			for (Word object : list) {
//				HashMap<String, Object> map = new HashMap();
//				map.put("value", object.getId());
//				map.put("name", object.getName());
//				data.add(map);
//			}
			return ResultUtil.success(ResultEnum.SUCCESS);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResultUtil.error(ResultEnum.SERVER_ERROR);
		}
	}


}

