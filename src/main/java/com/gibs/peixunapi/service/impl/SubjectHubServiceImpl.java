package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.SubOptionVO;
import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.VO.post.PostSubjectHubVO;
import com.gibs.peixunapi.VO.show.ShowTestPaperVO;
import com.gibs.peixunapi.dao.SubjectHubDao;
import com.gibs.peixunapi.enums.SubjectEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.mapper.SubjectHubMapper;
import com.gibs.peixunapi.model.SubjectHub;
import com.gibs.peixunapi.service.SubOptionService;
import com.gibs.peixunapi.service.SubjectHubService;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.google.common.base.Joiner;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.gibs.peixunapi.enums.ResultEnum.CREATE_FAIL;
import static com.gibs.peixunapi.enums.ResultEnum.UPDATE_FAIL;

@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class SubjectHubServiceImpl implements SubjectHubService {

    @Autowired
    private SubjectHubDao subjectHubDao;
    @Autowired
    private SubOptionService subOptionService;
    @Autowired
    private SubjectHubService subjectHubService;
    @Autowired
    private SubjectHubMapper subjectHubMapper;


    @Override
    public SubjectHub get(Integer id) {
        return subjectHubDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public SubjectHub create(SubjectHubVO object) {
        try {
            SubjectHub subjectHub = new SubjectHub();
            ConverterUtil.copyNotBlankProperties(object, subjectHub);
            SubjectHub result = subjectHubDao.save(subjectHub);
            if (!Collections.isEmpty(object.getOptionVOList())) {
                subOptionService.create(object.getOptionVOList(), result);
            }
            return result;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL, e);
        }
    }


    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void edit(PostSubjectHubVO object) {
        try {
            SubjectHub subjectHub = subjectHubDao.findById(object.getId()).orElseThrow(EntityNotFoundException::new);
            ConverterUtil.copyNotNullProperties(object, subjectHub);

            if (object.getOptionVOList() != null && (object.getOptionVOList().size() >= 1)) {
                subOptionService.edit(object.getOptionVOList(), subjectHub.getId());
            }
            ConverterUtil.copyNotNullProperties(object, subjectHub);
            subjectHubDao.save(subjectHub);

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(UPDATE_FAIL, e);
        }
    }


    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        subjectHubDao.deleteById(id);
    }

    @Override
    public List<SubjectHubVO> getVOListByIdList(List<Integer> idList) {
        List<SubjectHub> subjectHubList = subjectHubDao.findSubjectHubsByIdIn(idList, Joiner.on(",").join(idList));
        if (Collections.isEmpty(subjectHubList)) {
            throw new NoDataException();
        }
        List<SubjectHubVO> subjectHubVOList = new ArrayList<>();
        for (SubjectHub subjectHub : subjectHubList) {
            SubjectHubVO vo = new SubjectHubVO();
            ConverterUtil.copyProperties(subjectHub, vo, "analysis");
            if ("fill".equals(subjectHub.getType())) {
                if (subjectHub.getAnswer().contains(",")) {
                    String[] strs = subjectHub.getAnswer().split(",");
                    vo.setFillNum(strs.length);
                } else if (subjectHub.getAnswer().contains("，")) {
                    String[] strs = subjectHub.getAnswer().split("，");
                    vo.setFillNum(strs.length);
                }else {
                    vo.setFillNum(1);
                }
            }
            List<SubOptionVO> subOptionVOList = subOptionService.getSubOptions(subjectHub.getId());
            if (!Collections.isEmpty(subjectHubList)) {
                vo.setOptionVOList(subOptionVOList);
            }
            vo.setSort(subjectHubList.indexOf(subjectHub) + 1);
            subjectHubVOList.add(vo);
        }
        return subjectHubVOList;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer persist(SubjectHubVO object) {
        try {
            SubjectHub subjectHub = new SubjectHub();
            ConverterUtil.copyNotBlankProperties(object, subjectHub);
            SubjectHub result = subjectHubDao.save(subjectHub);
            if (!object.getOptionVOList().isEmpty()) {
                subOptionService.create(object.getOptionVOList(), result);
            }
            return result.getId();
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL, e);
        }
    }

    @Override
    public Integer countByCourseAndType(Integer courseId, SubjectEnum enums) {
        return subjectHubDao.countByCourseAndType(courseId, enums.getCode()).intValue();
    }

    @Override
    public SubjectHubVO getVO(Integer id) {
        SubjectHub subjectHub = subjectHubService.get(id);
        SubjectHubVO vo = new SubjectHubVO();
        ConverterUtil.copyProperties(subjectHub, vo, "analysis");
        if ("fill".equals(subjectHub.getType())) {
            if (subjectHub.getAnswer().contains(",")) {
                String[] strs = subjectHub.getAnswer().split(",");
                vo.setFillNum(strs.length);
            } else if (subjectHub.getAnswer().contains("，")) {
                String[] strs = subjectHub.getAnswer().split("，");
                vo.setFillNum(strs.length);
            }else {
                vo.setFillNum(1);
            }
        }
        vo.setOptionVOList(subOptionService.getSubOptions(subjectHub.getId()));
        return vo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SubjectHub saveImport(SubjectHub subjectHub) {
        try {
            return subjectHubDao.save(subjectHub);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL, e);
        }

    }

    @Override
    public List<SubjectHubVO> getSubjectHubOption(Integer courseId, String key, String type) {
        List<SubjectHubVO> list = subjectHubMapper.findSubjectHubOption(courseId, key, type);
        if (CollectionUtils.isEmpty(list)) {
            throw new NoDataException();
        }
        return list;
    }

    @Override
    public ShowTestPaperVO sortSubject(Map<String, List<SubjectHubVO>> subjectHubVOMap) {
        ShowTestPaperVO vo = new ShowTestPaperVO();
        for (Map.Entry<String, List<SubjectHubVO>> entry : subjectHubVOMap.entrySet()) {
            if ("radio".equals(entry.getKey())) {
                vo.setRadioList(entry.getValue());
                vo.setRadioCount(entry.getValue().size());
            }
            if ("check".equals(entry.getKey())) {
                vo.setCheckList(entry.getValue());
                vo.setCheckCount(entry.getValue().size());
            }
            if ("judge".equals(entry.getKey())) {
                vo.setJudgeList(entry.getValue());
                vo.setJudgeCount(entry.getValue().size());
            }
            if ("fill".equals(entry.getKey())) {
                vo.setFillList(entry.getValue());
                vo.setFillCount(entry.getValue().size());
            }
            if ("question".equals(entry.getKey())) {
                vo.setQuestionList(entry.getValue());
                vo.setQuestionCount(entry.getValue().size());
            }
        }
        return vo;
    }


}

