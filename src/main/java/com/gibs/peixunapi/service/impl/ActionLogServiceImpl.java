package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.mapper.ActionLogMapper;
import com.gibs.peixunapi.model.ActionLog;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.service.ActionLogService;
import com.gibs.peixunapi.utils.DateUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class ActionLogServiceImpl implements ActionLogService {

    @Autowired
    private ActionLogMapper actionLogMapper;

    @Override
    public PageData<ActionLog> getListPage(String content, Date dateFrom, Date dateTo, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<ActionLog> pageData = new PageInfo<>(actionLogMapper.getActionLog(content, DateUtils.toString(dateFrom), DateUtils.toString(dateTo)));
        if (CollectionUtils.isEmpty(pageData.getList())) {
            throw new NoDataException("page");
        }
        List<ActionLog> list = pageData.getList();

        return new PageData<>((int) pageData.getTotal(), limit, page, list);
    }
}

