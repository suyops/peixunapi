package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.post.NoticeVO;
import com.gibs.peixunapi.VO.show.ShowNoticeVO;
import com.gibs.peixunapi.dao.NoticeDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.model.Business;
import com.gibs.peixunapi.model.Notice;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.service.BusinessService;
import com.gibs.peixunapi.service.NoticeService;
import com.gibs.peixunapi.service.UserRoleService;
import com.gibs.peixunapi.service.UserService;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.google.common.base.Joiner;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.LinkEnum.SYSTEM_CALL;

@Service
@Slf4j
public class NoticeServiceImpl implements NoticeService {

    @Autowired
    private NoticeDao noticeDao;
    @Autowired
    private NoticeService noticeService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private BusinessService businessService;


    @Override
    public List<ShowNoticeVO> getNotice(Integer userId) {
        List<Notice> list = noticeDao.findByAccepter(userId);
        if (CollectionUtils.isEmpty(list)) {
            throw new NoDataException();
        }
        List<ShowNoticeVO> voList = new ArrayList<>(list.size());
        for (Notice notice : list) {
            ShowNoticeVO vo = new ShowNoticeVO();
            ConverterUtil.copyProperties(notice, vo, "content");
            vo.setAccepterName(userService.get(notice.getAccepter()).getUsername());
            if (notice.getSender() == 0) {
                vo.setSenderName("系统通知");
            }else{
                vo.setSenderName(userService.get(notice.getSender()).getUsername());
            }
            voList.add(vo);
        }
        return voList;
    }

    @Override
    public void sendNotice(NoticeVO noticeVO) {
        List<Integer> userIdList = new ArrayList<>();

        List<Notice> list = new ArrayList<>();
        if (StringUtils.isNotBlank(noticeVO.getAcceptors())) {
            if (noticeVO.getAcceptors().contains(",")) {
                userIdList = Arrays.stream(noticeVO.getAcceptors().split(",")).map(Integer::parseInt).collect(Collectors.toList());
            } else {
                userIdList.add(Integer.parseInt(noticeVO.getAcceptors()));
            }
        }

        List<Integer> busineddIdList = new ArrayList<>();
        if (StringUtils.isNotBlank(noticeVO.getBusinessIds())) {
            if (noticeVO.getBusinessIds().contains(",")) {
                busineddIdList = Arrays.stream(noticeVO.getBusinessIds().split(",")).map(Integer::parseInt).collect(Collectors.toList());
            } else {
                busineddIdList.add(Integer.parseInt(noticeVO.getBusinessIds()));
            }
            for (Integer i : busineddIdList) {
                List<User> userList = userService.getListByBusiness(i);
                userIdList.addAll(userList.stream().map(User::getId).collect(Collectors.toList()));
            }
        }

        List<String> roleList = new ArrayList<>();
        if (StringUtils.isNotBlank(noticeVO.getRoles())) {
            if (noticeVO.getRoles().contains(",")) {
                roleList = Arrays.stream(noticeVO.getRoles().split(",")).collect(Collectors.toList());
            } else {
                roleList.add(noticeVO.getRoles());
            }

            for (String code : roleList) {
                userIdList.addAll(userRoleService.getUserByRole(code));
            }
        }

        for (Integer i : userIdList) {
            Notice notice = new Notice();
            ConverterUtil.copyProperties(noticeVO, notice);
            notice.setAccepter(i);
            list.add(notice);
        }
        try {
            noticeDao.saveAll(list);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public void sendNoticeToUser(String title, String content, String link, List<Integer> idList, Integer sender) {
        NoticeVO vo = new NoticeVO();
        vo.setTitle(SYSTEM_CALL.getLink() + title);
        vo.setContent(content);
        vo.setLink(link);
        vo.setAcceptors(Joiner.on(",").join(idList));
        vo.setSender(sender);
        noticeService.sendNotice(vo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void sendNoticeListToUser(String title, Map<Integer, String> contentMap, String link, Integer sender) {
        List<NoticeVO> voList = new ArrayList<>();
        for (Map.Entry<Integer, String> entry : contentMap.entrySet()) {
            NoticeVO vo = new NoticeVO();
            vo.setTitle(SYSTEM_CALL.getLink() + title);
            vo.setContent(entry.getValue());
            vo.setLink(link);
            vo.setAcceptors(entry.getKey().toString());
            vo.setSender(sender);
        }
        noticeService.sendNoticeBatch(voList);
    }



    @Override
    public void sendNoticeToUser(String title, String content, String link, String id, Integer sender) {
        NoticeVO vo = new NoticeVO();
        vo.setTitle(SYSTEM_CALL.getLink() + title);
        vo.setContent(content);
        vo.setLink(link);
        vo.setAcceptors(id);
        vo.setSender(sender);
        noticeService.sendNotice(vo);
    }


    @Override
    public void sendNoticeToBusiness(String title, String content, String link, List<Integer> idList, Integer sender) {
        NoticeVO vo = new NoticeVO();
        vo.setTitle(title);
        vo.setContent(content);
        vo.setLink(link);
        vo.setBusinessIds(Joiner.on(",").join(idList));
        vo.setSender(sender);
        noticeService.sendNotice(vo);
    }

    @Override
    public void sendNoticeToBusinessCount(String title, String content, String link, Integer id, Integer sender) {
        NoticeVO vo = new NoticeVO();
        vo.setTitle(title);
        vo.setContent(content);
        vo.setLink(link);
        Business business = businessService.get(id);
        vo.setBusinessIds(business.getManagerId().toString());
        vo.setSender(sender);
        noticeService.sendNotice(vo);
    }

    @Override
    public void sendNoticeToRole(String title, String content, String link, List<String> idList, Integer sender) {
        NoticeVO vo = new NoticeVO();
        vo.setTitle(title);
        vo.setContent(content);
        vo.setLink(link);
        vo.setRoles(Joiner.on(",").join(idList));
        vo.setSender(sender);
        noticeService.sendNotice(vo);
    }

    @Override
    public void sendNoticeToRole(String title, String content, String link, String id, Integer sender) {
        NoticeVO vo = new NoticeVO();
        vo.setTitle(title);
        vo.setContent(content);
        vo.setLink(link);
        vo.setRoles(id);
        vo.setSender(sender);
        noticeService.sendNotice(vo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public ShowNoticeVO readNotice(Integer noticeId) {
        Notice notice = noticeDao.findById(noticeId).orElseThrow(EntityNotFoundException::new);

        ShowNoticeVO vo = new ShowNoticeVO();
        ConverterUtil.copyProperties(notice, vo);
        vo.setAccepterName(userService.get(notice.getAccepter()).getUsername());
        if (notice.getSender() != 0) {
            vo.setSenderName(userService.get(notice.getSender()).getUsername());
        }else{
            vo.setSenderName("系统");
        }

        notice.setRead(1);
        notice.setReadTime(new Date());
        try {
            noticeDao.save(notice);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
        return vo;

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void deleteReadNotice() {
        User user = userService.getAttributeUser();
        try {
            noticeDao.deleteReadNotice(user.getId());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.DELETE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void sendNoticeBatch(List<NoticeVO> voList) {
        List<Notice> list = new ArrayList<>();
        for (NoticeVO noticeVO : voList) {
            Notice notice = new Notice();
            ConverterUtil.copyProperties(noticeVO, notice);
            notice.setAccepter(Integer.parseInt(noticeVO.getAcceptors()));
            list.add(notice);
        }
        try {
            noticeDao.saveAll(list);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }
}

