package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.SubOptionVO;
import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.VO.post.PostSubjectHubVO;
import com.gibs.peixunapi.VO.show.CourseSubjectHubVO;
import com.gibs.peixunapi.VO.show.ExerciseVO;
import com.gibs.peixunapi.dao.ExercisesDao;
import com.gibs.peixunapi.dao.WrongExercisesDao;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.mapper.ExercisesMapper;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import com.google.common.base.Joiner;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.CourseEnum.DOCUMENT;
import static com.gibs.peixunapi.enums.CourseEnum.VIDEO;
import static com.gibs.peixunapi.enums.ResultEnum.CREATE_FAIL;
import static com.gibs.peixunapi.enums.ResultEnum.UPDATE_FAIL;

@Service
@Slf4j
public class ExercisesServiceImpl implements ExercisesService {

    @Autowired
    private ExercisesDao exercisesDao;
    @Autowired
    private WrongExercisesDao wrongExercisesDao;
    @Autowired
    private CourseSubjectHubService courseSubjectHubService;
    @Autowired
    private ExercisesMapper exercisesMapper;
    @Autowired
    private SubjectHubService subjectHubService;
    @Autowired
    private SubOptionService subOptionService;
    @Autowired
    private UserService userService;
    @Autowired
    private ExerciseRecordService exerciseRecordService;

    @Override
    public Exercises get(Integer id) {
        return exercisesDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Transactional
    @Modifying(clearAutomatically = true)
    public Result changeExercises(List<ExerciseVO> exerciseVOList) {
        List<Exercises> exercisesList = new ArrayList<>(exerciseVOList.size());
        for (ExerciseVO exerciseVO : exerciseVOList) {
            Exercises exercises = exercisesDao.findById(exerciseVO.getId()).orElseThrow(EntityNotFoundException::new);
            if (!exercises.getCourseSubjectHub().getId().equals(exerciseVO.getId())) {
                CourseSubjectHub courseSubjectHub = courseSubjectHubService.get(exerciseVO.getId());
                exercises.setCourseSubjectHub(courseSubjectHub);
            }
            exercises.setSort(exerciseVO.getSort());
            exercisesList.add(exercises);
        }
        try {
            exercisesDao.saveAll(exercisesList);
            return ResultUtil.success(ResultEnum.UPDATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(UPDATE_FAIL, e);
        }
    }


    @Override
    public List<Integer> getSubjectHubIdList(Integer chapterId, String type) {
        List<CourseSubjectHubVO> courseSubjectHubList = exercisesMapper.getCourseSubjectHubList(chapterId, type.trim());
        if (Collections.isEmpty(courseSubjectHubList)) {
            return null;
        }
        List<Integer> data = courseSubjectHubList.stream().map(CourseSubjectHubVO::getSubjectHubId).collect(Collectors.toList());
        return data;
    }

    @Override
    public List<SubjectHubVO> getSomeChapterExercise(Integer chapterId, Integer number, String type) {
        User user = userService.getAttributeUser();
//      User user = userService.get(2);
        List<Exercises> list = new ArrayList<>();
        if (DOCUMENT.getMessage().equals(type)) {
            list = exercisesDao.getDocumentExerciseList(chapterId, number, user.getId());
        } else if (VIDEO.getMessage().equals(type)) {
            list = exercisesDao.getVideoExerciseList(chapterId, number, user.getId());
        }
        if (Collections.isEmpty(list)) {
            return null;
        }
        List<SubjectHubVO> voList = new ArrayList<>();
        for (Exercises exercises : list) {
            SubjectHubVO vo = new SubjectHubVO();

            ConverterUtil.copyProperties(exercises.getCourseSubjectHub().getSubjectHub(), vo);
            vo.setExercisesId(exercises.getId());
            if ("fill".equals(exercises.getCourseSubjectHub().getSubjectHub().getType())) {
                if (exercises.getCourseSubjectHub().getSubjectHub().getAnswer().contains(",")) {
                    String[] strs = exercises.getCourseSubjectHub().getSubjectHub().getAnswer().split(",");
                    vo.setFillNum(strs.length);
                } else if (exercises.getCourseSubjectHub().getSubjectHub().getAnswer().contains("，")) {
                    String[] strs = exercises.getCourseSubjectHub().getSubjectHub().getAnswer().split("，");
                    vo.setFillNum(strs.length);
                }else {
                    vo.setFillNum(1);
                }
            }
            List<SubOptionVO> subOptionVOList = subOptionService.getSubOptions(exercises.getCourseSubjectHub().getSubjectHub().getId());
            vo.setOptionVOList(subOptionVOList);
            voList.add(vo);

        }
        return voList;
    }


    @Override
    public Integer countByCourse(Integer courseId) {
        return exercisesMapper.countByCourse(courseId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addWrongExercises(ExerciseRecord record, Integer student) {
        WrongExercises wrongExercises = wrongExercisesDao.findByStudentAndExercise(record.getExercise().getId(), student);
        if (wrongExercises == null) {
            wrongExercises = new WrongExercises();
            wrongExercises.setExerciseRecode(record);
            wrongExercises.setStudent(student);
            try {
                wrongExercisesDao.save(wrongExercises);
            } catch (Exception e) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                throw new BaseException(CREATE_FAIL, e);
            }
        }

    }


    @Override
    public List<SubjectHubVO> getWrongExercises(Integer studentId, Integer courseId) {
        List<WrongExercises> list = wrongExercisesDao.findByStudent(studentId);
        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
        List<Integer> idList = list.stream().map(i -> i.getExerciseRecode().getExercise().getCourseSubjectHub().getSubjectHub().getId()).collect(Collectors.toList());
        List<SubjectHubVO> voList = subjectHubService.getVOListByIdList(idList);
        for (SubjectHubVO vo : voList) {
            vo.setMyAnswer(list.get(voList.indexOf(vo)).getExerciseRecode().getMyAnswer());
            vo.setExercisesId(list.get(voList.indexOf(vo)).getExerciseRecode().getExercise().getId());
        }
        return voList;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void changeHangUpSubject(Integer chapterId, Integer courseId, Integer newSubjectHubId, CourseEnum courseEnum) {
        //清空所有防挂机绑定
        exercisesDao.deleteByVideoClass(chapterId);
        //创建新防挂机题目
        Exercises exercises = new Exercises();
        exercises.setVideoClass(chapterId);
        exercises.setType(1);
        exercises.setSort(1);
        CourseSubjectHub newOne = courseSubjectHubService.getBySubjectHub(chapterId, newSubjectHubId, VIDEO);
        exercises.setCourseSubjectHub(newOne);
        try {
            exercisesDao.save(exercises);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createInChapter(PostSubjectHubVO subject) {
        try {
            SubjectHub subjectHub = null;
            if (subject.getId() == null) {
                SubjectHubVO vo = new SubjectHubVO();
                ConverterUtil.copyProperties(subject, vo);
                subjectHub = subjectHubService.create(vo);
            }
            CourseSubjectHub courseSubjectHub = null;
            Exercises exercises = new Exercises();

            if (VIDEO.getMessage().equals(subject.getChapterType())) {
                if (subject.getId() == null && subjectHub != null) {
                    courseSubjectHub = courseSubjectHubService.createInChapter(subjectHub, subject.getChapterId(), VIDEO);
                } else {
                    courseSubjectHub = courseSubjectHubService.getBySubjectHub(subject.getChapterId(), subject.getId(), VIDEO);
                }
                exercises.setVideoClass(subject.getChapterId());
                exercises.setType(0);
            }
            if (DOCUMENT.getMessage().equals(subject.getChapterType())) {
                if (subject.getId() == null && subjectHub != null) {
                    courseSubjectHub = courseSubjectHubService.createInChapter(subjectHub, subject.getChapterId(), DOCUMENT);
                } else {
                    courseSubjectHub = courseSubjectHubService.getBySubjectHub(subject.getChapterId(), subject.getId(), DOCUMENT);
                }
                exercises.setDocumentation(subject.getChapterId());
            }
            exercises.setCourseSubjectHub(courseSubjectHub);
            exercises.setSort(subject.getSort());
            exercisesDao.save(exercises);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL, e);
        }
    }

    @Override
    public Map<String, List<SubjectHubVO>> classify(List<SubjectHubVO> subjectHubList) {

        Map<String, List<SubjectHubVO>> result = new HashMap<>();

        /* 循环遍历转换为VO类 放入Map中返回结果*/
        for (int i = 0; i < subjectHubList.size(); i++) {
            /*如果下一个type不同那么结束收集此类型完毕*/
            if (subjectHubList.get(i).getType() != null) {
                if (i + 1 <= subjectHubList.size()) {

//                    if (subjectHubList.get(i).getType().equals(subjectHubList.get(i + 1).getType())) {
//                        if (Collections.isEmpty(result.get(subjectHubList.get(i).getType()))) {
//                            result.put(subjectHubList.get(i).getType(), new ArrayList<>());
//                        }
//                        result.get(subjectHubList.get(i).getType()).add(subjectHubList.get(i));
//                    } else {
//                        result.put(subjectHubList.get(i + 1).getType(), new ArrayList<>());
//                        result.get(subjectHubList.get(i).getType()).add(subjectHubList.get(i));
//                    }
                    if (!result.containsKey(subjectHubList.get(i).getType())) {
                        result.put(subjectHubList.get(i).getType(), new ArrayList<>());
                    }
                }
                result.get(subjectHubList.get(i).getType()).add(subjectHubList.get(i));
            }
        }
        return result;
    }

    @Override
    public List<SubjectHubVO> getRandomExercises(Integer limit, Integer chapterId, String type) {
        List<Integer> idList = new ArrayList<>();
        if (VIDEO.getMessage().equals(type)) {
            idList = exercisesDao.findIdOfVideoExercise(chapterId);
        }
        if (DOCUMENT.getMessage().equals(type)) {
            idList = exercisesDao.findIdOfDocumentExercise(chapterId);
        }
        if (Collections.isEmpty(idList)) {
            throw new BaseException(CREATE_FAIL);
        }

        List<Integer> randomList = new ArrayList<>();
        int times = limit <= idList.size() ? limit : idList.size();
        for (Integer i = 0; i < times; i++) {
            int num;
            do {
                num = (int) Math.floor(Math.random() * idList.size());
            }
            while (randomList.contains(num));
            randomList.add(idList.get(num));
            idList.remove(num);
        }

        if (Collections.isEmpty(randomList)) {
            throw new BaseException(CREATE_FAIL);
        }
        List<Exercises> list = exercisesDao.findByIdIn(randomList, Joiner.on(",").join(randomList));
        List<SubjectHubVO> voList = new ArrayList<>();
        for (Exercises exercises : list) {
            SubjectHubVO vo = new SubjectHubVO();

            ConverterUtil.copyProperties(exercises.getCourseSubjectHub().getSubjectHub(), vo);
            if ("fill".equals(exercises.getCourseSubjectHub().getSubjectHub().getType())) {
                if (exercises.getCourseSubjectHub().getSubjectHub().getAnswer().contains(",")) {
                    String[] strs = exercises.getCourseSubjectHub().getSubjectHub().getAnswer().split(",");
                    vo.setFillNum(strs.length);
                } else if (exercises.getCourseSubjectHub().getSubjectHub().getAnswer().contains("，")) {
                    String[] strs = exercises.getCourseSubjectHub().getSubjectHub().getAnswer().split("，");
                    vo.setFillNum(strs.length);
                }else {
                    vo.setFillNum(1);
                }
            }
            vo.setExercisesId(exercises.getId());
            List<SubOptionVO> subOptionVOList = subOptionService.getSubOptions(exercises.getCourseSubjectHub().getSubjectHub().getId());
            vo.setOptionVOList(subOptionVOList);
            voList.add(vo);
        }
        return voList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void BindExerciseFromSubjectHub(String ids, Integer chapterId, String type) {
        List<Integer> idList = new ArrayList<>();
        if (StringUtils.isNotBlank(ids) && !ids.contains(",")) {
            idList.add(Integer.parseInt(ids));
        } else {
            idList = Arrays.stream(ids.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        }
        List<CourseSubjectHub> courseSubjectHubList = courseSubjectHubService.getByIdList(idList);
        List<Exercises> exercisesList = new ArrayList<>(courseSubjectHubList.size());
        for (CourseSubjectHub courseSubjectHub : courseSubjectHubList) {
            Exercises exercises = new Exercises();
            exercises.setCourseSubjectHub(courseSubjectHub);
            if (DOCUMENT.getMessage().equals(type)) {
                exercises.setDocumentation(chapterId);
            } else {
                exercises.setVideoClass(chapterId);
                exercises.setType(0);
            }
            exercisesList.add(exercises);
        }
        try {
            exercisesDao.saveAll(exercisesList);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL, e);
        }
    }

    @Override
    public List<SubjectHubVO> getHangUpSubjectChooseList(Integer courseId) {
        List<CourseSubjectHub> list = courseSubjectHubService.findHangUpSubjectChooseList(courseId);
        if (CollectionUtils.isEmpty(list)) {
            throw new NoDataException();
        }
        List<SubjectHubVO> voList = new ArrayList<>();
        for (CourseSubjectHub courseSubjectHub : list) {
            SubjectHubVO vo = new SubjectHubVO();

            ConverterUtil.copyProperties(courseSubjectHub.getSubjectHub(), vo, "analysis");

            List<SubOptionVO> subOptionVOList = subOptionService.getSubOptions(courseSubjectHub.getSubjectHub().getId());
            vo.setOptionVOList(subOptionVOList);
            voList.add(vo);
        }
        return voList;
    }

    @Override
    public Integer countExercisesInChapter(Integer id, CourseEnum courseEnum) {
        return exercisesMapper.countExercisesInChapter(id, courseEnum.getMessage());
    }



    @Override
    public List<SubjectHubVO> getHangUpSubjectVO(Integer classId) {
        List<Exercises> subjectHubList = exercisesDao.findHangUpSubject(classId);
        if (Collections.isEmpty(subjectHubList)) {
            return null;
        }
        List<SubjectHubVO> subjectHubVOList = new ArrayList<>();
        for (Exercises exercises : subjectHubList) {
            SubjectHubVO vo = new SubjectHubVO();
            ConverterUtil.copyProperties(exercises.getCourseSubjectHub().getSubjectHub(), vo, "analysis");
            List<SubOptionVO> subOptionVOList = subOptionService.getSubOptions(exercises.getCourseSubjectHub().getSubjectHub().getId());
            if (!Collections.isEmpty(subjectHubList)) {
                vo.setOptionVOList(subOptionVOList);
            }
            vo.setSort(subjectHubList.indexOf(exercises) + 1);
            vo.setExercisesId(exercises.getId());
            subjectHubVOList.add(vo);
        }
        return subjectHubVOList;
    }
}

