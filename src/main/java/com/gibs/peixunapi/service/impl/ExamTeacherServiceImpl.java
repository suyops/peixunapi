package com.gibs.peixunapi.service.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.dao.UserDao;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.gibs.peixunapi.dao.ExamTeacherDao;
import com.gibs.peixunapi.service.ExamTeacherService;
import com.gibs.peixunapi.model.ExamTeacher;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.CourseEnum.CORRECT;
import static com.gibs.peixunapi.enums.CourseEnum.MANAGE;

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class ExamTeacherServiceImpl implements ExamTeacherService {

    @Autowired
    private ExamTeacherDao examTeacherDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private ExamTeacherService examTeacherService;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public Boolean saveExamTeachers(List<Integer> manageIds, Integer classId, CourseEnum enums) {
        //先删除旧监考老师和阅卷老师
        examTeacherService.deleteByClassId(classId,enums.getMessage());

        List<ExamTeacher> list = new ArrayList<>(manageIds.size());
        for (Integer manageId : manageIds) {
            User teacher = userDao.findById(manageId).orElseThrow(EntityExistsException::new);
            ExamTeacher examTeacher = new ExamTeacher();
            examTeacher.setExamClassInfoId(classId);
            examTeacher.setTeacher(teacher);
            examTeacher.setType(enums.getMessage());
            list.add(examTeacher);
        }
        try {
            examTeacherDao.saveAll(list);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
        return true;
    }

    @Override
    public List<OptionVO> getTeacherList(Integer classId, CourseEnum enums) {
        List<ExamTeacher> examTeacherList = examTeacherDao.findByClassAndType(classId, enums.getMessage());
        return examTeacherList.stream()
                .map(i -> new OptionVO(i.getTeacher().getId(), null, i.getTeacher().getUsername(),null))
                .collect(Collectors.toList());
    }

    @Override
    public String getTeacherNames(Integer classId, CourseEnum enums) {

        List<ExamTeacher> examTeacherList = examTeacherDao.findByClassAndType(classId, enums.getMessage());
        if (CollectionUtils.isEmpty(examTeacherList)) {
            return "";
        } else {
            String names = examTeacherList.stream().map(i -> i.getTeacher().getUsername()).collect(Collectors.joining(","));
            return names;
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void editExamTeacher(String ids, Integer classId, String type) {
        //清空制定类型老师
        examTeacherService.deleteByClassId(classId, type);
        List<Integer> idList = new ArrayList<>();
        if (ids.contains(",")) {
            idList = Arrays.stream(ids.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        }else{
            idList.add(Integer.parseInt(ids));
        }

        //重新绑定
        if (MANAGE.getMessage().equals(type)) {
            examTeacherService.saveExamTeachers(idList, classId, MANAGE);
        }else if (CORRECT.getMessage().equals(type)) {
            examTeacherService.saveExamTeachers(idList, classId, CORRECT);
        } else {
            throw new BaseException(ResultEnum.PARAM_ERROR);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void deleteByClassId(Integer classId, String type) {
        examTeacherDao.deleteByClassId(classId, type);

    }
}



