package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.dao.ScheduledDao;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.mapper.ScheduledMapper;
import com.gibs.peixunapi.model.Scheduled;
import com.gibs.peixunapi.service.CourseService;
import com.gibs.peixunapi.service.JobService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.ResultEnum.JOB_UPDATE_ERROR;

/**
 * @author liangjiawei
 * @date 2020/10/28/18:26
 * @Version 1.0
 * @Description:
 */
@Slf4j
@Service
class JobServiceImpl implements JobService {


    /**
     * Scheduler：调度器，进行任务调度；quartz的大脑
     * Job：业务job，亦可称业务组件；定时任务的具体执行业务需要实现此接口，调度器会调用此接口的execute方法完成我们的定时业务
     * JobDetail：用来定义业务Job的实例，我们可以称之为quartz job，很多时候我们谈到的job指的是JobDetail
     * Trigger：触发器，用来定义一个指定的Job何时被执行
     * JobBuilder：Job构建器，用来定义或创建JobDetail的实例；JobDetail限定了只能是Job的实例
     * TriggerBuilder：触发器构建器，用来定义或创建触发器的实例
     */
    @Autowired
    private ScheduledDao scheduledDao;
    @Autowired
    private JobService jobAndTriggerService;
    @Autowired
    private ScheduledMapper scheduledMapper;
    @Autowired
    private CourseService courseService;

    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;

    private final static String JOB_BEAN = "com.gibs.peixunapi.schedule.JobBean";

//
//    @PostConstruct
//    public void init() {
//        // 这里获取任务信息数据
////        List<Scheduled> jobList = mapper.getCronList();
//        List<Scheduled> jobList = null;
//        if (jobList != null) {
//            jobList.forEach(job -> {
//                try {
//                    if (job.getStatus() == 0) {
//                        addOrUpdateJob(job);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    log.error("初始化定时任务列表异常->{}", e.getMessage());
//                }
//            });
//        }
//    }


    @Override
    public void isEnableScheduleJob(Integer id) throws Exception {
        Scheduled scheduled = scheduledMapper.getCron(id);
        if (scheduled.getIsEnable() == 0) {
            //停用任务
            scheduled.setIsEnable(1);
            scheduledDao.save(scheduled);
            jobAndTriggerService.stopJob(scheduled);
        } else if (scheduled.getIsEnable() == 1) {
            //启用任务
            scheduled.setIsEnable(0);
            scheduledDao.save(scheduled);
            jobAndTriggerService.updateJob(scheduled);
        }
    }

    @Override
    public void pauseScheduleJob(Integer id) throws Exception {
        Scheduled scheduled = scheduledMapper.getCron(id);
        if (scheduled.getStatus() == 0) {
            //暂停任务
            scheduled.setStatus(1);
            scheduledDao.save(scheduled);
            jobAndTriggerService.pauseJob(scheduled);
            Scheduler scheduler = schedulerFactoryBean.getScheduler();
            JobKey jobKey = JobKey.jobKey(scheduled.getTaskName());
            scheduler.pauseJob(jobKey);
        } else if (scheduled.getStatus() == 1) {
            //执行任务
            scheduled.setStatus(0);
            scheduledDao.save(scheduled);
            jobAndTriggerService.resumejob(scheduled);
        }
    }

    @Override
    public void deleteScheduleJob(Integer id) throws Exception {
        Scheduled scheduled = scheduledMapper.getCron(id);
        jobAndTriggerService.stopJob(scheduled);
        scheduledMapper.delete(scheduled.getId());
    }

    @Override
    public void runScheduleJob(Integer id) throws Exception {
        jobAndTriggerService.runAJobNow(scheduledMapper.getCron(id));
    }


    @Override
    public void addOrUpdateJob(Scheduled mgrScheduleJob) throws ClassNotFoundException, SchedulerException, IllegalAccessException, InstantiationException {
        //获去调度器实例
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        TriggerKey triggerKey = TriggerKey.triggerKey(mgrScheduleJob.getTaskName());
        CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);

        //trigger不存在，创建一个
        if (null == trigger) {
            //新增定时任务
            Class clazz = Class.forName(JOB_BEAN);
            //判断clazz是否为null，是则抛出异常
            Assert.notNull(clazz, "定时任务执行类为NULL");
            //初始化一个类，生成一个实例
            clazz.newInstance();
            // 构建job信息
            JobDetail jobDetail = JobBuilder.newJob(clazz)
                    .withIdentity(mgrScheduleJob.getTaskName()).build();
            //传入参数
            jobDetail.getJobDataMap().put("method", mgrScheduleJob);
            // 表达式调度构建器(即任务执行的时间)
            //如果时间设定在00:00:00  则推迟5分钟执行
            String cron = mgrScheduleJob.getCron();
            if(mgrScheduleJob.getCron().contains("00 00 00")){
                cron =  mgrScheduleJob.getCron().replace("00 00 00","00 05 00");
            }
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cron);

            // 按新的cronExpression表达式构建一个新的trigger
            trigger = TriggerBuilder.newTrigger().withIdentity(mgrScheduleJob.getTaskName())
                    .withSchedule(scheduleBuilder).build();
            //设置job执行
            scheduler.scheduleJob(jobDetail, trigger);
        } else {
            // Trigger已存在，那么更新相应的定时设置
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(mgrScheduleJob.getCron());

            // 按新的cronExpression表达式重新构建trigger
            trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();

            // 按新的trigger重新设置job执行
            scheduler.rescheduleJob(triggerKey, trigger);
        }
    }

    @Override
    public void updateJob(Scheduled schedule) {
        //获去调度器实例
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        TriggerKey triggerKey = TriggerKey.triggerKey(schedule.getTaskName());
        CronTrigger trigger;
        try {
            trigger = (CronTrigger) scheduler.getTrigger(triggerKey);

            // Trigger已存在，那么更新相应的定时设置
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(schedule.getCron());

            // 按新的cronExpression表达式重新构建trigger
            trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();

            // 按新的trigger重新设置job执行
            scheduler.rescheduleJob(triggerKey, trigger);
        } catch (SchedulerException e) {
            log.error("{}", e.getMessage());
            throw new BaseException(JOB_UPDATE_ERROR);
        }
    }

    @Override
    public void stopJob(Scheduled scheduleJob) throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        JobKey jobKey = JobKey.jobKey(scheduleJob.getTaskName());
        scheduler.deleteJob(jobKey);
    }


    @Override
    public void pauseJob(Scheduled scheduleJob) throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        JobKey jobKey = JobKey.jobKey(scheduleJob.getTaskName());
        scheduler.pauseJob(jobKey);
    }

    @Override
    public void resumejob(Scheduled scheduleJob) throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        JobKey jobKey = JobKey.jobKey(scheduleJob.getTaskName());
        scheduler.resumeJob(jobKey);
    }

    @Override
    public void runAJobNow(Scheduled scheduleJob) throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        JobKey jobKey = JobKey.jobKey(scheduleJob.getTaskName());
        scheduler.triggerJob(jobKey);
    }

    @Override
    public List<Scheduled> allDoingSchedule() throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        List<Scheduled> jobList = new ArrayList<>();
        GroupMatcher<JobKey> matcher = GroupMatcher.anyJobGroup();
        Set<JobKey> jobKeys = scheduler.getJobKeys(matcher);
        jobKeys.forEach(jobKey -> {
            List<? extends Trigger> triggers = null;
            try {
                triggers = scheduler.getTriggersOfJob(jobKey);
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
            if (triggers != null) {
                triggers.forEach(trigger -> {
                    Scheduled job = new Scheduled();
                    job.setTaskName(jobKey.getName());
                    if (trigger instanceof CronTrigger) {
                        CronTrigger cronTrigger = (CronTrigger) trigger;
                        String cronExpression = cronTrigger.getCronExpression();
                        job.setCron(cronExpression);
                    }
                    jobList.add(job);
                });
            }
        });
        return jobList;
    }

    @Override
    public List<Scheduled> getRunningJob() throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        List<JobExecutionContext> executingJobs = scheduler.getCurrentlyExecutingJobs();
        List<Scheduled> jobList = new ArrayList<Scheduled>(executingJobs.size());
        for (JobExecutionContext executingJob : executingJobs) {
            Scheduled job = new Scheduled();
            JobDetail jobDetail = executingJob.getJobDetail();
            JobKey jobKey = jobDetail.getKey();
            job.setTaskName(jobKey.getName());
            Trigger trigger = executingJob.getTrigger();
//                        Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
            if (trigger instanceof CronTrigger) {
                CronTrigger cronTrigger = (CronTrigger) trigger;
                String cronExpression = cronTrigger.getCronExpression();
                job.setCron(cronExpression);
            }
            jobList.add(job);
        }
        return jobList;
    }

    @Override
    public void pauseAllJobs() throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        scheduler.pauseAll();
    }

    @Override
    public void resumeAllJobs() throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        scheduler.resumeAll();
    }

    @Override
    @org.springframework.scheduling.annotation.Scheduled(cron = "0 0 23 * * ? ")
    public void stopAllJob() throws SchedulerException {
        List<Scheduled> list = allDoingSchedule();
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        List<JobKey> jobKeyList = list.stream().map(i -> JobKey.jobKey(i.getTaskName())).collect(Collectors.toList());
        scheduler.deleteJobs(jobKeyList);
    }

    @Override
    @org.springframework.scheduling.annotation.Scheduled(cron = "0 0 0 * * ? ")
    public void addTodayJobs() {
        LocalDate now = LocalDate.now();

        List<Scheduled> list = scheduledDao.findTodaysJob(now.toString());
        for (Scheduled scheduled : list) {
            try {

                jobAndTriggerService.addOrUpdateJob(scheduled);

            } catch (Exception e) {
                log.error("发生业务异常！原因是：{}\n方法名:{}\t第{}行\n详细:{}", e.getMessage(), e.getStackTrace()[0].getClassName() + "," + e.getStackTrace()[0].getClassName(), e.getStackTrace()[0].getLineNumber(), e.getStackTrace());
                continue;
            }
        }
    }

    @Override
    public List<Scheduled> allSchedule() {
        List<Scheduled> list = scheduledDao.getAll();
        if (CollectionUtils.isNotEmpty(list)) {
//            for (Scheduled scheduled : list) {
//                scheduled.setParamId(Optional.ofNullable(courseService.get(Integer.parseInt(scheduled.getParamId())))
//                        .orElse(new Course()).getName());
//            }
        }
        return list;
    }

    @Override
    public void editJob(Scheduled scheduled) {

    }

    @Override
    public Scheduled getJob(String taskName) {
        return scheduledDao.findFirstByTaskName(taskName);
    }
}
