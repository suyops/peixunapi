package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.post.StudyRecordVO;
import com.gibs.peixunapi.VO.show.StudyRecordListVO;
import com.gibs.peixunapi.dao.StudyProgressDao;
import com.gibs.peixunapi.dao.StudyRecordDao;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.gibs.peixunapi.enums.CourseEnum.*;
import static com.gibs.peixunapi.enums.ResultEnum.SAVE_FAIL;
import static com.gibs.peixunapi.enums.ResultEnum.UPDATE_FAIL;

@Service
@Slf4j
public class StudyProgressServiceImpl implements StudyProgressService {

    @Autowired
    private StudyProgressDao studyProgressDao;
    @Autowired
    private ExercisesService exercisesService;
    @Autowired
    private ExerciseRecordService exerciseRecordService;
    @Autowired
    private VideoClassService videoClassService;
    @Autowired
    private DocumentationService documentationService;
    @Autowired
    private UserService userService;
    @Autowired
    private StudyRecordDao studyRecordDao;
    @Autowired
    private StudyProgressService studyProgressService;
    @Autowired
    private StudentCourseService studentCourseService;

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void saveProgress(Integer studentId, List<StudyRecordVO> recordVOList) {
        //1判断章节类型
        Exercises exercises = exercisesService.get(recordVOList.get(0).getExerciseId());
        if (exercises.getDocumentation() != null && exercises.getVideoClass() == null) {
            //1.获取该章节练习题个数
            Integer countExercise = exercisesService.countExercisesInChapter(exercises.getDocumentation(), DOCUMENT);
            if (countExercise < 1) {
                throw new BaseException(ResultEnum.DATA_NOT_FIND);
            }

            Integer countRecord = exerciseRecordService.countStudentAndDocument(studentId, exercises.getDocumentation());
            //计算进度
            Double progress = (countRecord.doubleValue() / countExercise.doubleValue()) * 100;

            StudyProgress studyProgress = new StudyProgress();
            studyProgress.setDocumentation(documentationService.get(exercises.getDocumentation()));
            studyProgress.setProgress(BigDecimal.valueOf(progress));

            studyProgress.setStudent(userService.get(studentId));
            studyProgressDao.save(studyProgress);
        }
    }

    @Override
    public Result getVideoClassProgress(Integer studentId, Integer videoClassId) {
        StudyProgress studyProgress = studyProgressDao.findByVideoClass_IdAndStudent_Id(videoClassId, studentId);
        if (studyProgress == null) {
            throw new BaseException(ResultEnum.DATA_NOT_FIND);
        }
        return ResultUtil.success(ResultEnum.FIND_SUCCESS, studyProgress.getTimeRecord());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(Integer userId, String type, Integer chapterId, Double timeRecord) {
        StudyProgress studyProgress = new StudyProgress();
        studyProgress.setStudent(userService.get(userId));
        if (CourseEnum.VIDEO.getMessage().equals(type)) {
            studyProgress.setVideoClass(videoClassService.get(chapterId));
            studyProgress.setTimeRecord(BigDecimal.valueOf(timeRecord == null ? 0.00d : timeRecord));
        } else if (DOCUMENT.getMessage().equals(type)) {
            studyProgress.setDocumentation(documentationService.get(chapterId));
        }
        studyProgressDao.save(studyProgress);
    }


    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public Boolean updatePersonalProgress(Integer chapterId, Integer userId, Double timeRecord, String type) {
        //判断是否已报名
        if (studentCourseService.ifSubscribeByChapter(chapterId, type, userId)) {
            //获取学习进度
            StudyProgress studyProgress = null;
            if (CourseEnum.VIDEO.getMessage().equals(type)) {
                studyProgress = studyProgressDao.findByVideoClass_IdAndStudent_Id(chapterId, userId);
            } else if (DOCUMENT.getMessage().equals(type)) {
                studyProgress = studyProgressDao.findByDocumentation_IdAndStudent_Id(chapterId, userId);
            }
            //不存在该章节学习进度
            if (studyProgress == null) {
                //新增学习进度
                create(userId, type, chapterId, timeRecord);
            }
            //存在该章节学习进度
            //更新学习记录
            StudyRecord studyRecord = new StudyRecord();
            studyRecord.setStudentId(userId);
            studyRecord.setAction(READ.getMessage());
            if (CourseEnum.VIDEO.getMessage().equals(type)) {
                studyRecord.setVideoClassId(chapterId);
            } else if (DOCUMENT.getMessage().equals(type)) {
                studyRecord.setDocumentId(chapterId);
            }
            try {
                studyRecordDao.save(studyRecord);
            } catch (Exception e) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                throw new BaseException(UPDATE_FAIL, e);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public StudyProgress getStudyProgress(Integer id, Integer userId, CourseEnum type) {
        StudyProgress studyProgress = null;
        if (VIDEO.equals(type)) {
            studyProgress = studyProgressDao.findByVideoClass_IdAndStudent_Id(id, userId);
        }
        if (DOCUMENT.equals(type)) {
            studyProgress = studyProgressDao.findByDocumentation_IdAndStudent_Id(id, userId);
        }
        if (studyProgress == null) {
            return new StudyProgress();
        } else {
            return studyProgress;

        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void saveVideoTime(Integer userId, Integer videoClassId, String time, Integer isVideoPass) {

        StudyProgress studyProgress = studyProgressDao.findByVideoClass_IdAndStudent_Id(videoClassId, userId);

        studyProgress.setTimeRecord(BigDecimal.valueOf(Math.max(Double.parseDouble(time), studyProgress.getTimeRecord().doubleValue())));
        studyProgress.setIsVideoPass(isVideoPass);
//        studyProgress = studyProgressService.saveVideoProgress(userId, videoClassId, Double.parseDouble(time), studyProgress);
        VideoClass videoClass = videoClassService.get(videoClassId);
        Integer allTime = videoClass.getVideo().getDuration();
        //获取此学生在此章节的视频最长已看时长记录
        double newCountTime = Double.parseDouble(time);
        Double oldCountTime = studyProgressDao.findTimeRecord(videoClass.getId(), userId);
        //计算进度
        Double progress = (Math.max(newCountTime, oldCountTime) * 100d / allTime.doubleValue());

        studyProgress.setProgress(BigDecimal.valueOf(progress));
        try {
            studyProgressDao.save(studyProgress);

            studyProgressService.updatePersonalProgress(videoClassId, userId, Double.parseDouble(time), VIDEO.getMessage());

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }

    @Override
    public Integer countFinishDocument(Integer id, Integer userId) {
        return studyProgressDao.countFinishDocument(id, userId).intValue();
    }

    @Override
    public Integer countFinishVideoClass(Integer id, Integer userId) {
        return studyProgressDao.countFinishVideoClass(id, userId).intValue();
    }

    @Override
    public List<Double> getVideoProgressByCourseAndStudent(Integer courseId, Integer userId) {
        List<Double> data = studyProgressDao.findVideoProgressByCourseAndUser(courseId, userId);
        if (CollectionUtils.isEmpty(data)) {
            data.add(0d);
            return data;
        } else {
            for (Double d : data) {
                if (d == null) {
                    d = 0d;
                }
            }
        }
        return data;
    }

    @Override
    public PageData<StudyRecordListVO> getLatestStudyRecord(Integer userId, Integer page, Integer limit) {
        Pageable pageable = PageRequest.of(page - 1, limit);
        Page<StudyRecord> pageData = studyRecordDao.findByStudent(userId, pageable);
        if (CollectionUtils.isEmpty(pageData.getContent())) {
            throw new NoDataException("page");
        }
        List<StudyRecord> list = pageData.getContent();
        List<StudyRecordListVO> voList = new ArrayList<>(list.size());
        for (StudyRecord studyRecord : list) {
            StudyRecordListVO vo = new StudyRecordListVO();
            if (studyRecord.getDocumentId() != null) {
                Documentation document = documentationService.get(studyRecord.getDocumentId());
                vo.setChapterId(studyRecord.getDocumentId());
                vo.setCourseName(document.getCourse().getName());
                vo.setChapterName(document.getName());
                vo.setTimeRecord("");
            } else {
                VideoClass videoClass = videoClassService.get(studyRecord.getVideoClassId());
                vo.setChapterId(studyRecord.getVideoClassId());
                vo.setCourseName(videoClass.getCourse().getName());
                vo.setChapterName(videoClass.getName());
                double time = studyProgressService.getStudyProgress(studyRecord.getVideoClassId(), userId, VIDEO).getTimeRecord().doubleValue();
                int hour = (int) (time / (60 * 60));
                int min = (int) ((time - hour * 3600) / 60);
                int second = (int) (time - hour * 3600 - min * 60);
                vo.setTimeRecord(hour + ":" + min + ":" + second);
            }
            voList.add(vo);
        }
        return new PageData<>((int) pageData.getTotalElements(), limit, page, voList);
    }
}

