package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.dao.*;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.enums.ScheduledEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.service.ExamClassInfoService;
import com.gibs.peixunapi.service.ExamStudentService;
import com.gibs.peixunapi.service.ExamTeacherService;
import com.gibs.peixunapi.service.ScheduledService;
import com.gibs.peixunapi.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.LinkEnum.*;
import static com.gibs.peixunapi.enums.ResultEnum.EXPIRY_ERROR;
import static com.gibs.peixunapi.enums.ResultEnum.START_COURSE_ERROR;

@Service
@Slf4j
public class ScheduledServiceImpl implements ScheduledService {
    @Autowired
    private ScheduledDao scheduledDao;
    @Autowired
    private ExamClassInfoDao examClassInfoDao;
    @Autowired
    private ExamClassInfoService examClassInfoService;
    @Autowired
    private ExamStudentDao examStudentDao;
    @Autowired
    private NoticeServiceImpl noticeService;
    @Autowired
    private CertificateStudentDao certificateStudentDao;
    @Autowired
    private StudentCourseDao studentCourseDao;
    @Autowired
    private ExamTeacherService examTeacherService;
    @Autowired
    private CertificateClassDao certificateClassDao;
    @Autowired
    private ExamStudentService examStudentService;


    @Override
    public void tests() {
        log.info("test success ");
    }


    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void startCourse(Integer courseId) {
        try {
            studentCourseDao.startCourse(courseId);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(START_COURSE_ERROR, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void finishCourse(Integer courseId) {
        try {
            studentCourseDao.finishCourse(courseId);

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(START_COURSE_ERROR, e);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void startExam(Integer classId) {
        ExamClassInfo examClassInfo = examClassInfoService.get(classId);
        examClassInfo.setExamStatus(0);
        try {
            examClassInfoDao.save(examClassInfo);
            examStudentDao.startExam(classId);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void completeExam(Integer classId) {
        ExamClassInfo examClassInfo = examClassInfoService.get(classId);
        examClassInfo.setExamStatus(1);
        examClassInfo.setCorrectStatus(0);

        try {
            examClassInfoDao.save(examClassInfo);
            List<String> list = examStudentDao.findUnFinish(classId);
            if (!CollectionUtils.isEmpty(list)) {
                for (String uniqueCode : list) {
                    examStudentService.submitPaper(uniqueCode);
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void startCorrect(Integer classId) {
        ExamClassInfo examClassInfo = examClassInfoService.get(classId);
        examClassInfo.setCorrectStatus(0);
        try {
            examClassInfoDao.save(examClassInfo);
//            examStudentDao.correctStart(classId);

            /*消息通知*/
            String content = "您好，" + examClassInfo.getCreator().getUsername() + "管理员/老师邀请您参与于" +
                    examClassInfo.getCorrectStartTime() +
                    "组织的" +
                    examClassInfo.getName() +
                    "培训认证考试阅卷工作。了解详情，请在本系统考试中心页面中查询。具体可以联系相关管理员。";
            List<OptionVO> optionVOList = examTeacherService.getTeacherList(classId, CourseEnum.ASSIST_TEACHER);
            noticeService.sendNoticeToUser("阅卷通知", content, EXAM_MANAGE.getLink(),
                    optionVOList.stream().map(OptionVO::getId).collect(Collectors.toList()), SYSTEM_CALL.getNum());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void finishCorrect(Integer classId) {
        try {
            List<String> list = examStudentDao.findUnCorrect(classId);
            if (!CollectionUtils.isEmpty(list)) {
                for (String uniqueCode : list) {
                    examStudentService.finishCorrect(uniqueCode);
                }
            }
            //结束状态
            ExamClassInfo examClassInfo = examClassInfoService.get(classId);
            examClassInfo.setCorrectStatus(1);
            examClassInfoDao.save(examClassInfo);
            //统计通过人数
            Integer number = examStudentService.countPassExamNumber(classId);
            CertificateClass certificateClass = certificateClassDao.findByClass(classId);

            if (certificateClass != null) {
                certificateClass.setPassCount(number);
                certificateClassDao.save(certificateClass);
            }

        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void announcementScore(Integer classId) {
        try {
            List<ExamStudent> list = examStudentDao.findByExamClass("", classId);

            Map<Integer, String> contentIdMap = new HashMap<>(list.size());
            BigDecimal passScore = list.get(0).getExamClassInfo().getExamInfo().getPassScore();

            for (ExamStudent examStudent : list) {
                StringBuilder content = new StringBuilder("您好，您于" + DateUtils.toDateZNString(examStudent.getExamClassInfo().getExamStartTime()) +
                        "参加的" + examStudent.getExamClassInfo().getExamInfo().getCourse().getName() + "培训认证考试成绩已公布!" +
                        "您的最终得分为：" + examStudent.getFinalScore() + "，成绩判定为");
                if (examStudent.getFinalScore() != null && passScore.compareTo(examStudent.getFinalScore()) >= 0) {
                    content.append("合格");
                } else {
                    content.append("不合格");
                }
                content.append("如若需要了解此次考试详情，可以到系统考试中心查看");
                contentIdMap.put(examStudent.getId(), content.toString());
            }
            noticeService.sendNoticeListToUser("成绩公布", contentIdMap, SCORE_MANAGE.getLink(), SYSTEM_CALL.getNum());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(EXPIRY_ERROR, e);
        }
    }


    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void certificateExpiry() {
        try {
            certificateStudentDao.setExpiry(new Date());
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(EXPIRY_ERROR, e);
        }
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void certificateWarning() {

        Integer day = 15;
        List<CertificateStudent> list = certificateStudentDao.findComingExpiry(new Date(), day);

        Map<Integer, String> contentIdMap = new HashMap<>(list.size());
        String courseName = list.get(0).getExamStudent().getExamClassInfo().getExamInfo().getCourse().getName();
        for (CertificateStudent o : list) {

            String content = "您好，您于" +
                    DateUtils.toDateZNString(o.getCreateTime()) + "获得的" +
                    courseName + "培训的" + o.getName() + "证书，证书编号为:" + o.getUniqueCode() +
                    "。该证书即将到期，请尽快申请续期工作。了解详情，请在系统证书中查询。";
            contentIdMap.put(o.getUserId(), content);
        }
        noticeService.sendNoticeListToUser("证书即将过期", contentIdMap, MANAGE_CERTIFICATE.getLink(), SYSTEM_CALL.getNum());

    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void certificateUpdatePause() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<CertificateStudent> list = certificateStudentDao.getPause();
        list.forEach(i -> {
            LocalDateTime oldDate = LocalDateTime.parse(sdf.format(i.getExpiryDate()));
            LocalDateTime newDate = oldDate.plusDays(+1);
            try {
                i.setExpiryDate(sdf.parse(newDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
            } catch (ParseException e) {
                throw new BaseException(EXPIRY_ERROR, e);
            }
        });
        try {

            certificateStudentDao.saveAll(list);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(EXPIRY_ERROR, e);
        }
    }


    @Override
    public void
    create(String taskName, ScheduledEnum enums, String paramId, Date date) {

        SimpleDateFormat sdf = new SimpleDateFormat("ss mm HH dd MM ? yyyy");
        String cron = sdf.format(date);

        Scheduled scheduled = new Scheduled();
        scheduled.setTaskName(taskName);
        scheduled.setClassName(enums.getClassName());
        scheduled.setMethodName(enums.getMethodName());
        scheduled.setCron(cron);
        scheduled.setParamId(paramId);
        scheduled.setDate(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString());
        scheduled.setStatus(0);
        scheduled.setIsEnable(0);
        scheduled.setCreateTime(new Date());

        scheduledDao.save(scheduled);
    }


}
