package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.post.BusinessVO;
import com.gibs.peixunapi.VO.show.BusinessListVO;
import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.dao.BusinessDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.mapper.BusinessMapper;
import com.gibs.peixunapi.mapper.DepartmentMapper;
import com.gibs.peixunapi.model.Business;
import com.gibs.peixunapi.model.Department;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.BusinessService;
import com.gibs.peixunapi.service.UserService;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static com.gibs.peixunapi.enums.ResultEnum.BUSINESS_DISABLE;

/**
 * @author liangjiawei
 */
@Service
@Slf4j
public class BusinessServiceImpl implements BusinessService {

    @Autowired
    private BusinessDao businessDao;
    @Autowired
    private BusinessMapper businessMapper;
    @Autowired
    private DepartmentMapper departmentMapper;
    @Autowired
    private BusinessService businessService;
    @Autowired
    private UserService userService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result create(BusinessVO vo) {
        Business business = new Business();
        vo.setId(null);
        ConverterUtil.copyNotNullProperties(vo, business);
        try {
            businessDao.save(business);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
        return ResultUtil.success(ResultEnum.CREATE_SUCCESS);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result save(BusinessVO vo) {
        Business business = businessDao.findById(vo.getId()).orElseThrow(EntityNotFoundException::new);

        ConverterUtil.copyProperties(vo, business, "id", "manager", "invitedCode", "sort");
        if (vo.getManagerId() != null) {
            User user = userService.get(vo.getManagerId());
            business.setManagerId(user.getId());
        }
        try {
            businessDao.save(business);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }

        return ResultUtil.success(ResultEnum.UPDATE_SUCCESS);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public Boolean delete(Integer id) {
        userService.unBindDeleteUserInBusiness(id);
        businessDao.deleteById(id);
        return true;
    }

    @Override
    public Business get(Integer id) {
        Business business = businessDao.findById(id).orElseThrow(EntityNotFoundException::new);
//        if (business.getStatus() == 0) {
//            throw new BaseException(BUSINESS_DISABLE);
//        }
        return business;
    }

    @Override
    public BusinessVO getVO(Integer id) {
        Business business = businessDao.findById(id).orElseThrow(EntityNotFoundException::new);
        BusinessVO vo = new BusinessVO();
        ConverterUtil.copyProperties(business, vo);
        if (business.getManagerId() != null) {
            vo.setManagerName(userService.get(business.getManagerId()).getUsername());
        }
        return vo;
    }

    @Override
    public Business getByInvitedCode(String invitedCode) {
        Business business = businessDao.findByInvitedCode(invitedCode);
        if (business == null) {
            throw new BaseException(ResultEnum.DATA_NOT_FIND);
        }
        if (business.getStatus() == 0) {
            throw new BaseException(BUSINESS_DISABLE);
        }
        return business;
    }


    @Override
    public PageData<BusinessListVO> getListPage(String name, Integer status, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<BusinessListVO> pageDate = new PageInfo<>(businessMapper.getIdListByLikeNameAndStatus(name, status));
        List<BusinessListVO> list = pageDate.getList();
        if (Collections.isEmpty(list)) {
            throw new NoDataException("page");
        }
        for (BusinessListVO vo : list) {
            Integer manageId = userService.getManagerByBusiness(vo.getId());
            if (manageId != null) {
                User manage = userService.get(manageId);
                vo.setManagerName(manage == null ? "" : manage.getUsername());
            } else {
                vo.setManagerName("");

            }
            vo.setNumber(userService.countByBusinessId(vo.getId()));
            vo.setSort((list.indexOf(vo) + 1) * 10 * page);
        }
    return new PageData<>((int) pageDate.getTotal(), limit, page, list);

    }


    @Override
    public List<OptionVO> getOptions(String key) {
        List<OptionVO> list = businessMapper.getOptions();
        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
       return  list;
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void changeStatus(Integer businessId, Integer status) {
        Business business = businessService.get(businessId);
        business.setStatus(status);
        try {
            businessDao.save(business);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public Integer countByBusiness(Integer businessId) {
        return businessDao.countByBusiness(businessId);
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void saveManager(Business business) {
        try {
            businessDao.save(business);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public List<OptionVO> getDepartmentOptions() {
        List<OptionVO> list = departmentMapper.getOptions();
        if (Collections.isEmpty(list)) {
            throw new NoDataException();
        }
        return  list;
    }
}