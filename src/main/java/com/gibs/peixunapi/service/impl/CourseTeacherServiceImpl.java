package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.dao.CourseTeacherDao;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.mapper.CourseTeacherMapper;
import com.gibs.peixunapi.model.CourseTeacher;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.service.CourseTeacherService;
import com.gibs.peixunapi.service.UserService;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.CourseEnum.ASSIST_TEACHER;
import static com.gibs.peixunapi.enums.CourseEnum.TEACHER;
import static com.gibs.peixunapi.enums.ResultEnum.DATA_NOT_FIND;
import static com.gibs.peixunapi.enums.ResultEnum.NO_TEACHER;

/**
 * @author liangjiawei
 * @date 2020/09/18/16:10
 * @Version 1.0
 * @Description:
 */

@Service
@Slf4j
public class CourseTeacherServiceImpl implements CourseTeacherService {
    @Autowired
    private CourseTeacherDao courseTeacherDao;
    @Autowired
    private CourseTeacherMapper courseTeacherMapper;
    @Autowired
    private UserService userService;

    @Override
    public CourseTeacher getTeacher(Integer courseId) {
        List<CourseTeacher> list = courseTeacherDao.findByCourseIdAndType(courseId, TEACHER.getCode());
        if (Collections.isEmpty(list)) {
            return new CourseTeacher();
        }
        return list.get(0);
    }

    @Override
    public OptionVO getTeacherVO(Integer courseId) {
        List<CourseTeacher> list = courseTeacherDao.findByCourseIdAndType(courseId, TEACHER.getCode());
        if (Collections.isEmpty(list)) {
            throw new BaseException(DATA_NOT_FIND);
        }
        return new OptionVO(list.get(0).getTeacher().getId(), null, list.get(0).getTeacher().getUsername(), null);
    }

    @Override
    public List<OptionVO> getAssistantOption(Integer courseId) {
        List<CourseTeacher> list = courseTeacherDao.findByCourseIdAndType(courseId, ASSIST_TEACHER.getCode());
        if (Collections.isEmpty(list)) {
            return new ArrayList<>();
        }
        return list.stream()
                .map(i -> new OptionVO(i.getTeacher().getId(), null, i.getTeacher().getUsername(), null))
                .collect(Collectors.toList());
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void saveManage(Integer courseId, User teacher) {
        try {
            CourseTeacher courseTeacher = new CourseTeacher(courseId, teacher, TEACHER);
            courseTeacherDao.save(courseTeacher);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveAssist(Integer courseId, List<Integer> assistIdList) {
        try {
            List<CourseTeacher> list = new ArrayList<>(assistIdList.size());
            for (Integer i : assistIdList) {
                User assist = userService.get(i);
                list.add(new CourseTeacher(courseId, assist, ASSIST_TEACHER));
            }
            courseTeacherDao.saveAll(list);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public List<Integer> getTeachersId(Integer chapterId, CourseEnum courseEnum) {
        List<Integer> idList = courseTeacherMapper.findTeachersId(chapterId, courseEnum.getMessage());
        if (CollectionUtils.isEmpty(idList)) {
            throw new BaseException(NO_TEACHER);
        }
        return idList;
    }
}
