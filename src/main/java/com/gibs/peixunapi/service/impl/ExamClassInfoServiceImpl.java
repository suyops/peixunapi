package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.post.ExamClassVO;
import com.gibs.peixunapi.VO.post.ExamInfoVO;
import com.gibs.peixunapi.VO.show.*;
import com.gibs.peixunapi.dao.ExamClassInfoDao;
import com.gibs.peixunapi.dao.ExamInfoDao;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.mapper.ExamClassInfoMapper;
import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.DateUtils;
import com.gibs.peixunapi.utils.ResultUtil;
import com.gibs.peixunapi.utils.UUIDUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.base.Joiner;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.CourseEnum.CORRECT;
import static com.gibs.peixunapi.enums.CourseEnum.MANAGE;
import static com.gibs.peixunapi.enums.LinkEnum.EXAM_MANAGE;
import static com.gibs.peixunapi.enums.LinkEnum.SYSTEM_CALL;
import static com.gibs.peixunapi.enums.ResultEnum.CODE_NOT_EXIST;
import static com.gibs.peixunapi.enums.RoleEnum.GROUP_ADMIN;
import static com.gibs.peixunapi.enums.ScheduledEnum.*;


@Slf4j

@Service
public class ExamClassInfoServiceImpl implements ExamClassInfoService {

    @Autowired
    private ExamClassInfoDao examClassInfoDao;
    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private ExamClassInfoService examClassInfoService;
    @Autowired
    private ExamClassInfoMapper examClassInfoMapper;
    @Autowired
    private ExamInfoService examInfoService;
    @Autowired
    private ExamTeacherService teacherService;
    @Autowired
    private ExamStudentService examStudentService;
    @Autowired
    private CourseTeacherService courseTeacherService;
    @Autowired
    private ExamTeacherService examTeacherService;
    @Autowired
    private StudentCourseService studentCourseService;
    @Autowired
    private NoticeService noticeService;
    @Autowired
    private ScheduledService scheduledService;
    @Autowired
    private UserService userService;
    @Autowired
    private TestPaperService testPaperService;

    @Override
    public ExamClassInfo get(Integer id) {
        return examClassInfoDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Boolean isTestPaperUsed(Integer testPaperId) {
        //考试表中是否存在使用此试卷的考试
        List<ExamInfo> examInfoList = examInfoService.findExamInfosByTestPaper_Id(testPaperId);
        if (Collections.isEmpty(examInfoList)) {
            return false;
        }
        //考试班级中是否有已经开始考试的
        List<Integer> idList = examClassInfoDao.isExamClassStart(examInfoList).stream()
                .map(e -> Integer.parseInt(e.toString()))
                .collect(Collectors.toList());
        return !Collections.isEmpty(idList);
    }

    @Override
    public Integer countFinishClass(Integer courseId) {
        return examClassInfoDao.countFinishClass(courseId);
    }

    @Override
    public Integer countClass(Integer courseId) {
        return examClassInfoDao.countClass(courseId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result create(ExamClassVO examClassVO, User user) {
        try {
            //保存examInfo
            ExamInfoVO examInfoVO = new ExamInfoVO();
            ConverterUtil.copyProperties(examClassVO, examInfoVO);
            //设置结束时间
            LocalDateTime startTime =LocalDate.parse( examClassVO.getExamStartTime()).atTime(LocalTime.MIDNIGHT) ;
            LocalDateTime endTime = LocalDate.parse( examClassVO.getExamEndTime()).atTime(LocalTime.MIDNIGHT) ;
            Duration between = Duration.between(startTime, endTime);
            String date = between.toDays() + "天" + (between.toHours() - (between.toDays() * 24)) + "时" + (between.toMinutes() - (between.toHours() * 60)) + "分";
            examInfoVO.setDurationTime(date);
            ExamInfo examInfo = examInfoService.create(examInfoVO, user);

            //1.保存examClassInfo
            ExamClassInfo examClass = new ExamClassInfo();
            //设置examInfo
            examClass.setExamInfo(examInfo);
            ConverterUtil.copyProperties(examClassVO, examClass);

            examClass.setProportion(examClassVO.getProportion() == null ? BigDecimal.valueOf(100) : BigDecimal.valueOf(examClassVO.getProportion().doubleValue() / 100));

            //保存创建人
            examClass.setCreator(user);
            examClass.setCode(UUIDUtil.get8UUID().toUpperCase());

            ExamClassInfo newClass = examClassInfoDao.save(examClass);

            //3.保存阅卷老师
            List<Integer> correctIds = new ArrayList<>();
            if (examClassVO.getCorrectTeacher().contains(",")) {
                correctIds = Arrays.stream(examClassVO.getCorrectTeacher().split(",")).map(Integer::parseInt).collect(Collectors.toList());
            } else {
                correctIds.add(Integer.parseInt(examClassVO.getCorrectTeacher()));
            }
            teacherService.saveExamTeachers(correctIds, newClass.getId(), CORRECT);

            try {
                List<StudentSignUpTypeVO> id = studentCourseService.getStudentListByCourseOrBusiness(examInfoVO.getCourseId(), 1, null);
                noticeService.sendNoticeToUser("新增考试", "", EXAM_MANAGE.getLink(), id.stream().map(StudentSignUpTypeVO::getId).collect(Collectors.toList())
                        , SYSTEM_CALL.getNum());
            } catch (NoDataException e) {
                log.info("考试id:5场次id:11暂无考生");
            }
            Map<String, Integer> result = new HashMap<>();
            result.put("classId", newClass.getId());
            result.put("courseId", newClass.getExamInfo().getCourse().getId());
            /*添加定时任务*/
            scheduledService.create("startClass" + newClass.getId(), START_EXAM, newClass.getId().toString(), newClass.getExamStartTime());
            scheduledService.create("finishClass" + newClass.getId(), FINISH_EXAM, newClass.getId().toString(), newClass.getExamEndTime());
            scheduledService.create("startCorrect" + newClass.getId(), START_CORRECT, newClass.getId().toString(), newClass.getCorrectStartTime());
            scheduledService.create("finishCorrect" + newClass.getId(), FINISH_CORRECT, newClass.getId().toString(), newClass.getCorrectEndTime());
            scheduledService.create("announcementScore" + newClass.getId(), ANNOUNCEMENT_SCORE, newClass.getId().toString(), newClass.getAnnounceResultTime());

            return ResultUtil.success(ResultEnum.CREATE_SUCCESS, result);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    public String minConvertDayHourMin(Double second) {
        String html = "0分";
        if (second != null) {
            double m;
            m = second;
            String format;
            Object[] array;
            int days = (int) (m / (60 * 24));
            int hours = (int) (m / (60) - days * 24);
            int minutes = (int) (m - (hours + days * 24) * 60);
            if (days > 0) {
                format = "%1$,d天%2$,d时%3$,d分";
                array = new Object[]{days, hours, minutes};
            } else if (hours > 0) {
                format = "%1$,d时%2$,d分";
                array = new Object[]{hours, minutes};
            } else {
                format = "%1$,d分";
                array = new Object[]{minutes};
            }
            html = String.format(format, array);
        }
        return html;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public Result changeNumber(int length, Integer examClassId, String type) {

        ExamClassInfo examClassInfo = examClassInfoDao.findById(examClassId).orElseThrow(EntityNotFoundException::new);
        if ("add".equals(type)) {
            examClassInfo.setNumber(examClassInfo.getNumber() + length);
        } else if ("delete".equals(type)) {
            examClassInfo.setNumber(examClassInfo.getNumber() - length);
        }
        return ResultUtil.success(ResultEnum.UPDATE_SUCCESS);
    }

    @Override
    public PageData getExamClassList(String key, Integer page, Integer limit) {
        Pageable pageable = PageRequest.of(page - 1, limit);
        User user = userService.getAttributeUser();
        Page<ExamClassInfo> dataPage = null;
        try {
            if (GROUP_ADMIN.getCode().equals(user.getRole())) {
                dataPage = examClassInfoDao.findByExamInfo(key, pageable);
            } else {
                Integer userId = user.getId();
                dataPage = examClassInfoDao.findByStudent(userId, pageable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<ExamClassInfo> list = dataPage.getContent();
        if (Collections.isEmpty(list)) {
            throw new NoDataException("page");
        }
        List<ExamClassListVO> voList = new ArrayList<>(list.size());

        for (ExamClassInfo examClassInfo : list) {
            ExamClassListVO vo = new ExamClassListVO();
            ConverterUtil.copyProperties(examClassInfo, vo);
            CourseTeacher teacher = courseTeacherService.getTeacher(examClassInfo.getExamInfo().getCourse().getId());
            vo.setTeacherName(teacher.getTeacher().getUsername());
            vo.setCourseName(examClassInfo.getExamInfo().getCourse().getName());
            vo.setCourseId(examClassInfo.getExamInfo().getCourse().getId());
            vo.setExamInfoId(examClassInfo.getExamInfo().getId());
            if (examClassInfo.getExamStatus() == -1) {
                vo.setExamStatusStr("等待开始");
            }
            if (examClassInfo.getExamStatus() == 0) {
                vo.setExamStatusStr("考试中");
            }
            if (examClassInfo.getExamStatus() == 1) {
                vo.setExamStatusStr("考试完成");
            }
            vo.setDuration(examClassInfo.getExamInfo().getDurationTime());
            voList.add(vo);
        }
        return new PageData<>((int) dataPage.getTotalElements(), limit,
                page, voList);
    }

    @Override
    public ShowExamClassVO getExamClassVO(Integer classId) {
        ExamClassInfo examClassInfo = get(classId);
        ShowExamClassVO vo = new ShowExamClassVO();
        ConverterUtil.copyProperties(examClassInfo, vo);
        vo.setNumber(examStudentService.getClassNumber(classId));
        vo.setManageList(examTeacherService.getTeacherList(classId, MANAGE));
        vo.setCorrectList(examTeacherService.getTeacherList(classId, CORRECT));
        vo.setCourseName(examClassInfo.getExamInfo().getCourse().getName());
        vo.setExamInfo(examClassInfo.getExamInfo());
        return vo;
    }

    @Override
    public Integer findByCode(String code) {
        Integer id = examClassInfoDao.findByCode(code);
        if (id == null) {
            throw new BaseException(CODE_NOT_EXIST);
        } else {
            return id;
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void edit(ExamClassVO examClass, ExamInfo examInfo) {
        try {
            //1.保存examClassInfo
            ExamClassInfo classInfo = get(examClass.getId());
            classInfo.setExamInfo(examInfo);

            if (examClass.getExamStartTime() != null && !classInfo.getExamEndTime().equals(examClass.getExamStartTime())) {
                LocalDateTime localTime =LocalDate.parse(examClass.getExamStartTime()).atTime(LocalTime.MIDNIGHT) ;
                LocalDateTime endTime = LocalDate.parse(examClass.getExamEndTime()).atTime(LocalTime.MIDNIGHT) ;
                long minutes;
                minutes = Duration.between(localTime, endTime).toMinutes();
                examClass.setDurationTime(minConvertDayHourMin((double) minutes));
            }

            //2.保存监考老师
            List<Integer> manageIds = new ArrayList<>();
            if (examClass.getManageTeacher().contains(",")) {
                manageIds = Arrays.stream(examClass.getManageTeacher().split(",")).map(Integer::parseInt).collect(Collectors.toList());
            } else {
                manageIds.add(Integer.parseInt(examClass.getManageTeacher()));
            }
            teacherService.saveExamTeachers(manageIds, examClass.getId(), MANAGE);
            //3.保存阅卷老师
            List<Integer> correctIds = new ArrayList<>();
            if (examClass.getManageTeacher().contains(",")) {
                correctIds = Arrays.stream(examClass.getCorrectTeacher().split(",")).map(Integer::parseInt).collect(Collectors.toList());
            } else {
                correctIds.add(Integer.parseInt(examClass.getCorrectTeacher()));
            }
            teacherService.saveExamTeachers(correctIds, examClass.getId(), CORRECT);
            examClass.setName(classInfo.getName());
            ConverterUtil.copyDifferentPropertiesExcept(examClass, classInfo, "name");

            examClassInfoDao.save(classInfo);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public Boolean isStartExam(Integer id) {
        ExamClassInfo classInfo = get(id);
         /* 转化Date为LocalDateTime 方便计算
            1.Date转化为Instant
            2.加上时区
            3.使用LocalDateTime.ofInstant转换Instant类*/
        LocalDateTime time = classInfo.getExamStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime now = LocalDateTime.now();

        return now.isEqual(time) || now.isAfter(time);
    }

    @Override
    public List<Integer> findWaitingCorrectIdList(String name, Integer userId) {

        List<Integer> idList = examClassInfoMapper.findWaitingCorrectIdList(name, userId);
        if (Collections.isEmpty(idList)) {
            throw new NoDataException();
        }
        return idList;
    }

    @Override
    public List<CorrectManageVO> getCorrectManageList(String key) {

        User user = userService.getAttributeUser();
        Integer userId = null;
        if (!GROUP_ADMIN.getCode().equals(user.getRole())) {
            userId = user.getId();
        }
        List<Integer> idList = examClassInfoService.findWaitingCorrectIdList(key, userId);
        List<ExamClassInfo> examClassInfoList = examClassInfoDao.findByIdIn(idList, Joiner.on(",").join(idList));
        if (Collections.isEmpty(examClassInfoList)) {
            throw new NoDataException();
        }
        List<CorrectManageVO> voList = new ArrayList<>();
        for (ExamClassInfo examClassInfo : examClassInfoList) {
            CorrectManageVO vo = new CorrectManageVO();
            ConverterUtil.copyProperties(examClassInfo, vo);
            vo.setExamNum(examStudentService.countExamNumber(examClassInfo.getId()));
            vo.setCourseName(examClassInfo.getExamInfo().getCourse().getName());
            vo.setTeacherName(examTeacherService.getTeacherNames(examClassInfo.getId(), CourseEnum.CORRECT));
            vo.setExamInfoId(examClassInfo.getExamInfo().getId());
            if (vo.getCorrectStatus() == 1) {
                vo.setCorrectStatusString("阅卷完成");
            } else if (vo.getCorrectStatus() == 0) {
                vo.setCorrectStatusString("阅卷中");
            } else {
                vo.setCorrectStatusString("等待阅卷");
            }

            voList.add(vo);
        }
        return voList;
    }

    @Override
    public List<Integer> findIdByStartedExam(String key, Date time, Integer userId) {
        List<Integer> idList = examClassInfoMapper.findIdByStartedExam(key, DateUtils.toString(time), userId);
        if (Collections.isEmpty(idList)) {
            throw new NoDataException();
        }
        return idList;
    }

    @Override
    public List<ExamManageVO> getExamManageList(String key) {
        User user = userService.getAttributeUser();
        Integer userId = null;
        if (!GROUP_ADMIN.getCode().equals(user.getRole())) {
            userId = user.getId();
        }
        List<Integer> idList = examClassInfoService.findIdByStartedExam(key, new Date(), userId);
        List<ExamClassInfo> examClassInfoList = examClassInfoDao.findByIdIn(idList, Joiner.on(",").join(idList));

        List<ExamManageVO> voList = new ArrayList<>(examClassInfoList.size());
        for (ExamClassInfo examClassInfo : examClassInfoList) {
            ExamManageVO vo = new ExamManageVO();
            ConverterUtil.copyProperties(examClassInfo, vo);
            vo.setCourseName(examClassInfo.getExamInfo().getCourse().getName());
            vo.setDurationTime(examClassInfo.getExamInfo().getDurationTime());
            vo.setStudentCount(examStudentService.getClassNumber(examClassInfo.getId()));
            vo.setSort((examClassInfoList.indexOf(examClassInfo) + 1));
            voList.add(vo);
        }
        return voList;
    }

    @Override
    public List<StudentExamCenterVO> findClassInCourse(String key, Integer userId) {
        return examClassInfoMapper.findClassInCourse(key, userId, new Date());
    }


    @Override
    public List<StudentExamCenterVO> getStudentExamClassList(String key, Integer studentId) {
        //TODO 重写
//        List<StudentCourse> studentCourseList = studentCourseService.getStudentSignUpList(studentId, true);
//        List<Integer> idList = studentCourseList.stream().map(StudentCourse::getCourse).map(Course::getId).collect(Collectors.toList());
        List<StudentExamCenterVO> examClassInfoList = examClassInfoService.findClassInCourse(key,studentId);
        if (Collections.isEmpty(examClassInfoList)) {
            throw new NoDataException();
        }
        List<StudentExamCenterVO> voList = new ArrayList<>(examClassInfoList.size());
        for (StudentExamCenterVO vo : examClassInfoList) {
            vo.setSort((examClassInfoList.indexOf(vo) + 1));
            vo.setClassId(vo.getId());
            ExamStudent examStudent = examStudentService.getByStudentAndClass(vo.getId(), studentId);

            if (examStudent.getExamStatus() == -1) {
                vo.setSignUpStatus("未开始");
            } else if (examStudent.getExamStatus() == 0) {
                vo.setSignUpStatus("待考试");
            } else if (examStudent.getExamStatus() == 1) {
                vo.setSignUpStatus("已考试");
            }
            if (examStudent != null) {
                vo.setApplyDelayedExam(examStudent.getApplyDelayedExam());
                vo.setUniqueCode(examStudent.getUniqueCode());
            } else {
                vo.setApplyDelayedExam(-2);
                vo.setUniqueCode("");
            }
            if (vo.getApplyDelayedExam() == -1) {
                vo.setApplyDelayedExamString("未通过");
            } else if (vo.getApplyDelayedExam() == 0) {
                vo.setApplyDelayedExamString("未申请");
            } else if (vo.getApplyDelayedExam() == 1) {
                vo.setApplyDelayedExamString("已申请");
            } else if (vo.getApplyDelayedExam() == 2) {
                vo.setApplyDelayedExamString("已通过");
            } else {
                vo.setApplyDelayedExamString("");
            }
            if (vo.getDeadLineStatus() == 1) {
                vo.setDeadLineStatusString("已截止");
            }
            voList.add(vo);
        }
        return voList;
    }

    @Override
    public PageInfo<Integer> getIssueCertificateIdList(String name, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<Integer> pageData = new PageInfo<>(examClassInfoMapper.findFinishedClass(name));
        if (Collections.isEmpty(pageData.getList())) {
            throw new NoDataException("page");
        }
        return pageData;
    }

    @Override
    public PageData getIssueCertificateList(String name, Integer page, Integer limit) {
        PageInfo<Integer> pageData = examClassInfoService.getIssueCertificateIdList(name, page, limit);

        List<Integer> idList = pageData.getList();
        if (Collections.isEmpty(idList)) {
            throw new NoDataException("page");
        }
        List<ExamClassInfo> list = examClassInfoDao.findByIdIn(idList, Joiner.on(",").join(idList));

        List<IssueCertificateListVO> voList = new ArrayList<>(list.size());
        for (ExamClassInfo examClassInfo : list) {

            IssueCertificateListVO vo = new IssueCertificateListVO();
            vo.setSort((list.indexOf(examClassInfo) + 1) + (page - 1) * limit);
            ConverterUtil.copyProperties(examClassInfo, vo);
            vo.setCourseName(examClassInfo.getExamInfo().getCourse().getName());
            //报名课程人数
            vo.setSignUpCount(studentCourseService.countSignUpNumberByCourse(examClassInfo.getExamInfo().getCourse().getId()));
            //参加考试人数
            vo.setCandidateCount(examStudentService.countExamNumber(examClassInfo.getId()));
            //通过人数
            vo.setPassCount(examStudentService.countPassExamNumber(examClassInfo.getId()));

            voList.add(vo);
        }
        return new PageData<>((int) pageData.getTotal(), limit, page, voList);
    }

    @Override
    public List<LocalDate> getExamDate(Integer userId, LocalDate today, LocalDate maxDate) {
        //考试日期
        List<ExamClassInfo> list = examClassInfoDao.findPersionalExamBetween(userId,
                today.atTime(0, 0).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                maxDate.atTime(0, 0).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        if (CollectionUtils.isEmpty(list)) {
            return new ArrayList<>();
        }
        return list.stream().map(ExamClassInfo::getExamStartTime).map(i -> i.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).collect(Collectors.toList());
    }

    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    @Override
    public void bindTestPaperToClass(Integer classId, Integer testPaperId) {
        ExamClassInfo classInfo = examClassInfoService.get(classId);
        TestPaper testPaper = testPaperService.get(testPaperId);
        ExamInfo examInfo = classInfo.getExamInfo();
        examInfo.setTestPaper(testPaper);
        examInfo.setTotalScore(testPaper.getTotalScore());
        try {
            examInfoDao.save(examInfo);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

}

