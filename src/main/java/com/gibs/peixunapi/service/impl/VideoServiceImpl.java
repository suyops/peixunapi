package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.dao.VideoDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.model.Video;
import com.gibs.peixunapi.service.VideoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.EntityNotFoundException;

@Service
@Slf4j
public class VideoServiceImpl implements VideoService {

    @Autowired
    private VideoDao videoDao;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Video save(Video video) {
        try {
            return videoDao.save(video);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public Video get(Integer id) {
        return videoDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

}

