package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.show.CourseManageVO;
import com.gibs.peixunapi.dao.CourseBusinessDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.mapper.CourseBusinessMapper;
import com.gibs.peixunapi.model.CourseBusiness;
import com.gibs.peixunapi.model.Course;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ResultUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * @author liangjiawei
 */
@Service
@Slf4j
public class CourseBusinessServiceImpl implements CourseBusinessService {

    @Autowired
    private CourseBusinessDao courseBusinessDao;
    @Autowired
    private CourseBusinessMapper courseBusinessMapper;
    @Autowired
    private CourseService courseService;
    @Autowired
    private StudentCourseService studentCourseService;
    @Autowired
    private CourseTeacherService courseTeacherService;
    @Autowired
    private UserService userService;
    @Autowired
    private BusinessService businessService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result delete(Integer id) {
        try {

            return ResultUtil.success(ResultEnum.DELETE_SUCCESS);
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResultUtil.error(ResultEnum.SERVER_ERROR);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean subscribeCourse(Integer businessId, Integer courseId) {
        //判断是否已订阅
        if (courseBusinessDao.findByBusinessAndCourse(businessId, courseId) != null) {
            return false;
        }

        CourseBusiness courseBusiness = new CourseBusiness();
        Course course = courseService.get(courseId);
        courseBusiness.setBusinessId(businessId);
        courseBusiness.setCourse(course);
        User user = userService.getAttributeUser();
        courseBusiness.setCreatorId(user.getId());
        courseBusiness.setSubscribeTime(new Date());
        try {
            courseBusinessDao.save(courseBusiness);
            return true;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public PageData<CourseManageVO> getBusinessCourseList(Integer businessId, String name, Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        PageInfo<CourseManageVO> pageData = new PageInfo<>(courseBusinessMapper.findByBusinessPage(businessId, name));
        if (pageData.getList().isEmpty()) {
            throw new NoDataException("page");
        }
        List<CourseManageVO> courseList = pageData.getList();
        for (CourseManageVO vo : courseList) {
            if (vo.getIsSubscribe() == 1) {
                vo.setSubscribeCount(businessService.countByBusiness(businessId));
            } else {
                vo.setSubscribeCount(studentCourseService.countSubscribeByCourseAndBusiness(vo.getId(), businessId));
            }
            vo.setSignUpCount(studentCourseService.countSignUpByCourseAndBusiness(vo.getId(), businessId));
            vo.setTeacherName(courseTeacherService.getTeacher(vo.getId()).getTeacher().getUsername());

        }
        return new PageData<>((int) pageData.getTotal(), limit, page, courseList);
    }


    @Override
    public List<CourseBusiness> getByBusiness(Integer businessId) {
        List<CourseBusiness> list = courseBusinessDao.findByBusiness(businessId);
        if (CollectionUtils.isEmpty(list)) {
            throw new NoDataException();
        }
        return list;
    }
}

