package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.post.ExamClassVO;
import com.gibs.peixunapi.VO.post.ExamInfoVO;
import com.gibs.peixunapi.dao.ExamInfoDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.model.ExamInfo;
import com.gibs.peixunapi.model.TestPaper;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.CourseService;
import com.gibs.peixunapi.service.ExamInfoService;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static com.gibs.peixunapi.enums.ResultEnum.*;

/**
 * @author liangjiawei
 */
@Slf4j

@Service
public class ExamInfoServiceImpl implements ExamInfoService {

    @Autowired
    private ExamInfoDao examInfoDao;
    @Autowired
    private CourseService courseService;
    @Autowired
    private ExamInfoService examInfoService;


    @Override
    public ExamInfo get(Integer id) {
        return examInfoDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public TestPaper getTestPaperByExamClass(Integer examClassId) {
        ExamInfo examInfo = examInfoDao.findTestPaperByExamClassId(examClassId);
        if (examInfo == null) {
            throw new EntityNotFoundException();
        }
        return examInfo.getTestPaper();
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public Result updateTestPaper(TestPaper testPaper, Integer examInfoId) {
        ExamInfo examInfo = examInfoDao.findById(examInfoId).orElseThrow(EntityNotFoundException::new);
        examInfo.setTestPaper(testPaper);
        try {
            examInfoDao.save(examInfo);
            return ResultUtil.success(ResultEnum.UPDATE_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(UPDATE_FAIL, e);
        }
    }

    @Override
    public List<ExamInfo> findExamInfosByTestPaper_Id(Integer testPaperId) {
        return examInfoDao.findExamInfosByTestPaper_Id(testPaperId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ExamInfo create(ExamInfoVO examInfoVO, User user) {
        ExamInfo examInfo = new ExamInfo();
        ConverterUtil.copyProperties(examInfoVO, examInfo, "id");
        examInfo.setCourse(courseService.get(examInfoVO.getCourseId()));
//        examInfo.setTestPaper(testPaperService.get(examInfoVO.getTestPaperId()));
        examInfo.setCreator(user);
        try {
            ExamInfo result = examInfoDao.save(examInfo);
            return result;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public ExamInfo edit(ExamClassVO examInfoVO) {
        ExamInfo examInfo = examInfoService.get(examInfoVO.getExamInfoId());
        if (!examInfo.getCourse().getId().equals(examInfoVO.getCourseId())) {

            examInfo.setCourse(courseService.get(examInfoVO.getCourseId()));
        }
        //TODO 以后用于替换试卷
/*        if (!examInfo.getTestPaper().getId().equals(examInfoVO.getTestPaperId())) {
            examInfo.setTestPaper(testPaperService.get(examInfoVO.getTestPaperId()));
        }*/


        ConverterUtil.copyDifferentPropertiesExcept(examInfoVO, examInfo, "id", "name", "code");
        try {
            examInfo = examInfoDao.save(examInfo);
            return examInfo;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void bindTestPaper(Integer id, Integer testPaperId) {

        examInfoDao.bindTestPaper(id, testPaperId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(flushAutomatically = true)
    public void save(ExamInfo examInfo) {
        try {
            examInfoDao.save(examInfo);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }
}
