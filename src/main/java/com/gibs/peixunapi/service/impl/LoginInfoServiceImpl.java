package com.gibs.peixunapi.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import com.gibs.peixunapi.DTO.BarDataDTO;
import com.gibs.peixunapi.DTO.BusinessECharDTO;
import com.gibs.peixunapi.VO.post.UserVO;
import com.gibs.peixunapi.VO.show.*;
import com.gibs.peixunapi.dao.LoginInfoDao;
import com.gibs.peixunapi.dao.SmsRecordDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.enums.SmsEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.mapper.*;
import com.gibs.peixunapi.model.Business;
import com.gibs.peixunapi.model.LoginInfo;
import com.gibs.peixunapi.model.SmsRecord;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.DigestUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.ResultEnum.*;


@Transactional(rollbackFor = Exception.class)
@Slf4j
@Service
public class LoginInfoServiceImpl implements LoginInfoService {


    @Autowired
    private LoginInfoDao loginInfoDao;
    @Autowired
    private LoginInfoMapper loginInfoMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private StudyRecordMapper studyRecordMapper;
    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private BusinessMapper businessMapper;
    @Autowired
    private ExamStudentMapper examStudentMapper;
    @Autowired
    private OfferRecordMapper offerRecordMapper;
    @Autowired
    private ExamClassInfoService examClassInfoService;
    @Autowired
    private StudentCourseService studentCourseService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private LoginInfoService loginInfoService;
    @Autowired
    private SmsRecordDao smsRecordDao;
    @Autowired
    private BusinessService businessService;


    final static String PUBLIC_KEY = "e70769380ff7446fa0deb73c485422c5";
    final static String PRIVATE_KEY = "6ada682286244b12b7d6cd56cbe07a11";

    @Override
    public LoginInfo getLoginInfo(Integer id) {
        return loginInfoDao.findFirstByUserIdOrderByLoginTimeDesc(id);
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void create(LoginInfo loginInfo) {
        loginInfo.setId(null);
        try {
            loginInfoDao.save(loginInfo);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(CREATE_FAIL, e);
        }
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public Result logout(Integer userId) {
        LoginInfo loginInfo = getLoginInfo(userId);
        loginInfo.setOffLineTime(new Date());

        LocalDateTime loginTime = loginInfo.getLoginTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime logoutTime = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

        long minutes = loginInfo.getOnLineTime().longValue() + ChronoUnit.MINUTES.between(loginTime, logoutTime);
        loginInfo.setOnLineTime(Double.parseDouble(Long.toString(minutes)));
        try {
            loginInfoDao.save(loginInfo);
            return ResultUtil.success(OPERATING_SUCCESS);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(OPERATING_FAIL, e);
        }
    }

    @Override
    public Integer[] getUserBarData(Integer businessId) {
        //1. 如果code是属于企业管理员那么根据roleCode查询userRole+Role 获得idList
        List<Integer> idList = null;
        idList = userMapper.findUserListByBusinessId(businessId);
        if (Collections.isEmpty(idList)) {
            return getMonthData(null);
        }

        //2.根据idList查询
        //获取当前年份
        LocalDate date = LocalDate.now();
        List<BarDataDTO> barData = loginInfoMapper.findUserBarDataByBusiness(idList, date.getYear());

        return getMonthData(barData);
    }

    @Override
    public List<Integer[]> getCourseLineData(Integer businessId, Integer studentId) {

        //获取当前年份
        LocalDate date = LocalDate.now();
        /*文档*/
        List<BarDataDTO> documentList = studyRecordMapper.findDocumentLineData(studentId, date.getYear());
        Integer[] documentData = getMonthData(documentList);
        /*视频*/
        List<BarDataDTO> videoList = studyRecordMapper.findVideoLineData(studentId, date.getYear());
        Integer[] videoData = getMonthData(videoList);

        List<Integer[]> result = new ArrayList<>(2);
        result.add(documentData);
        result.add(videoData);
        return result;
    }

    @Override
    public MultiLineVo getAllBusinessLineData() {
        /*legend*/
        LegendVO legendVO = new LegendVO(new String[]{"学习", "考试", "颁证"});
        /*XAxis*/
        List<String> xaxisList = businessMapper.findBusiness();
        String[] data = xaxisList.stream().toArray(String[]::new);
        XAxisVO xAxisVO = new XAxisVO(data);
        /*seriesVO*/
        List<SeriesVO> voList = new ArrayList<>(3);
        //获取当前月份
        LocalDate date = LocalDate.now();
        /* 学习 */
        List<BusinessECharDTO> studyList = studyRecordMapper.findSeriesStudyData(date.getMonthValue());
        Integer[] studyData = studyList.stream().map(BusinessECharDTO::getValue).toArray(Integer[]::new);
        SeriesVO seriesStudy = new SeriesVO("学习", studyData);
        voList.add(seriesStudy);
        /*考试*/
        List<BusinessECharDTO> examList = examStudentMapper.findSeriesExamData(date.getMonthValue());
        Integer[] examData = examList.stream().map(BusinessECharDTO::getValue).toArray(Integer[]::new);
        SeriesVO seriesExam = new SeriesVO("考试", examData);
        voList.add(seriesExam);

        /*颁证*/
        List<BusinessECharDTO> offerList = offerRecordMapper.findSeriesOfferData(date.getMonthValue());
        Integer[] offerData = offerList.stream().map(BusinessECharDTO::getValue).toArray(Integer[]::new);
        SeriesVO seriesOffer = new SeriesVO("颁证", offerData);
        voList.add(seriesOffer);

        return new MultiLineVo(legendVO, xAxisVO, voList);
    }

    @Override
    public MultiLineVo getOneBusinessLineData(Integer businessId) {
        /*legend*/
        LegendVO legendVO = new LegendVO(new String[]{"学习", "考试", "颁证"});
        /*XAxis*/
        String[] data = new String[]{"1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"};
        XAxisVO xAxisVO = new XAxisVO(data);
        /*seriesVO*/
        List<SeriesVO> voList = new ArrayList<>();
        //获取当前年份
        LocalDate date = LocalDate.now();
        /* 学习 */
        List<BusinessECharDTO> studyList = studyRecordMapper.findSeriesStudyDataByBusiness(date.getYear(), businessId);
        SeriesVO seriesStudy = new SeriesVO("学习", getSeriesMonthData(studyList));
        voList.add(seriesStudy);
        /*考试*/
        List<BusinessECharDTO> examList = examStudentMapper.findSeriesExamDataByBusiness(date.getYear(), businessId);
        SeriesVO seriesExam = new SeriesVO("考试", getSeriesMonthData(examList));
        voList.add(seriesExam);
        /*颁证*/
        List<BusinessECharDTO> offerList = offerRecordMapper.findSeriesOfferDataByBusiness(date.getYear(), businessId);
        SeriesVO seriesOffer = new SeriesVO("颁证", getSeriesMonthData(offerList));
        voList.add(seriesOffer);

        return new MultiLineVo(legendVO, xAxisVO, voList);
    }

    @Override
    public List<PieVO> getUserPieData() {
        return userMapper.findUserPieData();
    }

    @Override
    public List<PieVO> getCoursePieData() {
        return courseMapper.findCoursePieData();
    }

    private Integer[] getSeriesMonthData(List<BusinessECharDTO> list) {
        Integer[] seriesMonth = new Integer[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        List<BusinessECharDTO> local = list;
        for (BusinessECharDTO s : local) {
            int integer = Integer.parseInt(s.getName());
            seriesMonth[integer - 1] = s.getValue();
        }
        return seriesMonth;
    }

    private Integer[] getMonthData(List<BarDataDTO> list) {
        //12个月份
        Integer[] month = new Integer[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        if (list == null) {
            return month;
        }
        Map<Integer, Integer> barMap = list.stream().collect(Collectors.toMap(BarDataDTO::getMonth, BarDataDTO::getCount));


        for (Integer integer : barMap.keySet()) {
            month[integer - 1] = barMap.get(integer);
        }
        return month;
    }

    @Override
    public CalendarVO getCalendarStroke() {
        User user = userService.getAttributeUser();
        CalendarVO vo = new CalendarVO();
        LocalDate today = LocalDate.now();
        LocalDate maxDate = today.plusMonths(3).with(TemporalAdjusters.lastDayOfMonth());
        //考试日期
        List<LocalDate> examDateList = examClassInfoService.getExamDate(user.getId(), today, maxDate);
        vo.setExamDate(examDateList);
        //学习日期
        List<LocalDate> studyDateList = studentCourseService.getStudyDate(user.getId(), today, maxDate);
        vo.setStudyDate(studyDateList);

        return vo;
    }
    @Override
    public HashMap<String, Object> login(HttpServletRequest request, String username, String password) {
        User user = userService.getByUserName(username);
        if (user == null) {
            throw new BaseException(ResultEnum.NO_USER);
        }
        if (user.getStatus() == 0) {
            throw new BaseException(ResultEnum.USER_DISABLE);
        }
        if (user.getAudit() == 0) {
            throw new BaseException(ResultEnum.USER_NOT_AUDIT);
        }
        if (!DigestUtils.md5DigestAsHex(password.trim().getBytes()).equals(user.getPassword())) {
            throw new BaseException(ResultEnum.PASSWORD_WRONG);
        }
        return loginInfo(request, user);
    }
    @Override
    public HashMap<String, Object> loginInToken(HttpServletRequest request, String token) {
        //TODO 替换为token检验

        String map = SecretUtils.isEncoding2(token);
        log.info(map);
        String[] data = map.split("_");
        String userId = data[0];
        String asyncID = data[1];

        String username = data[2];
        String unitId = data[3];
        String departId = data[4];
        User user = userService.findByUUID(userId);
        if (user == null) {
            throw new BaseException(ResultEnum.NO_USER);
        }

        if (user.getStatus() == 0) {
            throw new BaseException(ResultEnum.USER_DISABLE);
        }
        if (user.getAudit() == 0) {
            throw new BaseException(ResultEnum.USER_NOT_AUDIT);
        }

        return loginInfo(request, user);
    }

    @Override
    public void sendPIN(HttpServletResponse response, String telephone, SmsEnum smsEnum) {
        Random random = new Random();
        StringBuilder code = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            code.append(random.nextInt(10));
        }
        try {
            //发送短信
            SmsUtils.sendSms(telephone, code.toString(), smsEnum);

            // 短信记录
            String message = smsEnum.getMessage();
            SmsRecord smsRecord = new SmsRecord();
            smsRecord.setMessage(message);
            smsRecord.setTelephone(telephone);
            User user = userService.getByTelephone(telephone);
            smsRecord.setReceiver(user.getId());
            smsRecordDao.save(smsRecord);

            //设置缓存
            Cookie cookie = new Cookie(telephone, code.toString());
            cookie.setMaxAge(10 * 60);
            response.addCookie(cookie);
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

    @Override
    public HashMap<String, Object> checkLoginPIN(HttpServletRequest request, String pin, String telephone, String code) {
        if (!pin.equals(code)) {
            throw new BaseException(PIN_ERROR);
        }
        User user = userService.getByTelephone(telephone);
        return loginInfo(request, user);
    }

    private HashMap<String, Object> loginInfo(HttpServletRequest request, User user) {

        //记录登录信息
        LoginInfo loginInfo = new LoginInfo();
        String ip = request.getHeader("clientIp");
        loginInfo.setIPAddress(ip);
        //TODO 补充设备类型
//        loginInfo.setDeviceType();
        loginInfo.setUserId(user.getId());
        loginInfo.setLoginTime(new Date());
        LoginInfo preLogin = loginInfoService.getLoginInfo(user.getId());
        if (preLogin != null && preLogin.getOffLineTime() != null) {
            loginInfo.setLoginCount(preLogin.getLoginCount() + 1);
        }
        loginInfoDao.save(loginInfo);
        //返回登录信息
        HashMap<String, Object> result = new HashMap<>();
        String token = SecretUtils.setEncoding(PUBLIC_KEY, PRIVATE_KEY, user.getUuid());
        result.put("token", token);
        List<RoleVO> list = userRoleService.getRoleVOListByUserId(user.getId());
        result.put("role", list.stream().map(RoleVO::getCode).collect(Collectors.joining(",")));
        HashMap<String, Object> userMap = new HashMap<>();
        userMap.put("username", user.getUsername());
        userMap.put("name", user.getUsername());
        userMap.put("id", user.getId().toString());
        userMap.put("tel", user.getTelephone());
        userMap.put("sex", user.getSex());
        userMap.put("email", user.getEmail());

        HashMap<String, Object> business = new HashMap<>();
        if (!"admin".equals(user.getUsername())) {
            business.put("name", user.getBusiness().getName());
            business.put("id", user.getBusiness().getId());
            business.put("isBelongSyndicate", user.getBusiness().getIsBelongSyndicate());
        } else {
            Business business1 = businessService.get(1);
            business.put("name", business1.getName());
            business.put("id", business1.getId());
            business.put("isBelongSyndicate", business1.getIsBelongSyndicate());
        }

        userMap.put("business", business);
        result.put("user", userMap);
        return result;
    }
}

