package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.DTO.CommentReferenceDTO;
import com.gibs.peixunapi.VO.post.CommentVO;
import com.gibs.peixunapi.VO.show.ShowCommentVO;
import com.gibs.peixunapi.dao.CommentDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.exception.SaveException;
import com.gibs.peixunapi.model.Comment;
import com.gibs.peixunapi.model.Documentation;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.model.VideoClass;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.gibs.peixunapi.enums.CourseEnum.DOCUMENT;
import static com.gibs.peixunapi.enums.CourseEnum.VIDEO;
import static com.gibs.peixunapi.enums.LinkEnum.QUESTION_LINK;
import static com.gibs.peixunapi.enums.LinkEnum.SYSTEM_CALL;

@Service
@Slf4j
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDao commentDao;
    @Autowired
    private UserService userService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private CourseTeacherService courseTeacherService;
    @Autowired
    private NoticeService noticeService;
    @Autowired
    private VideoClassService videoClassService;
    @Autowired
    private DocumentationService documentationService;

    @Override
    public Comment get(Integer id) {
        return commentDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer pushComment(CommentVO commentVO, String subscribeChannel) {
        Comment comment = new Comment();
        ConverterUtil.copyInSpecifyProperties(commentVO, comment, "content", "type", "reference");
        comment.setCreator(userService.getAttributeUser());
        comment.setSubscribeChannel(subscribeChannel);
        comment.setType(1);
        try {
            comment = commentDao.save(comment);
            return comment.getId();
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public ShowCommentVO getOneComment(Integer id) {
        Comment comment = commentService.get(id);
        return getVO(comment);
    }

    @Override
    public List<ShowCommentVO> getAllComment(String subscribeChannel, int type) {
        List<Comment> list = commentDao.findBySubscribeChannel(subscribeChannel, type);
        if (CollectionUtils.isEmpty(list)) {
            throw new NoDataException();
        }
        List<ShowCommentVO> voList = new ArrayList<>(list.size());
        list.forEach(comment -> voList.add(getVO(comment)));
        return voList;
    }

    private ShowCommentVO getVO(Comment comment) {
        ShowCommentVO vo = new ShowCommentVO();
        ConverterUtil.copyProperties(comment, vo);
        vo.setAvatarUrl(comment.getCreator().getAvatar() == null ? "" : comment.getCreator().getAvatar().getUrl());
        vo.setUsername(comment.getCreator().getUsername());
        if (comment.getReference() != null) {
            CommentReferenceDTO dto = new CommentReferenceDTO();
            Comment reference = commentService.get(comment.getReference());
            dto.setContent(reference.getContent());
            dto.setId(reference.getId());
            dto.setUsername(reference.getCreator().getUsername());
            vo.setReferenceDTO(dto);
        }
        return vo;
    }

    @Override
    public Integer pushQuestion(CommentVO commentVO, String subscribeChannel) {
        Comment comment = new Comment();
        ConverterUtil.copyInSpecifyProperties(commentVO, comment, "content", "type", "reference");
        User user = userService.getAttributeUser();
        comment.setCreator(user);
        comment.setSubscribeChannel(subscribeChannel);
        comment.setType(2);
        /*提取ChapterId*/
        String[] id = subscribeChannel.split("video|document");
        /*接收人为该课程所有的相关老师*/
        if (commentVO.getAccepter() == null) {
            comment.setAccepter(Integer.parseInt(id[1]));
        } else {
            comment.setAccepter(commentVO.getAccepter());
        }
        try {
            comment = commentDao.save(comment);
            /*消息通知*/
            if (commentVO.getAccepter() == null) {
                List<Integer> idList = new ArrayList<>();
                StringBuilder content = new StringBuilder("您好，系统已经收到对");
                if (subscribeChannel.contains(VIDEO.getMessage())) {
                    idList= courseTeacherService.getTeachersId(comment.getAccepter(),VIDEO);
                    VideoClass chapter = videoClassService.get(comment.getAccepter());
                    content.append(chapter.getCourse().getName()).append("培训").append(chapter.getName());
                } else if (subscribeChannel.contains(DOCUMENT.getMessage())) {
                    idList = courseTeacherService.getTeachersId(comment.getAccepter(),DOCUMENT);
                    Documentation chapter = documentationService.get(comment.getAccepter());
                    content.append(chapter.getCourse().getName()).append("培训").append(chapter.getName());
                }
                content.append("的课堂提出的课程提问，请及时进行处理");
                noticeService.sendNoticeToUser("课程提问", content.toString(), QUESTION_LINK.getLink(), idList, SYSTEM_CALL.getNum());
            } else {
                StringBuilder content = new StringBuilder("您好，您对");
                if (subscribeChannel.contains(VIDEO.getMessage())) {
                    VideoClass chapter = videoClassService.get(comment.getAccepter());
                    content.append(chapter.getCourse().getName()).append("培训").append(chapter.getName());
                } else if (subscribeChannel.contains(DOCUMENT.getMessage())) {
                    Documentation chapter = documentationService.get(comment.getAccepter());
                    content.append(chapter.getCourse().getName()).append("培训").append(chapter.getName());
                }
                content.append("的课堂提出的课程提问，已经被").append(user.getUsername()).append("老师回答。请到学习中心查看。");

                noticeService.sendNoticeToUser("回复提问", content.toString(), QUESTION_LINK.getLink(), commentVO.getAccepter().toString()
                        , SYSTEM_CALL.getNum());
            }

            return comment.getId();
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new SaveException(ResultEnum.SAVE_FAIL, e);
        }
    }

    @Override
    public Integer questionCount(Integer userId, Integer courseId) {
        Integer count = commentDao.questionCountByCourse(userId, courseId).intValue();
        return count == null ? 0 : count;

    }

    @Override
    public List<ShowCommentVO> getPersonQuestion(Integer id, int i) {
        List<Comment> list = commentDao.getPersonQuestion(id);
        if (CollectionUtils.isEmpty(list)) {
            throw new NoDataException();
        }
        List<ShowCommentVO> voList = new ArrayList<>(list.size());
        list.forEach(comment -> voList.add(getVO(comment)));
        return voList;
    }
}

