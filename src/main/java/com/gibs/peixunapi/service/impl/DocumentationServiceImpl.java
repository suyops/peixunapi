package com.gibs.peixunapi.service.impl;

import com.gibs.peixunapi.VO.post.ChapterVO;
import com.gibs.peixunapi.VO.show.ChapterListVO;
import com.gibs.peixunapi.VO.show.ShowChapterVO;
import com.gibs.peixunapi.dao.DocumentationDao;
import com.gibs.peixunapi.enums.ResultEnum;
import com.gibs.peixunapi.exception.BaseException;
import com.gibs.peixunapi.exception.NoDataException;
import com.gibs.peixunapi.model.Course;
import com.gibs.peixunapi.model.Documentation;
import com.gibs.peixunapi.model.FileInfo;
import com.gibs.peixunapi.model.StudyProgress;
import com.gibs.peixunapi.result.Result;
import com.gibs.peixunapi.service.*;
import com.gibs.peixunapi.utils.ConverterUtil;
import com.gibs.peixunapi.utils.ResultUtil;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.gibs.peixunapi.enums.CourseEnum.DOCUMENT;
import static com.gibs.peixunapi.enums.ResultEnum.DELETE_FAIL;
import static com.gibs.peixunapi.enums.ResultEnum.SAVE_FAIL;

/**
 * @author liangjiawei
 */

@Service
@Slf4j
public class DocumentationServiceImpl implements DocumentationService {

    @Autowired
    private DocumentationDao documentationDao;
    @Autowired
    private CourseService courseService;
    @Autowired
    private FileInfoService fileInfoService;
    @Autowired
    private StudyProgressService studyProgressService;
    @Autowired
    private ExercisesService exercisesService;
    @Autowired
    private ExerciseRecordService exerciseRecordService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result create(ChapterVO chapterVO) {
//        for (ChapterVO chapterVO : documentList) {
        Documentation documentation = new Documentation();
        ConverterUtil.copyProperties(chapterVO, documentation);

        Course course = courseService.get(chapterVO.getCourseId());

        FileInfo file = fileInfoService.get(chapterVO.getFileInfoId());

        documentation.setCourse(course);
        documentation.setFileInfo(file);
        documentationDao.save(documentation);
//        }
        return ResultUtil.success(ResultEnum.CREATE_SUCCESS);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public Result editDocument(ChapterVO documentationVO) {
        Documentation documentation = documentationDao.findById(documentationVO.getChapterId()).orElseThrow(EntityNotFoundException::new);
        Course course;
        FileInfo file;

        ConverterUtil.copyNotNullProperties(documentationVO, documentation);

        if (!documentation.getCourse().getId().equals(documentationVO.getCourseId())) {
            course = courseService.get(documentationVO.getCourseId());
            documentation.setCourse(course);
        }
        if (!documentation.getFileInfo().getId().equals(documentationVO.getFileInfoId())) {
            file = fileInfoService.get(documentationVO.getFileInfoId());
            documentation.setFileInfo(file);
        }
        documentationDao.save(documentation);

        return ResultUtil.success(ResultEnum.UPDATE_SUCCESS);
    }

    @Override
    public Documentation get(Integer id) {
        return documentationDao.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public ShowChapterVO getVO(Integer id, Integer userId) {
        Documentation documentation = documentationDao.findById(id).orElseThrow(EntityNotFoundException::new);
        ShowChapterVO vo = new ShowChapterVO();
        ConverterUtil.copyProperties(documentation, vo, "document");

        vo.setPicture(fileInfoService.getDocumentPicture());
        vo.setFileInfo(documentation.getFileInfo());
        vo.setChapterId(documentation.getId());
        vo.setCourseId(documentation.getCourse().getId());
//        if (documentation.getAudit() == 1) {
//            vo.setAuditString("通过");
//        }
//        if (documentation.getAudit() == 0) {
//            vo.setAuditString("未通过");
//        }
//        if (documentation.getStatus() == 1) {
//            vo.setStatusString("开放");
//        }
//        if (documentation.getStatus() == 0) {
//            vo.setStatusString("关闭");
//        }
        StudyProgress studyProgress = studyProgressService.getStudyProgress(documentation.getId(), userId, DOCUMENT);

        if (studyProgress.getId() == null) {
            vo.setProgress(studyProgress.getProgress());
        }
        return vo;
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void auditDocument(Integer audit, Integer courseId) {
        try {
            documentationDao.auditChapterByCourse(courseId, audit);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(DELETE_FAIL, e);
        }
    }

    @Override
    public List<ChapterListVO> studentGetList(Integer courseId, Integer userId) {
        List<Documentation> docList = documentationDao.findByCourseId(courseId);
        if (Collections.isEmpty(docList)) {
            return null;
        }
        List<ChapterListVO> voList = new ArrayList<>(docList.size());
        for (Documentation doc : docList) {
            ChapterListVO vo = new ChapterListVO();
            ConverterUtil.copyProperties(doc, vo);
            vo.setPicture(fileInfoService.getDocumentPicture());
//            vo.setAudit(doc.getAudit());
            StudyProgress studyProgress = studyProgressService.getStudyProgress(doc.getId(), userId, DOCUMENT);
            if (studyProgress.getId() == null) {
                vo.setStudyProgress(studyProgress.getProgress());
            }
            //练习题完成数量
            Integer finishExerciseCount = exerciseRecordService.getFinishExerciseCountByStudent(doc.getId(), userId, DOCUMENT);
            vo.setFinishExerciseCount(finishExerciseCount);
            Integer exerciseCount = exercisesService.countExercisesInChapter(doc.getId(), DOCUMENT);
            vo.setExerciseCount(exerciseCount);
            voList.add(vo);
        }
        return voList;
    }

    @Override
    public List<ShowChapterVO> getList(Integer courseId) {
        List<Documentation> docList = documentationDao.findByCourseId(courseId);
        if (Collections.isEmpty(docList)) {
            throw new NoDataException();
        }
        List<ShowChapterVO> chapterVOList = new ArrayList<>(docList.size());
        for (Documentation doc : docList) {
            ShowChapterVO chapterVO = new ShowChapterVO();
            ConverterUtil.copyProperties(doc, chapterVO);
            chapterVO.setFileInfo(doc.getFileInfo());
            chapterVO.setChapterId(doc.getId());
//            chapterVO.setAudit(doc.getAudit());
            chapterVO.setCourseId(courseId);
            chapterVO.setSort(doc.getSort());
            chapterVOList.add(chapterVO);
        }
        return chapterVOList;
    }

    @Override
    @Modifying(clearAutomatically = true)
    @Transactional(rollbackFor = Exception.class)
    public void linkClass(Integer courseId, Integer linkCourseId) {
        List<Documentation> documentationList = documentationDao.findByCourseId(courseId);

        if (!Collections.isEmpty(documentationList)) {
            List<Documentation> newList = documentationList.stream().map(documentation -> {
                Documentation temp = new Documentation();
                ConverterUtil.copyProperties(documentation, temp,
                        "id", "creatorId", "audit", "createTime", "updateTime");
                return temp;
            }).collect(Collectors.toList());
            try {
                documentationDao.saveAll(newList);
            } catch (Exception e) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                throw new BaseException(DELETE_FAIL, e);
            }
        }
    }

    @Override
    public List<Documentation> findByCourseId(Integer courseId) {

        List<Documentation> docList = documentationDao.findByCourseId(courseId);
        if (Collections.isEmpty(docList)) {
            new ArrayList<>();
        }
        return docList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void changeChapterStatus(Integer id, Integer status) {
        try {
            documentationDao.changeStatus(id, status);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void auditChapter(Integer id, Integer audit) {
        try {
            documentationDao.auditChapter(id, audit);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void applyAudit(Integer courseId) {
        try {
            documentationDao.applyAudit(courseId);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(SAVE_FAIL, e);
        }
    }

    @Override
    public Integer countByCourse(Integer id) {
        return documentationDao.countByCourse(id).intValue();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Modifying(clearAutomatically = true)
    public void deleteChapter(Integer chapterId) {
        try {
            documentationDao.deleteChapter(chapterId);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new BaseException(DELETE_FAIL, e);
        }
    }
}

