package com.gibs.peixunapi.service;

import java.util.List;

import com.gibs.peixunapi.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import javax.validation.Valid;
import com.gibs.peixunapi.model.Word;

public interface WordService {

        Result create(@Valid Word object);

	Result save(@Valid Word object);

Result delete(Integer id);

	Result get(Integer id);

	Result getList(String key);

	Result getListPage(String key, Integer current, Integer limit);

	Result getOptions(String key);
	

}

