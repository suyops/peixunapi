package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.post.CertificateVO;
import com.gibs.peixunapi.VO.post.IssueCertificateVO;
import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.VO.show.PersonCertificateVO;
import com.gibs.peixunapi.VO.show.ShowCertificateVO;
import com.gibs.peixunapi.VO.show.StudentCertificateVO;
import com.gibs.peixunapi.model.Certificate;
import com.gibs.peixunapi.result.PageData;

import java.util.List;

public interface CertificateService {

    /**
     * 新增证书模板
     *
     * @param vo 证书模板VO类
     */
    void create(CertificateVO vo);

    /**
     * 编辑证书模版
     *
     * @param vo 证书模板VO类
     */
    void save(CertificateVO vo);

    /**
     * 获取证书模板
     *
     * @param id id
     * @return
     */
    Certificate get(Integer id);


    /**
     * 获取模版中心列表
     *
     * @param name  名称搜索关键字
     * @param page  页数
     * @param limit 最大条数
     * @return
     */
    PageData getCertificateList(String name, Integer page, Integer limit);

    /**
     * 获取单个证书模板
     *
     * @param id 证书模板id
     * @return
     */
    ShowCertificateVO getCertificate(Integer id);


    /**
     * 创建证书_考试场次
     * 保存考试通过人数
     *
     * @param classId 考试场次
     */
    void createCertificateClass(Integer classId);


    /**
     * 发放证书
     *
     * @param vo 发放证书VO
     * @return
     */
    Integer issueCertificate(IssueCertificateVO vo);

    /**
     * 新增发放证书数量
     *
     * @param examClassId 考试班级
     * @param number      新增数量
     */
    void addIssueCount(Integer examClassId, Integer number);

    /**
     * 查询所有证书
     *
     * @param expiry       已过期
     * @param status       已吊销
     * @param pause        已暂停
     * @param comingExpiry 即将到期
     * @param businessId   单位Id
     * @param key          搜索关键字
     * @param page
     * @param limit
     * @return
     */
    PageData getCertificateManageList(Integer expiry, Integer status, Integer pause, Integer comingExpiry, Integer businessId, String key, Integer page, Integer limit);

    /**
     * 检查证书
     *
     * @param uniqueCode 唯一编码
     * @return
     */
    void checkCertificate(String uniqueCode);

    /**
     * 获取个人证书
     *
     * @param id 唯一编码
     * @return
     */
    PersonCertificateVO getCertificateStudent(Integer id);


    /**
     * 个人证书暂停
     *
     * @param uniqueCode 唯一编码
     * @param status     状态
     */
    void pause(String uniqueCode, Integer status);

    /**
     * 个人证书吊销
     *
     * @param uniqueCode 唯一编码
     * @param status     状态
     */
    void revoke(String uniqueCode, Integer status);

    /**
     * 证书模板选项列表
     *
     * @return
     */
    List<OptionVO> getOptionList();

    /**
     * 学生个人证书列表
     * @return
     */
    List<StudentCertificateVO> getStudentCertificate();

    /**
     * 个人证书续期
     *
     * @param uniqueCode 唯一编码
     */
    void renewalBatch(List<String> uniqueCode);

    /**
     * 申请证书续期
     *
     * @param uniqueCode
     */
    void applyRenewal(String uniqueCode);


    /**
     * 班级证书表是否存在
     *
     * @param classId
     * @return
     */
    Boolean CertificateClassExist(Integer classId);
}

