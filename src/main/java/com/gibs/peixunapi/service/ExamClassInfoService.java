package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.post.ExamClassVO;
import com.gibs.peixunapi.VO.show.CorrectManageVO;
import com.gibs.peixunapi.VO.show.ExamManageVO;
import com.gibs.peixunapi.VO.show.ShowExamClassVO;
import com.gibs.peixunapi.VO.show.StudentExamCenterVO;
import com.gibs.peixunapi.model.ExamClassInfo;
import com.gibs.peixunapi.model.ExamInfo;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;
import com.github.pagehelper.PageInfo;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface ExamClassInfoService {

    /**
     * 获取班级信息
     *
     * @param id 班级id
     * @return
     */
    ExamClassInfo get(Integer id);

    /**
     * 检查试卷是否已经被使用过
     *
     * @param testPaperId 试卷实体类Id
     * @return 考试以开始就为true, 只有等待开始为false
     */
    Boolean isTestPaperUsed(Integer testPaperId);

    /**
     * 获取此课程已考试场次
     *
     * @param courseId 课程id
     * @return
     */
    Integer countFinishClass(Integer courseId);

    /**
     * 获取此课程考试场次
     *
     * @param courseId 课程id
     * @return
     */
    Integer countClass(Integer courseId);

    /**
     * 新增考试班级
     *
     * @param examClassVO 新增考试班级VO
     * @param user        用户
     * @return
     */
    Result create(ExamClassVO examClassVO, User user);

    /**
     * 更改考试班级中考试人数统计
     *
     * @param number      人数
     * @param examClassId 考试班级id
     * @param type
     * @return
     */
    Result changeNumber(int number, Integer examClassId, String type);

    /**
     * 获取此课程的考试班级列表
     *
     * @param key
     * @param page  页数
     * @param limit 最大条数
     * @return
     */
    PageData getExamClassList(String key, Integer page, Integer limit);

    /**
     * 获取考试班级详细信息
     *
     * @param classId 考试场次id
     * @return
     */
    ShowExamClassVO getExamClassVO(Integer classId);

    /**
     * 根据校验码查询班级id
     *
     * @param code 校验码
     * @return
     */
    Integer findByCode(String code);


    /**
     * 编辑考试班级VO类
     *
     * @param examClass 考试班级VO
     * @param examInfo
     */
    void edit(ExamClassVO examClass, ExamInfo examInfo);

    /**
     * 判断是否考试是否开始了
     *
     * @param id 考试场次id
     * @return
     */
    Boolean isStartExam(Integer id);

    /**
     * 查询等待阅卷的考试场次
     *
     * @param name
     * @param userId
     * @return
     */
    List<Integer> findWaitingCorrectIdList(String name, Integer userId);

    /**
     * 阅卷管理列表
     *
     * @param key
     * @return
     */
    List<CorrectManageVO> getCorrectManageList(String key);

    /**
     * 查询已经开始考试的场次
     *
     * @param key
     * @param time
     * @param userId
     * @return
     */
    List<Integer> findIdByStartedExam(String key, Date time, Integer userId);

    /**
     * 考务管理-学生考试进度列表
     *
     * @param key
     * @return
     */
    List<ExamManageVO> getExamManageList(String key);


    /**
     * 学生考试中心列表
     * 根据学生id查找报名课程
     * 报名课程中查找未开始考试场次
     *
     * @param key
     * @param studentId
     * @return
     */
    List<StudentExamCenterVO> getStudentExamClassList(String key, Integer studentId);

    /**
     * 获取需要发放证书的班级id列表
     *
     * @param name
     * @param page
     * @param limit
     * @return
     */
    PageInfo<Integer> getIssueCertificateIdList(String name, Integer page, Integer limit);

    /**
     * 证书发放列表
     *
     * @param name  课程名
     * @param page  页数
     * @param limit 最大条数
     * @return
     */
    PageData getIssueCertificateList(String name, Integer page, Integer limit);


    /**
     * 学生考试中心
     * 根据已报名课程获取其地下的所有考试场次
     *
     * @param key
     * @param date
     * @return
     */
    List<StudentExamCenterVO> findClassInCourse(String key, Integer date);

    List<LocalDate> getExamDate(Integer userId, LocalDate today, LocalDate maxDate);

    /**
     * 绑定试卷到考试班级
     *
     *
     * @param classId
     * @param testPaperId
     */
    void bindTestPaperToClass(Integer classId, Integer testPaperId);
}

