package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.model.CourseSubjectHub;
import com.gibs.peixunapi.model.SubjectHub;
import com.gibs.peixunapi.result.Result;

import java.util.List;
import java.util.Map;

public interface CourseSubjectHubService {

    /**
     * 创建该章节所属的课程的题目
     *
     * @param subjectHub 题目id
     * @param chapterId  章节id
     * @param courseEnum 章节类型
     * @return
     */
    CourseSubjectHub createInChapter(SubjectHub subjectHub, Integer chapterId, CourseEnum courseEnum);

    /**
     * 关联课程-绑定题目
     *
     *
     * @param link
     * @param beLink
     * @return
     */
    void linkSubjectHubByCourseId(Integer link, Integer beLink);

    /**
     * 根据题目类型查询课程底下题目
     *
     * @param courseId
     * @param type
     * @return
     */
    Result getSubjectList(Integer courseId, String type);

    /**
     * 获取课程题库
     *
     * @param id id
     * @return
     */
    CourseSubjectHub get(Integer id);


    /**
     * 按类型获取随机试题
     *
     * @param courseId
     * @param type     题目类型
     * @param limit    条数
     * @return
     */
    List<CourseSubjectHub> getRandomByCourseOfType(Integer courseId, String type, Integer limit);


    /**
     * 根据章节id以及类型和题目id获取
     * 该题目在课程题库的信息
     *
     * @param chapterId    章节id
     * @param subjectHubId 题目id
     * @param enums        章节类型
     * @return
     */
    CourseSubjectHub getBySubjectHub(Integer chapterId, Integer subjectHubId, CourseEnum enums);

    /**
     * 绑定练习题
     *
     * @param courseId
     * @param subjectHubId
     * @return
     */
    CourseSubjectHub getByCourseAndSubjectHub(Integer courseId, Integer subjectHubId);

    /**
     * 视频防挂机题目的选择列表
     *
     * @param courseId
     * @return
     */
    List<CourseSubjectHub> findHangUpSubjectChooseList(Integer courseId);

    /**
     * 导入保存
     * @param courseSubjectHubList
     */
    void createBatch(List<CourseSubjectHub> courseSubjectHubList);

    /**
     * 根据id获取课程题库
     * @param idList
     * @return
     */
    List<CourseSubjectHub> getByIdList(List<Integer> idList);

    /**
     * 按课程id查询此课程的题库 并且进行分类
     * @param courseId
     * @return
     */
    Map<String, List<SubjectHubVO>> getCourseSubjectHub(Integer courseId);
}

