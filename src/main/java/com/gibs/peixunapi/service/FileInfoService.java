package com.gibs.peixunapi.service;

import com.gibs.peixunapi.model.*;
import com.gibs.peixunapi.result.Result;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.List;

public interface FileInfoService {

    /**
     * 获取文件
     *
     * @param id id
     * @return
     */
    FileInfo get(Integer id);

    /**
     * 查询文档章节专用封面图
     *
     * @return
     */
    String getDocumentPicture();

    /**
     * 批量上传文件
     *
     * @param files 文件流
     * @return
     */
    Result uploadFileMulti(MultipartFile[] files) throws Exception;

    /**
     * 上传文件
     *
     * @param files 文件流
     * @return
     */
    Result uploadFile(MultipartFile files) throws Exception;

    /**
     * 保存视频的截图
     *
     * @param video 文件流
     * @return
     * @throws Exception
     */
    FileInfo videoPic(File video) throws Exception;

    /**
     * 检查待上传文件是否存在
     * 检查文件是否上传
     *
     * @param fileMd5
     * @param fileExt
     * @return
     */
    Result checkFile(String fileMd5, String fileName, String fileExt);

    /**
     * 检查块文件
     * 如果存在则不重复上传
     *
     * @param fileMd5
     * @param chunk
     * @param chunkSize
     * @return
     */
    Boolean checkChunk(String fileMd5, Integer chunk, Integer chunkSize);

    /**
     * 块文件上传
     *
     * @param file
     * @param fileMd5
     * @param chunk
     * @return
     */
    String uploadChunk(MultipartFile file, String fileMd5, Integer chunk);


    /**
     * 合并块文件
     * 从临时文件中合并块文件并且合并到存储文件夹中
     *
     * @param fileMd5
     * @param fileName
     * @param fileSize
     * @param fileExt
     * @return
     */
    HashMap<String, String> mergeChunks(String fileMd5, String fileName, Long fileSize, String fileExt);

    /**
     * 添加证书内容为水印
     * @param list
     * @param certificate
     * @param name
     * @param content
     * @param foot
     */
    void addWatermark(List<CertificateStudent> list, Certificate certificate, String name, String content, String foot);
}

