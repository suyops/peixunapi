package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.show.ExerciseVO;
import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.VO.post.PostSubjectHubVO;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.model.ExerciseRecord;
import com.gibs.peixunapi.model.Exercises;
import com.gibs.peixunapi.result.Result;

import java.util.List;
import java.util.Map;

/**
 * @author liangjiawei
 */
public interface ExercisesService {


    /**
     * 获取练习题
     *
     * @param id id
     * @return
     */
    Exercises get(Integer id);

    /**
     * 更换练习题
     *
     * @param exerciseVOList 练习题VO列表
     * @return
     */
    Result changeExercises(List<ExerciseVO> exerciseVOList);

    /**
     * 根据章节id以及章节类型查找CourseSubjectHub中的题库id列表并返回
     *
     * @param chapterId 章节id
     * @param type      章节类型
     * @return 题库id列表
     */
    List<Integer> getSubjectHubIdList(Integer chapterId, String type);

    /**
     * 顺序获取一定数量本章节的练习题
     * @param chapterId
     * @param number
     * @param type
     * @return
     */
    List<SubjectHubVO> getSomeChapterExercise(Integer chapterId, Integer number, String type);

    /**
     * 统计培训的练习题数量
     *
     * @param courseId 文档课程id
     * @return
     */
    Integer countByCourse(Integer courseId);

    /**
     * 添加错题
     *  @param exercises
     * @param student
     */
    void addWrongExercises(ExerciseRecord exercises, Integer student);

    /**
     * 获取错题集
     *
     * @param studentId
     * @param courseId
     * @return
     */
    List<SubjectHubVO> getWrongExercises(Integer studentId, Integer courseId);


    /**
     * 获取视频课程选择防挂机选项练习题
     *
     * @param courseId
     * @return
     */
    List<SubjectHubVO> getHangUpSubjectChooseList(Integer courseId);

    /**
     * 更换练习题
     *
     * @param chapterId
     * @param courseId
     * @param newSubjectHubId
     * @param courseEnum
     */
    void changeHangUpSubject(Integer chapterId, Integer courseId, Integer newSubjectHubId, CourseEnum courseEnum);

    /**
     * 获取视频挂机题目VO
     *
     * @param classId
     * @return
     */
    List<SubjectHubVO> getHangUpSubjectVO(Integer classId);

    /**
     * 视频绑定练习题
     * @param ids
     * @param chapterId
     * @param type
     */
    void  BindExerciseFromSubjectHub(String ids, Integer chapterId, String type);


    /**
     * 在课程中新增题目
     *
     * @param subject
     */
    void createInChapter(PostSubjectHubVO subject);

    /**
     * 题目分类
     *
     * @param data
     * @return
     */
    Map<String, List<SubjectHubVO>> classify(List<SubjectHubVO> data);

    /**
     * 随机获取课程练习题
     *
     * @param number
     * @param courseId
     * @param type
     * @return
     */
    List<SubjectHubVO> getRandomExercises(Integer number, Integer courseId, String type);


    /**
     * 统计此章节底下题目数量
     *
     * @param id
     * @param document
     * @return
     */
    Integer countExercisesInChapter(Integer id, CourseEnum document);

}

