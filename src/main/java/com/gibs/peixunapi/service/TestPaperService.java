package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.VO.post.RandomTestPaperVO;
import com.gibs.peixunapi.VO.show.ShowTestPaperListVO;
import com.gibs.peixunapi.VO.show.ShowTestPaperVO;
import com.gibs.peixunapi.model.TestPaper;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface TestPaperService {


    /**
     *
     *按题目类型分类,复制原有分值,新增试卷并返回id
     * @param subjectHubVOList 题目VO类列表
     * @param testPaperId 试卷Id
     * @return
     */
    TestPaper copy(List<SubjectHubVO> subjectHubVOList, Integer testPaperId);


    /**
     * 获取试卷
     *
     * @param id id
     * @return
     */
    TestPaper get(Integer id);

    /**
     * 获取试卷选项列表
     *
     *
     * @param courseId
     * @param key 查询关键字
     * @return
     */
    Result getOptions(Integer courseId, String key);



    /**
     * 转换归类题目类型,根据map的key把value添加到VO类中相对应的题目列表中
     *
     * @param testPaper       试卷
     * @param subjectHubVOMap 按题型整理
     * @return
     */
    ShowTestPaperVO getVOList(TestPaper testPaper, Map<String, List<SubjectHubVO>> subjectHubVOMap);

    /**
     * 生成随机试卷
     *
     * @param randomVO 课程id
     * @return
     */
    Integer randomTestPaper(RandomTestPaperVO randomVO);

    /**
     * 根据准考证获取考试时回答情况
     *
     * @param uniqueCode 学生准考证号码
     * @return
     */
    TestPaper getTestPaperIdByUniqueCode(String uniqueCode);

    /**
     * 根据课程查询考试列表
     *
     * @param courseId
     * @param key
     * @param page
     * @param limit
     * @return
     */
    PageData<ShowTestPaperListVO> getTestPaperList(Integer courseId, String key, Integer page, Integer limit);

    /**
     * 创建空白试卷
     *
     * @param courseId
     * @param name
     * @param radioScore
     * @param checkScore
     * @param judgeScore
     * @param fillScore
     * @param questionScore
     * @return
     */
    Integer create(Integer courseId, String name, BigDecimal radioScore, BigDecimal checkScore,
                   BigDecimal judgeScore, BigDecimal fillScore, BigDecimal questionScore);

    void saveCount(TestPaper testPaper);
}

