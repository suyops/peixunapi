package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.model.CourseTeacher;
import com.gibs.peixunapi.model.User;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/18/16:09
 * @Version 1.0
 * @Description:
 */
public interface CourseTeacherService {
    /**
     * 获取课程负责老师
     *
     * @param courseId 课程id
     * @return
     */
    CourseTeacher getTeacher(Integer courseId);

    /**
     * 获取课程负责老师VO
     *
     * @param courseId 课程id
     * @return
     */
    OptionVO getTeacherVO(Integer courseId);

    /**
     * 获取课程助教VO
     *
     * @param courseId 课程id
     * @return
     */
    List<OptionVO> getAssistantOption(Integer courseId);

    /**
     * 保存负责老师
     *
     * @param courseId 课程id
     * @param teacher  老师
     */
    void saveManage(Integer courseId, User teacher);

    /**
     * 保存助教
     *
     * @param courseId     课程id
     * @param assistIdList 助教id列表
     */
    void saveAssist(Integer courseId, List<Integer> assistIdList);

    List<Integer> getTeachersId(Integer chapterId, CourseEnum courseEnum);
}
