package com.gibs.peixunapi.service;

import com.gibs.peixunapi.model.ActionLog;
import com.gibs.peixunapi.result.PageData;

import java.util.Date;
import java.util.List;

public interface ActionLogService {

    PageData<ActionLog>  getListPage(String content, Date dateFrom, Date dateTo, Integer current, Integer limit);

}

