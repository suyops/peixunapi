package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.post.StudyRecordVO;
import com.gibs.peixunapi.VO.show.StudyRecordListVO;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.model.StudyProgress;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;

import java.util.List;

public interface StudyProgressService {

    /**
     * 保存学生的练习题进度信息
     *
     * @param studentId    学生id
     * @param recordVOList 章节练习题VO
     * @return
     */
    void saveProgress(Integer studentId, List<StudyRecordVO> recordVOList);

    /**
     * 查询视频课程进度
     *
     * @param studentId    学生id
     * @param videoClassId 视频课程id
     * @return
     */
    Result getVideoClassProgress(Integer studentId, Integer videoClassId);

    /**
     * 新增个人章节学习进度
     *
     * @param userId
     * @param type
     * @param chapterId
     * @param timeRecord
     */
    void create(Integer userId, String type, Integer chapterId, Double timeRecord);

    /**
     * 根据章节类型更新/新增学习记录
     *
     * @param chapterId  章节id
     * @param userId     用户id
     * @param timeRecord 视频时长
     * @param type       章节类型
     * @return
     */
    Boolean updatePersonalProgress(Integer chapterId, Integer userId, Double timeRecord, String type);

    /**
     * 查找学习进度记录 没有则创建
     *
     * @param id     章节id
     * @param userId 用户id
     * @param type   章节类型
     * @return
     */
    StudyProgress getStudyProgress(Integer id, Integer userId, CourseEnum type);

    /**
     * 保存视频观看时长进度
     *
     * @param userId
     * @param videoClassId
     * @param time
     * @param isVideoPass
     */
    void saveVideoTime(Integer userId, Integer videoClassId, String time, Integer isVideoPass);

    /**
     * 已打开文档章节数量
     *
     * @param id
     * @param userId
     * @return
     */
    Integer countFinishDocument(Integer id,Integer userId);

    /**
     * 已完成视频章节数量
     *
     * @param id
     * @param userId
     * @return
     */
    Integer countFinishVideoClass(Integer id, Integer userId);


    /**
     * 获取此课程全部已看视频时长
     * @param courseId
     * @param userId
     * @return
     */
    List<Double> getVideoProgressByCourseAndStudent(Integer courseId, Integer userId);

    /**
     * 获取最新学习记录
     * @param userId
     * @param page
     * @param limit
     * @return
     */
    PageData<StudyRecordListVO> getLatestStudyRecord(Integer userId, Integer page, Integer limit);
}

