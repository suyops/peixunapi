package com.gibs.peixunapi.service;

import com.gibs.peixunapi.enums.ScheduledEnum;
import com.gibs.peixunapi.model.Scheduled;
import org.quartz.SchedulerException;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 *
 */
public interface ScheduledService {

    void tests();

    /**
     * 开始课程 学习状态更改
     *
     * @param courseId 班级id
     */
    void startCourse(Integer courseId);

    /**
     * 结束课程 学习状态更改
     *
     * @param courseId 班级id
     */
    void finishCourse(Integer courseId);

    /**
     * 开始考试
     *
     * @param classId 班级id
     */
    void startExam(Integer classId);

    void completeExam(Integer classId);

    void startCorrect(Integer classId);

    void finishCorrect(Integer classId);

    void announcementScore(Integer classId);

    void certificateExpiry();

    @Transactional(rollbackFor = Exception.class)
    void certificateWarning();

    void certificateUpdatePause();

    void create(String taskName, ScheduledEnum enums, String paramId, Date date);


    /*!=================================================================================================================!*/




}
