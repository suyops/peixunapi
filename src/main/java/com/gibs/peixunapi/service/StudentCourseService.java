package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.show.AllowExamVO;
import com.gibs.peixunapi.VO.show.CourseStudyVO;
import com.gibs.peixunapi.VO.show.SignUpStudentListVO;
import com.gibs.peixunapi.VO.show.StudentSignUpTypeVO;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.model.StudentCourse;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;

import java.time.LocalDate;
import java.util.List;

/**
 * @author liangjiawei
 */
public interface StudentCourseService {

    /**
     * 获取学生订阅列表
     *
     * @param studentId 学生id
     * @return
     */
    List<StudentCourse> getStudentSubscribeList(Integer studentId, Boolean isSubscribe);

    /**
     * 获取学生报名列表
     *
     * @param studentId 学生id
     * @return
     */
    List<StudentCourse> getStudentSignUpList(Integer studentId, Boolean isSignUp);

    /**
     * 学员自主订阅课程
     *
     * @param studentId 学生id
     * @param courseId  课程id
     * @return
     */
    Result subscribeCourse(Integer studentId, Integer courseId);

    /**
     * 批量订阅
     *
     * @param studentIds 学生id列表
     * @param courseId   课程id
     * @return
     */
    void subscribeCourse(List<Integer> studentIds, Integer courseId);

    /**
     * 学员自主报名课程
     *
     * @param studentId 学生id列表
     * @param courseId  课程id
     * @return
     */
    Result signUpCourse(Integer studentId, Integer courseId);

    /**
     * 批量报名课程
     *
     * @param studentIds 学生id列表
     * @param courseId   课程id
     * @return
     */
    void signUpCourse(List<Integer> studentIds, Integer courseId);

    /**
     * 统计此课程有考试资格人数
     *
     * @param courseId 课程Id
     * @return 人数
     */
    Integer countFinishStudy(Integer courseId);


    /**
     * 此课程拥有考试资格学生列表
     *
     * @param department
     * @param year
     * @param key
     * @param examId
     * @return
     */
    List<AllowExamVO> getAllowExamList(Integer department,Integer year, String key, Integer examId);


    /**
     * 获取此课程的报名人数
     *
     * @param courseId 课程id
     * @return
     */
    Integer countSignUpNumberByCourse(Integer courseId);


    /**
     * 统计公司内课程报名数
     *
     * @param courseId
     * @param business
     * @return
     */
    Integer countSignUpByCourseAndBusiness(Integer courseId, Integer business);

    /**
     * 获取该课程订阅人数
     *
     * @param courseId 课程
     * @return
     */
    Integer countSubscribeNumberByCourse(Integer courseId);

    /**
     * 获取该公司底下课程订阅人数
     *
     * @param courseId
     * @param businessId
     * @return
     */
    Integer countSubscribeByCourseAndBusiness(Integer courseId, Integer businessId);

    /**
     * 根据课程id以及学生id查询学生课程表
     *
     * @param courseId  课程id
     * @param studentId 学生id
     * @return
     */
    StudentCourse getByCourseAndStudent(Integer courseId, Integer studentId);

    /**
     * 根据课程名  +可选 是否报名\公司id 获取学生选项列表
     *
     * @param courseId
     * @param signUp
     * @param businessId
     * @return
     */
    List<StudentSignUpTypeVO> getStudentListByCourseOrBusiness(Integer courseId, Integer signUp, Integer businessId);

    /**
     * 学员学习情况
     *
     * @param courseId
     * @param page
     * @param limit
     * @return
     */
    PageData<CourseStudyVO> getCourseStudyList(Integer courseId, Integer page, Integer limit);


    /**
     * 判断是否完成课程学习具备考试资格
     *
     * @param userId
     * @param chapter
     * @param type
     */
    void isFinishCourse(Integer userId, Integer chapter, String type);

    /**
     * 获取已报名列表
     *
     * @param courseId
     * @return
     */
    List<SignUpStudentListVO> getSignUpList(Integer courseId);

    /**
     * 更新学生课程学习进度
     * @param documentationId
     * @param userId
     * @param courseEnum
     */
    void updateCourseProgress(Integer documentationId, Integer userId, CourseEnum courseEnum);

    /**
     * 学习日期列表
     * @param userId
     * @param today
     * @param maxDate
     * @return
     */
    List<LocalDate> getStudyDate(Integer userId, LocalDate today, LocalDate maxDate);

    /**
     * 根据章节id以及用户id查询是否已报名
     * @param chapterId
     * @param type
     * @param userId
     * @return
     */
    boolean ifSubscribeByChapter(Integer chapterId, String type, Integer userId);
}

