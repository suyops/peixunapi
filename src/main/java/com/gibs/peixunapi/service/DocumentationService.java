package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.post.ChapterVO;
import com.gibs.peixunapi.VO.show.ChapterListVO;
import com.gibs.peixunapi.VO.show.ShowChapterVO;
import com.gibs.peixunapi.model.Documentation;
import com.gibs.peixunapi.result.Result;

import java.util.List;

/**
 * @author liangjiawei
 */
public interface DocumentationService {

    /**
     * 通过审核后新增文档信息,同时绑定课程以及文件Id
     *
     * @param chapterVOList 文档信息VO类
     * @return 业务返回结果
     */
    Result create(ChapterVO chapterVOList);

    /**
     * 编辑更改单条文档课程信息
     *
     * @param documentationVO 文档信息VO类
     * @return 业务返回结果
     */
    Result editDocument(ChapterVO documentationVO);

    /**
     * 根据文档课程id获取文档课程VO
     *
     * @param id     id
     * @param userId
     * @return 文档详细信息
     */
    ShowChapterVO getVO(Integer id, Integer userId);

    /**
     * 根据文档课程id获取文档课程
     *
     * @param id id
     * @return
     */
    Documentation get(Integer id);

    /**
     * 返回文档章节列表
     *
     * @param courseId 课程id
     * @param userId
     * @return
     */
    List<ChapterListVO> studentGetList(Integer courseId, Integer userId);

    /**
     * 老师/管理员获取文档列表
     *
     * @param courseId 课程id
     * @return
     */
    List<ShowChapterVO> getList(Integer courseId);

    /**
     * 管理员审核文档课程
     *
     * @param audit      审核结果
     * @param courseId 文档课程id
     * @return
     */
    void auditDocument(Integer audit, Integer courseId);


    /**
     * 关联文档章节
     *
     * @param courseId     课程id
     * @param linkCourseId 被关联课程id
     */
    void linkClass(Integer courseId, Integer linkCourseId);

    /**
     * 根据课程id查找文档
     *
     * @param courseId 课程id
     * @return
     */
    List<Documentation> findByCourseId(Integer courseId);

    /**
     * 改变章节状态
     *
     * @param id
     * @param id 章节id
     */
    void changeChapterStatus(Integer id, Integer status);

    /**
     * 审批文档章节
     *
     * @param id
     * @param audit
     */
    void auditChapter(Integer id, Integer audit);

    /**
     * 根据课程更改状态为申请审批
     *
     * @param courseId
     */
    void applyAudit(Integer courseId);

    /**
     * 文档章节数量
     *
     * @param id
     * @return
     */
    Integer countByCourse(Integer id);

    /**
     * 删除文档章节
     * @param chapterId
     */
    void deleteChapter(Integer chapterId);
}

