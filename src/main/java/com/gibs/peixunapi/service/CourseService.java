package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.show.StudentCourseVO;
import com.gibs.peixunapi.VO.post.CourseInfoVO;
import com.gibs.peixunapi.VO.show.*;
import com.gibs.peixunapi.enums.CourseEnum;
import com.gibs.peixunapi.model.Course;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/01/17:29
 * @Version 1.0
 * @Description: 课程信息服务层
 */
public interface CourseService {


    /**
     * 获取课程
     *
     * @param id id
     * @return
     */
    Course get(Integer id);

    /**
     * 保存前端传来的课程基本信息
     *
     * @param courseInfoVO 课程VO类
     * @return 执行结果
     */
    Result create(CourseInfoVO courseInfoVO);

    /**
     * 管理员审核课程基本信息是否通过
     *
     * @param audit    审核结果
     * @param courseId 课程id
     * @return
     */
    Result auditCourse(Integer audit, Integer courseId);

    /**
     * 获取单个课程详细信息
     *
     * @param courseId 课程id
     * @param type
     * @return 课程详细信息VO
     */
    ShowCourseInfoVO getCourseInfo(Integer courseId, String type);

    /**
     * 获取该学员已订阅的课程列表
     *
     * @param studentId   学员id
     * @param courseEnums
     * @return
     */
    List<StudentCourseVO> getStudentCourseList(Integer studentId, CourseEnum courseEnums);

    /**
     * 根据创建人id获取课程列表,用于题库管理入口
     *
     * @param page      页数
     * @param limit     最大条数
     * @param teacherId 老师id
     * @param key
     * @return
     */
    Result getSubjectHubCourseList(Integer page, Integer limit, Integer teacherId, String key);

    /**
     * 根据创建人id获取课程列表,用于试卷管理入口
     *
     * @param page      页数
     * @param limit     最大条数
     * @param teacherId 老师id
     * @param key
     * @return
     */
    PageData<CourseListVO> getTestPaperCourseList(Integer page, Integer limit, Integer teacherId, String key);

    /**
     * 传入VO类,保存课程基本信息
     *
     * @param courseInfoVO 课程VO类
     * @return
     */
    Result editCourse(CourseInfoVO courseInfoVO);


    /**
     * 老师/管理员获取此课程的章节列表(审核用)
     *
     * @param courseId 课程id
     * @param enums    课程类型
     * @return
     */
    List<ShowChapterVO> getList(Integer courseId, CourseEnum enums);


    /**
     * 查看课程按时间排序
     *
     * @param page  页数
     * @param limit 最大条数
     * @return 返回分页数据 CourseListVO
     */
    PageData<CourseListVO> getLatestCourseList(Integer page, Integer limit);


    /**
     * 关联课程基本信息
     *
     * @param courseId     当前课程id
     * @param linkCourseId 被关联课程id
     * @return
     */
    Result linkCourse(Integer courseId, Integer linkCourseId);

    /**
     * 考试管理列表
     *
     * @param name  搜索课程名关键字
     * @param page  页数
     * @param limit 最大条数
     * @return
     */
    Result getExamCourseVOInUndone(String name, Integer page, Integer limit);

    /**
     * 课程选项单选列表
     *
     * @param key 搜索关键字
     * @return
     */
    Result getOptionList(String key);


    /**
     * 课程管理的全部或者待审批列表
     * 需要统计已开班级数
     * 报名人员
     * 申请老师就是创建人=负责老师
     *
     * @param isAudit   是否已审批
     * @param teacherId
     * @param name
     * @param page      页数
     * @param limit     最大条目数
     * @return
     */
    PageData<CourseManageVO> getCourseManageList(Integer isAudit, Integer teacherId, String name, Integer page, Integer limit);

    /**
     * 获取全部章节
     *
     * @param teacherId
     * @return
     */
    List<ChapterOfAdminTeacherVO> findAllAudit(Integer teacherId);

    /**
     * 按课程获取全部章节
     *
     * @param type 类型
     * @param teacherId
     * @return
     */
    List<ChapterOfAdminTeacherVO> getPassChapterList(String type, Integer teacherId);

    /**
     * 题库管理列表
     *
     * @param name  搜索关键字
     * @param page
     * @param limit
     * @return
     */
    PageData getSubjectHubManageList(String name, Integer page, Integer limit);

    /**
     * 首页 当前课程
     *
     * @param id
     * @return
     */
    List<StartingCourseVO> getStartingCourseList(Integer id);

    /**
     * 设置习题进度限制
     *
     * @param courseId           当前课程id
     * @param learnChapterMaxNum 习题进度限制
     * @param learnRestrict      习题进度限制状态
     */
    void setLearnRestrict(Integer courseId, Integer learnChapterMaxNum, Integer learnRestrict);

    /**
     * 课程详细页面-获取其他课程列表
     *
     * @param courseId
     * @param type
     * @return
     */
    List<CourseOptionVO> getOptionListByCourse(Integer courseId, String type);

    /**
     * 企业管理-培训信息-订阅课程
     * 公司的订阅记录列表
     *
     * @param businessId 公司id
     * @return
     */
    List<BusinessSubscribeVO> businessSubscribeList(Integer businessId);

    /**
     * 添加助教老师
     *
     * @param courseId 课程id
     * @param ids      老师ids
     */
    void addAssistTeacher(Integer courseId, String ids);

    /**
     * 申请审批课程以及底下的视频/文档
     *
     * @param courseId 课程id
     */
    void applyAudit(Integer courseId);

    Result deleteCourse(Integer delete, Integer courseId);
}

