package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.post.UserVO;
import com.gibs.peixunapi.VO.show.AllowExamVO;
import com.gibs.peixunapi.model.StudentCourse;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.Result;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

public interface UserService {


    /**
     * 新增单个用户
     *
     * @param object 前段传递的经过验证的User类
     * @return 返回操作结果
     */
    Result create(UserVO object);

    /**
     * 新增单个用户
     *
     * @param object 前段传递的经过验证的User类
     * @return 返回操作结果
     */
    Result create(User object,String role,Integer businessId);

    /**
     * 根据User对象编辑用户,同时排除null值,避免空值全覆盖数据,权限列表如果不改动则传递null,如果全部取消则传递""空字符串
     *
     * @param object 前段传递的经过验证的User类
     * @return 返回操作结果
     */
    Result save(UserVO object);

    /**
     * 删除用户
     *
     * @param id 前段传递的id
     */
    void delete(Integer id);

    /**
     * 删除用户
     *
     * @param id 前段传递的id
     * @return 返回操作结果
     */
    Result deleteBatch(List<Integer> id);

    /**
     * 根据id 获取个人信息
     *
     * @param id 用户id
     * @return 返回用户
     */
    User get(Integer id);

    /**
     * 根据id返回用户VO
     *
     * @param id id
     * @return
     */
    Result getVO(Integer id);

    /**
     * 根据关键字获取列表
     *
     * @param key 查询关键字
     * @return
     */
    Result getList(String key);


    /**
     * 获取用户弹窗列表
     *
     * @param key   模糊查询关键字
     * @param page  页数
     * @param limit 最大条数
     * @return 返回列表
     */
    Result getOptions(String key, Integer page, Integer limit);


    /**
     * 批量审核用户名单
     *
     * @param idList 用户idList
     * @return 返回操作结果
     */
    Result audit(List<Integer> idList);

    /**
     * 根据参数修改用户密码
     *
     * @param id       用户id
     * @param password 新密码
     */
    void changePassword(Integer id, String password);


    /**
     * 根据企业id查找用户id列表
     *
     * @param businessId 企业id
     * @return 用户List
     */
    List<User> getListByBusiness(Integer businessId);

    /**
     * 老师选项列表
     *
     * @param
     * @param name 查询关键字
     * @return 用户List
     */
    Result getTeacherOption(String name);


    /**
     * 老师选项列表分页
     *
     * @param
     * @param name 查询关键字
     * @return 用户List
     */
    Result getTeacherOptionPage(String name, Integer page, Integer limit);

    /**
     * 企业管理员账户管理列表
     *
     * @param username   用户名
     * @param businessId 企业id
     * @param audit      审核状态
     * @param page       页数
     * @param limit      最大条数
     * @return
     */
    Result getUserList(String username, Integer businessId, Integer audit, Integer page, Integer limit);

    /**
     * 管理员账户管理列表
     *
     * @param username   用户名
     * @param businessId 公司Id
     * @param code       用户角色类型
     * @param status     启用状态
     * @param audit      审核状态
     * @param page       页数
     * @param limit      最大条数
     * @return
     */
    Result getUserList(String username, Integer businessId, String code, Integer status, Integer audit, Integer page, Integer limit);


    /**
     * 根据id列表查询实体类(分页)
     *
     * @param idList   id列表
     * @param pageable 分页工具
     * @return
     */
    Page<User> findByIdIn(List<Integer> idList, Pageable pageable);

    /**
     * 根据id列表查询实体类
     *
     * @param idList id列表
     * @return
     */
    List<User> findByIdIn(List<Integer> idList);

    /**
     * 根据公司id查询该公司旗下人数
     *
     * @param businessId 公司id
     * @return
     */
    Integer countByBusinessId(Integer businessId);

    /**
     * 初始化密码
     *
     * @param id 用户id
     */
    void resetPassword(Integer id);


    /**
     * 改变用户状态
     *
     * @param id     用户id
     * @param status 状态
     */
    void changeStatus(Integer id, Integer status);

    /**
     * 公司员工选项列表
     *
     * @param businessId 公司id
     * @param name 查询关键字
     * @return 用户List
     */
    Result getStaffOption(String name, Integer businessId);

    User findByUUID(String uuid);

    User getAttributeUser();

    /**
     * 根据公司的manageid获取该管理员的详细信息
     *
     * @param id
     * @return
     */
    Integer getManagerByBusiness(Integer id);

    /**
     * 添加助教老师的选项列表 同时排除已经添加的助教
     *
     * @param name
     * @return
     */
    Result getAssistTeacherOption(String name);

    /**
     * 根据用户名查找
     * @param username
     * @return
     */
    User getByUserName(String username);

    User getByTelephone(String telephone);

    void forgetPassword(String passWord);

    /**
     * 接触该公司底下 已删除用户的绑定关系
     * @param id
     */
    void unBindDeleteUserInBusiness(Integer id);


    /**
     * 获取未报名此课程考试的用户
     * @param department
     * @param year
     * @param key
     * @param examId
     * @return
     */
    List<AllowExamVO> getExamList(Integer department, Integer year, String key, Integer examId);

    /**
     * 获取未通过考试的用户
     * @param courseId
     * @param examClassId
     * @return
     */
    List<User> getNoPassExam(Integer courseId, Integer examClassId);

    /**
     *
     * @param ids
     * @return
     */
    List<User> getIdIn( List<Integer> ids);
}

