package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.show.CalendarVO;
import com.gibs.peixunapi.VO.show.MultiLineVo;
import com.gibs.peixunapi.VO.show.PieVO;
import com.gibs.peixunapi.enums.SmsEnum;
import com.gibs.peixunapi.model.LoginInfo;
import com.gibs.peixunapi.result.Result;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface LoginInfoService {

    /**
     * 获取用户登录次数
     *
     * @param id 用户id
     * @return
     */
    LoginInfo getLoginInfo(Integer id);

    /**
     * 新建登录信息
     *
     * @param loginInfo 登录信息
     */
    void create(LoginInfo loginInfo);

    /**
     * 用户登出
     *
     * @param userId 用户id
     * @return
     */
    Result logout(Integer userId);

    /**
     * 获取首页柱状图数据
     *
     * @param businessId 公司id
     * @return
     */
    Integer[] getUserBarData(Integer businessId);

    /**
     * 获取培训统计折线图数据(按年统计)
     *
     * @param businessId 公司id
     * @param studentId  学生id
     * @return
     */
    List<Integer[]> getCourseLineData(Integer businessId, Integer studentId);

    /**
     * 获取单位统计多线折线图数据(按月统计)
     *
     * @return
     */
    MultiLineVo getAllBusinessLineData();

    /**
     * 获取单位统计多线折线图数据(按月统计)
     *
     * @param businessId 公司id
     * @return
     */
    MultiLineVo getOneBusinessLineData(Integer businessId);

    /**
     * 查询用户比例饼图数据
     *
     * @return
     */
    List<PieVO> getUserPieData();

    /**
     * 查询培训比例饼图数据
     *
     * @return
     */
    List<PieVO> getCoursePieData();

    /**
     * 获取日历上的行程安排
     *
     * @return
     */
    CalendarVO getCalendarStroke();

    /**
     * 登录 检查是否存在token
     *
     * @param username
     * @param password
     * @return
     */
    HashMap<String, Object> login(HttpServletRequest request,String username, String password);

    HashMap<String, Object> loginInToken(HttpServletRequest request, String token);

    /**
     * 发送短信验证码
     * @param response
     * @param telephone
     * @param smsEnum
     */
    void sendPIN(HttpServletResponse response, String telephone, SmsEnum smsEnum);

    /**
     * 手机登录
     * @param request
     * @param pin
     * @param telephone
     * @param code
     * @return
     */
    Map<String, Object> checkLoginPIN(HttpServletRequest request, String pin, String telephone, String code);
}

