package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.post.NoticeVO;
import com.gibs.peixunapi.VO.show.ShowNoticeVO;

import java.util.List;
import java.util.Map;

public interface NoticeService {


    /**
     * 获取个人消息
     *
     * @param userId
     * @return
     */
    List<ShowNoticeVO> getNotice(Integer userId);

    /**
     * 新增消息
     */
    void sendNotice(NoticeVO notice);

    /**
     * 新增消息-指定用户列表
     */
    void sendNoticeToUser(String title, String content, String link, List<Integer> idList, Integer sender);

    /**
     * 新增消息-指定用户列表
     */
    void sendNoticeListToUser(String title, Map<Integer,String> contentList, String link, Integer sender);

    /**
     * 新增消息-指定单个用户
     */
    void sendNoticeToUser(String title, String content, String link, String id, Integer sender);

    /**
     * 新增消息-指定多个公司
     */
    void sendNoticeToBusiness(String title, String content, String link, List<Integer> idList, Integer sender);

    /**
     * 新增消息-指定企业账户
     */
    void sendNoticeToBusinessCount(String title, String content, String link, Integer id, Integer sender);

    /**
     * 新增消息-指定角色账户
     */
    void sendNoticeToRole(String title, String content, String link, List<String> idList, Integer sender);

    /**
     * 新增消息-指定角色账户
     */
    void sendNoticeToRole(String title, String content, String link, String idList, Integer sender);

    /**
     * 阅读消息
     *
     * @param noticeId
     * @return
     */
    ShowNoticeVO readNotice(Integer noticeId);

    /**
     * 删除已读消息
     */
    void deleteReadNotice();

    void sendNoticeBatch(List<NoticeVO> voList);
}

