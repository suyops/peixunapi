package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.show.RoleVO;
import com.gibs.peixunapi.result.Result;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface UserRoleService {

    /**
     * 绑定用户角色关系
     *
     * @param userId 用户id
     * @param roleId 权限id
     * @return
     */
    Result create(Integer userId, Integer roleId);

    /**
     * 根据用户id获取该用户所有角色VO列表
     *
     * @param userId 用户id
     * @return
     */
    List<RoleVO> getRoleVOListByUserId(Integer userId);

    /**
     * 根据用户id获取该用户所有角色名
     *
     * @param userId 用户id
     * @return
     */
    String getRoleNameByUserId(Integer userId);

    /**
     * 企业管理员账号管理列表
     *
     * @param name       姓名
     * @param businessId 公司id
     * @param audit      用户审核状态
     * @param page       页数
     * @param limit      最大条数
     * @return 用户idList
     */
    List<Integer> getUserIdList(String name, Integer businessId, Integer audit, Integer page, Integer limit);

    /**
     * 管理员账号管理列表
     *
     * @param name       姓名
     * @param code       角色类型
     * @param businessId 公司id
     * @param status     用户状态
     * @param audit      用户审核状态
     * @param page       页数
     * @param limit      最大条数
     * @return 用户idList
     */
    PageInfo<Integer> getUserIdList(String name, String code, Integer businessId, Integer status, Integer audit, Integer page, Integer limit);

    /**
     * 清空全部关系
     *
     * @param userId 用户id
     */
    void deleteAll(Integer userId);

    /**
     * 批量绑定
     *
     * @param codeIdList 角色idList
     * @param userId     用户id
     */
    void createBatch(List<Integer> codeIdList, Integer userId);


    /**
     * 根据角色查询相关用户
     *
     * @param code
     * @return
     */
    List<Integer> getUserByRole(String code);
}

