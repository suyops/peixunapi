package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.show.CourseManageVO;
import com.gibs.peixunapi.model.CourseBusiness;
import com.gibs.peixunapi.result.PageData;
import com.gibs.peixunapi.result.Result;

import javax.validation.Valid;
import java.util.List;

/**
 * @author liangjiawei
 */
public interface CourseBusinessService {

    Result delete(Integer id);

	/**
	 * 企业订阅课程,使其员工自动添加报名
	 *  @param businessId 企业id
	 * @param courseId 课程id
     * @return
     */
    Boolean subscribeCourse(Integer businessId, Integer courseId);

    /**
     * 企业管理员-订阅管理
     *
     * @param id
     * @param name
     * @param page
     * @param businessId
     * @return
     */
    PageData<CourseManageVO> getBusinessCourseList(Integer id, String name, Integer page, Integer businessId);

    /**
     * 根据公司获取订阅列表
     * @param businessId
     * @return
     */
    List<CourseBusiness> getByBusiness(Integer businessId);
}

