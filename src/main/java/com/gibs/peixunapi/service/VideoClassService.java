package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.post.ChapterVO;
import com.gibs.peixunapi.VO.show.ChapterListVO;
import com.gibs.peixunapi.VO.show.ShowChapterVO;
import com.gibs.peixunapi.model.VideoClass;
import com.gibs.peixunapi.result.Result;

import java.util.List;

/**
 * @author liangjiawei
 */
public interface VideoClassService {

    /**
     * 保存前段传来的VO类列表,绑定课程以及视频关系进行保存
     *
     * @param chapterVOList 前端传递的视频VO类列表
     * @return 业务返回结果
     */
    Result create(ChapterVO chapterVOList);

    /**
     * 编辑更改单条视频课程信息
     *
     * @param chapterVO 前端传递的视频VO类列表
     * @return 业务返回结果
     */
    Result save(ChapterVO chapterVO);


    /**
     * 根据视频章节id获取视频章节VO
     *
     * @param id     id
     * @param userId
     * @param type
     * @return 视频详细信息
     */
    ShowChapterVO getVO(Integer id, Integer userId, String type);

    /**
     * 根据视频章节id获取视频章节
     *
     * @param id id
     * @return 视频详细信息
     */
    VideoClass get(Integer id);

    /**
     * 学生查看已订阅的课程的视频列表
     *
     * @param courseId 课程id
     * @param userId   学生id
     * @return
     */
    List<ChapterListVO> studentGetList(Integer courseId, Integer userId);

    /**
     * 老师/管理员查看此课程的视频列表
     *
     * @param courseId 课程id
     * @return
     */
    List<ShowChapterVO> getList(Integer courseId);

    /**
     * 管理员审核视频
     *
     * @param audit    审核结果
     * @param courseId 章节id
     * @return
     */
    void auditByCourse(Integer audit, Integer courseId);

    /**
     * 把前端传来的章节信息更新
     *
     * @param chapterVO 章节信息VO类(包括文档以及视频
     * @return
     */
    Result editVideoClass(ChapterVO chapterVO);


    /**
     * 关联视频章节
     *
     * @param courseId     课程id
     * @param linkCourseId 被关联课程id
     */
    void linkClass(Integer courseId, Integer linkCourseId);


    /**
     * 根据课程id获取视频列表
     *
     * @param courseId 课程id
     * @return
     */
    List<VideoClass> findByCourseId(Integer courseId);

    /**
     * 改变章节状态
     *
     * @param id
     * @param status 章节id
     */
    void changeChapterStatus(Integer id, Integer status);

    /**
     * 审批视频章节
     *
     * @param id
     * @param audit
     */
    void auditChapter(Integer id, Integer audit);

    /**
     * 替换视频课程中的视频
     *
     * @param videoClassId
     * @param videoId
     */
    void changeVideo(Integer videoClassId, Integer videoId);


    /**
     * 根据课程更改状态为申请审批
     *
     * @param courseId
     */
    void applyAudit(Integer courseId);

    /**
     * 全部视频课程统计
     *
     * @param id
     * @return
     */
    Integer countByCourse(Integer id);

    /**
     * 删除视频章节
     *
     * @param chapterId
     */
    void deleteChapter(Integer chapterId);

}

