package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.show.OptionVO;
import com.gibs.peixunapi.enums.CourseEnum;

import java.util.List;

public interface ExamTeacherService {

    /**
     * 根据类型保存考试场次负责老师名单
     *
     * @param manageIds id名单
     * @param classId   考试场次id
     * @param enums     老师职责类型
     * @return
     */
    Boolean saveExamTeachers(List<Integer> manageIds, Integer classId, CourseEnum enums);

    /**
     * 获取考场监考老师/阅卷老师列表
     *
     * @param classId 考试场次id
     * @param enums   老师职责类型
     * @return
     */
    List<OptionVO> getTeacherList(Integer classId, CourseEnum enums);

    /**
     * 编辑考试负责老师
     *
     * @param ids     ids字符串
     * @param type    老师类型
     * @param classId
     */
    void editExamTeacher(String ids, Integer classId, String type);

    /**
     * 根据班级删除老师
     *
     * @param classId 班级classId
     * @param type
     */
    void deleteByClassId(Integer classId, String type);

    /**
     * 根据班级获取老师名称
     *
     * @param classId 班级classId
     */
    String getTeacherNames(Integer classId, CourseEnum enums);
}

