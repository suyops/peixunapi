package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.show.ShowTestPaperVO;
import com.gibs.peixunapi.model.AnswerSheet;
import com.gibs.peixunapi.model.ExamInfo;
import com.gibs.peixunapi.result.Result;

import java.math.BigDecimal;

public interface AnswerSheetService {


    AnswerSheet get(Integer id);

	/**
	 * 考生填写答卷,客观题自动判断分数
	 *
	 * @param uniqueCode 准考证
	 * @param testPaperSubjectId 试卷题目id
	 * @param studentAnswer 考生回答内容
	 */
    void fillTheAnswerPaper(String uniqueCode, Integer testPaperSubjectId, String studentAnswer);

    /**
     * 判断是否有为批改答卷
     * @param classId
     * @return
     */
    Result haveNextPaper(Integer classId);

    /**
     * 获取答卷
     *
     * @param classId 考试班级id
     * @return
     */
    Result getAnswerPaper(Integer classId);

    /**
     * 批改答卷
     *
     * @param answerSheetId 答卷题目id
     * @param score 得分
     * @param markManId 批改人id
     */
    void correct(Integer answerSheetId, BigDecimal score, Integer markManId);

    /**
     * 统计学生考试分数
     *
     * @param examStudentId 学生考试id
     * @return
     */
    BigDecimal calculateScore(Integer examStudentId);

    /**
     * 查询考试进度
     * @param examStudentId 学生考试id
     * @param examInfo 考试信息
     * @return
     */
    BigDecimal getStudentProgress(Integer examStudentId, ExamInfo examInfo);

    /**
     * 根据考试学生id跟试卷题目id 定位已存在的回答记录
     *
     * @param examStudentId
     * @param testPaperSubjectId
     * @return
     */
    AnswerSheet findByExamStudentAndTestPaperSubject(Integer examStudentId, Integer testPaperSubjectId);

    /**
     * 监考考试查看实时进度
     *
     * @param uniqueCode
     * @return
     */
    ShowTestPaperVO watchAnswerPaper(String uniqueCode);
}

