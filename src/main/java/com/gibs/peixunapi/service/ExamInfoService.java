package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.post.ExamClassVO;
import com.gibs.peixunapi.VO.post.ExamInfoVO;
import com.gibs.peixunapi.model.ExamInfo;
import com.gibs.peixunapi.model.TestPaper;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.result.Result;

import java.util.List;

public interface ExamInfoService {


    /**
     * 获取考试信息
     *
     * @param id id
     * @return
     */
    ExamInfo get(Integer id);

	/**
	 * 在创建完成考试后,根据本场考试考试班级信息获取试卷实体类
	 * @param examClassId 考试班级Id
	 * @return
	 */
	TestPaper getTestPaperByExamClass(Integer examClassId);

    /**
     * 更新考试信息中的试卷信息
     * @param testPaper 试卷实体类
     * @return
     */
    Result updateTestPaper(TestPaper testPaper,Integer examInfoId);



    /**
     * 通过试卷id查找考试基本信息
     *
     * @param testPaperId 试卷id
     * @return
     */
    List<ExamInfo> findExamInfosByTestPaper_Id(Integer testPaperId);

    /**
     * 新建考试信息
     * @param examInfoVO 考试信息VO
     * @param user 用户
     * @return
     */
    ExamInfo create(ExamInfoVO examInfoVO, User user);

    /**
     * 编辑课程基本信息
     *
     * @param examInfoVO 课程信息VO
     * @return
     */
    ExamInfo edit(ExamClassVO examInfoVO);

    /**
     * 绑定试卷和考试
     *
     * @param id 考试id
     * @param testPaperId
     */
    void bindTestPaper(Integer id, Integer testPaperId);

    void save(ExamInfo examInfo);
}

