package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.post.CommentVO;
import com.gibs.peixunapi.VO.show.ShowCommentVO;
import com.gibs.peixunapi.result.Result;

import javax.validation.Valid;

import com.gibs.peixunapi.model.Comment;

import java.util.List;

public interface CommentService {

    Comment get(Integer id);

    /**
     * 添加讨论
     *
     * @param comment
     * @param subscribeChannel
     * @return
     */
    Integer pushComment(CommentVO comment, String subscribeChannel);

    /**
     * 获得单条讨论Vo
     *
     * @param id
     * @return
     */
    ShowCommentVO getOneComment(Integer id);

    /**
     * 获取这个章节底下全部评论
     *
     * @param subscribeChannel
     * @param type
     * @return
     */
    List<ShowCommentVO> getAllComment(String subscribeChannel, int type);

    /**
     * 添加提问
     *
     * @param comment
     * @param subscribeChannel
     * @return
     */
    Integer pushQuestion(CommentVO comment, String subscribeChannel);


    /**
     * 统计学生在此课程提问次数
     *
     * @param userId
     * @param courseId
     * @return
     */
    Integer questionCount(Integer userId, Integer courseId);

    /**
     * 老师首页 获取最新提问
     *
     * @param id
     * @param i
     * @return
     */
    List<ShowCommentVO> getPersonQuestion(Integer id, int i);
}

