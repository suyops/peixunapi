package com.gibs.peixunapi.service;

import com.gibs.peixunapi.VO.SubjectHubVO;
import com.gibs.peixunapi.VO.post.TestPaperVO;
import com.gibs.peixunapi.model.SubjectHub;
import com.gibs.peixunapi.model.TestPaper;
import com.gibs.peixunapi.model.TestPaperSubject;
import com.gibs.peixunapi.result.Result;

import java.util.List;
import java.util.Map;

/**
 * @author liangjiawei
 */
public interface TestPaperSubjectService {

    /**
     * 根据id查询试卷题目表
     * @param id
     * @return
     */
    TestPaperSubject get(Integer id);

    /**
     * 根据试卷id返回分类的map
     *
     * @param testPaperId 试卷id
     * @return Map key:题目分类 value:题目列表
     */
    Map<String, List<SubjectHubVO>> getMapByTestPaper(Integer testPaperId);

    /**
     * 更新试卷版本
     * 根据newTestPaperVO 与 oldSubjectHubVOList 进行对比更新需要的题目
     * 并且新增新的一版试卷(不影响已完成的考试中的答卷数据)
     *
     * @param oldSubjectHubVOList 数据库中的试卷题目列表
     * @param newTestPaperVO      需要更新的试卷题目列表
     * @return 新增成功的试卷实体类
     */
    TestPaper switchTestPaperSubject(List<SubjectHubVO> oldSubjectHubVOList, TestPaperVO newTestPaperVO);

    /**
     * 替换试卷中的题目id
     *
     * @param oldSubjectHubVOList
     * @param newTestPaperVO
     * @return
     */
    Result updateTestPaperSubject(List<SubjectHubVO> oldSubjectHubVOList, TestPaperVO newTestPaperVO);

    /**
     * 随机生成试题
     *
     * @param testPaper
     * @param courseId
     * @param user
     */
    void randomSubject(TestPaper testPaper, Integer courseId, Integer user);


    /**
     * 导入试卷保存
     *  @param subjectHubList
     * @param id
     * @param subjectType
     */
    void saveImport(List<SubjectHub> subjectHubList, Integer id, String subjectType);

    List<TestPaperSubject> getTestPaperSubject(Integer id);
}

