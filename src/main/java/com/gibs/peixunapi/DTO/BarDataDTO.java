package com.gibs.peixunapi.DTO;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/09/16/17:18
 * @Version 1.0
 * @Description:
 */
@Data
public class BarDataDTO {
    private Integer month;
    private Integer count;
}
