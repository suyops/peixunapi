package com.gibs.peixunapi.DTO;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/10/13/12:02
 * @Version 1.0
 * @Description:
 */
@Data
public class SubjectHubDTO {
    private Integer id;
    private String type;     //类型
    private String content;
    private String answer;
    private Integer fillNum;
    private String analysis;
    private Integer sort;
    private Integer testPaperSubjectHubId;
    private Integer exercisesId;
}
