package com.gibs.peixunapi.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/10/21/14:50
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("被引用讨论VO")
public class CommentReferenceDTO {

    /** id */
    @ApiModelProperty("id")
    private Integer id;

    /** 姓名 */
    @ApiModelProperty("姓名")
    private String username;

    /** 评论内容 */
    @ApiModelProperty("评论内容")
    private String content;
}
