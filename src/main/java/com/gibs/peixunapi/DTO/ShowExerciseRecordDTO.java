package com.gibs.peixunapi.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gibs.peixunapi.VO.SubjectHubVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liangjiawei
 * @date 2020/09/01/10:50
 * @Version 1.0
 * @Description:
 */
@Data
@ApiModel("练习记录")
public class ShowExerciseRecordDTO {

    @ApiModelProperty("章节id")
    private Integer chapterId;

    @ApiModelProperty("章节名")
    private String chapterName;

    @JsonIgnore
    private String type;

    @ApiModelProperty("题目")
    private List<SubjectHubVO> subjectHubVOList;
}
