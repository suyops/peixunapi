package com.gibs.peixunapi.DTO;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/10/22/18:03
 * @Version 1.0
 * @Description:
 */
@Data
public class CourseStudyDTO {
    private Integer sort;
    private Integer userId;
    private Integer studentCourseId;
    private String username;
    private String businessName;
    private Double videoProgress;
    private Double documentProgress;

}
