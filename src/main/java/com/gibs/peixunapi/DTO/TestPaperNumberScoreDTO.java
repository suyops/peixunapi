package com.gibs.peixunapi.DTO;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liangjiawei
 * @date 2020/10/11/19:09
 * @Version 1.0
 * @Description:
 */
@Data
public class TestPaperNumberScoreDTO {
    private String type;
    private Integer number;
    private BigDecimal score;
}
