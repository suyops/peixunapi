package com.gibs.peixunapi.DTO;

import lombok.Data;

/**
 * @author liangjiawei
 * @date 2020/09/17/15:08
 * @Version 1.0
 * @Description:
 */
@Data
public class BusinessECharDTO {
    private Integer value;
    private String name;
}
