package com.gibs.peixunapi.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ActionLogAop {
    String module() default ""; // 操作模块

    String type() default "";  // 操作类型

    String desc() default "";  // 操作说明
}
