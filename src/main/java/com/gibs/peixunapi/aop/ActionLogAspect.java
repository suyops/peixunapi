package com.gibs.peixunapi.aop;

import com.gibs.peixunapi.dao.ActionLogDao;
import com.gibs.peixunapi.model.ActionLog;
import com.gibs.peixunapi.model.User;
import com.gibs.peixunapi.service.UserService;
import com.gibs.peixunapi.utils.UUIDUtil;
import com.google.gson.Gson;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liangjiawei
 * @date 2020/11/09/9:00
 * @Version 1.0
 * @Description:
 */
@Aspect
@Component
public class ActionLogAspect {

    @Autowired
    private UserService userService;
    @Autowired
    private ActionLogDao actionLogDao;

    @Pointcut("@annotation(com.gibs.peixunapi.aop.ActionLogAop)")
    public void actionLogPointCut() {

    }
    @AfterReturning(value = "actionLogPointCut()", returning = "keys")
    public void saveLog(JoinPoint joinPoint, Object keys) {
        // 获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        // 从获取RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
        ActionLog operlog = new ActionLog();
        try {
            operlog.setId(UUIDUtil.get32NumberUUID()); // 主键ID
            // 从切面织入点处通过反射机制获取织入点处的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            // 获取切入点所在的方法
            Method method = signature.getMethod();
            // 获取操作
            ActionLogAop opLog = method.getAnnotation(ActionLogAop.class);
            if (opLog != null) {
                // 操作模块
                operlog.setModule(opLog.module());
                // 操作类型
                operlog.setType(opLog.type());
                // 操作描述
                operlog.setContent(opLog.desc());
            }
            // 获取请求的类名
            String className = joinPoint.getTarget().getClass().getName();
            // 获取请求的方法名
            String methodName = method.getName();
            methodName = className + "." + methodName;
            // 请求方法
            operlog.setMethod(methodName);
            // 请求的参数
            String params;
            // 将参数所在的数组转换成json
            if ("POST".equals(request.getMethod())) {
                params = new Gson().toJson(joinPoint.getArgs());
            } else {
                Map<String, String> rtnMap = new HashMap<>();
                request.getParameterMap().forEach((key, value) -> rtnMap.put(key, value[0]));
                params = new Gson().toJson(rtnMap);
            }

            // 请求参数
            operlog.setRequestParam(params);
            // 返回结果
            operlog.setResponseParam(new Gson().toJson(keys));
            User user = userService.getAttributeUser();
            // 请求用户ID
            operlog.setUserId(user.getId());
            // 请求用户名称
            operlog.setUserName(user.getUsername());
            // 请求IP
            operlog.setIp(request.getHeader("clientIp"));
            // 请求URI
            operlog.setUri(request.getRequestURI());
            // 创建时间
            operlog.setCreateTime(new Date());

            actionLogDao.save(operlog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
